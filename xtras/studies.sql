-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 09-10-2019 a las 14:38:12
-- Versión del servidor: 5.7.26
-- Versión de PHP: 7.3.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `studies`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blog`
--

CREATE TABLE `blog` (
  `id` int(10) NOT NULL,
  `orden` int(2) NOT NULL DEFAULT '0',
  `titulo` varchar(300) DEFAULT NULL,
  `txt` text,
  `fecha` date DEFAULT NULL,
  `video` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogpic`
--

CREATE TABLE `blogpic` (
  `id` int(10) NOT NULL,
  `item` int(10) NOT NULL,
  `titulo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `url` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci DEFAULT NULL,
  `orden` int(2) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carousel`
--

CREATE TABLE `carousel` (
  `id` int(10) NOT NULL,
  `orden` int(2) DEFAULT NULL,
  `titulo` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `txt` text COLLATE latin1_general_ci,
  `url` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

--
-- Volcado de datos para la tabla `carousel`
--

INSERT INTO `carousel` (`id`, `orden`, `titulo`, `txt`, `url`) VALUES
(2, 99, NULL, NULL, NULL),
(3, 99, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carousel2`
--

CREATE TABLE `carousel2` (
  `id` int(10) NOT NULL,
  `orden` int(2) DEFAULT NULL,
  `titulo` varchar(100) COLLATE latin1_general_ci DEFAULT NULL,
  `txt` text COLLATE latin1_general_ci,
  `url` text COLLATE latin1_general_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carrousel_escuelas`
--

CREATE TABLE `carrousel_escuelas` (
  `id` int(11) NOT NULL,
  `orden` int(2) DEFAULT '99',
  `titulo` text,
  `url` text,
  `escuelaid` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configuracion`
--

CREATE TABLE `configuracion` (
  `id` int(2) NOT NULL,
  `title` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL,
  `description` text COLLATE latin1_spanish_ci,
  `prodspag` int(5) DEFAULT NULL,
  `sliderhmin` int(5) DEFAULT '0',
  `sliderhmax` int(5) DEFAULT '1000',
  `sliderproporcion` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `slideranim` int(1) DEFAULT NULL,
  `slidertextos` int(1) DEFAULT NULL,
  `paypalemail` text COLLATE latin1_spanish_ci,
  `destinatario1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `destinatario2` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `remitente` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `telefono1` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `youtube` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `envio` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `envioglobal` varchar(10) COLLATE latin1_spanish_ci DEFAULT NULL,
  `iva` int(2) DEFAULT NULL,
  `incremento` int(2) DEFAULT NULL,
  `bank` text COLLATE latin1_spanish_ci,
  `tyct1` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct3` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyct4` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `tyc1` text COLLATE latin1_spanish_ci,
  `tyc2` text COLLATE latin1_spanish_ci,
  `tyc3` text COLLATE latin1_spanish_ci,
  `tyc4` text COLLATE latin1_spanish_ci,
  `pdf1` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen1` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen2` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen3` varchar(100) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagen4` text COLLATE latin1_spanish_ci,
  `about1` text COLLATE latin1_spanish_ci,
  `about2` text COLLATE latin1_spanish_ci,
  `about3` text COLLATE latin1_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `configuracion`
--

INSERT INTO `configuracion` (`id`, `title`, `description`, `prodspag`, `sliderhmin`, `sliderhmax`, `sliderproporcion`, `slideranim`, `slidertextos`, `paypalemail`, `destinatario1`, `destinatario2`, `remitente`, `telefono`, `telefono1`, `facebook`, `instagram`, `youtube`, `envio`, `envioglobal`, `iva`, `incremento`, `bank`, `tyct1`, `tyct2`, `tyct3`, `tyct4`, `tyc1`, `tyc2`, `tyc3`, `tyc4`, `pdf1`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `about1`, `about2`, `about3`) VALUES
(1, 'STUDIES', 'Template de desarrollo', 4, 300, 700, '7:2', 2, 0, 'studies@studies.com', 'ing_efrain@yahoo.com', NULL, 'desarrollo@wozial.com', '3338259519', '3314305376', 'https://www.facebook.com/', 'https://www.instagram.com/', 'https://pinterest.com.mx/', '100', '50', 16, 4, 'Bancomer', 'Aviso de privacidad', 'Métodos de pago', 'Devoluciones y envío', 'Términos y condiciones', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit.</p>\r\n<p>Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>\r\n<p>Suspendisse porta enim purus, sit amet accumsan ligula molestie quis. Cras rhoncus ultricies odio. Aliquam imperdiet dapibus aliquet. Curabitur et ullamcorper eros. Fusce ut massa sit amet dolor suscipit tincidunt. Phasellus at tincidunt massa. Praesent ac imperdiet est, ac laoreet libero. Ut in turpis velit. Morbi non diam dui.</p>', '<p>Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>', '<p>Quisque semper turpis in erat cursus, id auctor nisi sollicitudin. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Integer at blandit lectus. Pellentesque aliquet velit sem, vitae mollis eros tempor vel. Duis id orci in nulla viverra dignissim at a sem. Mauris iaculis nisl nec enim rhoncus iaculis. Curabitur dapibus fringilla quam, sed blandit ipsum accumsan nec. Donec ac elit lobortis purus sagittis convallis quis et est. Praesent vitae sagittis felis, ac sagittis tortor. Cras tortor lectus, molestie consequat ipsum id, efficitur ullamcorper felis. Sed sapien ipsum, rutrum a odio id, gravida ultrices neque. Nullam finibus mi vel ante dignissim auctor.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>\r\n<p>Suspendisse porta enim purus, sit amet accumsan ligula molestie quis. Cras rhoncus ultricies odio. Aliquam imperdiet dapibus aliquet. Curabitur et ullamcorper eros. Fusce ut massa sit amet dolor suscipit tincidunt. Phasellus at tincidunt massa. Praesent ac imperdiet est, ac laoreet libero. Ut in turpis velit. Morbi non diam dui.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin id nulla ac libero viverra laoreet. Duis varius scelerisque nunc at feugiat. Sed viverra est non fringilla pellentesque. Sed dictum suscipit tristique. In ultricies neque vel aliquam pharetra. Aliquam magna dolor, accumsan a mi id, commodo consequat purus. Nullam lobortis erat a tempor blandit.</p>\r\n<p>In nec diam in ipsum dictum auctor quis sit amet sapien. Mauris augue enim, volutpat a malesuada id, hendrerit vitae neque. Aliquam erat volutpat. Etiam ut finibus neque. Nulla et finibus felis. Etiam vestibulum orci id nisl iaculis sodales. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce fermentum rutrum mi non faucibus. Nulla molestie urna eu orci malesuada dictum. Integer eros dui, tempor ac ipsum a, consectetur facilisis sem. Proin placerat porttitor velit sed mattis. Suspendisse ut erat orci. In hac habitasse platea dictumst.</p>\r\n<p>Aenean cursus maximus odio, vel pharetra leo condimentum vel. In nec molestie massa. Suspendisse a tellus ultrices massa laoreet facilisis ac ultricies neque. Curabitur fringilla nunc sed interdum fermentum. Etiam egestas maximus arcu nec dictum. Integer ornare ligula ipsum, sit amet consequat justo euismod porta. Suspendisse a quam lorem. Donec ac ornare tortor. Suspendisse leo tortor, fringilla ut imperdiet ac, pulvinar nec eros. Etiam dignissim mauris sapien, vitae pulvinar nibh placerat dignissim. Pellentesque vitae vulputate nisl. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Aliquam elementum tempor lorem, blandit aliquam ipsum commodo id. Nam dictum iaculis neque, quis tempus tortor luctus sit amet.</p>', NULL, '809636955.jpg', '707689644.jpg', '808594938.jpg', '519329541.jpg', '<p>En Studies4Life creemos que nuestros programas de educaci&oacute;n internacional le otorgan al estudiante un impacto positivo a trav&eacute;s de la inmersi&oacute;n cultural total y eso se reflejar&aacute; en nuestra sociedad benefici&aacute;ndonos a todos por medio de ciudadanos preparados para afrontar los retos de este mundo globalizado. Esperamos que al promover la educaci&oacute;n internacional y los beneficios que ofrece el hecho de estudiar en el extranjero, hagamos un cambio en la calidad de vida y el crecimiento profesional de los estudiantes y sus familias. Ayuda a tus hijos a vivir esta incre&iacute;ble experiencia de vida viajando, estudiando y trabajando Somos una empresa certificada por ICEF (International Consultants for Education and Fairs) y tambi&eacute;n por PIER (Qualified education agents) con m&aacute;s de 5 a&ntilde;os de experiencia en programas educativos internacionales y migratorios a Canad&aacute;, solo representamos a los mejores en cada &aacute;rea esto incluye nuestros servicios migratorios a Canad&aacute;.</p>', '<p>Nullam et mi id massa pulvinar pharetra a ac augue. Maecenas malesuada turpis at nisl faucibus scelerisque. Sed interdum, massa ut suscipit rhoncus, sem tellus iaculis mauris, eget ultricies est risus ac sem. Cras quis dignissim felis. Suspendisse potenti. Mauris sed consequat sapien. Donec et urna luctus, fermentum dui hendrerit, eleifend felis. Donec laoreet nunc elit, sed euismod enim maximus ac. Vestibulum egestas ullamcorper arcu, ac malesuada lorem aliquet nec. Praesent diam sem, imperdiet vel justo ac, aliquet laoreet turpis. Sed hendrerit dolor ac odio auctor, at efficitur lorem gravida.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer aliquam, velit ut consectetur facilisis, ipsum lorem consequat felis, ut tempus eros turpis non mauris. Ut cursus tellus quis tortor egestas, vitae auctor sem condimentum. Praesent tempus, nisi eget porttitor suscipit, erat lectus feugiat velit, a elementum nibh erat ac odio. Fusce fermentum nibh sit amet gravida fermentum. Nunc et fringilla ligula. Mauris ac sollicitudin tellus, eget feugiat metus. Curabitur est lacus, suscipit id lobortis eu, varius eget arcu.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `escuelas`
--

CREATE TABLE `escuelas` (
  `id` int(10) NOT NULL,
  `categoria` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pais` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `estado` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `ciudad` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci,
  `txtdetalle` text COLLATE utf8_spanish_ci,
  `estatus` int(1) NOT NULL DEFAULT '0',
  `fecha` date DEFAULT NULL,
  `orden` int(2) DEFAULT '99',
  `imagen` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `logoescuela` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lat` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lon` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `escuelas`
--

INSERT INTO `escuelas` (`id`, `categoria`, `titulo`, `pais`, `estado`, `ciudad`, `txt`, `txtdetalle`, `estatus`, `fecha`, `orden`, `imagen`, `logoescuela`, `lat`, `lon`) VALUES
(7, NULL, 'VANCOUVER', 'CANADA', 'VANCOUVER', 'VANCOUVER', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', '<p class=\"text-8 uk-first-column\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>\r\n<p class=\"text-8 uk-grid-margin uk-first-column\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\r\n<p class=\"text-8 uk-grid-margin uk-first-column\">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>', 0, '2019-10-04', 99, '433065625.jpg', '152222773.png', '20.676457008264084', '-103.3279443058982');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `faq`
--

CREATE TABLE `faq` (
  `id` int(5) NOT NULL,
  `orden` int(2) NOT NULL,
  `pregunta` text NOT NULL,
  `respuesta` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `faq`
--

INSERT INTO `faq` (`id`, `orden`, `pregunta`, `respuesta`) VALUES
(1, 99, '&iquest;Pregunta n&uacute;mero 1?', '<p>Pellentesque sodales metus vitae placerat bibendum. Aenean ut finibus nisi. Etiam tempus vitae mauris non dictum. Cras a mollis justo. Proin nec dolor fringilla, molestie neque sed, vestibulum ante. Nullam at risus sit amet justo mollis posuere. Proin viverra justo id consequat mattis.</p>'),
(2, 99, '&iquest;Pregunta n&uacute;mero 2?', '<p>Etiam bibendum porttitor ante, at tempus massa porttitor eget. Curabitur ac efficitur massa. Duis vitae consequat nunc. Donec eu ante ac libero semper maximus in id nibh. Donec condimentum ut mi ut dapibus. Cras sed justo mi. Duis leo diam, ornare ut sodales nec, dignissim at augue. Mauris hendrerit elit sed orci vehicula eleifend. Integer condimentum, mi id bibendum interdum, lectus nisl lobortis ante, et sodales erat justo eu risus. Curabitur sed odio et quam vulputate placerat.</p>'),
(3, 99, '&iquest;Pregunta n&uacute;mero 3?', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam malesuada at arcu et scelerisque. Phasellus ut tincidunt diam. Cras tincidunt id quam pretium suscipit. Nullam eget rhoncus dui, a volutpat nulla. Proin posuere elit tellus. Etiam et congue dui, et faucibus metus. Aliquam egestas non mi id consectetur. Morbi libero nunc, semper a ultricies sed, pretium sed augue. Proin feugiat quam vel nisl porta, eget dapibus arcu pretium. Vestibulum at erat nec justo euismod ornare sed ut augue. Vestibulum ac dictum urna. Phasellus tempor odio ex, in commodo risus ultrices vel.</p>'),
(4, 99, '&iquest;Pregunta n&uacute;mero 4?', '<p>In faucibus odio a diam ullamcorper ullamcorper. Praesent vitae sagittis purus, eget porttitor ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id elit at velit fermentum vehicula eget et eros. Quisque vel turpis eget ante pulvinar sagittis non non lacus. Vestibulum dapibus libero non lectus congue rhoncus. Morbi fringilla leo nec metus pretium accumsan. Cras bibendum posuere magna, vel sodales lacus feugiat vitae. Donec pulvinar turpis nisl, sit amet hendrerit nulla fermentum sed. In hac habitasse platea dictumst. Integer pulvinar fermentum dolor ac venenatis. Donec gravida nisi ac velit feugiat, et tempus turpis maximus. In quam nunc, ornare id sapien non, consequat viverra nulla.</p>'),
(5, 99, '&iquest;Pregunta n&uacute;mero 4?', '<p>In faucibus odio a diam ullamcorper ullamcorper. Praesent vitae sagittis purus, eget porttitor ligula. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aliquam id elit at velit fermentum vehicula eget et eros. Quisque vel turpis eget ante pulvinar sagittis non non lacus. Vestibulum dapibus libero non lectus congue rhoncus. Morbi fringilla leo nec metus pretium accumsan. Cras bibendum posuere magna, vel sodales lacus feugiat vitae. Donec pulvinar turpis nisl, sit amet hendrerit nulla fermentum sed. In hac habitasse platea dictumst. Integer pulvinar fermentum dolor ac venenatis. Donec gravida nisi ac velit feugiat, et tempus turpis maximus. In quam nunc, ornare id sapien non, consequat viverra nulla.</p>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `favoritos`
--

CREATE TABLE `favoritos` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `producto` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `inicio`
--

CREATE TABLE `inicio` (
  `id` int(11) NOT NULL,
  `texto1` text COLLATE utf8_spanish_ci,
  `texto2` text COLLATE utf8_spanish_ci,
  `texto3` text COLLATE utf8_spanish_ci,
  `texto4` text COLLATE utf8_spanish_ci,
  `imagen1` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen2` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen3` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen4` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen5` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen6` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen7` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo1` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo3` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo4` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `texto5` text COLLATE utf8_spanish_ci,
  `titulo5` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `inicio`
--

INSERT INTO `inicio` (`id`, `texto1`, `texto2`, `texto3`, `texto4`, `imagen1`, `imagen2`, `imagen3`, `imagen4`, `imagen5`, `imagen6`, `imagen7`, `titulo1`, `titulo2`, `titulo3`, `titulo4`, `texto5`, `titulo5`) VALUES
(1, 'Nunc arcu tellus, pharetra eu sodales et, pellentesque ut urna. Integer et volutpat ligula. In vel molestie felis. \r\nDonec mauris erat, molestie ac augue quis, iaculis eleifend massa. Donec eu sapien lectus. \r\nEtiam placerat molestie erat, nec accumsan lorem ultricies quis. Aliquam eu ipsum vitae turpis blandit tristique non vitae quam.', '<p>Nunc arcu tellus, pharetra eu sodales et, pellentesque ut urna. Integer et volutpat ligula. In vel molestie felis. <br />Donec mauris erat, molestie ac augue quis, iaculis eleifend massa. Donec eu sapien lectus. <br />Etiam placerat molestie erat, nec accumsan lorem ultricies quis. Aliquam eu ipsum vitae turpis blandit tristique non vitae quam.<br /><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id varius massa, at tincidunt eros. Sed tortor urna, egestas at cursus at, iaculis quis nisi. Nulla sit amet tempor purus, id feugiat sem. Aenean sed sem pretium, accumsan justo nec, viverra mauris. Nullam dapibus consequat ante, nec dignissim lorem dapibus sit amet. <br />Cras id eros viverra, pulvinar est in, tempor eros. Aenean turpis augue, malesuada sit amet tincidunt nec, pharetra consectetur sapien. Integer fermentum, lorem quis hendrerit ullamcorper, nulla quam vulputate lectus, sollicitudin porttitor nisi elit sed nisl. Quisque feugiat non urna vitae accumsan. Mauris eu mattis elit. <br /><br />Praesent elementum nulla bibendum, aliquam mauris in, imperdiet dui. Vestibulum enim tortor, pulvinar vitae elit scelerisque, commodo hendrerit leo. Nunc accumsan pretium sem, et efficitur augue egestas varius. Aliquam a molestie magna. Etiam ultrices dictum quam. Mauris gravida congue dolor, et imperdiet leo dapibus vel.</p>', '<p>Si tu ya viviste la experiencia de Studies 4 life, refiere a tus amigos<br /><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id varius massa, at tincidunt eros. Sed tortor urna, egestas at cursus at, iaculis quis nisi.<br /><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. <br />Ut enim ad minim veniam.</p>', '<p>&iquest;Quieres ganar dinero extra?<br /><br />Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent id varius massa, at tincidunt eros.<br /><br />Sed tortor urna, egestas at cursus at, iaculis quis nisi.</p>', '201910087070.jpg', '201910082525.jpg', '201910085787.jpg', '20191008293.jpg', '201910039678.jpg', '201910022588.png', '201910039106.jpg', '¡¡¡COMPARTE UNA BECA Y GANA!!!', 'COSTUMBRE', '¡¡¡COMPARTE UNA BECA!!!', '¡¡¡COMPARTE UNA BECA Y GANA!!!', '<p>Integer mattis, leo at sollicitudin feugiat, nibh nibh vehicula diam, eu fermentum eros quam et ipsum. Integer sed lectus nec ex feugiat semper vitae ut nibh. Ut justo erat, tempus non augue ac, pulvinar gravida lorem. Aliquam eu congue sem. Suspendisse facilisis malesuada turpis. Etiam efficitur ipsum non velit vestibulum, ac efficitur ex pellentesque. Aenean eget lorem mi. Phasellus ut justo varius, laoreet diam sit amet, condimentum risus. Vestibulum bibendum consequat libero eu volutpat. Duis eu mauris eget enim condimentum imperdiet.</p>', 'Podemos ayudarte a encontrar el programa ideal para ti.');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id` int(11) NOT NULL,
  `nombre` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id`, `nombre`) VALUES
(1, 'Canada'),
(2, 'México'),
(5, 'Japon');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `id` int(10) NOT NULL,
  `categoria` int(2) DEFAULT NULL,
  `escuelaid` int(11) DEFAULT NULL,
  `clasif` int(2) DEFAULT NULL,
  `marca` int(2) DEFAULT NULL,
  `title` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `metadescription` text COLLATE utf8_spanish_ci,
  `precio` decimal(20,2) DEFAULT NULL,
  `descuento` int(2) NOT NULL DEFAULT '0',
  `titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `sku` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci,
  `material` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `inicio` int(1) NOT NULL DEFAULT '0',
  `estatus` int(1) NOT NULL DEFAULT '1',
  `fecha` date DEFAULT NULL,
  `orden` int(2) DEFAULT '99',
  `titulo1` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt1` text COLLATE utf8_spanish_ci,
  `txt1_detalle` text COLLATE utf8_spanish_ci,
  `titulo2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt2` text COLLATE utf8_spanish_ci,
  `txt2_detalle` text COLLATE utf8_spanish_ci,
  `titulo3` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt3` text COLLATE utf8_spanish_ci,
  `txt3_detalle` text COLLATE utf8_spanish_ci,
  `titulo4` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt4` text COLLATE utf8_spanish_ci,
  `txt4_detalle` text COLLATE utf8_spanish_ci,
  `titulo5` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt5` text COLLATE utf8_spanish_ci,
  `txt5_detalle` text COLLATE utf8_spanish_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`id`, `categoria`, `escuelaid`, `clasif`, `marca`, `title`, `metadescription`, `precio`, `descuento`, `titulo`, `sku`, `txt`, `material`, `imagen`, `inicio`, `estatus`, `fecha`, `orden`, `titulo1`, `txt1`, `txt1_detalle`, `titulo2`, `txt2`, `txt2_detalle`, `titulo3`, `txt3`, `txt3_detalle`, `titulo4`, `txt4`, `txt4_detalle`, `titulo5`, `txt5`, `txt5_detalle`) VALUES
(9, 11, 7, NULL, NULL, '', '', '500.00', 30, 'VANCOUVER CAMPAMENTO 2019', '', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, '689947279.png', 0, 1, '2019-10-07', 99, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productoscat`
--

CREATE TABLE `productoscat` (
  `id` int(11) NOT NULL,
  `parent` int(2) NOT NULL,
  `txt` text COLLATE latin1_spanish_ci,
  `imagen` varchar(50) COLLATE latin1_spanish_ci DEFAULT NULL,
  `imagenhover` varchar(20) COLLATE latin1_spanish_ci DEFAULT NULL,
  `estatus` int(1) NOT NULL DEFAULT '0',
  `orden` int(2) NOT NULL DEFAULT '99',
  `titulo` varchar(200) COLLATE latin1_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productoscat`
--

INSERT INTO `productoscat` (`id`, `parent`, `txt`, `imagen`, `imagenhover`, `estatus`, `orden`, `titulo`) VALUES
(11, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'CAMPAMENTO'),
(12, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'CURSO DE IDIOMA'),
(13, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'SECUNDARIA Y PREPA'),
(14, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'ESTUDIA Y TRABAJA'),
(15, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'DIPLOMADO'),
(16, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'CARRERAS MAESTRIA'),
(17, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'NI&Ntilde;ERAS EN USA'),
(18, 0, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor.</p>', NULL, NULL, 0, 99, 'PROGRAMAS MIGRATORIOS');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productospic`
--

CREATE TABLE `productospic` (
  `id` int(10) NOT NULL,
  `producto` int(10) NOT NULL,
  `titulo` text COLLATE latin1_spanish_ci,
  `title` text COLLATE latin1_spanish_ci,
  `txt` text COLLATE latin1_spanish_ci,
  `orden` int(2) NOT NULL DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;

--
-- Volcado de datos para la tabla `productospic`
--

INSERT INTO `productospic` (`id`, `producto`, `titulo`, `title`, `txt`, `orden`) VALUES
(1, 3, NULL, NULL, NULL, 99),
(2, 3, NULL, NULL, NULL, 99),
(4, 9, NULL, NULL, NULL, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `slidertxt`
--

CREATE TABLE `slidertxt` (
  `id` int(4) NOT NULL,
  `txt4` text COLLATE utf8_spanish_ci,
  `txt6` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `num` int(2) DEFAULT NULL,
  `orden` int(2) NOT NULL DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `slidertxt`
--

INSERT INTO `slidertxt` (`id`, `txt4`, `txt6`, `num`, `orden`) VALUES
(3, 'Pellentesque sodales metus vitae placerat bibendum. Aenean ut finibus nisi. Etiam tempus vitae mauris non dictum. Cras a mollis justo. Proin nec dolor fringilla, molestie neque sed, vestibulum ante. Nullam at risus sit amet justo mollis posuere. Proin viverra justo id consequat mattis.', 'escuela', 1, 1),
(4, 'Etiam bibendum porttitor ante, at tempus massa porttitor eget. Curabitur ac efficitur massa. Duis vitae consequat nunc. Donec eu ante ac libero semper maximus in id nibh. Donec condimentum ut mi ut dapibus. Cras sed justo mi.', 'suscripcion', 2, 2),
(5, 'Vestibulum tempor a dui ut facilisis. Curabitur a tincidunt nisi. Phasellus ac ipsum congue, cursus augue sed, sodales augue. Nulla facilisi. Nulla mauris nisl, pulvinar nec magna id, facilisis mollis lectus. Phasellus eu urna quis odio pulvinar finibus. Maecenas et magna nibh.', '../', 3, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursales`
--

CREATE TABLE `sucursales` (
  `id` int(10) NOT NULL,
  `categoria` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `titulo` varchar(200) COLLATE utf8_spanish_ci DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci,
  `txtdetalle` text COLLATE utf8_spanish_ci,
  `estatus` int(1) NOT NULL DEFAULT '0',
  `fecha` date DEFAULT NULL,
  `orden` int(2) DEFAULT '99',
  `imagen` varchar(30) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lat` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `lon` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `sucursales`
--

INSERT INTO `sucursales` (`id`, `categoria`, `titulo`, `txt`, `txtdetalle`, `estatus`, `fecha`, `orden`, `imagen`, `lat`, `lon`) VALUES
(1, NULL, 'Sucursal 1', '<p>Es de prueba</p>', NULL, 0, '2019-10-01', 99, NULL, '20.667703809107746', '-103.34699871873852');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `testimonios`
--

CREATE TABLE `testimonios` (
  `id` int(10) NOT NULL,
  `titulo` text COLLATE utf8_spanish_ci,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `txt` text COLLATE utf8_spanish_ci,
  `imagen` text COLLATE utf8_spanish_ci,
  `orden` int(2) DEFAULT '99'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(100) NOT NULL,
  `user` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `pass` text COLLATE utf8_spanish_ci NOT NULL,
  `fecha` datetime DEFAULT NULL,
  `nivel` int(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id`, `user`, `pass`, `fecha`, `nivel`) VALUES
(1, 'efra', '12eb5fef578326a527019871e4ca1c35', NULL, 2),
(23, 'studies', 'a086c3e969a3a344b4468dc49a720c79', NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(10) NOT NULL,
  `nivel` int(1) NOT NULL DEFAULT '0',
  `distribuidor` int(1) NOT NULL DEFAULT '0',
  `alta` date DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `udate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `nombre` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `telefono` varchar(20) COLLATE utf8_spanish_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pass` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `empresa` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `rfc` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL,
  `calle` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `noexterior` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nointerior` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `entrecalles` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pais` varchar(100) COLLATE utf8_spanish_ci DEFAULT 'Mexico',
  `estado` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `municipio` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cp` varchar(11) COLLATE utf8_spanish_ci DEFAULT NULL,
  `calle2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `noexterior2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `nointerior2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `entrecalles2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `pais2` varchar(100) COLLATE utf8_spanish_ci DEFAULT 'Mexico',
  `estado2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `municipio2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `colonia2` varchar(100) COLLATE utf8_spanish_ci DEFAULT NULL,
  `cp2` varchar(10) COLLATE utf8_spanish_ci DEFAULT NULL,
  `imagen` varchar(50) COLLATE utf8_spanish_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nivel`, `distribuidor`, `alta`, `fecha`, `udate`, `nombre`, `email`, `telefono`, `facebook`, `pass`, `empresa`, `rfc`, `calle`, `noexterior`, `nointerior`, `entrecalles`, `pais`, `estado`, `municipio`, `colonia`, `cp`, `calle2`, `noexterior2`, `nointerior2`, `entrecalles2`, `pais2`, `estado2`, `municipio2`, `colonia2`, `cp2`, `imagen`) VALUES
(1, 1, 1, '2018-10-19', '2019-05-24 00:24:24', '2019-09-03 15:05:39', 'Efraín Gonzalez Macías', 'ing_efrain@yahoo.com', '3314305376', '10155798137147653', '12eb5fef578326a527019871e4ca1c35', 'Wozial', 'gome771206pj9', 'Río Juárez', '1906', 'L 43', 'Rio Medellin e Insurgentes', 'Mexico', 'Jalisco', 'Guadalajara', 'El Rosario', '44898', 'Paquinhuata', '162', '', 'Tecuen y Vicente Santa María', 'Mx', 'Jalisco', 'Morelia', 'Feliz Ireta', '58070', '201901303821236');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `blogpic`
--
ALTER TABLE `blogpic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carousel`
--
ALTER TABLE `carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carousel2`
--
ALTER TABLE `carousel2`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `carrousel_escuelas`
--
ALTER TABLE `carrousel_escuelas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `escuelas`
--
ALTER TABLE `escuelas`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `inicio`
--
ALTER TABLE `inicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `productoscat`
--
ALTER TABLE `productoscat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id` (`id`);

--
-- Indices de la tabla `productospic`
--
ALTER TABLE `productospic`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `slidertxt`
--
ALTER TABLE `slidertxt`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `blogpic`
--
ALTER TABLE `blogpic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carousel`
--
ALTER TABLE `carousel`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `carousel2`
--
ALTER TABLE `carousel2`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `carrousel_escuelas`
--
ALTER TABLE `carrousel_escuelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `configuracion`
--
ALTER TABLE `configuracion`
  MODIFY `id` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `escuelas`
--
ALTER TABLE `escuelas`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `favoritos`
--
ALTER TABLE `favoritos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `inicio`
--
ALTER TABLE `inicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `productoscat`
--
ALTER TABLE `productoscat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `productospic`
--
ALTER TABLE `productospic`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `slidertxt`
--
ALTER TABLE `slidertxt`
  MODIFY `id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sucursales`
--
ALTER TABLE `sucursales`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `testimonios`
--
ALTER TABLE `testimonios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
