<?php

	if (isset($_SESSION['catid'])) {
		$catId = $_SESSION['catid'];
		unset($_SESSION['catid']);
		$catDefault = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
	}else{
		$catDefault = $CONEXION -> query("SELECT * FROM productoscat ORDER BY orden LIMIT 1");
	}

	if(isset($uid)){
		$modalName = "buscar";
		
	}else{
		$modalName="modal-container";
	}

	$rowDefault = $catDefault -> fetch_assoc();
	$catNameDefault = $rowDefault['titulo'];
	$catId = $rowDefault['id'];
	$rutaLogoCat ="./img/contenido/productoscat/";
	$rutaPicProg = "./img/contenido/productos/";
	//debug($rowDefault);
	//TRAE TODOS LOS PROGRAMAS POR CATEGORIA
	$progConsulta = $CONEXION -> query("SELECT * FROM productos	WHERE categoria = $catId");

	$rutaPic ='img/contenido/varios/';
	//Seccion programas (categorias)
	$consultaCat = $CONEXION -> query("SELECT * FROM productoscat ORDER BY orden");
	// Sección de imágenes 
	$consulta = $CONEXION -> query("SELECT * FROM inicio WHERE id = 1");
	$rowConsulta = $consulta -> fetch_assoc();
	foreach ($rowConsulta as $key => $value) {
		${$key}=$value;
	}
	for ($i=1; $i < 5; $i++) { 
		$imagen='imagen'.$i;
		$picTxt='picTxt'.$i;

		${$picTxt}='img/design/blank.jpg';
		$pic=$rutaPic.${$imagen};
		if(file_exists($pic) AND strlen(${$imagen})>0){
			${$picTxt}=$pic;
		}
	}
	$bgInicio=$rutaPic.'inicio-default.jpg';
	$pic=$rutaPic.$imagen5;
	if(file_exists($pic) AND strlen($imagen5)>0){
		$bgInicio=$pic;
	}
	$bgBecas=$rutaPic.'comparte.png';
	$pic=$rutaPic.$imagen6;
	if(file_exists($pic) AND strlen($imagen6)>0){
		$bgBecas=$pic;
	}
	$bgAbajo=$rutaPic.'img-azul.jpg';
	$pic=$rutaPic.$imagen7;
	if(file_exists($pic) AND strlen($imagen7)>0){
		$bgAbajo=$pic;
	}
?>

<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?=$modalBuscar?>					        
<!--   -->
<style type="text/css">
.note{
    width: 100%;
    min-height: 400px;
    background: linear-gradient(135deg, transparent 80px, white 60px);
}
</style>
<!---->
	<section class="uk-container uk-container-expand">
		<section class="uk-grid-collapse full-container uk-grid-collapse style-img" style="background:url(<?=$bgInicio?>);">
			
				<div class="uk-align-left fotos-galeria no-movil">
					<img src="./img/design/foto-avion.png" 
					style="position: absolute;
						    top:25vh;
						    left:310px;
						    z-index:1;">
					<div class="photo-album" uk-lightbox>
						<a href="<?=$picTxt3?>" class="polaroid img10">
							<div class="uk-width-1-1 uk-cover-container tam"> 
								<img uk-cover src="<?=$picTxt3?>" alt=""> 
							</div>
						</a>
						<a href="<?=$picTxt2?>" class="polaroid img15">
							<div class="uk-width-1-1 uk-cover-container tam"> 
								<img uk-cover src="<?=$picTxt2?>" alt="">
							</div>
						</a>
						<a href="<?=$picTxt1?>" class="medium polaroid img4">
							<div class="uk-width-1-1 uk-cover-container tam"> 
								<img uk-cover src="<?=$picTxt1?>" alt=""> 
							</div>
						</a>
						<a href="<?=$picTxt4?>" class="medium polaroid img9">
							<div class="uk-width-1-1 uk-cover-container tam"> 
								<img uk-cover src="<?=$picTxt4?>" alt=""> 
							</div>
						</a>
					</div>
					<img src="./img/design/logo-foto.png" 
					class="logo_foto">
				</div>
				<div class="uk-align-right uk-width-1-1@s uk-width-1-4@m margin-right-60">

					<div class="margin-top-90 slider-principal top-slider" style=""
					uk-slider="autoplay:true;autoplay-interval:3000;clsActivated: uk-transition-active; center: true">
					    <div class="uk-position-relative">
					        <div class="uk-slider-container uk-light">
					            <ul class="uk-slider-items uk-child-width-1-1">
					            	<?php
									$consulta = $CONEXION -> query("SELECT * FROM slidertxt ORDER BY orden");
									while ($rowConsulta = $consulta -> fetch_assoc()) {
										$id=$rowConsulta['id'];
					            	?>
					                <li class="" style="min-height:300px">
					                    <div class="uk-position-center uk-panel">
					                    	<div class="note">
					                    		<div class="padding-30" style="padding-bottom:0">
						                    		<p class="color-negro padding-top-60"
						                    		style="height: 200px;
														word-wrap: break-word;
														overflow: hidden;
														text-overflow: ellipsis;
														content: '...';">
						                    			<?=nl2br($rowConsulta['txt4'])?>
						                    		</p>
						                    		
						                    	</div>
						                    	<div class="uk-flex uk-flex-center uk-child-width-1-3" style="margin-top: -10px;">
									        		<a href="<?=($rowConsulta['txt6'])?>" class="uk-grid-collapse btn-header-container">
														<div class="btn-red text-7 uk-text-uppercase box-shadown uk-text-center">
															ver mas
														</div>
														<div class="btn-red-border">&nbsp;</div>
													</a>
												</div>
					                    	</div>
					                    </div>
					                </li>
					                <?php } ?>
					            </ul>
					        </div>
					        <div class="uk-flex uk-flex-center" style="margin-top:60px">
							        <div class="uk-visible@s uk-grid-collapse" 
							        style="position:absolute!important;bottom:0px;width:80px;">
							            <a class="uk-position-center-left-out uk-position-small uk-grid-collapse blue" href="#" uk-slidenav-previous uk-slider-item="previous" style="margin:0;padding:0;position:absolute!important;"></a>
							            <a class="uk-position-center-right-out uk-position-small uk-grid-collapse blue" href="#" uk-slidenav-next uk-slider-item="next" style="margin:0;padding:0;position:absolute!important;"></a>
							            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin puntitos" 
							            style="
							                width: 80px;
										    height: 10px;
										    overflow: hidden;
										    margin-top: -10px!important;
										    margin-left: 0;"></ul>
							        </div>
							    </div>

					        
					    </div>
					    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
					</div>

				</div>
		</section>

		<section class="uk-section uk-section-muted uk-grid-collapse padding-0">
			<div class="uk-width-1-1 uk-grid-match left-0 uk-grid-collapse" uk-grid>
				<div class="uk-width-1-2@m bg-secondary">
					<div class="" style="background:url('<?=$bgBecas?>');background-repeat:no-repeat;background-position:bottom left;background-size:50%;">
						<div class="" style="padding-top:30px;padding-left:30px">
							<h2 class="signika text-xxl" style="color:#fff"><?=$titulo1?></h2>
						</div>
						<div class="uk-text-center uk-grid-collapse" uk-grid>
						    <div class="uk-width-1-3@m">
						        <div class=""></div>
						    </div>
						    <div class="uk-width-expand@m">
						        <div class="">
						        	<div class="uk-text-left text-8 vertodo_beca">
						        		<?=nl2br($texto1)?>
						        	</div>
						        	<p style="padding:15px 0"></p>
						        </div>
						    </div>
						</div>

						<div class="uk-text-center uk-grid-collapse" uk-grid>
						    <div class="uk-width-expand@m"></div>
						    <div class="uk-width-1-1@s uk-width-1-3@m padding-h-30 margin-bottom-20 uk-align-right">
						        <a href="comparteBeca" class="uk-grid-collapse btn-header-container" >
									<div class="btn-red text-7 uk-text-uppercase box-shadown">
										VER TODO
									</div>
									<div class="btn-red-border">&nbsp;</div>
								</a>
						    </div>
						</div>
					</div>
				</div>
				<div class="uk-width-1-2@m uk-grid-collapse border-cero" style="border-bottom:solid 5px #ee313d;border-right:solid 5px #ee313d;"> 
					<div class="uk-text-center  uk-grid-collapse" uk-grid>
					    <div class="uk-width-auto@m uk-flex uk-flex-middle uk-grid-collapse">
					        <div class="uk-grid-collapse ">
					        	<div class="triangulo"></div>
					        </div>
					    </div>
					    <div class="uk-flex uk-flex-center uk-flex-middle uk-width-expand@m  uk-grid-collapse">
					        <div class=" uk-grid-collapse padding-h-30">
					        	<h1 class="signika text-xxl" style="font-weight:900;padding-top:6%; color:#ee313d">
					        		<?=$titulo5?>
					        	</h1>
					        	<div class="uk-text-center text-8"
					        	style="height:150px;
								    word-wrap: break-word;
								    overflow: hidden;
								    text-overflow: ellipsis;
								    content: '...';">
					        		<?=$texto5?>
					        	</div>
					        	<div class="uk-flex uk-flex-center uk-child-width-1-3 margin-bottom-20">

					        		<a href="#<?=$modalName ?>"  uk-toggle class="uk-grid-collapse btn-header-container">
										<div class="btn-red text-7 uk-text-uppercase box-shadown">
											EMPEZAR
										</div>
										<div class="btn-red-border">&nbsp;</div>
									</a>
								</div>
					        </div>
					    </div>
					</div>
				</div>
			</div>
		</section>
			
		<section class="uk-section uk-section-muted ">
			<div class="uk-flex uk-flex-center uk-align-center">
				<img src="./img/design/icon1.png" style="width:5%">
			</div>
			<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
				Programas en el extranjero
			</p>
			<div class="uk-child-width-1-2@m left-0" uk-grid>
				<div class="uk-grid-collapse" style="">
					<div class="uk-text-center bg-thirdly" uk-grid>
						<div class="uk-width-auto uk-grid-collapse border-cero" style="margin-right: 10px;">
							<div class="uk-text-uppercase padding-10 bg-green" >
							    <span class="padding-h-50 uk-text-uppercase " style="color: #FFF">Programas</span>
							</div>
						</div>
						<div class="uk-width-expand uk-grid-collapse uk-align-center" id="idioma">
							<select class="uk-select uk-grid-collapse" align='right' id="search">
							<?php while ($rowCats = $consultaCat ->fetch_assoc()): 
								
								if (isset($catId) AND $catId==$rowCats['id']) {
									$estatus='selected';
								}else{
									$estatus='';
								}
							?>
						        <option class="uk-text-right uk-text-uppercase" align='right' value="<?= $rowCats['id']?>" <?= $estatus ?>><a href="busqueda"><?= $rowCats['titulo']  ?></a></option>
						    <?php endwhile  ?>
						    </select>
						</div>
					</div>
				</div>
				<div></div>
			</div>
		</section>
		<section class="bg-gris-ligth padding-v-30">
			<div class="uk-text-center" uk-grid >
			    <div class="uk-width-1-4@m">
			        <div class="padding-20">
			        	<img data-src="<?= $rutaLogoCat.$rowDefault['imagen']?>" width="150" height="150" alt="" uk-img>
						<h3 class="uk-text-uppercase signika text-xxl color-verde2" style="font-weight:700"><?= $rowDefault['titulo']?> </h3>
						<div uk-margin>
							<div style="font-size:12px;text-align:justify;">
								<?= $rowDefault['txt']?>
							</div>	
						</div>
						<div class="uk-text-center margin-top-10" uk-grid >
							<div class="uk-width-expand@m">
			    			</div>
			    			<div class="uk-width-1-2@m">
			    				<a href="<?= $catId?>_catalogo_programas.php" class="uk-grid-collapse btn-header-container">
									<div class="btn-gral text-7 uk-text-uppercase box-shadown">
										VER TODO
									</div>
									<div class="btn-gral-border">&nbsp;</div>
								</a>
			    			</div>
			    		</div>
			        </div>					
			    </div>
			    <div class="uk-width-expand@m">
						<div uk-slider id="idiomas" class="margin-bottom-menos-74">
						    <div class="uk-position-relative">
						        <div class="uk-slider-container uk-light">
						            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m" id="idiomas">
									<?php  
									while ($programa = $progConsulta -> fetch_assoc()): 
										$escuelaId = $programa['escuelaid'];
										
										$consultaEscuela = $CONEXION -> query("SELECT id,logoescuela,imagen FROM escuelas WHERE id = $escuelaId");
										$cantEscuela =$consultaEscuela->num_rows; 
										
										if($cantEscuela>0){
											$escuelaRow = $consultaEscuela -> fetch_assoc();
											$consultaSliderEscuela = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaRow[id] LIMIT 1");
											$rowSlider = $consultaSliderEscuela -> fetch_assoc();
											//$imgEscuela = $rowSlider['id'].".jpg";
											$imgEscuela = $escuelaRow['imagen'];
										}
										
									?>
									<div class="uk-grid-collapse border-cero padding-cards-inicio">
						                <li class="uk-grid-collapse margin-cards">
						                	<div class="uk-card uk-card-default margin-16" style="margin-bottom:28px;">
									            <div class="uk-card-media-top uk-grid-collapse">
									            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-200" style="background-image: url('<?= "./img/contenido/escuelas/".$imgEscuela?>');"> </div>
									            </div>
									            <div class="uk-card-body bg-gris-ligth border-cero padding-30">
									            	<?php if($cantEscuela > 0):  ?>
									            	<div class="uk-grid-collapse bg-gris-ligth margin-top-menos-80">
										            	<div class="uk-background-contain uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-120" style="background-image: url('<?= "./img/contenido/escuelas/".$escuelaRow["logoescuela"]?>');"> </div>
										            </div>
										        	<?php endif  ?>
									                <p class="uk-grid-collapse uk-text-uppercase border-cero margin-top-40"><?= $catNameDefault  ?></p>
									                <h3 class="uk-card-title uk-grid-collapse uk-text-uppercase border-cero margin-top-10 text-9"><?= $programa['titulo']; ?></h3>
									            </div>
									            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
									    				<a href="<?=$programa[id] ?>_programa" class="uk-grid-collapse btn-more-container">
															<div class="btn-gral text-7 uk-text-uppercase box-shadown">
																VER MAS
															</div>
															<div class="btn-gral-border">&nbsp;</div>
														</a>
									    		</div>
									        </div>
						                </li>
						            </div>
						            <?php
									endwhile
									?>
						            </ul>
						        </div>
						        <div class="uk-hidden@s uk-light">
						            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
						            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
						        </div>

						        <div class="uk-flex uk-flex-center" style="margin-top:60px">
							        <div class="uk-visible@s uk-grid-collapse" 
							        style="position:absolute!important;bottom:0px;width:80px;">
							            <a class="uk-position-center-left-out uk-position-small uk-grid-collapse blue" href="#" uk-slidenav-previous uk-slider-item="previous" style="margin:0;padding:0;position:absolute!important;"></a>
							            <a class="uk-position-center-right-out uk-position-small uk-grid-collapse blue" href="#" uk-slidenav-next uk-slider-item="next" style="margin:0;padding:0;position:absolute!important;"></a>
							            <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin puntitos" 
							            style="
							                width: 80px;
										    height: 10px;
										    overflow: hidden;
										    margin-top: -10px!important;
										    margin-left: 0;"></ul>
							        </div>
							    </div>
						    </div>
						    
						</div>
			        </div>
			    
			</div>
		</section>

		<section class="uk-section uk-section-muted uk-grid-collapse padding-v-20">
			<div class="uk-child-width-1-2@m uk-grid-match" uk-grid>
				<div>
					<img src="<?=$bgAbajo?>">
				</div>
				<div class="bg-primary" uk-scrollspy="cls: uk-animation-slide-right; repeat: true"> </div>
			</div>
		</section>

		<section class="uk-section uk-section-muted uk-grid-collapse bg_white">
			<div class="uk-container uk-container-small">
				<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:700;">
					trabajamos con los mejores</p>
				<div uk-slider class="uk-grid-collapse">
				    <div class="uk-position-relative">
				        <div class="uk-slider-container uk-light">
				            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m">
				            	<?php
				            		$consultaSocios = $CONEXION -> query("SELECT * FROM empresas where estatus = 1 ORDER BY orden");
				            		while ($sociosRow = $consultaSocios -> fetch_assoc()):
				            	?>
				                <li>
				                    <img class="uk-align-center" src="./img/contenido/empresas/<?= $sociosRow['id'] ?>.png" alt="" style="max-height: 120px; max-width:300px; ">
				                </li>
				               <?php endwhile ?>
				            </ul>
				        </div>

				        <div class="uk-hidden@s uk-light">
				            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>

				        <div class="uk-visible@s">
				            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>
				    </div>
				    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
				</div>
			</div>
		</section>

	</section>

<!---->
<!---->
<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>