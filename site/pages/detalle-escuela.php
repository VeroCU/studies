<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php 
	$escuelaId=$rowEscuela['id']; 
	$rutaPic = "./img/contenido/escuelas/";
	$rutaCarousel = "./img/contenido/escuelas/carousel/";
	$noPic = "./img/design/detalle.jpg";
?>
<section class="uk-container uk-container-expand">
	<section class="uk-section uk-section-muted border-cero padding-top-50" style="padding-top:6%;">
		<div uk-slideshow="autoplay:true;animation:pull;min-height:400;max-height:600;" class="uk-grid-collapse" uk-grid>
		    <div class="uk-visible-toggle uk-width-1-1 uk-flex-first">
		        <div class="uk-position-relative">
		            <ul class="uk-slideshow-items">
		            	<?php
		            	$consultaCarousel = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaId");
		            	$cantPics = $consultaCarousel -> num_rows;
		            	if($cantPics > 0):
		            	?>
		            		<?php while ($rowCarousel = $consultaCarousel -> fetch_assoc()):
		            		?>
				                <li>
				                	<img src="<?= $rutaCarousel.$rowCarousel['id']."-orig.jpg" ?>" uk-cover>
				                   
				                </li>
				            <?php endwhile ?>
				        <?php else:  ?> 
				         	<li>
				                <img src="<?= $noPic ?>" uk-cover>  
				            </li>   
		                <?php endif ?>
		            </ul>
		        </div>
  				<ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
		        <!--<div class="uk-hidden@s uk-light">
		            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		        </div>
		        <div class="uk-visible@s">
		            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		        </div>-->
		    </div>
		    
		</div>
	</section>
	<section class="uk-section uk-section-muted border-cero bg_white detalle-escuela">
		<div class="uk-container margin-top-100">
			<div class="left-0 border-cero" uk-grid>
				<div class="uk-aling-center uk-text-center">
					<img class="uk-aling-center" src="<?=$rutaPic.$rowEscuela['logoescuela'] ?>" style="width:20%">
					<h2 class="signika text-xxl signika blue" style=""><?=$rowEscuela['titulo'] ?></h2>
					<div class="uk-flex uk-flex-center uk-flex-middle" style="">
						<hr class="uk-flex uk-flex-center" style="border-top: solid 4px #1559a9;
						    height: 1px;
						    margin:12px 0;
						    padding:0;
						    width:60px;">
					</div>
					<div class="uk-text-left text-8 margin-h-180 color-terciario">
						<?= $rowEscuela['txt'] ?>
					</div>
					<div>
						<a href="<?=$escuelaId?>_programas_escuela.php" class="uk-grid-collapse">
							<p class="uk-aling-center uk-text-center text-7 uk-grid-collapse" style="margin-top:0">
							<span style="padding:0 60px; padding-bottom:10px;border-bottom:solid #1559a9;">Ver programas</span></p>
						</a>
					</div>
				</div>
			</div>
		</div>
	</section>

	<section class="uk-section uk-section-muted border-cero bg_white margin-top-60">
		<div class="uk-container uk-container-expand border-cero">
			<div class=" left-0 bg-primary uk-text-left" uk-grid style="height: 100px"> &nbsp; </div>
		</div>
	</section>
	<section class="uk-section uk-section-muted bg-thirdly">
		<div class="uk-container" style="">
			<div class="left-0 uk-padding" uk-grid>
				<div class="text-8 uk-text-justify color-terciario">
					<?= $rowEscuela['txtdetalle'] ?>
				</div>
				
				<div class="uk-width-1-1">
					<div id="map" class="border-cero box-shadown bg_white" style="
			     	 border:5px solid white;
			     	 margin-bottom:-180px;"></div>
			     	 <script>
					    // Initialize and add the map
						function initMap() {
							console.log("El whats","'<?php echo $socialWhats ?>'");
						    var pos = {lat:<?=$rowEscuela['lat']  ?>, lng:<?=$rowEscuela['lon']  ?>};
						    var map = new google.maps.Map(
						    document.getElementById('map'), {zoom: 14, center: pos});
						    var marker = new google.maps.Marker({position: pos, map: map});
						}
					    </script>
					    <script async defer src="https://maps.googleapis.com/maps/api/js?key=<?=$googleMaps?>&callback=initMap"></script>
			    </div>
			</div>
		</div>

		<section class="uk-section uk-section-muted border-cero bg_white">
			<div class="uk-container uk-container-expand border-cero">
				<div class=" left-0 bg-primary uk-text-left" uk-grid style="height: 100px"> &nbsp; </div>
			</div>
		</section>
	</section>
	<div class="padding-50">&nbsp;</div>
</section>

<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			