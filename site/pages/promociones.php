<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
	<section class="uk-container uk-container-expand">
		<div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
		    <div>
		        <div class="uk-padding bg-secondary border-cero text-8">&nbsp;</div>
		    </div>
		    <div>
		        <div class="uk-padding border-cero text-8" style="background:#fe5761">&nbsp;</div>
		    </div>
		</div>

					<section class="uk-section uk-section-muted border-cero bg_white" style="">
						<div class="uk-flex uk-flex-center uk-align-center padding-top-50">
							<img src="./img/design/icon3.png" class="width-5">
						</div>
						<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
							Promociones
						</p>
					</section>

					<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white">
							<div class="uk-padding uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center border-cero left-0  bg_white" uk-grid>
		    					<?php  
								$consultaProgramas = $CONEXION -> query("SELECT * FROM productos WHERE descuento > 0 ORDER BY orden");

								while($programasRow = $consultaProgramas -> fetch_assoc()):
									$escuelaId=$programasRow['escuelaid'];
									$consultaEscuela = $CONEXION -> query("SELECT id,logoescuela FROM escuelas WHERE id = $escuelaId");
									$cantEscuela =$consultaEscuela->num_rows; 
									
									if($cantEscuela>0){
										$escuelaRow = $consultaEscuela -> fetch_assoc();
										$consultaSliderEscuela = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaRow[id] LIMIT 1");
										$rowSlider = $consultaSliderEscuela -> fetch_assoc();
										$imgEscuela = $rowSlider['id'].".jpg";
									}
								?>
					               <div class="uk-grid-collapse border-cero padding-cards-container">
				                	<div class="uk-card uk-card-default box-shadown margin-cards">
							            <div class="uk-card-media-top  uk-grid-collapse">
							            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-200" style="background-image: url('<?= "./img/contenido/escuelas/carousel/".$imgEscuela?>');"> </div>
							            </div>
							            <div class="uk-card-body bg-gris-ligth border-cero padding-30">
							            	<div class="uk-grid-collapse bg-gris-ligth margin-top-menos-80">
								            	<div class="uk-background-contain uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-120" style="background-image: url('<?= "./img/contenido/escuelas/".$escuelaRow["logoescuela"]?>');"> </div>
								            </div>
							                <p class="uk-grid-collapse uk-text-uppercase border-cero margin-top-40"><?= $rowCategoria['titulo'];  ?></p>
							                <h5 class="uk-card-title uk-grid-collapse uk-text-uppercase border-cero margin-top-10 text-9"><?= $programasRow['titulo'];  ?> </h5>
							            </div>
							            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
							    				<a href="<?=$programasRow["id"] ?>_programa" class="uk-grid-collapse btn-more-container">
													<div class="btn-gral text-7 uk-text-uppercase box-shadown">
														VER MAS
													</div>
													<div class="btn-gral-border">&nbsp;</div>
												</a>
							    		</div>
							        </div>
				                </div>
					            <?php
								endwhile
								?>
							</div>
						
						<div class="padding-50"></div>
					</section>
	</section>	

<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			