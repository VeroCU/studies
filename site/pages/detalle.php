<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php ////debug($rowPrograma);
	//Rutas de img's
	$rutaEscuela = "./img/contenido/escuelas/";
	$rutaCat =  "./img/contenido/productoscat/";

	$escuelaId = $rowPrograma['escuelaid']; 
	$catId =  $rowPrograma['categoria'];

	$catConsulta = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
	$catRow = $catConsulta -> fetch_assoc();
	
	$logoCat = $catRow['imagen'];
	if($escuelaId != ""){
		$consultaEscuela = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $escuelaId");
		$rowEscuela = $consultaEscuela -> fetch_assoc();
		$logoEscuela = $rowEscuela['logoescuela'];
		$consultaSlider = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaId");

	}

	//Descripciones de programa
	$descConsulta = $CONEXION -> query("SELECT * FROM descripciones WHERE producto = $id");

?>
		<section class="uk-container uk-container-expand">
			<section class="uk-section uk-section-muted border-cero padding-top-50" style="padding-top:6%;">
				<div uk-slider>
				    <div class="uk-position-relative">
				        <div class="uk-slider-container uk-light">
				            <ul class="uk-slider-items uk-child-width-1-1">
				            	<?php
				            	while ($slider = $consultaSlider -> fetch_assoc()):
				            		
				            	?>
				                <li>
				                    <div class="uk-background-cover uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/contenido/escuelas/carousel/<?= $slider['id'] ?>.jpg);height:550px"> </div>
				                </li>
				                <?php endwhile ?>
				            </ul>
				        </div>

				        <!--<div class="uk-hidden@s uk-light">
				            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>
				        <div class="uk-visible@s">
				            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>-->
				    </div>
				    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
				</div>
			</section>
			<section class="uk-section uk-section-muted border-cero bg_white margin-top-60">
				<div class="uk-container">
					<div class="left-0 border-cero" uk-grid>
						<div class="uk-aling-center uk-text-center">
							<img class="uk-aling-center" src="<?= $rutaCat.$logoCat ?>">
							<h2 class="signika text-xxl uk-text-uppercase" style="color:#000"><?= $rowPrograma['titulo']  ?></h2>
							<div class="uk-text-left text-8" style="color:#000;">
								<?= $rowPrograma['txt']; ?>
							</div>
						</div>
					</div>
				</div>
			</section>
			<section class="uk-section uk-section-muted border-cero bg_white detalle-escuela">
				<div class="uk-container margin-top-20">
					<a href="<?=$escuelaId ?>_detalle.php" class="uk-grid-collapse">
						<img class="uk-aling-center" src="<?= $rutaEscuela.$logoEscuela ?>" style="margin-left:auto;margin-right:auto;text-align:center;width:20%">
						<p class="uk-aling-center uk-text-center text-7 uk-grid-collapse" style="margin-top:0">
							<span style="padding:0 60px; padding-bottom:10px;border-bottom:solid #1559a9;">Ver escuela</span></p>
					</a>
				</div>
			</section>

			<section class="uk-section uk-section-muted border-cero bg_white margin-top-60">
				<div class="uk-container uk-container-expand border-cero">
					<div class=" left-0 bg-green uk-text-left" uk-grid>
						<div class="uk-width-1-5@m">
							<div class="uk-background-contain uk-height-medium uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/detalle4.png);height:80px;margin-top:-2px">
					            
					        </div>
				        </div>
			        </div>
				</div>
			</section>
			<section class="uk-section uk-section-muted bg-thirdly">
				<div class="detalle-margin">
					<div class="uk-container box-shadown bg_white detalle-programa">
						<div class="left-0 uk-padding" uk-grid>
							<!--Para movil-->
							<div class="uk-width-1-1 uk-hidden@m" style="background:#f8f8f8;padding:20px!important">
								<div class="" uk-grid style="margin-left:0;margin-top:10px">
									<?php 

										$consultaD= $CONEXION ->query("SELECT * FROM descripciones WHERE producto = $id");
									
										$ids=array();
										$descripcionesMovil =array();
										while($descmovil = $consultaD -> fetch_assoc()){
											array_push($ids, $descmovil['id']);
											array_push($descripcionesMovil, $descmovil);
										}

										$separado_por_comas1 = implode(",", $ids);

										for ($i=0; $i < sizeof($descripcionesMovil) ; $i++):
										
									?>
									<div class="uk-width-1-1 uk-grid-collapse" style="padding-left:0;margin-top:10px">
										<a class="actualselector" data-id="<?= $descripcionesMovil[$i]["id"]  ?>" data-array="<?= $separado_por_comas1 ?>">	
											<div class="" uk-grid style="margin-left:0">
												<div class="uk-width-auto uk-grid-collapse uk-aling-center" style="padding-left:0;">
													<img src="./img/contenido/descripciones/<?= $descripcionesMovil[$i]['imagen']  ?>"  style="max-height: 30px; min-width: 30px;">
												</div>
												<div class="uk-width-expand" style="padding-left:0;">
													<div class="uk-grid-collapse">
														<div class="uk-h4 yk-grid-collapse padding-top-10" style="margin-bottom:0"><?= $descripcionesMovil[$i]['titulo']  ?></div>
													</div>
												</div>
												<!--div class="uk-width-1-1 text-7 uk-grid-collapse uk-padding-small uk-text-truncate" style="font-weight:100;max-height: 80px; margin-top:0; padding:0;">
													<?= $descripcionesMovil[$i]['descripcion']  ?>
												</div-->
											</div>
										</a>
									</div>
									<?php 
										endfor  
									?>
								
								</div>
							</div>
							<?php 
								$consultaDescripcion = $CONEXION ->query("SELECT * FROM descripciones WHERE producto = $id");
								$first = "";
								$ids=array();
								while($descActual = $consultaDescripcion -> fetch_assoc()):
								array_push($ids, $descActual['id']);
							?>
						
								<div class="uk-width-expand padding-h-50" id="actual<?= $descActual['id'] ?>" <?= $first?>>
									<div class="uk-h3 uk-grid-collapse" style="font-weight:700; margin:0;"><?= $descActual['titulo'] ?></div>
									<div class="uk-h4 uk-grid-collapse" style="font-weight:100; margin:0;">Descripción de el programa</div>
									<hr>
									<div class="text-8" style="text-align:justify;" style="border:solid;">
										<?= $descActual['descripcion'] ?>
									</div>
								</div>
					
							

							<?php 
								$first ="hidden";
								endwhile;  
								$separado_por_comas = implode(",", $ids);
							?>
							<!--Para PC-->
							<div class="uk-width-1-3 uk-padding-small uk-visible@m" style="background:#f8f8f8;border-radius:40px!important">
								<div class="" uk-grid style="margin-left:0;margin-top:10px">
									<?php 
										while($descRow = $descConsulta -> fetch_assoc()):
											$truncado = truncate($descRow["descripcion"],200);
									?>
									<div class="uk-width-1-1 uk-grid-collapse grey-hover" style="padding-left:0;margin-top:10px">
										<a class="actualselector" data-id="<?= $descRow["id"]  ?>" data-array="<?= $separado_por_comas ?>">
											<div class="" uk-grid style="margin-left:0">
												
												<div class="uk-width-auto uk-grid-collapse uk-aling-center" style="padding-left:0;">

														<img src="./img/contenido/descripciones/<?= $descRow["imagen"]  ?>" style="max-height: 40px; max-width: 40px;">
													
												</div>
												<div class="uk-width-expand" style="padding-left:5px;">
													<div class="uk-grid-collapse">
														<div class="uk-h4 yk-grid-collapse padding-top-10" style="margin-bottom:0"><?= $descRow["titulo"]  ?></div>
													</div>
												</div>
												<!--div class="uk-width-1-1 text-7 uk-grid-collapse uk-padding-small " style="font-weight:100;max-height: 80px; margin-top:0; padding:0; text-align:justify;overflow: hidden; text-overflow: ellipsis;">
													<?= 

													$truncado
													?>
												</div-->
											
											</div>
											
										</a>
									</div>
									<?php endwhile ?>
								</div>
							</div>
						</div>	
					</div>
					<div class="uk-width-1-1 uk-flex uk-flex-center margin-top-menos-20" >
						<a class="zero" href="<?=$id?>_cotizar">
							<div class="uk-align-center uk-text-center uk-text-uppercase" style="background:#1559a9;border-radius:40px;color:#fff;font-size:14px;padding:6px 0;width:140px">
								Cotizar
							</div>
						</a>
					</div>
				</div>
			</section>

			<section class="uk-section uk-section-muted uk-grid-collapse bg_white border-cero">
				<div class="uk-container uk-container-small margin-top-80">
					<p class="blue uk-align-center signika border-cero" style="font-size:46px;text-align:center;font-weight:700;">
						TESTIMONIOS
					</p>
					<div class="uk-flex uk-flex-center uk-flex-middle" style="">
						<hr class="uk-flex uk-flex-center" style="border-top: solid 4px #1559a9;
					    height: 1px;
					    margin:12px 0;
					    padding:0;
					    width:40px;">
					</div>
				</div>
			</section>
			<section class="uk-section uk-section-muted border-cero bg_white margin-bottom-50">
				<div class="uk-container">
					<div class="uk-flex uk-flex-center uk-align-center">
					<div class="uk-child-width-1-3@s uk-child-width-1-5@m uk-text-center left-0" uk-grid>
					
						<?php
						$consultaTestimonios = $CONEXION -> query("SELECT * FROM testimonios ORDER BY orden");
						 while($row_testimonios = $consultaTestimonios->fetch_assoc()):
						 
						?>
					    <div class="border-cero padding-10">
					        <div class="uk-card uk-card-default padding-20">
					        	<p class="blue uk-align-center signika text-9 border-cero" style="text-align:center;font-weight:700;">
									<?= $row_testimonios['titulo']  ?>
								</p>
								<div class="uk-flex uk-flex-center uk-flex-middle" style="">
									<hr class="uk-flex uk-flex-center" style="border-top: solid 4px #1559a9;
								    height: 1px;
								    margin:12px 0;
								    padding:0;
								    width:40px;">
								</div>
								<div class="border-cero text-7" style="color:#000; text-align:justify!important;">
									<?= $row_testimonios['txt']  ?>
								</div>
								
						            <div class="uk-flex uk-flex-center uk-flex-middle border-cero margin-top-20" style="">
						            <!-- CALIFICACIÓN -->
						            	<div class="color-gris uk-text-left">
							                <i class="fas fa-star color-gris-claro'.$star1.'"></i>
							                <i class="fas fa-star color-gris-claro'.$star2.'"></i>
							                <i class="fas fa-star color-gris-claro'.$star3.'"></i>
							                <i class="fas fa-star color-gris-claro'.$star4.'"></i>
							                <i class="fas fa-star color-gris-claro'.$star5.'"></i>
							                &nbsp;
							            	<span class="text-sm"></span>
						            	</div>
						            </div>
			          			
					        </div>
					    </div>
					    <?php
						endwhile
						?>
					</div>
					</div>
				</div>
				<div class="padding-20">&nbsp;</div>
			</section>
		</section>
<?=$footer?>
<?=$scriptGNRL?>
<script >
	$( document ).ready(function() {
		$(".actualselector").click(function(){
			var id = $(this).data("id");
			var arrayIds = $(this).data("array");
			arrayIds = arrayIds.split(",");
			
			for (var i = 0; i < arrayIds.length; i++) {
				
				if(arrayIds[i] == id){
					var elemento = "actual"+arrayIds[i];
					$("#" + elemento).show();
					$("#" + elemento).attr('hidden',false);
				}else{
					var elemento = "actual"+arrayIds[i];
					$("#" + elemento).hide();
				}
			}
			
		});
	});

</script>

</body>
</html>


			