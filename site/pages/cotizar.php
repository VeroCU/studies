<?php
	$rutaPic ='img/contenido/varios/';

// Sección de imágenes 
	$consulta = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");

	$rowConsulta = $consulta -> fetch_assoc();
	$about1=$rowConsulta["about1"];
	$about2=$rowConsulta["about2"];
	$about3=$rowConsulta["about3"];
	$bgprincipal=$rutaPic.$rowConsulta["imagen2"];
	$bgnosotros=$rutaPic.$rowConsulta["imagen4"];
//CONSULTA DE PROGRAMA
	$consultaPrograma = $CONEXION -> query("
		SELECT p.*,d.id as divisaId,d.nombre,d.simbolo,d.precio_pesos,d.cambio,d.valor_actual FROM productos p 
		JOIN divisas d ON d.id = p.divisa
		WHERE p.id = $id");

	$programa = $consultaPrograma -> fetch_assoc();
	$cuotaServio = $programa['cuota'];
	$cambio = $programa['cambio'];
	//debug($programa);
	$escuelaId = $programa['escuelaid'];
	$divisa = $programa['nombre'];
	$divSimbolo = $programa['simbolo'];

//CONSULTA DE PRECIOS DEL PROGRAMA
	$consultaPrecios = $CONEXION -> query("SELECT * FROM productos_precios WHERE producto = $id AND estatus = 1 ORDER BY obligatorio DESC");
	

//CONSULTA DE ESCUELA
	$consultaEscuela = $CONEXION -> query("
		SELECT es.id,es.categoria,es.titulo as nombreescuela,es.estado,es.ciudad,p.nombre as pais,p.id,p.imagen as paisimg,pr.id,pr.nombre as provincia
		FROM escuelas es
		JOIN paises p ON es.pais = p.id
		JOIN provincias pr ON es.provincia = pr.id
		WHERE es.id = $escuelaId");

	$escuela = $consultaEscuela -> fetch_assoc();
	//debug($escuela);

//CONSULTA DE HOSPEDAJES
	$hospedajes=array();
	$consultaHosp = $CONEXION -> query("SELECT * FROM hospedajes where escuela = $escuelaId");
	while ($rowConsultaH = $consultaHosp -> fetch_assoc()){
		array_push($hospedajes, $rowConsultaH);
	}
?>


<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section class="uk-flex-center"  style="min-height:600px;padding-top: 120px">
	<div class="uk-container uk-container-large">
		<div uk-grid style="margin-left: 0">
		<!--pestañas -->
			<ul class="uk-width-1-1 uk-subnav uk-subnav-pill" uk-switcher id="cotizador-tab" >
			    <li class="uk-active">
			    	<a href="" class="uk-flex-middle">
			    		<div class="text-xxl">1.</div>
			    		<div>Detalles del programa</div>
			    	</a>
			    </li>
			    <li>
			    	<a href="">
			    		<div class="text-xxl">2.</div>
			    		<div>Costos del programa</div>
			    	</a>
			    </li>
			    <li>
			    	<a href="">
			    		<div class="text-xxl">3.</div>
			    		<div>Hospedaje</div>
			    	</a>
			    </li>
			    <!--li class="uk-disabled">
			    	<a href="#">
			    		<div class="text-xxl">4.</div>
			    		<div>Cotizar</div>
			    	</a>
			    </li-->
			</ul>
		<!--Contenido pestañas-->
			<ul class="uk-switcher uk-margin uk-width-1-1">
			<!-- INFORMACION BASICA DEL PROGRAMA -->
			    <li>
			    	<div class="uk-container uk-container-large">
			    		<div class="uk-card uk-card-default uk-card-body color-gris-contizador">
		   					<p>
		   						<span class="color-gris-7 negritas">Nombre del programa: </span> <?= $programa['titulo']  ?>
		   					</p>
		   					<p>
		   						<span class="color-gris-7 negritas">Escuela: </span> <?= $escuela['nombreescuela']  ?>&nbsp;&nbsp;
		   						<span class="color-gris-7 negritas">Pais :</span> <?= $escuela['pais']  ?> &nbsp;&nbsp;
		   						<span class="color-gris-7 negritas">Provincia :</span> <?= $escuela['provincia']  ?> &nbsp;&nbsp;
		   					</p>
		   					<p>
		   						<span class="color-gris-7 negritas">Divisas: </span> <?= $programa['nombre']  ?>
		   					</p>
		   					<p data-inscri="<?=$programa['inscripcion'] ?>" id="inscripcion">
		   						<span class="color-gris-7 negritas">Costo del inscripcion: </span> <?= $programa['simbolo'].$programa['inscripcion']?> (no reembolsable)
		   					</p>
		   					<!--p data-costo="<?=$programa['precio'] ?>" id="costo">
		   						<span class="color-gris-7 negritas">Costo del curso: </span> <?= $programa['simbolo'].$programa['precio']?> 
		   					</p-->
			   				<div class="uk-margin uk-width-1-1@s uk-width-1-4@m">
			   					<label class="color-gris-7 negritas">Cuantos meses tienes disponibles <span id="meses"> 0 </span></label>
            					<input class="uk-range meses-picker" type="range" value="0" min="0" max="48">
	       					</div>
							
							<div class="uk-width-1-3">
								<a href="#" uk-switcher-item="next" class="uk-button uk-button-default uk-width-1-3">SIGUIENTE</a>
		       				</div>
								
			    		</div>
			    	</div>

				</li>
			<!--Precios del programa -->
			    <li>
					<div class="uk-container uk-container-large">
			    		<div class="uk-card uk-card-default uk-card-body color-gris-contizador">
			    			<div class="uk-container uk-container-large">
		    					<div style="margin-left: 0" uk-grid>
					    			<h2 class="text-xl negritas uk-text-center uk-width-1-1">Personaliza tu programa </h2>
					    			<?php
					    				$obligatoriosKeys = array();
					    				$obligatorios = 0;
					    				while ($rowPrecios = $consultaPrecios -> fetch_assoc()):
					    					
						    				if($rowPrecios['obligatorio'] == 1){
						    					$obligatorios = $obligatorios	+ $rowPrecios['precio'];
						    					array_push($obligatoriosKeys, $rowPrecios['id']); 
						    				}

						    				if($rowPrecios['periodo'] == 1){
						    					$precio =  $rowPrecios['precio'] * $rowPrecios['inicio'];
						    					
						    				}else{
						    					$precio = $rowPrecios['precio'];
						    				}
						    				
					    			 ?>
					    			 	<?php if($rowPrecios['obligatorio'] == 1 ):  ?>
											<div class="uk-width-2-3">
												<div class=""uk-grid>
													<div class="uk-width-auto">
					   									<span class="color-gris-7 negritas">Concepto: </span> <?= $rowPrecios['concepto']  ?> 
					   								</div>
					   								<div class="uk-width-expand padding-5" style="border-bottom: 2px dotted;">
					   									
					   								</div>
												</div>
					   							
					   						</div>
					   						<div class="1-3">
					   							<span class="color-gris-7 negritas">Precio: </span> <?= $divSimbolo.$precio." ".$rowPrecios['periodotxt']." (obligatorio)" ?>
					   						</div>
					   					<?php else:  ?>
											<div class="uk-width-2-3">
												<div class=""uk-grid>
						   							<div class="uk-width-auto">
						   								<span class="color-gris-7 negritas">Concepto: </span> <?= $rowPrecios['concepto']  ?> 
						   							</div>
						   							<div class="uk-width-expand padding-5" style="border-bottom: 2px dotted;"></div>	
					   							</div>
					   						</div>
					   						<div class="uk-width-1-3">
					   							<span class="color-gris-7 negritas">Precio: <?= $divSimbolo?> </span> <span id="precio<?=$rowPrecios['id'] ?>"> <?= $precio ?>  </span>  
					   							<label><input id="checkprecio<?=$rowPrecios['id']?>" class="uk-checkbox precios" type="checkbox" value="<?=$precio?>" data-id="<?=$rowPrecios['id']?>" style="height:20px;width: 20px"></label>
					   							<?php  if($rowPrecios['periodo'] == 1):?>
					   							<div class="uk-width-1-1">
					   								<label class="color-gris-7 negritas">Selecciona un rango : <span id="periodo<?=$rowPrecios['id']?>"></span></label>
					   								<input id="rangeprecio<?=$rowPrecios['id']?>" class="uk-range periodo-picker" data-id="<?= $rowPrecios['id'] ?>" data-precio="<?=$rowPrecios['precio']?>" type="range" value="<?= $rowPrecios['inicio']?>" min="<?= $rowPrecios['inicio']  ?>" max="<?=$rowPrecios['fin']?>">
					   							</div>
												<?php endif ?>
					   						</div>
										<?php endif ?>

					    			<?php endwhile ?>

					    			<div class="negritas text-5  parrafo-margin uk-width-2-3">
			       						<?= $leyenda  ?>
			       					</div>
			       					<div class="uk-width-1-2">
										<a href="#" uk-switcher-item="next" class="uk-button uk-button-default uk-width-1-3 uk-text-center">SIGUIENTE</a>
					       			</div>
				    			</div>
			    			</div>
			    		</div>
			    	</div>
			    </li>
			<!-- HOSPEDAJES -->
			    <li>
		    		<div class="uk-container uk-container-large">
		    			<div class="uk-card uk-card-default uk-card-body color-gris-contizador">
		    				<h2 class=" text-xl negritas uk-text-center">Te ofrecemos estas opciones de hospedaje</h2>
		    				<div class="uk-divider-icon"></div>
		    				<div uk-grid style="margin-left: 0">
								
								<ul class="uk-width-2-3@m" uk-accordion="multiple: true">
		    					<?php for ($i=0; $i < sizeof($hospedajes) ; $i++): ?>
		    						
								    <li class="">
										<a class="uk-accordion-title uk-link-reset" href="#"><span class="color-gris-7 negritas"><?= $hospedajes[$i]['titulo']  ?></span></a>
								        <div class="uk-accordion-content">
								        	
								            <div class="margin-h-20 color-gris-contizado margin-left-10 padding-left-20"><?= $hospedajes[$i]['txt']  ?></div>
								        </div> 
								    </li>
								
						         <?php endfor ?>
						     	</ul>

						     	
						     	<div class="uk-width-1-3@m padding-left-10">
						     		<div class="uk-form-controls uk-form-controls-text">
						     			
						     			<?php for ($i=0; $i < sizeof($hospedajes) ; $i++): ?>
						     				<div class="uk-padding-remove">
							            		<label class="color-gris-7 negritas">
							            			<input class="uk-radio circle" type="radio" name="radio1" data-id="<?= $hospedajes[$i]['id']  ?>" data-precio="<?= $hospedajes[$i]['precio']  ?>"> 
							            			<?= $hospedajes[$i]['titulo']   ?> Costo: <?= $divSimbolo .$hospedajes[$i]['precio'] ?> 
							            		</label>
						     				</div><br>
							            <?php endfor ?>
							        </div>
							       
						     	</div>

							</div>
						</div>
			    		<div class="uk-card uk-card-default uk-card-body color-gris-7">
							<h2 class=" text-xl negritas uk-text-center">Desgolse de cotización</h2>
			    			<div uk-grid>

			    				<div class="uk-width-1-1 text-lg uk-margin-small-top" id="sub-divisa">
			    					Sub en divisa  <?=$divSimbolo ?> <span id="sub-divisa-number"></span>
			    				</div>
			    				<div class="uk-width-1-1 text-lg uk-margin-small-top" id="sub-mx">
			    					Subtotal en MX $<span id="sub-mx-number"></span>
			    				</div>
			    				<!--div class="uk-width-1-1 text-lg uk-margin-small-top">
									Cuota de Servicio y Transferencia $<?= $cuotaServio ?>
								</div-->

								<div class="uk-width-1-1 text-xl uk-margin-small-top negritas" >
									 Total (contado) $<span id="precio"></span> MX.

								</div>
								<div class="uk-width-1-3">
										<button class="uk-button uk-button-default uk-width-1-3 buybuttoncotizar" data-id="<?=$programa['id']?>">INSCRIBIRSE</button>
			       				</div>
			    			</div>
			    		</div>
					

					</div>
			    </li>
			<!-- TABLA DETALLE COTIZADO -->
			    <!--li>
					<div class="uk-card uk-card-default uk-card-body">
			    		Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</li>
			    	</div>
					
				</li-->

			</ul>		
		
	</div>
</section>




<?=$footer?>
<?=$scriptGNRL?>
<script type="text/javascript">
	$( document ).ready(function() {
		var divisa = '<?= $divisa ?>';
		//var costoPrograma = parseFloat($("#costo").data("costo"));
		var cambio = '<?= $cambio ?>';
		cambio = parseFloat(cambio);
		var obligatorios = <?= $obligatorios ?>;
		var obligatoriosKeys = <?= json_encode($obligatoriosKeys)?>;
		var rangosKeys = [];
		var hospedaje="";

		console.log("obligatorio",obligatorios);
		console.log("array ids",obligatoriosKeys);
		//couta = parseFloat(cuota);
		var inscripcion = parseFloat($("#inscripcion").data("inscri"));
		var lastHosp = 0;
		var subMx = 0;
		var total = 0;

		var subDivisa = 0;
		subDivisa = subDivisa + obligatorios;
		subMx = subDivisa * cambio;
		
		total = subMx;
		

		var cotizacion = {
			'divisa': divisa,
			'idHospedaje':'',
			'inscripcion': inscripcion
		}
		
		$(".circle").click(function(){
			hospedaje = $(this).data("id");
			var precioH =  parseFloat($(this).data("precio"));
			
			cotizacion.idHospedaje = hospedaje;
			subDivisa = subDivisa + precioH - lastHosp;
			lastHosp = precioH;
			subMx = subDivisa * cambio;
			total = subMx;

			$("#sub-divisa-number").text(subDivisa.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$("#sub-mx-number").text(subMx.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$("#precio").text(total.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

		});

		$(".precios").on( 'change', function(){
			var idPrecio = $(this).data("id").toString();

		    if( $(this).is(':checked') ) {
		    	obligatoriosKeys.push(idPrecio);
		        subDivisa = subDivisa + parseFloat($(this).val());
		        $("#rangeprecio"+idPrecio).addClass("uk-disabled");
		        var multiplicador=$("#rangeprecio"+idPrecio).val();

		        rangosKeys.push(idPrecio+'_'+multiplicador);
		        //console.log("checado",rangosKeys);
		      
		    } else {
		    	obligatoriosKeys.splice(obligatoriosKeys.indexOf(idPrecio),1);
		        subDivisa = subDivisa - parseFloat($(this).val());
		        $("#rangeprecio"+idPrecio).removeClass("uk-disabled");
		        rangosKeys.splice(obligatoriosKeys.indexOf(idPrecio+'_'+multiplicador),1);
		        //console.log("deschecado",rangosKeys);

		    }

		    subMx = subDivisa * cambio;
			total = subMx;

		    $("#sub-divisa-number").text(subDivisa.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
		    $("#sub-mx-number").text(subMx.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
			$("#precio").text(total.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

			

		});

		$("#sub-divisa-number").text(subDivisa.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
		$("#sub-mx-number").text(subMx.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));
		$("#precio").text(total.toFixed(0).replace(/\d(?=(\d{3})+\.)/g, '$&,'));

		
		$(".meses-picker").on('change', function(){
			meses=$(this).val();
			$("#meses").text(meses);
		});

		$(".periodo-picker").on('change', function(){
			periodo=$(this).val();
			var pID = $(this).data("id");
			var precio = $(this).data("precio");
			precio = precio * periodo;
			console.log("Precio = ",precio);
			$("#periodo"+pID).text(periodo);
			$("#precio"+pID).text(precio);

			var checkprecio = $("#checkprecio"+pID);

			checkprecio.val(precio);

		});

		// Agregar al carro
		$(".buybuttoncotizar").click(function(){
			var id=$(this).data("id");
			var cantidad=1;
			console.log("Los ids de los precios seleccionados",obligatoriosKeys);
			if(hospedaje){
				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						preciosIds:obligatoriosKeys,
						rangosIds : rangosKeys,
						hospedaje: hospedaje,
							inscripcion:1,
							addtocart: 1
						}
				})
				.done(function( msg ) {
					//console.log("response",msg);
					datos = JSON.parse(msg);
					console.log("datos",datos.msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			}else{
				UIkit.notification("<div class='uk-text-center  bg-danger padding-10 text-11'><i class='fa fa-times'></i> &nbsp; Selecciona un hospedaje</div>");
			}
			
		});

	});


</script>

</body>
</html>


			