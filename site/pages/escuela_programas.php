<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php  

//PAGINADOR
	$pag=(isset($_GET['pag']))?$_GET['pag']:0;
	
	$prodsPagina =8;
	
	$prodInicial= $prodsPagina*$pag;
	
	
	$rutaEscuela =  "./img/contenido/escuelas/";
	
	$consulta = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $id");
	$escuela = $consulta -> fetch_assoc();
	$logoEscuela = $escuela['logoescuela'];
	$imgEscuela = $escuela['imagen'];
	$escuelaId = $escuela['id'];

	$sql = "SELECT p.*,cat.titulo as categoria_titulo FROM productos AS p
					JOIN productoscat AS cat ON p.categoria = cat.id
					WHERE p.escuelaid = $escuelaId";

	$consultaProgramas = $CONEXION -> query($sql);
	$cantProgramas = $consultaProgramas -> num_rows;
	$sql .= " LIMIT $prodInicial, $prodsPag";
	
	$consultaProgramas = $CONEXION -> query($sql);


?>
	<section class="uk-container uk-container-expand ">
		<section class="uk-section uk-section-muted border-cero padding-top-50">
			<div uk-slider>
			    <div class="uk-position-relative">
			        <div class="uk-slider-container uk-light">
			            <ul class="uk-slider-items uk-child-width-1-1">
			            	<?php
			            	for ($i=0; $i <3 ; $i++):
			            	?>
			                <li>
			                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(./img/design/detalle.jpg);height:450px;margin-top: 50px; "> </div>
			                </li>
			                <?php
			            	endfor
			            	?>
			            </ul>
			        </div>
			    </div>
				
				<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>
			<div class="uk-width-1-2@s uk-width-1-4@m bg_white container-programa margin-top-80" style="position: absolute; top:0">
			        <div class="uk-grid-collapse padding-h-30 uk-flex uk-flex-center">
			        	<img src="<?= $rutaEscuela.$logoEscuela;  ?>">
			        </div>
				</div>
		</section>
	
		<section class="uk-section uk-section-muted ">
			<div class="uk-flex uk-flex-center uk-align-center">
				<img src="./img/design/icon1.png" style="width:5%">
			</div>
			<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
				<?= $escuela['titulo']  ?>
			</p>
			<p class="blue uk-text-center signika">
				Programas disponibles : <?= $cantProgramas  ?>
			</p>
		</section>
		<!-- SECCION DE PROGRAMAS POR ESCUELA -->
		<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white">
			<div class="uk-padding uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center border-cero left-0" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 500">
				<?php

				while($programasRow = $consultaProgramas -> fetch_assoc()):
					$catId = $programasRow['categoria'];
					
				?>
	                <div class="uk-grid-collapse border-cero padding-cards-container">
	                	<div class="uk-card uk-card-default box-shadown margin-cards">
				            <div class="uk-card-media-top  uk-grid-collapse">
				            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-200" style="background-image: url('<?= "./img/contenido/escuelas/".$imgEscuela?>');"> </div>
				            </div>
				            <div class="uk-card-body bg-gris-ligth border-cero padding-30">
				            	<div class="uk-grid-collapse bg-gris-ligth margin-top-menos-80">
					            	<div class="uk-background-contain uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-120" style="background-image: url('<?= "./img/contenido/escuelas/".$logoEscuela?>');"> </div>
					            </div>
				                <p class="uk-grid-collapse uk-text-uppercase border-cero margin-top-40"><?= $programasRow['categoria_titulo'];  ?></p>
				                <h5 class="uk-card-title uk-grid-collapse uk-text-uppercase border-cero margin-top-10 text-9"><?= $programasRow['titulo'];  ?> </h5>
				            </div>
				            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
				    				<a href="<?=$programasRow["id"] ?>_programa" class="uk-grid-collapse btn-more-container">
										<div class="btn-gral text-7 uk-text-uppercase box-shadown">
											VER MAS
										</div>
										<div class="btn-gral-border">&nbsp;</div>
									</a>
				    		</div>
				        </div>
	                </div>
	            <?php
				endwhile
				?>
				<!-- PAGINATION -->
				<div class="bg-bottom uk-width-1-1" >
				  <div class="padding-v-50">
				    <ul class="uk-pagination uk-flex-center uk-text-center">
				      <?php
				      $txt=urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($catName))));
				      if ($pag!=0) {
				        $link=$escuelaId.'_'.($pag-1).'_programas_escuelas-.php';
				        echo'
				        <li><a href="'.$link.'" class="pagination-arrows"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
				      }
				      $pagTotal=intval($cantProgramas/$prodsPag);
				      $modulo=$cantProgramas % $prodsPag;
				      if (($modulo) == 0){
				        $pagTotal=($cantProgramas/$prodsPag)-1;
				      }
				      for ($i=0; $i <= $pagTotal; $i++) { 
				        $clase='';
				        if ($pag==$i) {
				          $clase='uk-active';
				        }
				        $link=$escuelaId.'_'.$i.'_programas_escuelas-.php';
				        echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
				      }
				      if ($pag!=$pagTotal AND $cantProgramas!=0) {
				        $link=$escuelaId.'_'.($pag+1).'_programas_escuelas-.php';
				        echo'
				        <li><a href="'.$link.'" class="pagination-arrows">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>
				        ';
				      }
				      ?>
				    </ul>
				  </div>
				</div><!-- PAGINATION -->

			</div>
		</section>
	
	</section>

<?=$footer?>
<?=$scriptGNRL?>
<script>
	$(document).ready(function () {   
		$("#cambiacat").on("change", function() {
			var id = this.value;
			console.log("ID",id);
			var url = './'+id+'_catalogo_programas.php';
			window.location.href = url;
		});
	});


</script>
</body>
</html>


			