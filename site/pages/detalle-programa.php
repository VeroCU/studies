<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php  
	$rutaCat =  "./img/contenido/productoscat/";
	$catId = $rowCategoria['id'];
	$consultaCat = $CONEXION -> query("SELECT * FROM productoscat ORDER BY orden");
?>
	<section class="uk-container uk-container-expand" >
		<section class="uk-section uk-section-muted border-cero padding-top-50" style="min-height:550px">
			<div uk-slider>
			    <div class="uk-position-relative">
			        <div class="uk-slider-container uk-light">
			            <ul class="uk-slider-items uk-child-width-1-1">
			            	<?php
			            		$consultaCarousel = $CONEXION -> query("SELECT * FROM carousel_categorias WHERE categoria = $catId ORDER BY orden");
			            		while ($rowImgs = $consultaCarousel -> fetch_assoc()):
			            			$imgRuta = "./img/contenido/productoscat/carousel/";
			            			$picSlider = $imgRuta . $rowImgs['id'] ."-orig.jpg";
			            			
			            	?>
			                <li>
			                    <div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url(<?= $picSlider  ?>);height:550px;margin-top: 50px;margin-bottom: 70px "> </div>
			                </li>
			                <?php endwhile ?>
			            </ul>
			        </div>
			        <!--<div class="uk-hidden@s uk-light">
			            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			        <div class="uk-visible@s">
			            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>-->
			    </div>
				
				<ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>
			<div class="uk-width-1-2@s uk-width-1-4@m bg_white container-programa margin-top-80" style="position: absolute; top: 0">
			        <div class="uk-grid-collapse padding-h-30 uk-flex uk-flex-center">
			        	<img src="<?= $rutaCat.$rowCategoria['imagen'];  ?>">
			        </div>
					<h3 class="uk-text-uppercase signika text-xxl color-verde2 border-cero padding-h-20 uk-text-center" style=""> <?= $rowCategoria['titulo'];  ?> </h3>
					<div class="padding-h-20 uk-text-break" style="font-size:12px;text-align:justify;">
						<?= $rowCategoria['txt'];  ?>
					</div>
				</div>
		</section>
		<!-- SECCION DE PROGRAMAS POR CATEGORIA -->
		<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white">
			<div class="uk-padding uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center border-cero left-0" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 500">
				<?php
				$consultaProgramas = $CONEXION -> query("SELECT * FROM productos WHERE categoria = $catId AND estatus = 1 ORDER BY orden");

				while($programasRow = $consultaProgramas -> fetch_assoc()):
					$escuelaId=$programasRow['escuelaid'];
					$consultaEscuela = $CONEXION -> query("SELECT id,logoescuela,imagen FROM escuelas WHERE id = $escuelaId");
					$cantEscuela =$consultaEscuela->num_rows; 
					
					if($cantEscuela>0){
						$escuelaRow = $consultaEscuela -> fetch_assoc();
						$consultaSliderEscuela = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaRow[id] LIMIT 1");
						$rowSlider = $consultaSliderEscuela -> fetch_assoc();
						$imgEscuela = $escuelaRow['imagen'];
					}

				?>
	                <div class="uk-grid-collapse border-cero padding-cards-container">
	                	<div class="uk-card uk-card-default box-shadown margin-cards">
				            <div class="uk-card-media-top  uk-grid-collapse">
				            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-200" style="background-image: url('<?= "./img/contenido/escuelas/".$imgEscuela?>');"> </div>
				            </div>
				            <div class="uk-card-body bg-gris-ligth border-cero padding-30">
				            	<div class="uk-grid-collapse bg-gris-ligth margin-top-menos-80">
					            	<div class="uk-background-contain uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-120" style="background-image: url('<?= "./img/contenido/escuelas/".$escuelaRow["logoescuela"]?>');"> </div>
					            </div>
				                <p class="uk-grid-collapse uk-text-uppercase border-cero margin-top-40"><?= $rowCategoria['titulo'];  ?></p>
				                <h5 class="uk-card-title uk-grid-collapse uk-text-uppercase border-cero margin-top-10 text-9"><?= $programasRow['titulo'];  ?> </h5>
				            </div>
				            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
				    				<a href="<?=$programasRow["id"] ?>_programa" class="uk-grid-collapse btn-more-container">
										<div class="btn-gral text-7 uk-text-uppercase box-shadown">
											VER MAS
										</div>
										<div class="btn-gral-border">&nbsp;</div>
									</a>
				    		</div>
				        </div>
	                </div>
	            <?php
				endwhile
				?>
			</div>
		</section>

		<section class="uk-section uk-section-muted ">
			<div class="uk-flex uk-flex-center uk-align-center">
				<img src="./img/design/icon1.png" style="width:5%">
			</div>
			<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
				Programas en el extranjero
			</p>
			<div class="uk-child-width-1-2@m left-0" uk-grid>
				<div class="uk-grid-collapse" style="">
					<div class="uk-text-center bg-thirdly" uk-grid>
						<div class="uk-width-auto uk-grid-collapse border-cero" style="margin-right: 10px;">
							<div class="uk-text-uppercase padding-10 bg-green" style="">
							    <span class="padding-h-50" style="color:#fff">IDIOMAS</span>
							</div>
						</div>
						<div class="uk-width-expand uk-grid-collapse uk-align-center" id="idioma">
							<select class="uk-select uk-grid-collapse" align='right' id="cambiacat">
						        <?php while ($rowCats = $consultaCat ->fetch_assoc()): 
								
								if (isset($catId) AND $catId==$rowCats['id']) {
									$estatus='selected';
								}else{
									$estatus='';
								}
								?>
						        <option class="uk-text-right uk-text-uppercase" align='right' value="<?= $rowCats['id']?>" <?= $estatus ?>><a href="busqueda"><?= $rowCats['titulo']  ?></a></option>
						    <?php endwhile  ?>
						    </select>
						</div>
					</div>
				</div>
				<div></div>
			</div>
		</section>
	</section>

<?=$footer?>
<?=$scriptGNRL?>
<script>
	$(document).ready(function () {   
		$("#cambiacat").on("change", function() {
			var id = this.value;
			console.log("ID",id);
			var url = './'+id+'_catalogo_programas.php';
			window.location.href = url;
		});
	});


</script>
</body>
</html>


			