<?php
	$rutaPic ='img/contenido/varios/';
	
	// Sección de imágenes 
	$consulta = $CONEXION -> query("SELECT * FROM inicio WHERE id = 1");
	$rowConsulta = $consulta -> fetch_assoc();
	foreach ($rowConsulta as $key => $value) {
		${$key}=$value;
	}
	$bgInicio=$rutaPic.'inicio-default.jpg';
	$pic=$rutaPic.$imagen5;
	if(file_exists($pic) AND strlen($imagen5)>0){
		$bgInicio=$pic;
	}
	$bgBecas=$rutaPic.'comparte.png';
	$pic=$rutaPic.$imagen6;
	if(file_exists($pic) AND strlen($imagen6)>0){
		$bgBecas=$pic;
	}

	$tituloIzq = $rowConsulta['titulocomparte'];
	$txtIzq = $rowConsulta['textocomparte'];

	$tituloDer = $rowConsulta['titulocomparte2'];
	$txtDer = $rowConsulta['textocomparte2'];

	if(isset($uid)){
	    $consultaCodigo = $CONEXION -> query("SELECT codigo FROM usuarios WHERE id = $uid");
	    $row = $consultaCodigo -> fetch_assoc();
	    $codigo = $row['codigo'];
	}
	if(isset($codeid)){
		$_SESSION['code'] = $codeid;
	}
?>

<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>

<?= modalCodigo($ruta, $codigo,$tituloIzq,$txtIzq,$tituloDer,$txtDer) ?>

<section class="uk-container uk-container-expand">
	<section class="uk-section uk-section-muted bg-secondary border-cero padding-top-50"  style="padding-top:8%;">
		<div class="left-0 text-align-center left-0" uk-grid style="position: relative;margin-bottom:0;padding-bottom:0;">
		    <div class="uk-width-1-3@m  uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle border-cero" style="background-image: url('<?=$bgBecas?>');">
		       
		    </div>
		    <div class="uk-width-expand@m padding-right-60 padding-top:8%;">
		        <h2 class="signika text-xxl" style="color:#fff"><?=$titulo1?></h2>
		        <div class="uk-text-left text-8" style="color:#fff;">
				    <?=nl2br($texto2)?>
				    <br><br>
				</div>
		    </div>
	    </div>
	</section>
</section>

<section class="uk-section uk-section-muted uk-grid-collapse bg_white"> 
	<div class="uk-container uk-container-large">
		<div class="uk-child-width-expand@s uk-text-center left-0" uk-grid>
		    <div class="margin-moneditas">
		        <div class="padding-40" style="background:#f6f5f5">
		        	<img src="./img/design/moneda.png">
		        	<h2 class="signika text-xl border-cero" style="color:#000"><?=$titulo3?></h3>

		        	<div class="uk-text-left text-8 color-negro padding-top-10" style="height:180px;">

					    <?=nl2br($texto3)?>
					</div>
					<a class=" padding-top-10" href="#modal-codigo" uk-toggle>
						<div class="uk-align-center uk-text uk-text-uppercase login-parner-button">
							Login Parner
						</div>
					</a>
		        </div>
		    </div>
		    <div class="margin-moneditas">
		        <div class="padding-40" style="background:#f6f5f5">
		        	<img src="./img/design/moneda.png">
		        	<h3 class="signika text-xl border-cero" style="color:#000"><?=$titulo4?></h3>
		        	<div class="uk-text-left text-8 color-negro padding-top-10" style="height:180px;">
					    <?=nl2br($texto4)?>
					</div>
					<a class="padding-top-10" href="registro_voluntarios">
						<div class="uk-align-center uk-text uk-text-uppercase login-parner-button uk-text-center">
							Login Parner
						</div>
					</a>
		        </div>
		    </div>
		</div>
	</div>
</section>



<section class="uk-section uk-section-muted uk-grid-collapse bg_white">
	<div class="uk-container uk-container-small">
		<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:700;">
			trabajamos con los mejores</p>
		<div uk-slider class="uk-grid-collapse">
		    <div class="uk-position-relative">
		        <div class="uk-slider-container uk-light">
            
               	 	<ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m">
		            	<?php
		            		$consultaSocios = $CONEXION -> query("SELECT * FROM empresas where estatus = 1 ORDER BY orden");
		            		while ($sociosRow = $consultaSocios -> fetch_assoc()):
		            	?>
		                <li>
		                    <img class="uk-align-center" src="./img/contenido/empresas/<?= $sociosRow['id'] ?>.png" alt="" style="max-height: 120px; max-width:300px; ">
		                </li>
		               <?php endwhile ?>
		           	</ul>
		          
		        </div>

		        <div class="uk-hidden@s uk-light">
		            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		        </div>

		        <div class="uk-visible@s">
		            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
		            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
		        </div>
		    </div>
		    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
		</div>
	</div>
</section>



<?=$footer?>
<?=$scriptGNRL?>
<script type="text/javascript">
	$( document ).ready(function() {
    console.log( "ready!" );
		var id = "<?= $codeid ?>";
		if(id.length > 6)
			UIkit.modal("#modal-codigo").show();
	
	});
	
</script>

</body>
</html>


			