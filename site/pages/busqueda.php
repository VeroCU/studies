<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?php

	$sql="";
	$sql2="";
	$bandera=true;
	$result=array();
	
	if(isset($_SESSION['pais']) && $_SESSION['pais'] != null){
		$pais = $_SESSION['pais'];
		
		$sql ="SELECT * FROM escuelas WHERE pais = $pais";
		//si existe la provincia
		if(isset($_SESSION['provincia']) && $_SESSION['provincia'] != null){
			$provincia = $_SESSION['provincia'];
			
			$sql .= " AND provincia = $provincia";
		}

		if(isset($_SESSION['escuela']) && $_SESSION['escuela'] != null){

			$sql .= " AND id = " . $_SESSION['escuela'];;
		}

		$sql .= " ORDER BY orden ";

		//debug($sql);
		$consultaEsc = $CONEXION -> query($sql);

		while ($escuelasRow = $consultaEsc -> fetch_assoc()){
			//debug($escuelasRow);
			$escuelaId = $escuelasRow['id']; 
			$imgEscuela = $escuelasRow['imagen'];
			$logo = $escuelasRow['logoescuela'];
			//Buscamos los cursos de esa escuela
			$sql2 = "SELECT * FROM productos WHERE escuelaid = $escuelaId";	

			if(isset($_SESSION['categoriaid']) && $_SESSION['categoriaid'] != ""){
				$catId = $_SESSION['categoriaid'];
				$sql2 .= " AND categoria = $catId";
			
			}

			if(isset($_SESSION['palabras'])){
				$palabras = $_SESSION['palabras'];
				$sql2 .= " AND MATCH(palabras) AGAINST ('$palabras')";
				
			}
			
			if(isset($_SESSION['precio'])){
				$orden = $_SESSION['precio'];
				$sql2 .= " ORDER by precio " .$orden;
			}

			//debug($sql2);
			$consultaProds = $CONEXION -> query($sql2);
			while($productosRow = $consultaProds -> fetch_assoc()){
				$consultaCat = $CONEXION -> query("SELECT titulo FROM productoscat where id = $productosRow[categoria]");
				$rowCat = $consultaCat -> fetch_assoc();
				$productosRow['tituloCat'] = $rowCat['titulo'];

				$productosRow['logoescuela'] = $logo;
				$productosRow['imagenescuela'] = $imgEscuela;
				array_push($result, $productosRow);
			
			}
		}
	}
	//Caso de busqueda con categoria sin pais
	else if(isset($_SESSION['categoriaid']) && !isset($_SESSION['pais'])){
		$catId = $_SESSION['categoriaid'];
		
		$sql = "SELECT * FROM productos WHERE categoria = $catId";
		
		if(isset($_SESSION['escuela']) && $_SESSION['escuela'] != null){

			$sql .= " AND escuelaid = " . $_SESSION['escuela'];;
		}

		if(isset($_SESSION['palabras'])){
			$palabras = $_SESSION['palabras'];
			$sql .= " AND MATCH(palabras) AGAINST ('$palabras')";
			
		}

		if(isset($_SESSION['precio'])){
			$orden = $_SESSION['precio'];
			$sql .= " ORDER by precio " .$orden;
		}
		
		$consultaProds = $CONEXION -> query($sql);
		//debug($sql);

		while($productosRow = $consultaProds -> fetch_assoc()){
			$consultaCat = $CONEXION -> query("SELECT titulo FROM productoscat where id = $productosRow[categoria]");
			$rowCat = $consultaCat -> fetch_assoc();
			$productosRow['tituloCat'] = $rowCat['titulo'];
			$escuelaId = $productosRow['escuelaid'];
			$consultaEsc = $CONEXION -> query("SELECT imagen,logoescuela FROM escuelas WHERE id = $escuelaId");
			$escuelaRow = $consultaEsc -> fetch_assoc();
			$productosRow['logoescuela'] = $escuelaRow['logoescuela'];
			$productosRow['imagenescuela'] = $escuelaRow['imagen'];
			array_push($result, $productosRow);
			
		}
	}
	// cuando la busqueda es lo por palabras
	else if ($_SESSION['palabras'] and !isset($_SESSION['pais']) AND !isset($_SESSION['categoriaid'])) {
		$sql = "SELECT * FROM productos WHERE ";
		$palabras = $_SESSION['palabras'];

		$sql .= " MATCH(palabras) AGAINST ('$palabras')";

		if(isset($_SESSION['escuela']) && $_SESSION['escuela'] != null){

			$sql .= " AND escuelaid = " . $_SESSION['escuela'];;
		}
		//debug($sql);

		$consultaProds = $CONEXION -> query($sql);
		while($productosRow = $consultaProds -> fetch_assoc()){

			$consultaCat = $CONEXION -> query("SELECT titulo FROM productoscat where id = $productosRow[categoria]");
			$rowCat = $consultaCat -> fetch_assoc();
			$escuelaId = $productosRow['escuelaid'];

			$consultaEsc = $CONEXION -> query("SELECT imagen,logoescuela FROM escuelas WHERE id = $escuelaId");

			$escuelaRow = $consultaEsc -> fetch_assoc();
			$productosRow['tituloCat'] = $rowCat['titulo'];

			$productosRow['logoescuela'] = $escuelaRow['logoescuela'];
			$productosRow['imagenescuela'] = $escuelaRow['imagen'];
			array_push($result, $productosRow);
			
		}
	}
	// cuando la busqueda es por SOLO Escuela
	else if ($_SESSION['escuela'] and !isset($_SESSION['pais']) AND !isset($_SESSION['categoriaid'])) {
		$sql = "SELECT * FROM productos WHERE ";
		$sql .= " escuelaid = " . $_SESSION['escuela'];
		if(isset($_SESSION['palabras'])){
			$palabras = $_SESSION['palabras'];
			$sql2 .= " AND MATCH(palabras) AGAINST ('$palabras')";
			
		}
		//debug($sql);

		$consultaProds = $CONEXION -> query($sql);
		while($productosRow = $consultaProds -> fetch_assoc()){

			$consultaCat = $CONEXION -> query("SELECT titulo FROM productoscat where id = $productosRow[categoria]");
			$rowCat = $consultaCat -> fetch_assoc();
			$escuelaId = $productosRow['escuelaid'];

			$consultaEsc = $CONEXION -> query("SELECT imagen,logoescuela FROM escuelas WHERE id = $escuelaId");

			$escuelaRow = $consultaEsc -> fetch_assoc();
			$productosRow['tituloCat'] = $rowCat['titulo'];

			$productosRow['logoescuela'] = $escuelaRow['logoescuela'];
			$productosRow['imagenescuela'] = $escuelaRow['imagen'];
			array_push($result, $productosRow);
			
		}
	}
	//BORRAMOS TODAS LAS VARIABLES DE SESSION
	unset($_SESSION['palabras']);
	unset($_SESSION['pais']);
	unset($_SESSION['categoriaid']);
	unset($_SESSION['provincia']);
	unset($_SESSION['precio']);
	unset($_SESSION['escuela']);

	if(sizeof($result) == 0){
		$bandera = false;
	}

 ?>
	<section class="uk-container uk-container-expand">

		<section class="uk-section uk-section-muted border-cero padding-top-50 bg_white" style="">
			<div class="uk-flex uk-flex-center uk-align-center padding-top-50">
				<img src="./img/design/icon1.png" class="width-5">
			</div>
			<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
					Programas en el extranjero
			</p>
		</section>

		<section class="uk-section uk-section-muted zero" style="background:#ee313d">
			<p class="uk-align-center padding-10" style="color:#fff;font-size:46px;text-align:center;">
				¡Esto es lo que encontramos para ti!</p>
		</section>
		<?php 
		if($bandera): ?>
			<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white margin-bottom-20">
				<div class="uk-text-warning">Se encontraron las siguientes programas</div>
				<div class="uk-padding uk-grid-column-small uk-grid-row-large uk-child-width-1-3@s uk-child-width-1-4@m uk-text-center border-cero left-0" uk-grid style="min-height:200px;">
					
					<?php  
					for($i=0; $i<sizeof($result); $i++):
					?>
		                <div class="uk-grid-collapse border-cero padding-cards-container"  style="margin-top:80px">
		                	<div class="uk-card uk-card-default box-shadown margin-cards">
					            <div class="uk-card-media-top  uk-grid-collapse">
					            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-200" style="background-image: url('./img/contenido/escuelas/<?= $result[$i]['imagenescuela'] ?>');"> </div>
					            </div>
					            <div class="uk-card-body bg-gris-ligth border-cero padding-30">
					            	<div class="uk-grid-collapse bg-gris-ligth margin-top-menos-80">
						            	<div class="uk-background-contain uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse height-120" style="background-image: url('./img/contenido/escuelas/<?= $result[$i]['logoescuela'] ?>');"> </div>
						            </div>
					                <p class="uk-grid-collapse uk-text-uppercase border-cero margin-top-20"><?= $result[$i]['tituloCat']  ?></p>
					                <h3 class="uk-card-title uk-grid-collapse uk-text-uppercase border-cero margin-top-10"><?= $result[$i]['titulo'] ?></h3>
					            </div>
					            	<div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
					    				<a href="<?=$result[$i][id] ?>_programa" class="uk-grid-collapse btn-more-container">
											<div class="btn-gral text-7 uk-text-uppercase box-shadown">
												VER MAS
											</div>
											<div class="btn-gral-border">&nbsp;</div>
										</a>
					    			</div>
					        </div>
		                </div>
		            <?php
					endfor
					?>
				</div>
			</section>
		<?php else:  ?>
			<div class="uk-text-warning uk-margin text-xxl">No se encontraron coincidencias</div>
			<div class="uk-flex uk-flex-center uk-child-width-1-3 margin-v-50">
        		<a href="Inicio"  uk-toggle class="uk-grid-collapse btn-header-container">
					<div class="btn-red text-7 uk-text-uppercase box-shadown">
						Volver
					</div>
					<div class="btn-red-border">&nbsp;</div>
				</a>
			</div>
		<?php endif ?>
		<!--div class="uk-child-width-1-2@m left-0 padding-v-50 left-0 border-cero" uk-grid>
			<div>
				<div class="uk-text-center bg-thirdly" uk-grid>
				    <div class="uk-width-auto uk-grid-collapse border-cero" style="margin-right: 10px;">
				        <div class="uk-text-uppercase padding-10 bg-green" style="">
				        	<span class="padding-h-50" style="color:#fff">IDIOMAS</span>
				        </div>
				    </div>
				    <div class="uk-width-expand uk-grid-collapse uk-align-center" id="idioma">
				        <select class="uk-select uk-grid-collapse" align='right'>
			                <option class="uk-text-right" align='right'><a href="busqueda">Option 01</a></option>
			                <option class="uk-text-right" align='right'><a href="busqueda">Option 02</a></option>
			            </select>
				    </div>
				</div>
			</div>
			<div></div>
		</div-->

	</section>	

<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			