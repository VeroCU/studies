<?php
	$rutaPic ='img/contenido/varios/';

	// Sección de imágenes 
	$consulta = $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");

	$rowConsulta = $consulta -> fetch_assoc();
	$about1=$rowConsulta["about1"];
	$about2=$rowConsulta["about2"];
	$about3=$rowConsulta["about3"];
	$bgprincipal=$rutaPic.$rowConsulta["imagen2"];
	$bgnosotros=$rutaPic.$rowConsulta["imagen4"];
?>


<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<section class="uk-section uk-section-muted uk-grid-collapse padding-0 bg_white">
	<div class="bg-primary" style="margin-top:14%;">
		<div class="style-img banner-header">
			<div class="uk-container" >
				<div class="uk-text-center" uk-grid>
				    <div class="uk-width-1-3@m"></div>
				    <div class="uk-width-expand@m">
				    	
				        <div class="uk-padding">
				        	<div class="uk-padding" style="margin-top:-140px">
				        		<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url('<?=$bgprincipal?>');">
        						</div>
				        	</div>
				        	<div class="padding-v-0 uk-text-justify" style="color:#fff">
					        	<?=$about1?>
							</div>
				        </div>
				    </div>
				</div>  
			</div>
		</div>
	</div>
</section>


<section class="uk-section uk-section-muted uk-grid-collapse bg_white">
	<div class="uk-container uk-container-small">
		<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:700;">
			trabajamos con los mejores</p>
			<div uk-slider class="uk-grid-collapse">
				    <div class="uk-position-relative">
				        <div class="uk-slider-container uk-light">
				            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m">
				            	<?php
				            		$consultaSocios = $CONEXION -> query("SELECT * FROM empresas where estatus = 1 ORDER BY orden");
				            		while ($sociosRow = $consultaSocios -> fetch_assoc()):
				            	?>
				                <li>
				                    <img class="uk-align-center" src="./img/contenido/empresas/<?= $sociosRow['id'] ?>.png" alt="" style="max-height: 120px; max-width:300px; ">
				                </li>
				               <?php endwhile ?>
				            </ul>
				        </div>

				        <div class="uk-hidden@s uk-light">
				            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>

				        <div class="uk-visible@s">
				            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>
				    </div>
				    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
				</div>
			</div>
</section>

<section class="uk-container uk-container-expand">
	<section class="uk-section uk-section-muted padding-v-20">
		<div class="uk-child-width-1-2@m uk-grid-match left-0" uk-grid style="margin-left:0">
			<div class="uk-grid-collapse left-0 uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle" style="background-image: url('<?=$bgnosotros?>');height:auto;">
			</div> 
			<div class="bg-secondary"> 	
				<div class="" uk-grid>
				    <div class="uk-card-top uk-grid-collapse padding-40 uk-flex-middle">
				    	<h2 class="uk-card-title signika text-xxl" style="color:#fff">Misión</h2>
				        <div class="text-8" style="color:#fff">
				        	<?=$about2?>
				        </div>
				    </div>
				    <div class="uk-card-bottom uk-grid-collapse padding-40 uk-flex-middle" style="background:#ff535e">
				    	<h2 class="uk-card-title signika text-xxl" style="color:#fff">Visión</h2>
				        <div class="text-8" style="color:#fff">
				        	<?=$about3?>
				        </div>
				    </div>
				</div>
			</div>
		</div>
	</section>

	<!--section class="uk-section uk-section-muted margin-top-40 padding-v-20 bg_white">
		<div class="uk-container">
			<div class="uk-child-width-expand@s uk-text-center" uk-grid>
			    <div>
			        <div class="uk-card uk-card-default uk-card-body">Item</div>
			    </div>
			    <div>
			        <div class="uk-card uk-card-default uk-card-body">Item</div>
			    </div>
			    <div>
			        <div class="uk-card uk-card-default uk-card-body">Item</div>
			    </div>
			    <div>
			        <div class="uk-card uk-card-default uk-card-body">Item</div>
			    </div>
			    <div>
			        <div class="uk-card uk-card-default uk-card-body">Item</div>
			    </div>
			</div>
		</div>
	</section-->

	<section class="uk-section uk-section-muted padding-v-20 bg_white">
		<div class="uk-container">
			<div class="uk-child-width-expand@s uk-text-center" uk-grid>
			    <div>
			        <div class="padding-top-30 padding-bottom-100">
			        	<p class="text-8" style="text-align:justify;">
			        		En Studies4Life creemos que nuestros programas de educación internacional le otorgan al estudiante un impacto positivo a través de la inmersión cultural total y eso se reflejará en nuestra sociedad beneficiándonos a todos por medio de ciudadanos preparados para afrontar los retos de este mundo globalizado.
			        	</p>
			        </div>
			    </div>
			    <div>
			        <div class=""></div>
			    </div>
			</div>
		</div>
	</section>
</section>
<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			