<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
<?=$modalBuscar?>
<?php 
$rutaLogoCat ="./img/contenido/productoscat/";  
?>
	<section class="uk-container uk-container-expand">
					<section class="uk-section uk-section-muted border-cero padding-top-50 bg_white" style="">
						<div class="uk-flex uk-flex-center uk-align-center padding-top-50">
							<img src="./img/design/icon1.png" class="width-5">
						</div>
						<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
								Programas en el extranjero
						</p>
					</section>

					<section class="uk-section uk-section-muted zero bg_white" style="background:#ee313d">
						<p class="uk-flex uk-flex-center padding-10" style="color:#fff;font-size:46px;text-align:center;line-height: 46px;">
							Podemos ayudarte a encontrar <br> el programa ideal para ti</p>
					</section>

					<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white">
						<div class="uk-container uk-container-small padding-50">
							<p class="uk-text-center text-8 uk-text-center margin-h-70">
								Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.
							</p>
										
										<div class="uk-flex uk-flex-center">
											<div class="uk-width-1-6 uk-align-center" uk-grid>
							    				<a href="#modal-container"  uk-toggle class="uk-grid-collapse btn-header-container" class="uk-grid-collapse btn-more-container">
													<div class="btn-red uk-text-center text-7 uk-text-uppercase box-shadown">
														EMPEZAR
													</div>
													<div class="btn-red-border">&nbsp;</div>
												</a>
							    			</div>
							    		</div>
						</div>
						<div class="padding-50"></div>
						<div class="bg-thirdly" style="border:solid transparent;">
							<div class="uk-padding uk-grid-column-small uk-grid-row-large uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center border-cero left-0  bg_white" uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 500" uk-grid style="background:transparent;border:solid transparent;margin-top:-100px;margin-bottom:-100px">
		    					<?php  
		    					$catConsulta = $CONEXION -> query("SELECT * FROM productoscat ORDER BY orden");
		    					while ($categoriasRow = $catConsulta -> fetch_assoc()):
		    	
								?>
					                <div class="uk-grid-collapse border-cero padding-cards-inicio">
					                	<div class="uk-card uk-card-default box-shadown margin-cards" style="">
								            <div class="uk-card-media-top uk-grid-collapse">
								            	<div style="padding:0 20px;height:140px;padding-top:20px">
								            		<div class="uk-background-contain uk-height-medium"  style="background-image: url('<?= $rutaLogoCat.$categoriasRow['imagen'];  ?>');height:120px"> </div>
								            	</div>
								            	<p class="blue uk-align-center signika text-11 border-cero" style="text-align:center;font-weight:700;">
													<?= $categoriasRow['titulo'];  ?>
												</p>
												<div class="uk-flex uk-flex-center uk-flex-middle" style="">
													<hr class="uk-flex uk-flex-center" style="border-top: solid 4px #1559a9;
												    height: 1px;
												    margin:12px 0;
												    padding:0;
												    width:40px;">
												</div>
								            </div>
								            <div class="uk-card-body bg-gris-ligth border-cero padding-30" style="background:#e5e5e5;overflow: hidden;height:80px;">
								            	<div class="uk-text-center text-8 uk-text-justify">
									        		<?= $categoriasRow['txt'];  ?>
									        	</div>
								            </div>
								            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
								    				<a href="<?= $categoriasRow['id']?>_catalogo_programas.php" class="uk-grid-collapse btn-more-container">
														<div class="btn-gral text-7 uk-text-uppercase box-shadown">
															VER MAS
														</div>
														<div class="btn-gral-border">&nbsp;</div>
													</a>
								    		</div>
								        </div>
					                </div>
					            <?php
								 endwhile
								?>
							</div>
						</div>
						<div class="padding-50"></div>
					</section>

		<section class="uk-section uk-section-muted uk-grid-collapse bg_white">
			<div class="uk-container uk-container-small">
				<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:700;">
					Trabajamos con los mejores</p>
				<div uk-slider class="uk-grid-collapse">
				    <div class="uk-position-relative">
				        <div class="uk-slider-container uk-light">
				            <ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-3@m">
				            	<?php
				            		$consultaSocios = $CONEXION -> query("SELECT * FROM empresas where estatus = 1 ORDER BY orden");
				            		while ($sociosRow = $consultaSocios -> fetch_assoc()):
				            	?>
				                <li>
				                    <img class="uk-align-center" src="./img/contenido/empresas/<?= $sociosRow['id'] ?>.png" alt="" style="max-height: 120px; max-width:300px; ">
				                </li>
				               <?php endwhile ?>
				            </ul>
				        </div>

				        <div class="uk-hidden@s uk-light">
				            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>

				        <div class="uk-visible@s">
				            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
				            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
				        </div>
				    </div>
				    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
				</div>
			</div>
		</section>
	</section>	

<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			