<?php
	$ruta='./img/contenido/escuelas/';
	$rutaPais='./img/contenido/paises/';
?>
<!DOCTYPE html>
<?=$headGNRL?>
<body>
  
<?=$header?>
	<section class="uk-container uk-container-expand">
		<div class="uk-grid-collapse uk-child-width-expand@s uk-text-center uk-margin-large-top" uk-grid>
		    <div>
		        <div class="uk-padding bg-secondary border-cero text-8">&nbsp;</div>
		    </div>
		    <div>
		        <div class="uk-padding border-cero text-8" style="background:#fe5761">&nbsp;</div>
		    </div>
		</div>

		<section class="uk-section uk-section-muted border-cero bg_white" style="">
			<div class="uk-flex uk-flex-center uk-align-center padding-top-50">
				<img src="./img/design/icon2.png" class="width-5">
			</div>
			<p class="blue uk-align-center signika" style="font-size:46px;text-align:center;font-weight:600;">
				Nuestras Escuelas
			</p>

			<div uk-slider="sets: true">
			    <div class="uk-position-relative">
			        <div class="uk-slider-container uk-light">
            	    	<ul class="uk-slider-items uk-child-width-1-2@s uk-child-width-1-6@m">
							<?php  
							$consultaPaises = $CONEXION ->query("SELECT * FROM paises ORDER BY nombre");
							
							while ($paisesRow = $consultaPaises->fetch_assoc()):
								$idPais = $paisesRow['id'];
								$paisImg = $rutaPais . $paisesRow['imagen'];
								
							?>
							    
							    <li class="margin-10" style="padding-left:0;">
									<a href="<?=$idPais?>_escuela">
								        <div class=""  style="background-image: url('<?= $paisImg  ?>');min-height:50px;width:100%; background-size:contain!important;background-repeat:no-repeat;background-position:center;" > </div>
								        
								        <p class="blue uk-align-center signika text-11 border-cero padding-top-10" style="text-align:center;font-weight:100;"> 
								        	<?= $paisesRow['nombre']  ?>
										</p>
									</a>	
									<div class="uk-flex uk-flex-center uk-flex-middle" style="">
										<hr class="uk-flex uk-flex-center" style="border-top: solid 2px #1559a9;
												    height: 1px;
												    margin:0;
												    margin-bottom:30px;
												    padding:0;
												    width:80%;">
									</div>
							    </li>
							    
							<?php
							endwhile
							?>
    					</ul>
			        </div>
			        <div class="uk-hidden@s uk-light">
			            <a class="uk-position-center-left uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			        <div class="uk-visible@s">
			            <a class="uk-position-center-left-out uk-position-small" href="#" uk-slidenav-previous uk-slider-item="previous"></a>
			            <a class="uk-position-center-right-out uk-position-small" href="#" uk-slidenav-next uk-slider-item="next"></a>
			        </div>
			    </div>
			    <ul class="uk-slider-nav uk-dotnav uk-flex-center uk-margin"></ul>
			</div>
		</section>



		<section class="uk-section uk-section-muted uk-grid-collapse border-cero bg_white">
				<div class="uk-padding uk-grid-column-small uk-grid-row-large  uk-child-width-1-2@s uk-child-width-1-4@m uk-text-center border-cero left-0  bg_white" uk-grid uk-scrollspy="target: > div; cls: uk-animation-fade; delay: 500">
					<?php
					$sql = "SELECT * FROM escuelas";
					if(isset($id)){
						$sql .= " WHERE pais = $id";
					}
					$sql .= " ORDER BY orden";
					
					$consulta = $CONEXION -> query($sql);
					while ($rowConsulta = $consulta -> fetch_assoc()) {
						$idEscuela = $rowConsulta["id"];
						echo '
		                <div class="uk-grid-collapse border-cero padding-cards-inicio"  style="margin-top:80px">
		                	<div class="uk-card uk-card-default box-shadown margin-cards">
					            <div class="uk-card-media-top uk-grid-collapse">
					            	<div class="uk-card-media-top  uk-grid-collapse">
						            	<div class="uk-background-cover uk-height-medium uk-panel uk-flex uk-flex-center uk-flex-middle uk-grid-collapse" style="background-image: url('.$ruta.$rowConsulta["imagen"].');height:180px;"> </div>
						            </div>
					            	<div style="padding:0 40px;height:60px;padding-top:20px">
					            		<div class="uk-background-contain uk-height-medium"  style="background-image: url('.$ruta.$rowConsulta["logoescuela"].');height:60px"> </div>
					            	</div>
					            </div>
					            <div class="uk-card-body bg-gris-ligth border-cero padding-20" style="background:#1559a9">
					            	<div class="uk-text-center text-8 uk-text-justify"
					            	style="height:100px; color:#fff;
										word-wrap: break-word;
										overflow: hidden;
										text-overflow: ellipsis;">
						        		'.$rowConsulta["txt"].'
						        	</div>
					            </div>
					            <div class="" uk-grid  style="position:absolute;bottom:-20px;right:0">
					    				<a href="'.$idEscuela.'_detalle.php" class="uk-grid-collapse btn-more-container">
											<div class="btn-gral text-7 uk-text-uppercase box-shadown">
												VER MAS
											</div>
											<div class="btn-gral-border">&nbsp;</div>
										</a>
					    		</div>
					        </div>
		                </div>
		            ';
				    } ?>
				</div>
			
			<div class="padding-50"></div>
		</section>
	</section>	

<?=$footer?>
<?=$scriptGNRL?>

</body>
</html>


			