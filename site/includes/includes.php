<?php
/* %%%%%%%%%%%%%%%%%%%% MENSAJES               */
	if($mensaje!=''){
		$mensajes='
			<div class="uk-container">
				<div uk-grid>
					<div class="uk-width-1-1 margen-v-20">
						<div class="uk-alert-'.$mensajeClase.'" uk-alert>
							<a class="uk-alert-close" uk-close></a>
							'.$mensaje.'
						</div>					
					</div>
				</div>
			</div>';
	}

/* %%%%%%%%%%%%%%%%%%%% RUTAS AMIGABLES        */
		$rutaInicio			=	'Inicio';
		$rutaNosotros		=	'nosotros';
		$rutaCompartebeca	=	'compartebeca';
		$rutaPrograma   	=	'programa';
		$rutaEscuela   	    =	'escuela';
		$rutaPromociones    =	'promociones';
		$rutaPedido			=	'Revisar_orden';
		$rutamicuenta       =   'mi-cuenta';
		

/* %%%%%%%%%%%%%%%%%%%% MENU                   */
	$menu='
		<li class="'.$nav1.'"><a href="'.$rutaInicio.'">Inicio</a></li>
		';

	$menuMovil='
		<li><a class="'.$nav1.'" href="'.$rutaInicio.'">INICIO</a></li>
		<li><a class="'.$nav2.'" href="'.$rutaNosotros.'">NOSOTROS</a></li>
		<li><a class="'.$nav3.'" href="'.$rutaCompartebeca.'">COMPARTE BECA</a></li>
		<li><a class="'.$nav8.'" href="'.$rutaPrograma.'">PROGRAMAS</a></li>
		<li><a class="'.$nav10.'" href="'.$rutaPromociones.'">PROMOCIONES</a></li>

		';

/* %%%%%%%%%%%%%%%%%%%% HEADER                 */
	$header='
		<div class="uk-offcanvas-content uk-position-relative">
			<header>
						<section class="uk-width-1-1 zero bg_white" style="position: fixed; z-index:999;">
						    	<div class="uk-container uk-container-large uk-align-center uk-grid-collapse " style="margin-bottom:0;">
						    		<div class="uk-grid-collapse" uk-grid>
									    <div class="movil-logo uk-width-auto@m"> 
									    	<a href="'.$rutaInicio.'">
									    		<img src="./img/design/logo.png" class="width-70"> 
									    	</a>
									    </div>
									    <div class="uk-width-expand no-movil"></div>
									    <div class="uk-width-expand@s uk-width-1-2@m uk-align-right uk-grid-collaps next-to-logo" style="margin-left:0">
									    	<div class="uk-text-center uk-grid-collaps" uk-grid style="margin-left:0">
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu no-movil">
											        <p class="text-7 margin-top-25 blue">TELS.// '.$telefonoSeparado.'</p>
											    </div>
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu">
											        <img class="uk-grid-collaps pestana" src="./img/design/pestana.png" >
											        <div class="uk-text-center top_menu">
											        	<div class="uk-text-center text-9 uk-grid-collapse margin-v-8">
																'.$loginButton.'
														</div>
											        	<a href="#menu-movil" uk-toggle id="menu" class="border-cero">
											        		<div>
											        			<i class="fa fa-bars fa-1x blue" aria-hidden="true"></i>
											        		</div>
											        	</a>
											        </div>
											    </div>
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu">
													<a href="'.$socialWhats.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: whatsapp; ratio: 1" style="padding:0 4px"></span>
													</a>
													<a href="'.$socialFace.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: facebook; ratio: 1" style="padding:0 4px"></span>
													</a>
													<a href="'.$socialInst.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: instagram; ratio:1" style="padding:0 4px"></span>
													</a>
											    </div>
											</div>
									    </div>
									</div>
						    	</div>
						</section>
			</header>

			'.$mensajes.'

			<!-- Menú móviles-->
			<div id="menu-movil" uk-offcanvas="mode: push;overlay: true">

				<div class="uk-offcanvas-bar uk-flex uk-flex-column">
					<section class="uk-width-1-1 zero bg_white" style="position: fixed; z-index:999;">
						    	<div class="uk-container uk-container-large uk-align-center uk-grid-collapse " style="margin-bottom:0;">
						    		<div class="uk-grid-collapse" uk-grid>
									    <div class="movil-logo uk-width-auto@m" style="margin:0;padding:0;height:80px;"> 
									    	<a href="'.$rutaInicio.'" style="margin:0;padding:0;">
									    		<img src="./img/design/logo.png" class="width-70" style="margin:0;padding:0;margin-top:-20px;"> 
									    	</a>
									    </div>
									    <div class="uk-width-expand no-movil"></div>
									    <div class="uk-width-expand@s uk-width-1-2@m uk-align-right uk-grid-collaps next-to-logo" style="margin-left:0">
									    	<div class="uk-text-center uk-grid-collaps" uk-grid style="margin-left:0">
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu no-movil">
											        <p class="text-7 margin-top-25 blue">TELS.// + '.$telefonoSeparado.'</p>
											    </div>
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu">
											        <img class="uk-grid-collaps pestana" src="./img/design/pestana.png" >
											        <div class="uk-text-center top_menu">
											        	<div class="uk-text-center text-9 uk-grid-collapse margin-v-8">
																'.$loginButton.'
														</div>
											        </div>
											    </div>
											    <div class="uk-width-1-3@s uk-width-expand@m uk-grid-collapse next-menu">
													<a href="'.$socialWhats.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: whatsapp; ratio: 1" style="padding:0 4px;"></span>
													</a>
													<a href="'.$socialFace.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: facebook; ratio: 1" style="padding:0 4px;"></span>
													</a>
													<a href="'.$socialInst.'" class="border-cero"  target="_blank">
														<span class="margin-top-25 blue" uk-icon="icon: instagram; ratio:1" style="padding:0 4px;"></span>
													</a>
											    </div>
											</div>
									    </div>
									</div>
						    	</div>
						</section>
					<button class="uk-offcanvas-close" type="button" uk-close style="z-index:99999;"></button>
					<!--ul class="uk-nav uk-nav-primary uk-nav-parent-icon uk-nav-center uk-margin-auto-vertical" uk-nav>
						'.$menuMovil.'
					</ul-->
							
					<section class="uk-section uk-section-muted full-container bg-primary" id="header">
						<div class="style-header banner-header" style="height:100vh!important">
						    <div class="uk-container">
						    	<div class="uk-width-1-1 uk-align-right uk-child-width-1-3@m margin-top-90">
							        <ul class="zero">
							        	<li class=""> 
							        		<a  href="'.$rutaInicio.'"class="text-11">INICIO</a></li>
							        	<li class=""> 
							        		<a href="'.$rutaNosotros.'" class="text-11">NOSOTROS</a></li>
							        	<!--li class=""> 
							        		<a class="text-11">SERVICIOS</a></li-->
							        	<li class=""> 
							        		<a href="'.$rutaPromociones.'" class="text-11">PROMOCIONES</a></li>
							        	<li class=""> 
							        		<a href="'.$rutaPrograma.'" class="text-11">PROGRAMAS</a></li>
							        	<li class=""> 
							        		<a href="'.$rutaEscuela.'" class="text-11">ESCUELA</a></li>
							        </ul>
							    </div>
							    
							    <div class="uk-container margin-top-menos-20">
									<div class="uk-text-center left-0" uk-grid style="border:solid transparent;">
									    <div class="uk-width-1-3@m uk-grid-collapse">
									        <div class=""></div>
									    </div>
									    <div class="uk-width-expand@m" style="padding-left:0">
									    	<div class="uk-flex uk-flex-center uk-text-center uk-grid-collapse " uk-grid  style="margin-left:0;">
									        	<p class="zero white text-9 uk-text-uppercase border-text">
									        		<span class="border-text" uk-icon="icon:mail; ratio:2"></span>
										        	<br>
										        	¿NECESITAS NUESTRA AYUDA?
										        	<br>
										        	CONTACTANOS
										        </p>
									        </div>
									        <div class="uk-grid-collapse" uk-grid style="margin-left:0;">
									        	<div class="left-0 uk-width-1-3@m padInput">
											        <input class="inputs" type="text" id="contactonombre" placeholder="NOMBRE">
											    </div>
											    <div class="left-0 uk-width-1-3@m padInput">
											        <input class="inputs" type="text" id="contactoemail"" placeholder="CORREO">
											    </div>
											    <div class="left-0 uk-width-1-3@m padInput">
											        <input class="inputs" type="text" id="contactotelefono" placeholder="TELEFONO">
											    </div>
											    <div class="left-0 uk-width-1-1@m padInput">
											        <input class="inputs" type="text"  id="contactoasunto" placeholder="MENSAJE">
											    </div>
									        </div>
									        <div class="uk-flex uk-flex-center uk-text-center uk-align-center uk-grid-collapse" uk-grid>
									        	<a href="" class="uk-grid-collapse btn-header-container">
									        		<div  id="contactosend" class="btn-header text-7 uk-text-uppercase">
										        			Enviar
										        	</div>
										        	<div class="btn-header-border">&nbsp;</div>
									        	</a>
									        </div>
									        <div class="uk-align-center uk-text-center uk-grid-collapse" uk-grid>
									        	<p class="white text-7 font-light">
									        		<a href="'.$socialInst.'" class="border-cero"  target="_blank">
														<span class="color-blanco" uk-icon="icon:instagram; ratio:2" style="color:#fff!important"></span>
													</a>
													<a href="'.$socialFace.'" class="border-cero"  target="_blank">
														<span class="color-blanco" uk-icon="icon:facebook; ratio:2" style="color:#fff!important"></span>
													</a>
													<a href="'.$socialWhats.'" class="border-cero"  target="_blank">
														<span class="color-blanco" uk-icon="icon:whatsapp; ratio:2" style="color:#fff!important"></span>
													</a>
									        	</p>
									        </div>
									        <div class="uk-flex uk-flex-center uk-text-center uk-grid-collapse" uk-grid>
									        	<p class="uk-grid-collapse white text-7 font-light">
									        		STUDIES 4 LIFE 2019 TODOS LOS DERECHOS RESERVADOS
									        	</p>
									        </div>
									        <div class="uk-flex uk-flex-center uk-text-center uk-grid-collapse" uk-grid>
									        	<p class="uk-grid-collapse white text-5 font-light">
										        	DISEÑO POR WOZIAL MARKETING LOVERS
										        </p>
									        </div>
									    </div>
									</div>    
								</div>
							   
						    </div>
						</div>
					</section>
				</div>
			</div >'

			;

/* %%%%%%%%%%%%%%%%%%%% FOOTER                 */
	$stickerClass=($carroTotalProds==0 OR $identificador==500 OR $identificador==501 OR $identificador==502)?'uk-hidden':'';
	$footer = '
		<footer>
			<div class="bg-footer" style="z-index: 0;">
				<section class="uk-section uk-section-muted auto-container bg-thirdly" id="footer">
					<div class="uk-align-center style-img banner-header">
				    
						<div class="uk-container  uk-align-center uk-grid-collapse">
							<div class="uk-text-center" uk-grid style="border:solid transparent;">
							    <div class="uk-width-1-3@m uk-grid-collapse">
							        <div class="uk-align-left">
							        	<a href="'.$rutaInicio.'">
							        		<img style="margin-top:130%;" src="./img/design/logo.png">
							        	</a>
							        </div>
							    </div>
							    <div class="uk-width-expand@m margin-top-10">

									<div class="uk-child-width-1-3@m" uk-grid uk-scrollspy="cls: uk-animation-fade; target: .uk-card; delay: 500; repeat: true">
									    <div>
								            <h5 class="zero uk-text-left blue text-9">NAVEGACIÓN</h5>
						            		<hr>
											<ul class="uk-grid-collapse uk-text-left">
												<li><a href="'.$rutaInicio.'">INICIO</a></li>
												<li><a href="'.$rutaNosotros.'">NOSOTROS</a></li>
												<li><a href="'.$rutaPrograma.'">PROGRAMAS</a></li>
												<li><a href="'.$rutaPromociones.'">PROMOCIONES</a></li>
												<li><a href="'.$rutaEscuela.'">ESCUELAS</a></li>
											</ul>
											<hr>
									    </div>
									    <div>
									        <h5 class="zero uk-text-left blue text-9">AYUDA</h5>
									        <hr>
											<ul class="uk-grid-collapse uk-text-left">
												
												<li><a class="uk-text-uppercase" href="preguntas-frecuentes">PREGUNTAS FRECUENTES</a></li>
												<li><a class="uk-text-uppercase" href="1_politicas" class="uk-text-uppercase">'.$tyct1.'</a></li>
												<li><a class="uk-text-uppercase" href="4_politicas">'.$tyct4.'</a></li>
												<li><a class="uk-text-uppercase" href="3_politicas">'.$tyct3.'</a></li>
												<li>&nbsp;</li>
											</ul>
											<hr>
									    </div>
									    <div>
									        <h5 class="zero uk-text-left blue text-9">SOCIAL</h5>
									        <hr>
											<ul class="uk-grid-collapse uk-text-left">
												<li><a href="#">CONTACTO</a></li>
												<li><a href="'.$rutamicuenta .'">MI CUENTA</a></li>
												<li><a href="'.$socialInst.'">INSTAGRAM</a></li>
												<li><a href="'.$socialFace.'">FACEBOOK</a></li>
												<li><a href="'.$socialWhats.'">WATSAPP</a></li>
											</ul>
											<hr>
									    </div>
									</div>

							    	<div class="uk-flex uk-flex-center uk-text-center margin-top-25" uk-grid>
							        	<p class="zero blue text-9 uk-text-uppercase">
							        		¿NECESITAS NUESTRA AYUDA? CONTACTANOS
								        </p> 
							        </div>
							        <div class="uk-grid-collapse" uk-grid>
							        	<div class="left-0 uk-width-1-3@m padInput">
									        <input class="inputs bg-primary" type="text"  placeholder="NOMBRE:" id="contactonombre">
									    </div>
									    <div class="left-0 uk-width-1-3@m padInput">
									        <input class="inputs bg-primary" type="text" placeholder="CORREO:" id="contactoemail">
									    </div>
									    <div class="left-0 uk-width-1-3@m padInput">
									        <input class="inputs bg-primary" type="text" placeholder="TELEFONO:" id="contactotelefono">
									    </div>
									    <div class="left-0 uk-width-1-1@m padInput">
									        <input class="inputs bg-primary" type="text" placeholder="MENSAJE:" id="contactocomentarios">
									    </div>
							        </div>
							        <div class="uk-flex uk-flex-center uk-text-center uk-align-center uk-grid-collapse zero" uk-grid>
							        	<div  id="contactosend" class="uk-grid-collapse btn-header-container">
							        		<div class="btn-footer text-7 uk-text-uppercase">
								        			Enviar
								        	</div>
								        	<div class="btn-footer-border">&nbsp;</div>
							        	</div>
							        </div>
							        <div class="uk-flex uk-flex-center uk-text-center uk-grid-collapse" uk-grid>
							        	<p class=" blue ite text-7 font-light margin-top-25">
							        		STUDIES 4 LIFE 2019 TODOS LOS DERECHOS RESERVADOS
							        	</p>
							        </div>
							       
							    </div>
							</div>    
						</div>
					</div>
				</section>
				<div class="uk-container uk-position-relative">
					<div class="uk-width-1-1 uk-text-center">
						<div class="blue">
							'.date('Y').' todos los derechos reservados Diseño por <a href="https://wozial.com/" target="_blank" class="blue">Wozial Marketing Lovers</a>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<div id="cotizacion-fixed" class="uk-position-top uk-height-viewport '.$stickerClass.'">
			<div>
				<a href="'.$rutaPedido.'"><img src="img/design/checkout.png"></a>
			</div>
		</div>

		'.$loginModal.'
		
		<div id="totop">
			<a href="#top" uk-scroll id="arrow-button" class="uk-icon-button uk-button-totop uk-box-shadow-large oscuro"><i class="fa fa-arrow-up fa-1x" aria-hidden="true"></i></a>
		</div>
	</div>
		
	</div>';

/* %%%%%%%%%%%%%%%%%%%% HEAD GENERAL                */
	$headGNRL='
		<html lang="'.$languaje.'">
		<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">

			<meta charset="utf-8">
			<title>'.$title.'</title>
			<meta name="description" content="'.$description.'" />
			<meta property="fb:app_id" content="'.$appID.'" />
			<link rel="image_src" href="'.$ruta.$logoOg.'" />

			<meta property="og:type" content="website" />
			<meta property="og:title" content="'.$title.'" />
			<meta property="og:description" content="'.$description.'" />
			<meta property="og:url" content="'.$rutaEstaPagina.'" />
			<meta property="og:image" content="'.$ruta.$logoOg.'" />

			<meta itemprop="name" content="'.$title.'" />
			<meta itemprop="description" content="'.$description.'" />
			<meta itemprop="url" content="'.$rutaEstaPagina.'" />
			<meta itemprop="thumbnailUrl" content="'.$ruta.$logoOg.'" />
			<meta itemprop="image" content="'.$ruta.$logoOg.'" />

			<meta name="twitter:title" content="'.$title.'" />
			<meta name="twitter:description" content="'.$description.'" />
			<meta name="twitter:url" content="'.$rutaEstaPagina.'" />
			<meta name="twitter:image" content="'.$ruta.$logoOg.'" />
			<meta name="twitter:card" content="summary" />

			<meta name="viewport"       content="width=device-width, initial-scale=1">

			<link rel="icon"            href="'.$ruta.'img/design/favicon.ico" type="image/x-icon">
			<link rel="shortcut icon"   href="img/design/favicon.ico" type="image/x-icon">
			<link rel="stylesheet"      href="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/css/uikit.min.css" />
			<link rel="stylesheet"      href="css/general.css">
			<link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700&display=swap" rel="stylesheet">
			<link rel="stylesheet"      href="https://fonts.googleapis.com/css?family=Lato:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
			<link rel="stylesheet"      href="https://use.fontawesome.com/releases/v5.0.6/css/all.css">
			

			<!-- jQuery is required -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

			<!-- UIkit JS -->
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit.min.js"></script>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/'.$uikitVersion.'/js/uikit-icons.min.js"></script>
			<script src="js/general.js"></script>
			
			<script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
			<script src="http://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
			<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
		
			<script src="js/jquery.tagsinput-revisited.js"></script>
			<link rel="stylesheet" href="js/jquery.tagsinput-revisited.css" />
		
		</head>';


/* %%%%%%%%%%%%%%%%%%%% SCRIPTS                */
	$scriptGNRL='

		<script>
			$("#contactosend").click(function(){
		      var fallo = 0;
		      var nombre = $("#contactonombre").val();
		      var email = $("#contactoemail").val();
		      var telefono = $("#contactotelefono").val();
		      var comentarios = $("#contactocomentarios").val();
		      
		      var alerta = "";
		      
		      $("input").removeClass("uk-form-danger");
		      $("textarea").removeClass("uk-form-danger");

		      
		      if (comentarios=="") { fallo=1; alerta="Falta comentarios"; id="contactocomentarios"; }
		      if (telefono.length < 8) { fallo=1; alerta="Ingrese telefono valido"; id="contactotelefono"; } 

		      if (email=="") { 
		        fallo=1; alerta="Falta email"; id="contactoemail";
		      }else{
		        var n = email.indexOf("@");
		        var l = email.indexOf(".");
		        if ((n*l)<2) { 
		          fallo=1; alerta="Proporcione un email válido"; id="contactoemail";
		        } 
		      }

		      if (nombre=="") { fallo=1; alerta="Falta nombre"; id="contactonombre"; }

		      var parametros = {
		        "contacto" : 1,
		        "nombre" : nombre,
		        "email" : email,
		        "telefono" : telefono,
		        "comentarios" : comentarios

		      };
		      if (fallo == 0) {
		        $.ajax({
		          data:  parametros,
		          url:   "includes/acciones.php",
		          type:  "post",
		          beforeSend: function () {
		            $("#contactosend").html("<div uk-spinner></div>");
		            $("#contactosend").prop("disabled",true);
		            $("#contactosend").disabled = true;
		            UIkit.notification.closeAll();
		            UIkit.notification(\'<div class="uk-text-center color-blanco bg-blue padding-10 text-lg"><i  uk-spinner></i> Espere...</div>\');
		          },
		          success:  function (msj) {
		            $("#contactosend").html("Enviar");
		            $("#contactosend").disabled = false;
		            $("#contactosend").prop("disabled",false);
		            console.log(msj);
		            datos = JSON.parse(msj);
		            UIkit.notification.closeAll();
		            UIkit.notification(datos.msj);
		            if (datos.estatus==0) {
		              $("#contactocomentarios").val("");
		              $("#contactonombre").val("");
		              $("#contactoemail").val("");
		              $("#contactoasunto").val("");
		            }
		          }
		        })
		      }else{
		        UIkit.notification.closeAll();
		        UIkit.notification(\'<div class="uk-text-center color-blanco bg-danger padding-10 text-lg"><i uk-icon="icon:warning;ratio:2;"></i> &nbsp; \'+alerta+\'</div>\');
		        $("#"+id).focus();
		        $("#"+id).addClass("uk-form-danger");
		      }
		    });

			$(".cantidad").keyup(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					$(this).val(inventario);
				}
				console.log(inventario+" - "+cantidad);
			});

			$(".cantidad").focusout(function() {
				var inventario = $(this).attr("data-inventario");
				var cantidad = $(this).val();
				inventario=1*inventario;
				cantidad=1*cantidad;
				if(inventario<=cantidad){
					//console.log(inventario*2+" - "+cantidad);
					$(this).val(inventario);
				}
			})

			// Agregar al carro
			$(".buybutton").click(function(){
				var id=$(this).data("id");
				var cantidad=1;

				$.ajax({
					method: "POST",
					url: "addtocart",
					data: { 
						id: id,
						cantidad: cantidad,
						addtocart: 1
					}
				})
				.done(function( msg ) {
					datos = JSON.parse(msg);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msg);
					$("#cartcount").html(datos.count);
					$("#cotizacion-fixed").removeClass("uk-hidden");
				});
			});

			$(function(){
				$("#form-tags-1").tagsInput({
					\'onAddTag\': function(input, value) {
						console.log(\'tag added\', input.value, value);
					},
					\'onRemoveTag\': function(input, value) {
						console.log(\'tag removed\', input, value);
					},
					\'onChange\': function(input, value) {
						console.log(\'change triggered\', input, value);
					}
				});
						
			});
		</script>
		';



	// Script login Facebook
	$scriptGNRL.=(!isset($_SESSION['uid']) AND $dominio != 'localhost' AND isset($facebookLogin))?'
		<script>
			// Esta es la llamada a facebook FB.getLoginStatus()
			function statusChangeCallback(response) {
				if (response.status === "connected") {
					procesarLogin();
				} else {
					console.log("No se pudo identificar");
				}
			}

			// Verificar el estatus del login
			function checkLoginState() {
				FB.getLoginStatus(function(response) {
					statusChangeCallback(response);
				});
			}

			// Definir características de nuestra app
			window.fbAsyncInit = function() {
				FB.init({
					appId      : "'.$appID.'",
					xfbml      : true,
					version    : "v3.2"
				});
				FB.AppEvents.logPageView();
			};

			// Ejecutar el script
			(function(d, s, id){
				var js, fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id)) {return;}
				js = d.createElement(s); js.id = id;
				js.src = "//connect.facebook.net/es_LA/sdk.js";
				fjs.parentNode.insertBefore(js, fjs);
			}(document, \'script\', \'facebook-jssdk\'));
			
			// Procesar Login
			function procesarLogin() {
				FB.api(\'/me?fields=id,name,email\', function(response) {
					console.log(response);
					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: { 
							facebooklogin: 1,
							nombre: response.name,
							email: response.email,
							id: response.id
						}
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							location.reload();
						}
					});
				});
			}

		
		</script>

		':'';


// Reportar actividad
	$scriptGNRL.=(!isset($_SESSION['uid']))?'':'
		<script>
			var w;
			function startWorker() {
			  if(typeof(Worker) !== "undefined") {
			    if(typeof(w) == "undefined") {
			      w = new Worker("js/activityClientFront.js");
			    }
			    w.onmessage = function(event) {
					//console.log(event.data);
			    };
			  } else {
			    document.getElementById("result").innerHTML = "Por favor, utiliza un navegador moderno";
			  }
			}
			startWorker();
		</script>
		';

		

/* %%%%%%%%%%%%%%%%%%%% BUSQUEDA               */
	
	$scriptGNRL.='
		<script>
			$(document).ready(function(){
				$(".search").keyup(function(e){
					if(e.which==13){
						var consulta=$(this).val();
						var l = consulta.length;
						if(l>2){
							window.location = ("'.$ruta.'"+consulta+"_gdl");
						}else{
							UIkit.notification.closeAll();
							UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
						}
					}
				});
				$(".search-button").click(function(){
					var consulta=$(".search-bar-input").val();
					var l = consulta.length;
					if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}
				});

				
			});

			$(document).ready(function () {   

				$("#search").on("change", function() {
					var id = this.value;
					console.log("ID",id);
						
					 $.ajax({
					      method: "POST",
					      url: "includes/acciones.php",
					      data: { 
					        searchId: id,
					      }
					    }).done(function() {
					      location.reload();
					    });
					});
			});
		</script>';

/* %%%%%%%%%%%%%%%%%%%% BUSCAR Vero               */
	
	$scriptGNRL.='
		<script>
			$("#codeinput").click(function() {
			   $(this).select(); 
			   document.execCommand("copy");
			});
			$("#e-paisb").on("change", function() {
				var estadoId = this.value;
				
				$.ajax({
					type: "POST",
					url: "includes/acciones.php",
					data: {
					 procesarestados : 1,
					 idpais : estadoId
					} 
				}).done(function(data){
					$("#e-provincia").find("option").remove();	
					console.log("data",data);
					$("#e-provincia").html(data);

					getEscuela(estadoId);
				}); 
			});

			$("#e-provincia").on("change", function() {
				var provinciaId = this.value;

				$.ajax({
						type: "POST",
						url: "includes/acciones.php",
						data: {
						 procesarescuelasprovincia : 1,
						 idprovincia : provinciaId
						} 
					}).done(function(response){
						console.log("Escuelas",response);
						$("#e-escuela").find("option").remove();	
						$("#e-escuela").html(response);

					});
			});

			function getEscuela (escuelaId){
				console.log(escuelaId);
				$.ajax({
						type: "POST",
						url: "includes/acciones.php",
						data: {
						 procesarescuelas : 1,
						 idpais : escuelaId
						} 
					}).done(function(response){
						console.log("Escuelas",response);
						$("#e-escuela").find("option").remove();	
						$("#e-escuela").html(response);

					});
			}

			function makeid(length) {
				   var result           = "";
				   var characters       = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
				   var charactersLength = characters.length;
				   for ( var i = 0; i < length; i++ ) {
				      result += characters.charAt(Math.floor(Math.random() * charactersLength));
				   }
				   return result;
			}

			$(document).ready(function(){
				$("#registrobuttonfalso").click(function(){
					console.log("registrando....");

					var credito = 500;
					var temporal=[];
					//datos del cliente 
					var nombre = $("#e-nombre").val();
					var telefono = $("#e-telefono").val();
					var email = $("#e-correo").val();
					var password = $("#e-password").val();
					var calle = $("#e-calle").val();
					var nume = $("#e-numext").val();
					var numi = $("#e-numint").val();
					var pais = $("#e-pais").val();
					var estado = $("#e-estado").val();
					var mun = $("#e-mun").val();
					var col = $("#e-col").val();
					var cp = $("#e-cp").val();


					temporal = nombre.split(" ");
					codigo = temporal[0].toLowerCase() + makeid(6);

					var fallo = 0;
					var alerta = "";
					if (telefono==\'\') { fallo=1; alerta=\'Falta telefono\'; id=\'telefono\'; }

					// Contraseña
					var l = password.length;
					if (l<6) { 
						fallo=1; alerta=\'Contraseña demasiado débil\'; id=\'password\';
					}

					// Correo
					if (email==\'\') { 
						fallo=1; alerta=\'Falta email\'; id=\'email1\';
					}else{
						var n = email.indexOf(\'@\')
						var o = email.indexOf(\'.\')
						if ((n*o)<6) {
							fallo=1; alerta=\'Proporcione un email válido\'; id=\'email1\';
						}
					}
					if (calle==\'\') { fallo=1; alerta=\'Falta Calle\'; id=\'calle\'; }
					if (nume==\'\') { fallo=1; alerta=\'Falta numero exterior\'; id=\'nume\'; }
					if (pais==\'\') { fallo=1; alerta=\'Falta país\'; id=\'pais\'; }
					if (estado==\'\') { fallo=1; alerta=\'Falta Estado\'; id=\'estado\'; }
					if (mun==\'\') { fallo=1; alerta=\'Falta municipio\'; id=\'mun\'; }
					if (cp==\'\') { fallo=1; alerta=\'Falta codigo postal\'; id=\'cp\'; }
					
					if(fallo == 0){
						console.log("busqueda modal");
						$.ajax({
							method: "POST",
							url: "includes/acciones.php",
							data: {
								"registrodeusuarios" : 1,
								"nombre" :nombre,
								"telefono" : telefono,
								"email" : email,
								"password" : password,
								"calle" : calle,
								"noexterior" : nume,
								"nointerior" :numi,
								"pais" : pais,
								"estado" : estado,
								"municipio" : mun,
								"colonia" : col,
								"cp" : cp,
								"codigo":codigo,
								"credito":500
							}
						})
						.done(function( response ) {
							console.log( response );
							datos = JSON.parse( response );
							UIkit.notification.closeAll();
							UIkit.notification(datos.msj);
							UIkit.modal("#buscar").show();
						});

					}else{
						//UIkit.modal("#buscar").show();
						alert(alerta);
						
					}
					
				});

				$("#buscarfiltro").click(function(){
					//datos del cliente 
					$("#e-nombre").val();
					$("#e-telefono").val();
					$("#e-correo").val();
					$("#e-ciudad").val();
					$("#e-pais").val();
					$("#e-nacionalidad").val();
					$("#e-estudios").val();
					

					//Datos de busqueda
					var catid = $("#e-cat").val();
					console.log("categoria no escojita",catid);
					var pais = $("#e-paisb").val();
					var provincia = $("#e-provincia").val();
					var palabras = $("#form-tags-1").val();
					var escuela = $("#e-escuela").val();

					$("#e-ciudad").val();
					var precio = $("#e-presupuesto").val();

					$("#e-fecha").val();
					$("#e-duracion").val();
					var data = {
						busquedaform : 1
						
					}
					if(precio)
						data.precio = precio;
					if(pais)
						data.pais = pais;
					if(catid)
						data.catid = catid;
					if(provincia)
						data.provincia = provincia;
					if(palabras)
						data.palabras = palabras;
					if(escuela)
						data.escuela = escuela;

					$.ajax({
						method: "POST",
						url: "includes/acciones.php",
						data: data
					})
					.done(function( response ) {
						console.log( response );
						datos = JSON.parse( response );
						UIkit.notification.closeAll();
						UIkit.notification(datos.msj);
						if(datos.estatus==0){
							UIkit.modal("#buscar").hide();
							
							window.location = ("busqueda");
						}
					});
					/*if(l>2){
						window.location = ("'.$ruta.'"+consulta+"_gdl");
					}else{
						UIkit.notification.closeAll();
						UIkit.notification("<div class=\'bg-danger color-blanco\'>Se requiren al menos 3 caracteres</div>");
					}*/
				});
			});
		</script>';


