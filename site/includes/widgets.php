<?php
  $consulta = $CONEXION -> query("SELECT * FROM productoscat ORDER BY titulo");
  $selectCat ="";
  $selectEscuelas="";
  //categorias
  while ($rowConsulta = $consulta -> fetch_assoc()) {
    $catId = $rowConsulta["id"];
    $categoriaTitulo=$rowConsulta["titulo"];
    $selectCat .= '<option class="uk-text-right" align="right" value="'.$catId.'">'.$categoriaTitulo.'</option>';
  }

  $consulta2 = $CONEXION -> query("SELECT * FROM paises ORDER BY nombre");
  $selectPais ="";

  while ($rowConsulta2 = $consulta2 -> fetch_assoc()) {
    $paisId = $rowConsulta2["id"];
    $pais=$rowConsulta2["nombre"];
    $selectPais .= '<option class="uk-text-right" align="right" value="'.$paisId.'">'.$pais.'</option>';
  }

  $consultaEscuelas = $CONEXION -> query("SELECT id, titulo FROM escuelas order by titulo");
  while ($escuelasRow = $consultaEscuelas -> fetch_assoc()) {  

    $escuelaId = $escuelasRow['id'];
    $escuela = $escuelasRow['titulo'];

    $selectEscuelas .= '<option class="uk-text-right" align="right" value="'.$escuelaId.'">'.$escuela.'</option>';
  }
 
//MODAL DE BUSQUEDA DE PROGRAMAS
$modalBuscar='
    <!--/* %%%%%%%%%%%%%%%%%%%% Modales para busqueda  */-->
      <div id="modal-container" class="uk-modal-container" uk-modal style="z-index:99999999">
          <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-default uk-align-center" type="button" uk-close></button>
              <div class="wk-width-1-1 uk-flex uk-flex-center uk-align-center" uk-grid>
                <img style="margin-top:20px;" src="./img/design/logo.png">
              </div>
              <p class="uk-modal-title blue uk-align-center signika" style="font-size:28px;text-align:center;font-weight:100;">
                DESCUBRE CUAL ES PROGRAMA IDEAL PARA TI<br>AYUDANOS CON LOS SIGUENTES DATOS</p>
              <div uk-grid id="modal-buscar">
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-nombre" id="e-nombre" placeholder="Nombre" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
        
                  <input class="inputs" type="date" name="e-nacimiento" id="e-nacimiento" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-telefono" id="e-telefono" placeholder="Telefono" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-correo" id="e-correo" placeholder="Correo" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-password" id="e-password" placeholder="Contraseña" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-calle" id="e-calle" placeholder="Calle" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-numext" id="e-numext" placeholder="No.Exterior" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-numint" id="e-numint" placeholder="No.Interior" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-pais" id="e-pais" placeholder="País" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-estado" id="e-estado" placeholder="Estado" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-mun" id="e-mun" placeholder="Municipio" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-col" id="e-col" placeholder="Colonia" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <div class="uk-width-1-4@m uk-paddings">
                  <input class="inputs" type="text" name="e-cp" id="e-cp" placeholder="Codigo postal" style="border-radius:30px;background:#e5e5e5;">
                </div>
                <!--div class="uk-width-1-4@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-estudios" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right">
                        <a href="busqueda">Estudios</a></option>
                    <option class="uk-text-right" align="right">
                        <a href="busqueda">SECUNDARIA</a></option>
                    <option class="uk-text-right" align="right">
                        <a href="busqueda">PREPARATORIA</a></option>
                    <option class="uk-text-right" align="right">
                        <a href="busqueda">INTERMEDIO ELITE</a></option>
                  </select>
                </div-->
              </div>
              <div class="wk-width-1-1 uk-flex uk-flex-center uk-align-center" uk-grid>
                  <div class="uk-width-1-4@m">
                <a id="registrobuttonfalso" class="uk-grid-collapse btn-header-container">
                  <img src="./img/design/icon4.png">
                </a>
              </div>
            </div>
          </div>
      </div>
      <div id="buscar" class="uk-modal-container" uk-modal>
          <div class="uk-modal-dialog uk-modal-body">
              <button class="uk-modal-close-default uk-align-center" type="button" uk-close></button>
              <div class="wk-width-1-1 uk-flex uk-flex-center uk-align-center" uk-grid>
                <img style="margin-top:20px;" src="./img/design/logo.png">
              </div>
              <p class="uk-modal-title blue uk-align-center signika" style="font-size:28px;text-align:center;font-weight:100;">
            DESCUBRE CUAL ES PROGRAMA IDEAL PARA TI<br>AYUDANOS CON LOS SIGUENTES DATOS</p>
              <div uk-grid id="modal-buscar">
                <div class="uk-width-1-2@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-cat" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right" value="">¿Qué busco?</option>
                    '.$selectCat.'
                  </select>
                </div>
                <div class="uk-width-1-2@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-paisb" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right" value="">Pais en el que te gustaria estudiar</option>
                      '.$selectPais.'
                  </select>
                </div>
               
                <div class="uk-width-1-2@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-provincia" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right" value="">Provincia en la que te gustaria estudiar</option>
                  </select>
                </div>

                <div class="uk-width-1-2@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-escuela" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right" value="">Escuela en el que te gustaria estudiar</option>
                      '.$selectEscuelas.'
                  </select>
                </div>
                <div class="uk-width-1-2@m uk-paddings">
                  <select class="uk-select uk-grid-collapse" align="right" id="e-presupuesto" style="border-radius:30px;background:#e5e5e5;color: #000;border: solid transparent;">
                    <option class="uk-text-right" align="right" value="" selected>Rango de precio</option>
                    <option class="uk-text-right" align="right" value="DESC">Mayor costo</option>
                    <option class="uk-text-right" align="right" value="ASC">Menor Costo</option>
                  </select>
                </div>
                <div class="uk-width-1-2@m uk-paddings">
                <input id="form-tags-1"  class="uk-input" name="palabras" type="text">
                </div>
                <!--div class="uk-width-1-2@m uk-paddings">
                  <input class="inputs" type="text" name="e-fecha" id="e-fecha" placeholder="Fecha de inicio" style="border-radius:30px;background:#e5e5e5;">
                </div-->
              </div>
              <div class="wk-width-1-1 uk-flex uk-flex-center uk-align-center" uk-grid>
              <div class="uk-width-1-4@m" style="font-size: 20px; font-weight: 100;">
                <button id="buscarfiltro" name="send" class="uk-button uk-button-primary uk-button-large">BUSCAR</button>
              </div>
            </div>
          </div>
        
      </div>
';

//MODAL DE CODIGO

function modalCodigo($ruta,$code,$titulo1,$txt1,$titulo2,$txt2){

    $socialWhats  = 'https://api.whatsapp.com/send?text='.urlencode($ruta.'code_'.$code);
    $shareTwitter = 'https://twitter.com/intent/tweet?text='.urlencode($ruta.'code_'.$code).'&ref=plugin&src=share_button&button_hashtag=#'.urlencode('Studies4Life').'&screen_name='.urlencode('Studies4Life').'';
    $shareFace    = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($ruta.'code_'.$code);

  $modal ='
    <div id="modal-codigo" class="uk-modal-container" uk-modal style="z-index:99999999">
      <div class="uk-modal-dialog uk-modal-body">
        <button class="uk-modal-close-default uk-align-center" type="button" uk-close></button>
        <div class="wk-width-1-1 uk-flex uk-flex-center uk-align-center" uk-grid>
          <img style="margin-top:20px;" src="./img/design/logo.png">
        </div>
        <p class="uk-modal-title blue uk-align-center signika" style="font-size:28px;text-align:center;font-weight:100;">
          DESCUBRE CUAL ES PROGRAMA IDEAL PARA TI<br>Studies4Life</p>
        <div class="uk-grid-small" uk-grid>
          <div class="uk-width-1-2@m uk-padding-small">
            <h2 class="signika text-xl border-cero uk-text-center" style="margin-top: 10px;padding: 20px 50px;"> '.$titulo1.' </h2>
            <div class=" margin-top-30" uk-grid style="margin-left: 0;">';
    if($code != null){
      $modal .='<div class="uk-width-1-1@m uk-padding-remove">
                  <input id="codeinput" class="inputs comparte " type="readonly"  value="'.$ruta.'code_'.$code. '">

                </div>
                <div class="uk-width-1-1@m uk-padding-remove uk-margin uk-text-center">
                  <span class="color-secondary">COMPARTIR EN:</span> &nbsp;&nbsp;&nbsp;
                   
                <a data-id="'.$id.'" class="share color-primary" data-type="shareWhatsapp" href="'.$socialWhats.'" target="_blank"> <span uk-icon="icon: whatsapp; ratio: 1.2;" ></span><sub>'.$row_CONSULTA['shareWhatsapp'].'</sub></a>
                <a data-id="'.$id.'" class="share color-primary" data-type="shareFacebook" href="'.$shareFace.'" target="_blank">&nbsp; <span uk-icon="icon: facebook; ratio: 1.2;" ></span><sub>'.$row_CONSULTA['shareFacebook'].'</sub>&nbsp; </a>
                <a data-id="'.$id.'" class="share color-primary" data-type="shareTwitter" href="'.$shareTwitter.'" target="_blank"> <span uk-icon="icon: twitter; ratio: 1.2;" ></span><sub>'.$row_CONSULTA['shareTwitter'].'</sub></a>

                </div>';
    }
    else{
      $modal .='
              <div class="uk-width-1-1@m uk-padding-remove uk-align-center">
                <a class="padding-top-10" href="Registro">
                  <div class="uk-align-center uk-text uk-text-uppercase comienza-button" >
                    Comienza ahora
                  </div>
                </a>
              </div>';
    
    }
      $modal .='      
            </div>  
          </div>
          <div class="uk-width-1-2@m">
              <img class="uk-align-center" src="./img/design/moneda.png" style="max-height:125px;margin-bottom:0">
              <h2 class="signika text-xl border-cero uk-text-center" style="color:#000">'.$titulo2.'</h3>
              <div class="uk-text-left text-8 color-negro padding-top-10" style="height:180px;">
              '.$txt2.'
              </div>    
          </div>
        </div>
      </div>
    </div>
    ';
  echo $modal;
}
// CARRO DE COMPRA       
  //unset($_SESSION['carro']);
  if (isset($_POST['emptycart'])) {
    unset($_SESSION['carro']);
  }

  $carroTotalProds=0;
  // Si ya hay productos en la variable de sesión
  if(isset($_SESSION['carro'])){
    $arreglo=$_SESSION['carro'];
    foreach ($arreglo as $key => $value) {
      $carroTotalProds+=$value['Cantidad'];
    }
  }

  // Remover artículos del carro
  if (isset($_POST['removefromcart'])) {
    $id=$_POST['id'];
    $arregloAux=$_SESSION['carro'];
    unset($arreglo);
    $num=0;
    foreach ($arregloAux as $key => $value) {
      if ($id!=$value['Id']) {
        $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
      }
      $num++;
    }
    $_SESSION['carro']=$arreglo;
  }

  // Agregar artículos al carro
  if (isset($_POST['addtocart'])) {
    
    if (isset($_POST['cantidad']) and $_POST['cantidad']!==0 and $_POST['cantidad']!=='') {
      $id=$_POST['id'];

      $carroTotalProds+=$_POST['cantidad'];

      if(isset($_POST['inscripcion']) && $_POST['inscripcion']== 1){
         $arregloNuevo[]=array(
          'Id'=>$id,
          'Cantidad'=>$_POST['cantidad'],
          'inscripcion'=>$_POST['inscripcion'],
          'rangosIds' => $_POST['rangosIds'],
          'preciosIds' =>$_POST['preciosIds'],
          'hospedaje' => $_POST['hospedaje']
        );
      }
      else if(isset($_POST['liquidado']) && $_POST['liquidado']== 1){
         $arregloNuevo[]=array(
          'Id'=>$id,
          'Cantidad'=>$_POST['cantidad'],
          'liquidado'=>$_POST['liquidado'],
          'pedido' =>$_POST['pedidoid']
        );
      }
      else{
        $arregloNuevo[]=array(
          'Id'=>$id,
          'Cantidad'=>$_POST['cantidad']);
      }

      if (!isset($arreglo)) {
        $arreglo=$arregloNuevo;
      }else{
        $arregloAux=$arreglo;
        unset($arreglo);
        $num=0;
        foreach ($arregloAux as $key => $value) {
          if ($id!=$arregloAux[$num]['Id']) {
            $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$arregloAux[$num]['Cantidad']);
          }else{
            $carroTotalProds-=$arregloAux[$num]['Cantidad'];
          }
          $num++;
        }
        if ($_POST['cantidad']>0) {
          $arreglo[]=array('Id'=>$id,'Cantidad'=>$_POST['cantidad']);
        }
      }
      
      echo '{ "msg":"<div class=\'uk-text-center color-blanco bg-success padding-10 text-lg\'><i class=\'fa fa-check\'></i> &nbsp; Agregado al pedido</div>", "count":'.$carroTotalProds.' }';

      $_SESSION['carro']=$arreglo;
    }
  }

  if (isset($_POST['actualizarcarro'])) {
    $arregloAux=$_SESSION['carro'];
    unset($arreglo);
    $carroTotalProds=0;
    $num=0;
    foreach ($arregloAux as $key => $value) {
      $arreglo[]=array('Id'=>$arregloAux[$num]['Id'],'Cantidad'=>$_POST['cantidad'.$num]);
      $carroTotalProds+=$_POST['cantidad'.$num];
      $num++;
    }
    $_SESSION['carro']=$arreglo;
  }

// LIMITAR PALABRAS      
  function wordlimit($string, $length , $ellipsis)
  {
    $words = explode(' ', strip_tags($string));
    if (count($words) > $length)
    {
      return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
    }
    else
    {
      return $string;
    }
  }

// FECHA                 
  // FECHA CORTA
    function fechaCorta($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      return $fechaD.'-'.$fechaM.'-'.$fechaY;
    }
    
  // FECHA Y HORA
    function fechaHora($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaH=date('H',$fechaSegundos);
      $fechaI=date('i',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      return $fechaD.'-'.$fechaM.'-'.$fechaY.'<br>'.$fechaH.':'.$fechaI;
    }
    
  // SOLO HORA
    function soloHora($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaH=date('H',$fechaSegundos);
      $fechaI=date('i',$fechaSegundos);

      return $fechaH.':'.$fechaI;
    }

  function fechaSQL($fechaSQL){
    $fechaSegundos=strtotime($fechaSQL);

    $fechaY=date('Y',$fechaSegundos);
    $fechaM=date('m',$fechaSegundos);
    $fechaD=date('d',$fechaSegundos);
   
    return $fechaY.'/'.$fechaM.'/'.$fechaD;
  }
  
  // FECHA DIA
    function fechaDisplayDia($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaDay) {
        case 'mon':
        $fechaDia='Lunes';
        break;
        case 'tue':
        $fechaDia='Martes';
        break;
        case 'wed':
        $fechaDia='Miércoles';
        break;
        case 'thu':
        $fechaDia='Jueves';
        break;
        case 'fri':
        $fechaDia='Viernes';
        break;
        case 'sat':
        $fechaDia='Sábado';
        break;
        default:
        $fechaDia='Domingo';
        break;
      }
      return $fechaDia;
    }

  // FECHA MES
    function fechaDisplayMes($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaM) {
        case 1:
        $mes='enero';
        break;
        
        case 2:
        $mes='febrero';
        break;
        
        case 3:
        $mes='marzo';
        break;
        
        case 4:
        $mes='abril';
        break;
        
        case 5:
        $mes='mayo';
        break;
        
        case 6:
        $mes='junio';
        break;
        
        case 7:
        $mes='julio';
        break;
        
        case 8:
        $mes='agosto';
        break;
        
        case 9:
        $mes='septiembre';
        break;
        
        case 10:
        $mes='octubre';
        break;
        
        case 11:
        $mes='noviembre';
        break;
        
        default:
        $mes='diciembre';
        break;
      }

      return $mes;
    }

  // FECHA LARGA
    function fechaDisplay($fechaSQL){
      $fechaSegundos=strtotime($fechaSQL);
      $fechaY=date('Y',$fechaSegundos);
      $fechaM=date('m',$fechaSegundos);
      $fechaD=date('d',$fechaSegundos);
      $fechaDay=strtolower(date('D',$fechaSegundos));

      switch ($fechaM) {
        case 1:
        $mes='enero';
        break;
        
        case 2:
        $mes='febrero';
        break;
        
        case 3:
        $mes='marzo';
        break;
        
        case 4:
        $mes='abril';
        break;
        
        case 5:
        $mes='mayo';
        break;
        
        case 6:
        $mes='junio';
        break;
        
        case 7:
        $mes='julio';
        break;
        
        case 8:
        $mes='agosto';
        break;
        
        case 9:
        $mes='septiembre';
        break;
        
        case 10:
        $mes='octubre';
        break;
        
        case 11:
        $mes='noviembre';
        break;
        
        default:
        $mes='diciembre';
        break;
      }

      switch ($fechaDay) {
        case 'mon':
        $fechaDia='Lunes';
        break;
        case 'tue':
        $fechaDia='Martes';
        break;
        case 'wed':
        $fechaDia='Miércoles';
        break;
        case 'thu':
        $fechaDia='Jueves';
        break;
        case 'fri':
        $fechaDia='Viernes';
        break;
        case 'sat':
        $fechaDia='Sábado';
        break;
        default:
        $fechaDia='Domingo';
        break;
      }

      return $fechaDia.' '.$fechaD.' de '.$mes.' de '.$fechaY;
    }

// CARRUSEL              
  // Carousel Inicio
    function carousel($carousel){
      global $CONEXION;
      global $dominio;

      $CONSULTA= $CONEXION -> query("SELECT * FROM configuracion WHERE id = 1");
      $row_CONSULTA = $CONSULTA -> fetch_assoc();
      switch ($row_CONSULTA['slideranim']) {
        case 0:
          $animation='fade';
          break;
        case 1:
          $animation='slide';
          break;
        case 2:
          $animation='scale';
          break;
        case 3:
          $animation='pull';
          break;
        case 4:
          $animation='push';
          break;
        default:
          $animation='fade';
          break;
      }
      $CAROUSEL = $CONEXION -> query("SELECT * FROM $carousel ORDER BY orden");
      $numPics=$CAROUSEL->num_rows;
      if ($numPics>0) {
        echo '
            <!-- Start Carousel -->
            <div uk-slideshow="autoplay:true;ratio:'.$row_CONSULTA['sliderproporcion'].';animation:'.$animation.';min-height:'.$row_CONSULTA['sliderhmin'].';max-height:'.$row_CONSULTA['sliderhmax'].';" class="uk-grid-collapse" uk-grid>
              <div class="uk-visible-toggle uk-width-1-1 uk-flex-first">
                <div class="uk-position-relative">
                  <ul class="uk-slideshow-items">';
                    $num=0;
                    $activo=' active';
                    while ($row_CAROUSEL = $CAROUSEL -> fetch_assoc()) {
                      $caption='';
                      if (strlen($row_CAROUSEL['url'])>0) {
                        $pos=strpos($row_CAROUSEL['url'], $dominio);
                        $target=($pos>0)?'':'target="_blank"';
                        if ($row_CONSULTA['slidertextos']==1 AND strlen($row_CAROUSEL['titulo'])>0 AND strlen($row_CAROUSEL['url'])>0) {
                          $caption='
                          <div class="uk-position-bottom uk-transition-slide-bottom">
                            <div style="min-width:200px;min-height:100px;" class="uk-text-center">
                              <a href="'.$row_CAROUSEL['url'].'" '.$target.' class="uk-button uk-button-white uk-button-large">
                                '.$row_CAROUSEL['titulo'].'
                              </a>
                            </div>
                          </div>';
                        }
                      }
                      echo '
                          <li>
                            <img src="img/contenido/'.$carousel.'/'.$row_CAROUSEL['id'].'.jpg" uk-cover>
                            '.$caption.'
                          </li>';
                    }

                    echo '
                  </ul>

                  <a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slideshow-item="previous"></a>
                  <a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slideshow-item="next"></a>

                </div>
                <ul class="uk-slideshow-nav uk-dotnav uk-flex-center uk-margin"></ul>
              </div>
            </div>
            <!-- End Carousel -->
            ';
      }
      mysqli_free_result($CAROUSEL);
    }




// ITEM                   
  function item($id){
    global $CONEXION;
    global $caracteres_si_validos;
    global $caracteres_no_validos;

    $widget    = '';
    $style     = 'max-width:200px;';  
    $noPic     = 'img/design/camara.jpg';
    $rutaPics  = 'img/contenido/productos/';
    $firstPic  = $noPic;

    $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $id");
    $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
    $link=$id.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';

    // Fotografía
      $CONSULTA3 = $CONEXION -> query("SELECT * FROM productospic WHERE producto = $id ORDER BY orden,id LIMIT 1");
      while ($rowCONSULTA3 = $CONSULTA3 -> fetch_assoc()) {
        $firstPic = $rutaPics.$rowCONSULTA3['id'].'.jpg';
      }

      $picWidth=0;
      $picHeight=0;
      $picSize=getimagesize($firstPic);
      foreach ($picSize as $key => $value) {
        if ($key==3) {
          $arrayCadena1=explode(' ',$value);
          $arrayCadena1=str_replace('"', '', $arrayCadena1);
          foreach ($arrayCadena1 as $key1 => $value1) {

            $arrayCadena2=explode('=',$value1);
            foreach ($arrayCadena2 as $key2 => $value2) {
              if (is_numeric($value2)) {
                $picProp[]=$value2;
              }
            }
          }
        }
      }
      if (isset($picProp)) {
        $picWidth=$picProp[0];
        $picHeight=$picProp[1];

        $style=($picWidth<$picHeight)?'max-height:200px;':$style;
      }

    $widget.='
      <div id="item'.$id.'" class="uk-text-center">
        <div class="bg-white padding-20" style="border:solid 1px #CCC;">
          <a href="'.$link.'" style="color:black;">
            <div class="margin-10">
              <div class="uk-flex uk-flex-center uk-flex-middle" style="height: 200px;">
                <img data-src="'.$firstPic.'" uk-img style="'.$style.'">
              </div>
              <div style="min-height:100px;">
                <div>
                  '.$row_CONSULTA1['sku'].'
                </div>
                <div class="uk-flex uk-flex-center">
                  <div class="line-yellow"></div>
                </div>
                <div class="padding-v-10">
                </div>
                <div>
                  '.$row_CONSULTA1['titulo'].'
                </div>
              </div>
            </div>
          </a>
        </div>
      </div>';

    return $widget;
  }

