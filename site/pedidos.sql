-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 17-01-2020 a las 14:39:27
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `studies`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedidos`
--

CREATE TABLE `pedidos` (
  `id` int(10) NOT NULL,
  `idmd5` varchar(50) DEFAULT NULL,
  `uid` int(10) NOT NULL DEFAULT '0',
  `nombre` varchar(100) DEFAULT NULL,
  `email` varchar(200) DEFAULT NULL,
  `estatus` int(1) NOT NULL DEFAULT '0',
  `invisible` int(1) NOT NULL DEFAULT '0',
  `notify` int(1) NOT NULL DEFAULT '0',
  `guia` varchar(20) DEFAULT NULL,
  `linkguia` varchar(100) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `dom` int(11) NOT NULL DEFAULT '0',
  `factura` int(11) DEFAULT '0',
  `tabla` text,
  `cantidad` int(11) DEFAULT NULL,
  `importe` decimal(10,2) DEFAULT NULL,
  `envio` decimal(15,2) DEFAULT NULL,
  `comprobante` varchar(50) DEFAULT NULL,
  `imagen` varchar(10) DEFAULT NULL,
  `ipn` varchar(50) DEFAULT NULL,
  `calle` varchar(100) DEFAULT NULL,
  `noexterior` varchar(50) DEFAULT NULL,
  `nointerior` varchar(50) DEFAULT NULL,
  `entrecalles` varchar(200) DEFAULT NULL,
  `pais` varchar(20) DEFAULT 'Mexico',
  `estado` varchar(50) DEFAULT NULL,
  `municipio` varchar(50) DEFAULT NULL,
  `colonia` varchar(50) DEFAULT NULL,
  `cp` varchar(10) DEFAULT NULL,
  `papelera` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pedidos`
--

INSERT INTO `pedidos` (`id`, `idmd5`, `uid`, `nombre`, `email`, `estatus`, `invisible`, `notify`, `guia`, `linkguia`, `fecha`, `dom`, `factura`, `tabla`, `cantidad`, `importe`, `envio`, `comprobante`, `imagen`, `ipn`, `calle`, `noexterior`, `nointerior`, `entrecalles`, `pais`, `estado`, `municipio`, `colonia`, `cp`, `papelera`) VALUES
(54, 'a684eceee76fc522773286a895bc8436', 2, NULL, NULL, 1, 0, 0, NULL, NULL, '2020-01-17 05:46:09', 0, 0, '\r\n      <table style=\"width: 100%;\" cellspacing=\"2\" border=\"0\" bgcolor=\"#CCC\">\r\n        <tr style=\"background-color:#FFF;\">\r\n          <th style=\"padding: 8px; color: #666; width: 65%; text-align: left; \">Producto</th>\r\n          <th style=\"padding: 8px; color: #666; width: 15%; text-align: center; \">Descripción</th>\r\n          <th style=\"padding: 8px; color: #666; width: 15%; text-align: center; \">Cantidad</th>\r\n          <th style=\"padding: 8px; color: #666; width: 15%; text-align: right; \">Precio</th>\r\n          <th style=\"padding: 8px; color: #666; width: 15%; text-align: right; \">Importe</th>\r\n        </tr>\r\n        <tr style=\"background-color:#FFF;\">\r\n          <td style=\"padding: 8px; text-align: left; \">\r\n            6 -\r\n            Diploma en Ingenier&iacute;a Inform&aacute;tica\r\n          </td>\r\n            <td style=\"padding: 8px; text-align: center; \">\r\n            inscripción\r\n          </td>\r\n          <td style=\"padding: 8px; text-align: center; \">\r\n            1\r\n          </td>\r\n          <td style=\"padding: 8px; text-align: right; \">\r\n            10,000.00\r\n          </td>\r\n          <td style=\"padding: 8px; text-align: right; \">\r\n            10,000.00\r\n          </td>\r\n        </tr>\r\n      <tr style=\"background-color:#EEEEEE;\">\r\n        <td colspan=\"4\" style=\"padding: 8px;text-align:right;\">\r\n          Total\r\n        </td>\r\n        <td style=\"padding: 8px;text-align:right;\">\r\n          10,000.00\r\n        </td>\r\n      </tr>\r\n      \r\n    </table>', 1, '10000.00', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Mexico', NULL, NULL, NULL, NULL, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `pedidos`
--
ALTER TABLE `pedidos`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
