<?php
$pedido=$row_CONSULTA['id'];
$fechaSQL=$row_CONSULTA['fecha'];
$user=$row_CONSULTA['id'];
$pagarButton='';

$ConsultaDeposito= $CONEXION -> query("SELECT bank FROM configuracion WHERE id = 1");
$rowConsultaDeposito = $ConsultaDeposito-> fetch_assoc();

$estatus=$row_CONSULTA['estatus'];
switch ($estatus) {
  case 1:
    $estatusClase='primary';
    $estatusMensaje='pagado';
    break;
  case 2:
    $estatusClase='warning';
    $estatusMensaje='Enviado';
    if ($row_CONSULTA['guia']!='') {
      $estatusMensaje.='.<br>Número de guía: '.$row_CONSULTA['guia'];
    }
    break;
  case 3:
    $estatusClase='success';
    $estatusMensaje='Entregado';
    break;
  default:
    $pagarButton='
        <div class="uk-width-1-2@m uk-width-1-1">
          <div class="uk-card uk-card-default">
            <div class="uk-card-body">
              <div class="uk-width-1-1">
                <p>
                  <b>Datos para pago bancario</b><br>
                  '.nl2br($rowConsultaDeposito['bank']).'
                </p>
              </div>
            </div>
          </div>
        </div>
        <div class="uk-width-1-1 uk-text-center text-lg">
          Una vez que haya realizado su pago podrá notificarlo en el<br>
          <a href="mi-cuenta" class="uk-button uk-button-personal">panel de cliente.</a>
        </div>
        ';
    $estatusClase='warning';
    $estatusMensaje='registrado';
    break;
}

$dom=$row_CONSULTA['dom'];
switch ($dom) {
  case 0:
    $dom1='';
    $dom2='uk-hidden';
    $dom3='uk-hidden';
  case 1:
    $dom1='';
    $dom2='';
    $dom3='uk-hidden';
  case 2:
    $dom1='uk-hidden';
    $dom2='uk-hidden';
    $dom3='';

}
?>
<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>



<div class="uk-container">
  <div class="uk-margin">
  </div>
  <div class="color-blanco bg-inclinado-white padding-20 text-lg">
    Datos de su pedido
  </div>

  <div uk-grid>
    <div class="uk-width-1-1 margen-top-20">
      <div class="uk-alert uk-alert-<?=$estatusClase?> uk-text-center" data-uk-alert>
        <a href="#" class="uk-alert-close uk-close"></a>
        Su pedido se encuentra en estado: <b><?=$estatusMensaje?></b>
      </div>
    </div>

    <div class="uk-width-1-1 margen-v-20">
      <div uk-grid class="uk-child-width-1-3@m">

      <?php
      $titulo="";
      echo '
        <div class="uk-width-1-2@m uk-width-1-1">
          <b class="uk-text-large">Datos personales</b><br>
          <span class="uk-text-muted">Nombre:</span> <b>'.$row_USER['nombre'].'</b><br>
          <span class="uk-text-muted">RFC:</span> <b>'.$row_USER['rfc'].'</b><br>
          <span class="uk-text-muted">Email:</span> <b>'.$row_USER['email'].'</b><br>
          <span class="uk-text-muted">Fecha de creación:</span> <b><br>'.fechaDisplay($fechaSQL).'</b><br><br>
          <div class="uk-width-1-1 margen-v-20">
            <a href="'.$idmd5.'_orden.pdf" class="uk-button uk-button-large uk-button-personal" target="_blank"><i class="far fa-2x fa-file-pdf"></i> &nbsp; Descargar PDF</a>
          </div>
        </div>

        '.$pagarButton.'

        <div class="'.$dom1.'">
          <div class="uk-width-1-1 uk-text-large">
            '.$titulo.'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Nombre:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['nombre'].'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Email:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['email'].'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Telefono:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['telefono'].'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Domicilio:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['calle'].',
            No. '.$row_USER['noexterior'].'
            &nbsp; '.$row_USER['nointerior'].'<br>
            '.$row_USER['colonia'].',
            CP: '.$row_USER['cp'].'<br>
            '.$row_USER['municipio'].',
            '.$row_USER['estado'].',
            '.$row_USER['pais'].'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Entrecalles:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['entrecalles'].'
          </div>
        </div>
        ';
    if ($dom==1) {
      $titulo='Dirección de entrega';
      echo'
        <div class="'.$dom2.'">
          <div class="uk-width-1-1 uk-text-large">
            '.$titulo.'
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['calle2'].',
            No. '.$row_USER['noexterior2'].'
            &nbsp; '.$row_USER['nointerior2'].'<br>
            '.$row_USER['colonia2'].',
            CP: '.$row_USER['cp2'].'<br>
            '.$row_USER['municipio2'].',
            '.$row_USER['estado2'].',
            '.$row_USER['pais2'].'
          </div>
          <div class="uk-width-1-1 uk-text-muted uk-text-small">
            Entrecalles:
          </div>
          <div class="uk-width-1-1">
            '.$row_USER['entrecalles2'].'
          </div>
        </div>
        ';
    }
    if ($dom==2) {
      $titulo='Recoge en tienda';
      echo'
        <div class="'.$dom3.'">
          <div class="uk-width-1-1 uk-text-large">
            <h2>'.$titulo.'</h2>
          </div>
        </div>
        ';
    }

    echo '    
      </div>
    </div>';
?>


    <div class="uk-width-1-1 margen-top-50">
        <?php
        $tabla = str_replace('<table', '<table class="uk-table uk-table-striped uk-table-hover uk-table-middle"', $row_CONSULTA['tabla']);
        echo str_replace('color: white;', 'color: white; background-color: #EEE;', $tabla);
        ?>
    </div>

    <div class="uk-width-1-1 uk-text-center margen-top-50">
      <p class="text-xl">Detalles de la cotización</p>
      <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
          <thead>
            <tr>
              <td width="100px" class="uk-text-left">Concepto</td>
              <td width="100px" class="uk-text-left">Precio en Divisa</td>
              <td width="100px" class="uk-text-left">Precio en MN</td>
            </tr>
          </thead>
          <tbody>
          <?php 
            $consultaDetalles = $CONEXION -> query("SELECT * FROM pedidosdetalle WHERE pedido = $pedido");
            while ($detallesRows = $consultaDetalles -> fetch_assoc()):
              //CONSULTA PRECIOS
              $total = 0;
              $arrayPrecios = $detallesRows['preciosids'];
              $hospId = $detallesRows['hospedajeid'];
              $rangosIds = $detallesRows['rangosids'];
              $programaId = $detallesRows['producto'];

              $consultaProg = $CONEXION -> query("SELECT * FROM productos WHERE id = $programaId");
              $programaRow = $consultaProg ->fetch_assoc();
              $total += $programaRow['cuota'];
              $total -= $programaRow['inscripcion'];
              $total += $programaRow['seguro'];
             ////debug($rangosIds);
              $rangosArray = explode(",", $rangosIds);

              $consultaPrecios = $CONEXION -> query("
                SELECT p.*,d.nombre,d.simbolo,d.precio_pesos,d.cambio,d.valor_actual FROM productos_precios as p 
                JOIN divisas AS d ON d.id = p.divisa
                WHERE p.id IN ($arrayPrecios)");

              
              $consultaHosp = $CONEXION -> query("
                SELECT h.*,d.nombre,d.simbolo,d.precio_pesos,d.cambio,d.valor_actual 
                FROM hospedajes AS h 
                JOIN divisas AS d ON d.id = h.divisa
                WHERE h.id = $hospId");

              if($consultaHosp -> num_rows > 0){
                $hospRow = $consultaHosp -> fetch_assoc();
                $precioHospedajeMx = $hospRow['precio'] * $hospRow['cambio'];
                $total += $precioHospedajeMx;
              }
             
          ?>
            <tr>
                <td  style="text-align: left;">
                Inscripción abonable
                </td>
                 <td style="text-align: left;">
                 
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($programaRow['inscripcion'],2) ?>
                </td>
            </tr>
            <?php 
              while ($preciosRows = $consultaPrecios -> fetch_assoc()):
                $precio = $preciosRows['precio'];
                $precioMx = $preciosRows['precio'] * $preciosRows['cambio'];
                $multiplicador = 0;
                  
                for ($i=0; $i <sizeof($rangosArray) ; $i++) { 
                    $temporal = explode('_', $rangosArray[$i]);
                    if($temporal[0] == $preciosRows['id']){
                      $multiplicador =$temporal[1];
                    }
                 }
                if($multiplicador > 0){

                  $precioMx = $precioMx * $multiplicador;
                } 

                $total += $precioMx;
                  
            ?>
              <tr>
                <td  style="text-align: left;">
                 <?=$preciosRows['concepto'] ?>
                </td>
                 <td style="text-align: left;">
                 <?= $preciosRows['simbolo'].number_format($precio,2) ?>
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($precioMx,2) ?>
                </td>
              </tr>
            <?php endwhile  ?>

            <?php if(isset($hospRow)):  ?>
              <tr>
                <td  style="text-align: left;">
                 <?=$hospRow['titulo'] ?>
                </td>
                 <td style="text-align: left;">
                 <?= $hospRow['simbolo'].number_format($hospRow['precio'],2) ?>
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($precioHospedajeMx,2) ?>
                </td>
              </tr>

            <?php endif  ?>
             <tr>
                <td  style="text-align: left;">
                Seguro:
                </td>
                 <td style="text-align: left;">
                 
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($programaRow['seguro'],2) ?>
                </td>
            </tr>
            <tr>
                <td  style="text-align: left;">
                Cuota De Servicio Y Transferencia:
                </td>
                 <td style="text-align: left;">
                 
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($programaRow['cuota'],2) ?>
                </td>
            </tr>
           
        
              <tr>
                <td  style="text-align: left;">
                 
                </td>
                 <td style="text-align: left;">
                  Total
                </td>
                <td style="text-align: left; ">
                 $<?=number_format($total,2) ?>
                </td>
              </tr>

          <?php endwhile  ?>
       </tbody>
      </table>
    </div>



  </div> <!-- /grid -->
</div> <!-- /container -->

<div class="padding-v-50">
</div>

<?=$footer?>

<?=$scriptGNRL?>

</body>
</html>