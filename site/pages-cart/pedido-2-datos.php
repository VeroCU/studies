<?php  
$rfccorrecto=($_SESSION['requierefactura']==1 AND strlen($row_USER['rfc']) < 5)?'uk-hidden':'';

?>
<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

  <style type="text/css">
    #spinner{
      background: rgba(255,255,255,.8);
      position: fixed;
      top: -10000px;
      left: 0;
      width: 100%;
    }
  </style>

</head>

<body>

<?=$header?>

<div class="uk-container" <?=$rfccorrecto?>>
  <div class="uk-width-1-1 margin-top-50">
    <div id="revisardatos">
      <div>
        <p class="text-xl">Revise que todos los datos sean correctos</p>
      </div>
      <div class="uk-child-width-1-3@m" uk-grid>
        <div>
          <div class="uk-card uk-card-body">
            <div>
              <h2>Datos de cliente <a href="#editadatospersonales" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
            </div>
            <div>
              <label for="nombre" class="uk-form-label uk-text-capitalize">nombre</label>
              <?=$row_USER['nombre']?>
            </div>
            <div>
              <label for="email" class="uk-form-label uk-text-capitalize">email</label>
              <?=$row_USER['email']?>
            </div>
            <div>
              <label for="empresa" class="uk-form-label uk-text-capitalize">empresa</label>
              <?=$row_USER['empresa']?>
            </div>
            <div>
              <label for="rfc" class="uk-form-label uk-text-uppercase">rfc</label>
              <?=$row_USER['rfc']?>
            </div>
          </div>
        </div>
        <div>
          <div class="uk-card">
            <div class="uk-card-body">
              <div>
                <h2>Domicilio <a href="#editadomicilio1" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
              </div>
              <div>
                <?=$row_USER['calle']?> #<?=$row_USER['noexterior']?> <?=$row_USER['nointerior']?>
              </div>
              <div>
                <span class="uk-text-muted">Entre</span> &nbsp; <?=$row_USER['entrecalles']?>
              </div>
              <div>
                <?=$row_USER['colonia']?>, <?=$row_USER['cp']?>
              </div>
              <div>
                <?=$row_USER['municipio']?>, <?=$row_USER['estado']?>, <?=$row_USER['pais']?>
              </div>
              <!--<div class="uk-margin">
                <i class="domchange far fa-lg fa-circle uk-text-muted pointer" id="dom1" data-dom="1"></i> Envío a este domicilio
              </div>-->
            </div>
          </div>
        </div>
        <?php 
        if(strlen($row_USER['calle2'])>1){ 
          echo '
        <div>
          <div class="uk-card">
            <div class="uk-card-body">
              <div>
                <h2>Domicilio <a href="#editadomicilio2" uk-toggle><i class="fas fa-lg fa-edit uk-float-right"></i></a></h2>
              </div>
              <div>
                '.$row_USER['calle2'].' #'.$row_USER['noexterior2'].' '.$row_USER['nointerior2'].'
              </div>
              <div>
                <span class="uk-text-muted">Entre</span> &nbsp; '.$row_USER['entrecalles2'].'
              </div>
              <div>
                '.$row_USER['colonia2'].', '.$row_USER['cp2'].'
              </div>
              <div>
                '.$row_USER['municipio2'].', '.$row_USER['estado2'].', '.$row_USER['pais2'].'
              </div>
              <div class="uk-margin">
                <i class="domchange fas fa-lg fa-check-circle color-primary pointer" id="dom2" data-dom="2"></i> Envío a este domicilio
              </div>
            </div>
          </div>
        </div>';

        }else{
          echo '<!--
          <div class="uk-card">
            <div class="uk-card-body">
              <button id="editarbutton" class="uk-button uk-button-default border-orange">Agregar domicilio</button>
            </div>
          </div>-->';
        }
        ?>

      </div>
    </div>
  </div>
  <div class="uk-width-1-1 margen-top-50">
    <div class="uk-panel uk-panel-box">
      <h3 class="uk-text-center"><i class="uk-icon uk-icon-small uk-icon-check-square-o"></i> &nbsp; Productos y cantidades:</h3>
      <table class="uk-table uk-table-striped uk-table-hover uk-table-middle" id="tabla">
        <thead>
          <tr>
            <td width="50px"></td>
              <td >Programa</td>
              <td width="100px" class="uk-text-right">Concepto</td>
              <td width="100px" class="uk-text-right">Precio</td>
              <td width="100px" class="uk-text-right">Importe</td>
          </tr>
        </thead>
        <tbody>

          <?php
          $subtotal=0;
          $num=0;
          if(isset($_SESSION['carro'])){
            //debug($_SESSION);
            foreach ($arreglo as $key){
             
                $prodId=$key['Id'];
                
                $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
                $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
                
              //Cuando es un pago de inscriocion  
              if(isset($key['inscripcion']) and $key['inscripcion']){
                $conceptoTxt = "Inscripción";
                $importe=$row_CONSULTA1['inscripcion']*$key['Cantidad'];
                $concepto=$row_CONSULTA1['inscripcion'];
                $subtotal+=$importe;
              }//cuando es un pago de liquidacion
              else if(isset($key['liquidado']) and $key['liquidado']){
                //Precios base
                $conceptoTxt = "Liquidación";
                $importe=0*$key['Cantidad'];
                $concepto=0;
                $subtotal+=$importe;
              } 
             
              echo '
              <tr>
                <td>
                  <span class="quitar uk-icon-button uk-button-danger" data-id="'.$prodId.'"><i uk-icon="icon:trash"></i></span><br>
                </td>
                <td>
                  <a href="#pics" uk-scroll class="color-general">'.$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'].'</a>
                </td>
                <td class="uk-text-right">
                  '.$conceptoTxt.'
                </td>
                <td class="uk-text-right">
                 -
                </td>
                <td class="uk-text-right">
                  -
                </td>
              </tr>';

              //CASO CUANDO ES LIQUIDACION CONSULTAMOS LOS DETALLES DE LOS PRECIOS ELEGIDOS ANTERIORMENTE POR EL CLIENTE
              if(isset($key['liquidado']) and $key['liquidado']){
                $pedidoInscripcionId = $key['pedido'];
                //restamos la inscripcion, y sumamos la cuota de recuperacion y el seguro
                 $inscripcion = $row_CONSULTA1['inscripcion'];
                 $cuota = $row_CONSULTA1['cuota'];
                 $subtotal += $cuota - $inscripcion + $row_CONSULTA1['seguro'];
                 echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                      Pago de inscripción abonable
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      -'.number_format($inscripcion,2).'
                    </td>
                    <td class="uk-text-right">
                     -'.number_format($inscripcion,2).'
                    </td>
                  </tr>
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                     Cuota de recuperación
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      '.number_format($cuota,2).'
                    </td>
                    <td class="uk-text-right">
                     '.number_format($cuota,2).'
                    </td>
                  </tr>
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                     Seguro
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      '.number_format($row_CONSULTA1['seguro'],2).'
                    </td>
                    <td class="uk-text-right">
                     '.number_format($row_CONSULTA1['seguro'],2).'
                    </td>
                  </tr>';

                 //Consultamos el pedido de inscripcion para obtener el detalle
                $consultaPedidoInscripcion = $CONEXION -> query("SELECT p.*, pd.preciosids, pd.hospedajeid,pd.inscripcion,pd.rangosids
                  FROM pedidos as p
                  JOIN pedidosdetalle as pd ON p.id = pd.pedido and pd.producto = $prodId
                  WHERE p.id = $pedidoInscripcionId AND estatus = 1");

                $rowPedidoInscripcion = $consultaPedidoInscripcion -> fetch_assoc();
                //OBTENEMOS PRECIOS IDS Y HOSPEDAJE ID
                $preciosKeys = $rowPedidoInscripcion['preciosids'];

                $rangosIds = $rowPedidoInscripcion['rangosids'];

                $rangosArray = explode(",", $rangosIds);
               
                $hospedajeId =  $rowPedidoInscripcion['hospedajeid'];
                $sql2 = "SELECT * FROM productos_precios WHERE id IN ($preciosKeys)";
                $consultaPrecios = $CONEXION -> query($sql2);

                while ($priceRows = $consultaPrecios -> fetch_assoc()) {
                
                  $multiplicador = 0;
                  for ($i=0; $i <sizeof($rangosArray) ; $i++) { 
                    $temporal = explode('_', $rangosArray[$i]);
                    if($temporal[0] == $priceRows['id']){
                      $multiplicador =$temporal[1];
                    }
                  }
                 

                 // array_push($preciosarray, $priceRows)
                  $divId = $priceRows['divisa'];
                  //consultamos divisa
                  $consultaDivisa = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divId");
                  $divRows = $consultaDivisa -> fetch_assoc();
                  $titulo = $priceRows['concepto'];
                  //CAMBIAR SI EL PRECIO ES CON DIVISA

                  $precioMx = $priceRows['precio'] * $divRows['cambio'];

                  if($multiplicador > 0){                  
                    $precioMx = $precioMx * $multiplicador;
                  } 
                  
                  $subtotal += $precioMx;

                  echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td >
                     '.$priceRows['concepto'].'
                    </td>
                    <td class="uk-text-right">
                     
                    </td>
                    <td class="uk-text-right">
                    
                    </td>
                    <td class="uk-text-right">
                      '.number_format($precioMx,2).'
                    </td>
                  </tr>
                  ';
                }
                
                $consultaHospedaje = $CONEXION -> query("SELECT * FROM hospedajes WHERE id = $hospedajeId");
                $numHosp = $consultaHospedaje -> num_rows;
                if($numHosp > 0){
                  $hospedajeRow = $consultaHospedaje -> fetch_assoc(); 
                  $divisaHospedajeID = $hospedajeRow['divisa'];
                  //consultamos divisa
                  $divisaQuery = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divisaHospedajeID");
                  $divisaResult = $divisaQuery -> fetch_assoc();
                  $precioHospMx = $hospedajeRow['precio'] * $divisaResult['cambio'];
                  $subtotal += $precioHospMx;

                   echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td >
                     '.$hospedajeRow['titulo'].'
                    </td>
                    <td class="uk-text-right">
                     Hospedaje
                    </td>
                    <td class="uk-text-right">
                    
                    </td>
                    <td class="uk-text-right">
                      '.number_format($precioHospMx,2).'
                    </td>
                  </tr>
                  ';
                }
              }

                $num++;
            
            }
          }

          $envio=$shipping*$carroTotalProds;
          $subtotal=$subtotal+$envio+$shippingGlobal;
          $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
          $total=$subtotal+$iva;

          if ($total>0) {
            if ($shippingGlobal>0) {
              echo '
              <tr>
                <td style="text-align: left;">
                  Envío global
                </td>
                <td style="text-align: right; ">
                  1
                </td>
                <td style="text-align: right; ">
                  '.number_format($shippingGlobal,2).'
                </td>
                <td style="text-align: right; ">
                  '.number_format($shippingGlobal,2).'
                </td>
              </tr>';
            }
            if ($shipping>0) {
              echo '
              <tr>
                <td style="text-align: left; ">
                  Envío por pieza
                </td>
                <td style="text-align: right; ">
                  '.$carroTotalProds.'
                </td>
                <td style="text-align: right; ">
                  '.number_format($shipping,2).'
                </td>
                <td style="text-align: right; ">
                  '.number_format($envio,2).'
                </td>
              </tr>';
            }

            if($taxIVA>0){
              echo '
              <tr>
                <td colspan="3" class="uk-text-right">
                  Subtotal
                </td>
                <td class="uk-text-right">
                  '.number_format($subtotal,2).'
                </td>
              </tr>
              <tr>
                <td colspan="3" class="uk-text-right">
                  IVA
                </td>
                <td class="uk-text-right">
                  '.number_format($iva,2).'
                </td>
              </tr>
              <tr>
                <td colspan="3" class="uk-text-right">
                  Total
                </td>
                <td class="uk-text-right">
                  '.number_format($total,2).'
                </td>
              </tr>
              ';
            }else{
              echo '
              <tr>
                <td colspan="4" class="uk-text-right">
                  Total
                </td>
                <td class="uk-text-right">
                  '.number_format($subtotal,2).'
                </td>
              </tr>';
            }
          }
    echo '
        </tbody>
      </table>
    </div>
  </div>
  <div class="uk-width-1-1 uk-text-center padding-v-50">
    <div uk-grid class="uk-child-width-1-2@s">
      <div class="uk-text-right@m uk-text-center">
        <button data-enlace="procesar-deposito" class="siguiente uk-button uk-button-personal">Depósito o transferencia</button>
      </div>
      <div class="uk-text-left@m uk-text-center">
        <button data-enlace="procesar-pedido" class="siguiente uk-button uk-button-personal">Paga con PayPal</button>
      </div>
    </div>
    <div uk-grid class="uk-child-width-1-2@s">
      <div class="uk-text-right@m uk-text-center">
        <img src="img/design/pago-oxxo.jpg">
      </div>
      <div class="uk-text-left@m uk-text-center">
        <img src="img/design/pago-paypal.jpg">
      </div>
    </div>
  </div>';
?>

</div>

<div class="padding-v-50">
</div>

<?=$footer?>


<div id="editadatospersonales" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div>
      <h2>Editar atos personales</h2>
    </div>
    <div>
      <label for="nombre" class="uk-form-label uk-text-capitalize">nombre</label>
      <input type="text"  data-campo="nombre" value="<?=$row_USER['nombre']?>" class="editaruser uk-input">
    </div>
    <div>
      <label for="email" class="uk-form-label uk-text-capitalize">email</label>
      <input type="text"  data-campo="email" value="<?=$row_USER['email']?>" class="editaruser uk-input">
    </div>
    <div>
      <label for="empresa" class="uk-form-label uk-text-capitalize">empresa</label>
      <input type="text"  data-campo="emprea" value="<?=$row_USER['empresa']?>" class="editaruser uk-input">
    </div>
    <div>
      <label for="rfc" class="uk-form-label uk-text-uppercase">rfc</label>
      <input type="text"  data-campo="rfc" value="<?=$row_USER['rfc']?>" class="editaruser uk-input uk-text-uppercase">
    </div>
    <div class="uk-margin uk-text-center">
      <button class="uk-button uk-button-default uk-modal-close" type="button">Cerrar</button>
     
    </div>
  </div>
</div>

<div id="editadomicilio1" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <button class="uk-modal-close-default" type="button" uk-close></button>
    <div>
      <h2>Domicilio</h2>
    </div>
    <div>
      <label for="calle" class="uk-form-label uk-text-capitalize">calle</label>
      <input type="text" data-campo="calle" value="<?=$row_USER['calle']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="noexterior" class="uk-form-label uk-text-capitalize">no. exterior</label>
      <input type="text" data-campo="noexterior" value="<?=$row_USER['noexterior']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="nointerior" class="uk-form-label uk-text-capitalize">no. interior</label>
      <input type="text" data-campo="nointerior" value="<?=$row_USER['nointerior']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="entrecalles" class="uk-form-label uk-text-capitalize">entrecalles</label>
      <input type="text" data-campo="entrecalles" value="<?=$row_USER['entrecalles']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="pais" class="uk-form-label uk-text-capitalize">pais</label>
      <input type="text" data-campo="pais" value="<?=$row_USER['pais']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="estado" class="uk-form-label uk-text-capitalize">estado</label>
      <input type="text" data-campo="estado" value="<?=$row_USER['estado']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="municipio" class="uk-form-label uk-text-capitalize">municipio</label>
      <input type="text" data-campo="municipio" value="<?=$row_USER['municipio']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="colonia" class="uk-form-label uk-text-capitalize">colonia</label>
      <input type="text" data-campo="colonia" value="<?=$row_USER['colonia']?>" class="editaruser uk-input uk-input-grey" >
    </div>
    <div>
      <label for="cp" class="uk-form-label uk-text-uppercase">cp</label>
      <input type="text" data-campo="cp" value="<?=$row_USER['cp']?>" class="editaruser uk-input uk-input-grey" >
    </div>
  </div>
</div>

<div id="editadomicilio2" uk-modal>
  <div class="uk-modal-dialog uk-modal-body">
    <div>
      <h2>Domicilio 2</h2>
    </div>
    <div>
      <label for="calle" class="uk-form-label uk-text-capitalize">calle</label>
      <input type="text" data-campo="calle2" value="<?=$row_USER['calle2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="noexterior" class="uk-form-label uk-text-capitalize">no. exterior</label>
      <input type="text" data-campo="noexterior2" value="<?=$row_USER['noexterior2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="nointerior" class="uk-form-label uk-text-capitalize">no. interior</label>
      <input type="text" data-campo="nointerior2" value="<?=$row_USER['nointerior2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="entrecalles" class="uk-form-label uk-text-capitalize">entrecalles</label>
      <input type="text" data-campo="entrecalles2" value="<?=$row_USER['entrecalles2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="pais" class="uk-form-label uk-text-capitalize">pais</label>
      <input type="text" data-campo="pais2" value="<?=$row_USER['pais2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="estado" class="uk-form-label uk-text-capitalize">estado</label>
      <input type="text" data-campo="estado2" value="<?=$row_USER['estado2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="municipio" class="uk-form-label uk-text-capitalize">municipio</label>
      <input type="text" data-campo="municipio2" value="<?=$row_USER['municipio2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="colonia" class="uk-form-label uk-text-capitalize">colonia</label>
      <input type="text" data-campo="colonia2" value="<?=$row_USER['colonia2']?>" class="editaruser uk-input uk-input-grey">
    </div>
    <div>
      <label for="cp" class="uk-form-label uk-text-uppercase">cp</label>
      <input type="text" data-campo="cp2" value="<?=$row_USER['cp2']?>" class="editaruser uk-input uk-input-grey">
    </div>
  </div>
</div>


<div id="spinner" class="uk-flex uk-flex-middle uk-flex-center uk-height-viewport">
  <div class="uk-container uk-container-small">
    <div uk-grid class="uk-grid-match">
      <div>
        <div class="uk-flex uk-flex-middle uk-flex-center">
          <div>
            <img src="img/design/logo.png">
          </div>
        </div>
      </div>
      <div>
        <div class="uk-flex uk-flex-middle uk-flex-center">
          <div class="uk-transform-origin-top-right" uk-scrollspy="cls: uk-animation-kenburns; repeat: true">
            <img src="img/design/loading.gif" style="max-width:100px;">
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div id="rfcmodal" class="uk-modal-full" uk-modal>
  <div class="uk-modal-dialog uk-flex uk-flex-center uk-flex-middle uk-height-viewport">
    <div>
      <div class="uk-text-center" style="padding-top:50px;">
        Proporciona RFC:
        <input type="text" class="uk-input editaruser" data-campo="rfc" data-valor="<?=$row_USER['rfc']?>" value="<?=$row_USER['rfc']?>">
        <div class="margin-top-20">
          <button class="uk-modal-close uk-button uk-button-large uk-button-personal uk-text-uppercase" type="button">Guardar</button>
        </div>
      </div>
    </div>
  </div>
</div>

<?=$scriptGNRL?>

<script>
  $('.siguiente').click(function(){
    var datos = $(this).data();
      $('#spinner').css('top','0');
      window.location = (datos.enlace);
  });

  $('#dom2').click(function(){
    var dom2 = $(this).attr("data-dom2");
    if (dom2==0) {
      dom2 = 1;
      $(this).removeClass("fa-square");
      $(this).removeClass("far");
      $(this).removeClass("uk-text-muted");
      $(this).addClass("fa-check");
      $(this).addClass("fas");
      $(this).addClass("color-verde");
    }else{
      dom2 = 0;
      $(this).removeClass("fa-check");
      $(this).removeClass("fas");
      $(this).removeClass("color-verde");
      $(this).addClass("fa-square");
      $(this).addClass("far");
      $(this).addClass("uk-text-muted");
    }
    $(this).attr("data-dom2",dom2);

  });


  $(".editaruser").focusout(function() {
    var campo = $(this).attr("data-campo");
    var valor = $(this).val();
    $.ajax({
      method: "POST",
      url: "./includes/acciones.php",
      data: { 
        editacliente: 1,
        campo: campo,
        valor: valor
      }
    })
    .done(function( response ) {
      console.log( response )
      datos = JSON.parse(response);
      UIkit.notification.closeAll();
      if (datos.estatus==0) {
        UIkit.notification(datos.msj);
      }
    });
  });


  <?php  
    if ($_SESSION['requierefactura']==1 AND strlen($row_USER['rfc']) < 5) {
      echo 'UIkit.modal("#rfcmodal").show();

          UIkit.util.on("#rfcmodal", "hide", function () {
            location.reload();
          }); ';

    }
  ?>
</script>

</body>
</html>