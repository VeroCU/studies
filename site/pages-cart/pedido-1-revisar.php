<?php
  $requiereFactura=(isset($_SESSION['requierefactura']))?$_SESSION['requierefactura']:0;
  $requiereFacturaIcon=(isset($_SESSION['requierefactura']) AND $_SESSION['requierefactura']==1)?'fas fa-check color-verde':'far fa-square uk-text-muted';
  
?>
<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>

<div class="uk-container uk-container-center">
  <form method="post" action="<?=$rutaEstaPagina?>">
    <input type="hidden" name="actualizarcarro" value="1">

    <div class="uk-width-1-1 margin-top-50">
      <div class="uk-panel uk-panel-box">
        <h3 class="uk-text-center"><i class="uk-icon uk-icon-small uk-icon-check-square-o"></i> &nbsp; Productos y cantidades:</h3>
        <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
          <thead>
            <tr>
              <td width="50px"></td>
              <td >Programa</td>
              <td width="100px" class="uk-text-right">Concepto</td>
              <td width="100px" class="uk-text-right">Precio</td>
              <td width="100px" class="uk-text-right">Importe</td>
            </tr>
          </thead>
          <tbody>

          <?php
         
          $subtotal=0;
          $num=0;
          if(isset($_SESSION['carro'])){
         
            foreach ($arreglo as $key) {
             
              $prodId=$key['Id'];
              $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
              $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
              $CONSULTADIVISA = $CONEXION -> query("SELECT * FROM divisas WHERE id = $prodId");
              $divisa = $CONSULTADIVISA ->fetch_assoc();
              $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['titulo'])))).'-.html';
                //Cuando es un pago de inscriocion  
              if(isset($key['inscripcion']) and $key['inscripcion']){
                $conceptoTxt = "Inscripción";
                $importe=$row_CONSULTA1['inscripcion']*$key['Cantidad'];
                $concepto=$row_CONSULTA1['inscripcion'];
                $subtotal+=$importe;
              }//cuando es un pago de liquidacion
              else if(isset($key['liquidado']) and $key['liquidado']){
                //Precios base
                $conceptoTxt = "Liquidación";
                $importe=0*$key['Cantidad'];
                $concepto=0;
                $subtotal+=$importe;
              } 
             
              echo '
              <tr>
                <td>
                  <span class="quitar uk-icon-button uk-button-danger" data-id="'.$prodId.'"><i uk-icon="icon:trash"></i></span><br>
                </td>
                <td>
                  <a href="#pics" uk-scroll class="color-general">'.$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'].'</a>
                </td>
                <td class="uk-text-right">
                  '.$conceptoTxt.'
                </td>
                <td class="uk-text-right">
                  -
                </td>
                <td class="uk-text-right">
                 -
                </td>
              </tr>';

              //CASO CUANDO ES LIQUIDACION CONSULTAMOS LOS DETALLES DE LOS PRECIOS ELEGIDOS ANTERIORMENTE POR EL CLIENTE
              if(isset($key['liquidado']) and $key['liquidado']){
                $pedidoInscripcionId = $key['pedido'];
                //restamos la inscripcion, y sumamos la cuota de recuperacion y el seguro
                 $inscripcion = $row_CONSULTA1['inscripcion'];
                 $cuota = $row_CONSULTA1['cuota'];
                 $subtotal += $cuota - $inscripcion + $row_CONSULTA1['seguro'] ;
                 echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                      Pago de inscripción abonable
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      -'.number_format($inscripcion,2).'
                    </td>
                    <td class="uk-text-right">
                     -'.number_format($inscripcion,2).'
                    </td>
                  </tr>
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                     Cuota de recuperación
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      '.number_format($cuota,2).'
                    </td>
                    <td class="uk-text-right">
                     '.number_format($cuota,2).'
                    </td>
                  </tr>
                  <tr>
                    <td>
                     
                    </td>
                    <td>
                     Seguro
                    </td>
                    <td class="uk-text-right">

                    </td>
                    <td class="uk-text-right">
                      '.number_format($row_CONSULTA1['seguro'],2).'
                    </td>
                    <td class="uk-text-right">
                     '.number_format($row_CONSULTA1['seguro'],2).'
                    </td>
                  </tr>';

                //Consultamos el pedido de inscripcion para obtener el detalle
                $consultaPedidoInscripcion = $CONEXION -> query("SELECT p.*, pd.preciosids, pd.hospedajeid,pd.inscripcion,pd.rangosids
                  FROM pedidos as p
                  JOIN pedidosdetalle as pd ON p.id = pd.pedido and pd.producto = $prodId
                  WHERE p.id = $pedidoInscripcionId AND estatus = 1");

                $rowPedidoInscripcion = $consultaPedidoInscripcion -> fetch_assoc();
                //OBTENEMOS PRECIOS IDS Y HOSPEDAJE ID
                $preciosKeys = $rowPedidoInscripcion['preciosids'];

                $rangosIds = $rowPedidoInscripcion['rangosids'];

                $rangosArray = explode(",", $rangosIds);
               
                $hospedajeId =  $rowPedidoInscripcion['hospedajeid'];
                $sql2 = "SELECT * FROM productos_precios WHERE id IN ($preciosKeys)";
                $consultaPrecios = $CONEXION -> query($sql2);

                while ($priceRows = $consultaPrecios -> fetch_assoc()) {
                
                  $multiplicador = 0;
                  for ($i=0; $i <sizeof($rangosArray) ; $i++) { 
                    $temporal = explode('_', $rangosArray[$i]);
                    if($temporal[0] == $priceRows['id']){
                      $multiplicador =$temporal[1];
                    }
                  }
                 

                 // array_push($preciosarray, $priceRows)
                  $divId = $priceRows['divisa'];
                  //consultamos divisa
                  $consultaDivisa = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divId");
                  $divRows = $consultaDivisa -> fetch_assoc();
                  $titulo = $priceRows['concepto'];
                  //CAMBIAR SI EL PRECIO ES CON DIVISA

                  $precioMx = $priceRows['precio'] * $divRows['cambio'];

                  if($multiplicador > 0){
                  

                    $precioMx = $precioMx * $multiplicador;
                  } 
                  
                  $subtotal += $precioMx;

                  echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td >
                     '.$priceRows['concepto'].'
                    </td>
                    <td class="uk-text-right">
                     
                    </td>
                    <td class="uk-text-right">
                    
                    </td>
                    <td class="uk-text-right">
                      '.number_format($precioMx,2).'
                    </td>
                  </tr>
                  ';
                }
                
                $consultaHospedaje = $CONEXION -> query("SELECT * FROM hospedajes WHERE id = $hospedajeId");
                $numHosp = $consultaHospedaje -> num_rows;
                if($numHosp > 0){
                  $hospedajeRow = $consultaHospedaje -> fetch_assoc(); 
                  $divisaHospedajeID = $hospedajeRow['divisa'];
                  //consultamos divisa
                  $divisaQuery = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divisaHospedajeID");
                  $divisaResult = $divisaQuery -> fetch_assoc();
                  $precioHospMx = $hospedajeRow['precio'] * $divisaResult['cambio'];
                  $subtotal += $precioHospMx;

                   echo '
                  <tr>
                    <td>
                     
                    </td>
                    <td >
                     '.$hospedajeRow['titulo'].'
                    </td>
                    <td class="uk-text-right">
                     Hospedaje
                    </td>
                    <td class="uk-text-right">
                    
                    </td>
                    <td class="uk-text-right">
                      '.number_format($precioHospMx,2).'
                    </td>
                  </tr>
                  ';
                }
              }

              $num++;
            }

            $envio=$shipping*$carroTotalProds;
            $subtotal=$subtotal+$envio+$shippingGlobal;
            $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
            $total=$subtotal+$iva;

            if ($total>0) {
              if ($shippingGlobal>0) {
                echo '
                <tr>
                  <td style="text-align: left;" colspan="2">
                    Envío global
                  </td>
                  <td style="text-align: right; ">
                    1
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shippingGlobal,2).'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shippingGlobal,2).'
                  </td>
                </tr>';
              }
              if ($shipping>0) {
                echo '
                <tr>
                  <td style="text-align: left; " colspan="2">
                    Envío por pieza
                  </td>
                  <td style="text-align: right; ">
                    '.$carroTotalProds.'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($shipping,2).'
                  </td>
                  <td style="text-align: right; ">
                    '.number_format($envio,2).'
                  </td>
                </tr>';
              }

              if($taxIVA>0){
                echo '
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Subtotal
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>
                <tr>
                  <td colspan="4" class="uk-text-right">
                    IVA
                  </td>
                  <td class="uk-text-right">
                    '.number_format($iva,2).'
                  </td>
                </tr>
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($total,2).'
                  </td>
                </tr>
                ';
              }else{
                echo '
                <tr>
                  <td colspan="4" class="uk-text-right">
                    Total
                  </td>
                  <td class="uk-text-right">
                    '.number_format($subtotal,2).'
                  </td>
                </tr>';
              }
            }
          }
    echo '
          </tbody>
        </table>
      </div>
    </div>

    <div style="min-height: 50px;">
    </div>';


    if ($carroTotalProds>0) {

      echo '
       <div class="uk-width-1-1 uk-flex uk-flex-right padding-top-20">
            <i id="factura" class="'.$requiereFacturaIcon.' fa-2x pointer" data-factura="'.$requiereFactura.'"></i> &nbsp;&nbsp;Necesito factura
        </div>
      <div class="uk-width-1-1 uk-text-center margen-v-50">
        <div uk-grid class="uk-flex-center">
          <div>
            <a href="productos" class="uk-button uk-button-large uk-button-default"><i uk-icon="icon:arrow-left;ratio:1.5;"></i> &nbsp; Seguir comprando</a>
          </div>
          <div>
            <span class="emptycart uk-button uk-button-large uk-button-default"><i uk-icon="icon:trash"></i> &nbsp; Vaciar carrito</span>
          </div>
          <div>
            <button class="uk-button uk-button-personal uk-button-large uk-hidden" id="actualizar">Actualizar &nbsp; <i uk-icon="icon:refresh;ratio:1.5;"></i></button>
            <a href="Revisar_datos_personales" class="uk-button uk-button-large uk-button-personal" id="siguiente">Continuar &nbsp; <i uk-icon="icon:arrow-right;ratio:1.5;"></i></a>
          </div>
        </div>
      </div>';
    }else{
      echo '
      <div class="uk-width-1-1 uk-text-center margen-v-50">
        <div class="uk-alert uk-alert-danger text-xl">El carro está vacío</div>
      </div>';
    }
    ?>

  </form>
  

  <div style="min-height: 100px;">
  </div>

  <div uk-grid id="pics">
  <?php
  //SECCCION DE PRODUCTO CON SU FOTO
  if(isset($_SESSION['carro'])){
    foreach ($arreglo as $key) {

      $prodId=$key['Id'];
      
      $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
      $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
      $escuelaId = $row_CONSULTA1['escuelaid'];

      $CONSULTAESCUELA = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $escuelaId");
      $escuelaRow = $CONSULTAESCUELA -> fetch_assoc();
     
      $link=$prodId.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower("programa"))));

      $noPic='img/design/camara.jpg';
      $pic='img/contenido/escuelas/'.$escuelaRow['imagen'];

      $picHtml=(file_exists($pic) AND strlen($escuelaRow['imagen'])>0)?$pic:$noPic;
      $picHtml=(strpos($pic, 'ttp')>0)?$escuelaRow['imagen']:$picHtml;
      echo '
      <div class="uk-width-1-3@m" >
        <a href="'.$link.'" target="_blank">
          <div style="max-width:300px;">
            <img src="'.$picHtml.'" class="max-height-300px"> 
          </div>
          <div class="uk-card uk-card-body uk-card-default uk-width-1-1 uk-text-center" style="max-width:300px;">
            '.$row_CONSULTA1['titulo'].'
          </div>
        </a>
      </div>';
      //El detalle de la cotizacion seleccionada por el cliente
      if(isset($key['preciosIds']) and sizeof($key['preciosIds'] > 0)){
      echo'
      <div class="uk-width-2-3@m" >
        <h2>Detalle de la cotizacion</h2>
        <table class="uk-table uk-table-striped uk-table-hover uk-table-middle">
          <thead>
            <tr>
              <td width="100px" class="uk-text-left">Concepto</td>
              <td width="100px" class="uk-text-left">Precio en Divisa</td>
              <td width="100px" class="uk-text-left">Precio en MN</td>
            </tr>
          </thead>
          <tbody>';
     
          for ($i=0; $i < sizeof($key['preciosIds']); $i++){
            $sql = "SELECT * FROM productos_precios WHERE id = ".$key['preciosIds'][$i];
            $consultaPrecio = $CONEXION -> query($sql);
            $preciosRows = $consultaPrecio ->fetch_assoc();
            $divisa = $preciosRows['divisa'];
            //consultamos divisa
            $consultaDivisa = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divisa");
            $divisaRows = $consultaDivisa -> fetch_assoc();
            $titulo = $preciosRows['concepto'];
            $precioMx = $preciosRows['precio'] * $divisaRows['cambio'];
            
            echo '
              <tr>
             
              <td style="text-align: left;">
               '.$titulo.'
              </td>
               <td style="text-align: left;">
               '.$preciosRows['precio'] .'
              </td>
              <td style="text-align: left; ">
                '.number_format($precioMx,2).'
              </td>
            </tr>'; 
          }
        }
  echo '
        </tbody>
      </table>
    </div>
      ';

      $num++;
    }
  }
  ?>
  </div> <!-- grid -->
</div> <!-- container -->

<div class="uk-width-1-1 uk-text-center margen-top-50">
  &nbsp;
</div>

<?=$footer?>

<?=$scriptGNRL?>

<script type="text/javascript">
  $('#factura').click(function(){
    var factura = $(this).attr("data-factura");
    if (factura==0) {
      factura = 1;
      $(this).removeClass("fa-square");
      $(this).removeClass("far");
      $(this).removeClass("uk-text-muted");
      $(this).addClass("fa-check");
      $(this).addClass("fas");
      $(this).addClass("color-verde");
    }else{
      factura = 0;
      $(this).removeClass("fa-check");
      $(this).removeClass("fas");
      $(this).removeClass("color-verde");
      $(this).addClass("fa-square");
      $(this).addClass("far");
      $(this).addClass("uk-text-muted");
    }
    $(this).attr("data-factura",factura);
    $.ajax({
      method: "POST",
      url: "./includes/acciones.php",
      data: {
        requierefactura: factura
      }
    })
    .done(function( response ) {
      console.log(response);
      datos = JSON.parse(response);
      location.reload();
    });
  });

  $(".quitar").click(function(){
    var id = $(this).data("id");
    $.ajax({
      method: "POST",
      url: "addtocart",
      data: { 
        id: id,
        cantidad: 0,
        removefromcart: 1
      }
    })
    .done(function() {
      location.reload();
    });
  })

  $(".emptycart").click(function(){
    $.ajax({
      method: "POST",
      url: "emptycart",
      data: { 
        emptycart: 1
      }
    })
    .done(function() {
      location.reload();
    });
  })

  $(".cantidad").keyup(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })

  $(".cantidad").focusout(function() {
    var inventario = $(this).attr("data-inventario");
    var cantidad = $(this).val();
    console.log(cantidad);
    inventario=1*inventario;
    cantidad=1*cantidad;
    if(inventario<=cantidad){
      console.log(inventario*2+" - "+cantidad);
      $(this).val(inventario);
    }
    $("#actualizar").removeClass("uk-hidden");
    $("#siguiente").addClass("uk-hidden");
  })
</script>

</body>
</html>