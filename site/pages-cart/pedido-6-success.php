<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body onload="cuentaRegresiva()">

<?=$header?>

<div class="padding-v-100">
  <div class="uk-container">
    <div class="uk-alert-primary uk-text-center" uk-alert>
      <p class="text-xxl">Gracias por su pago</p>
      <p class="text-xl">Será redirigido a su panel de cliente</p>
      <p class="text-xxxl" id="numero">10</p>
    </div>
  </div>
</div>

<?=$footer?>

<?=$scriptGNRL?>

<script>
function cuentaRegresiva() {
  var count = 10;
  var number = document.getElementById('numero');
  var intervalo = setInterval(function(){
    count--; 
    number.innerHTML = count;
    if(count == 0){
      clearInterval(intervalo);
      window.location = ('Mi-Cuenta');
    }
  }, 1000);
}

</script>

</body>
</html>