<?php
// DIRECCIONES
	$rutaFinal='img/contenido/profile/';
	$calle=$row_USER['calle'];
	$noexterior=$row_USER['noexterior'];
	$nointerior=$row_USER['nointerior'];
	$entrecalles=$row_USER['entrecalles'];
	$pais=$row_USER['pais'];
	$estado=$row_USER['estado'];
	$municipio=$row_USER['municipio'];
	$colonia=$row_USER['colonia'];
	$cp=$row_USER['cp'];
	$code = $row_USER['codigo'];

	if($row_USER['voluntario'] == 1){
		$tipoUser = "Voluntario";
	}else{
		$tipoUser = "Cliente";
	}
?>
<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta?>img/design/logo-og.jpg">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>

<?=$header?>

<div class="uk-container uk-container-large padding-v-100">
	<div class="uk-card uk-card-default uk-card-body">
		<div class="uk-grid-match" uk-grid>
			<div class="uk-width-auto@m uk-width-1-1">
				<div>
					<?php
					$profileRuta= 'img/contenido/profile/';
					$noPic =$profileRuta."default.jpg";
					$profilePic = $profileRuta.$row_USER['imagen'].".jpg";
					if(!file_exists($profilePic)){
						$profilePic = $noPic;
					}
					echo '
					<div class="uk-inline-clip uk-transition-toggle">
						<img src="'.$profilePic.'" class="uk-border-rounded" alt="Profile picture">
						<a href="#profilepic" uk-toggle>
							<div class="uk-transition-slide-bottom uk-position-bottom uk-overlay uk-overlay-default uk-flex uk-flex-center uk-flex-middle">
								<p class="uk-h4 uk-margin-remove">Cambiar</p>
							</div>
						</a>
					</div>';
					?>
				</div>
				<div>
					<a href="logout" class="uk-button uk-button-danger">Cerrar sesión</a>
				</div>
			</div>
			<div class="uk-width-1-3@m uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-body">
						<p><i class="fa fa-2x fa-user"></i> &nbsp;&nbsp; Datos de usuario</p>
						<div>
							<button class="uk-button user-badge"><?= $tipoUser ?></button>
						</div>
						<p>
							<span class="uk-text-muted">Nombre:</span> <?=$unombre?><br>
							<span class="uk-text-muted">Email:</span> <?=$uemail?><br>
							<span class="uk-text-muted">Teléfono:</span> <?=$row_USER['telefono']?><br>
							
						</p>
					</div>
				</div>
			</div>
			<div class="uk-width-expand@m uk-width-1-1">
				<div class="uk-card uk-card-default">
					<div class="uk-card-body">
						<p>Datos para pago bancario</p>
						<p>
						<?php
						$CONSULTA= $CONEXION -> query("SELECT bank FROM configuracion WHERE id = 1");
						$rowCONSULTA = $CONSULTA-> fetch_assoc();
						echo nl2br($rowCONSULTA['bank']);
						$socialWhats  = 'https://api.whatsapp.com/send?text='.urlencode($ruta.'code_'.$code);
    					$shareTwitter = 'https://twitter.com/intent/tweet?text='.urlencode($ruta.'code_'.$code).'&ref=plugin&src=share_button&button_hashtag=#'.urlencode('Studies4Life').'&screen_name='.urlencode('Studies4Life').'';
    					$shareFace    = 'https://www.facebook.com/sharer/sharer.php?u='.urlencode($ruta.'code_'.$code);
			
						?>
						</p>
					</div>
				</div>
			</div>
		
			<div class="uk-width-1-2@m">
				<p class="uk-text-xxxl uk-margin-remove">Gana por cada nuevo usuario que recomiendes</p>
				<p class="uk-margin-remove"> Credito disponible : <?= $row_USER['credito'] ?> </p>
			</div>
			<div class="uk-width-1-2@m">
                 <input id="codeinput" class="inputs comparte " type="readonly"  value="<?= $ruta.'code_'.$code ?>">
				<p class="uk-text-center">
                <a class="share color-primary" data-type="shareWhatsapp" href="<?= $socialWhats ?>" target="_blank"> <span uk-icon="icon: whatsapp; ratio: 1.2;" ></span><sub><?= $row_CONSULTA['shareWhatsapp'] ?></sub></a>
                <a class="share color-primary" data-type="shareFacebook" href="<?= $shareFace ?>" target="_blank">&nbsp; <span uk-icon="icon: facebook; ratio: 1.2;" ></span><sub><?= $row_CONSULTA['shareFacebook'] ?></sub>&nbsp; </a>
                <a class="share color-primary" data-type="shareTwitter" href="<?= $shareTwitter ?>" target="_blank"> <span uk-icon="icon: twitter; ratio: 1.2;" ></span><sub><?= $row_CONSULTA['shareTwitter'] ?></sub></a>
				</p>
			</div>

		</div>
		<div uk-grid>
			<div class="uk-width-1-1 padding-v-50">
				<ul uk-tab uk-switcher>
					<li class="uk-active"><a href="">Compras</a></li>
					<li><a href="">Datos personales</a></li>
					<?php if($row_USER['voluntario']==0):?>
					<li><a href="">Enviar documentos</a></li>
					
					<?php endif  ?>
					<li><a href="">Mis programas</a></li>
					<li><a href="">Contraseña</a></li>
				</ul>

				<ul class="uk-switcher">
					<!-- Pedidos -->
					<li>
						<?php
						$CONSULTA = $CONEXION -> query("SELECT * FROM pedidos WHERE invisible = 0 AND uid = $uid ORDER BY id DESC");
						$numPedidos=$CONSULTA->num_rows;
						if ($numPedidos==0) {
							echo '
							<div uk-alert class="uk-alert-danger uk-text-center">
								No ha realizado pedidos
							</div>';
						}else{
							echo '
							<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-responsive uk-text-center">
								<thead>
									<tr>
										<td>Orden no.</td>
										<td>Fecha</td>
										<td>Programa</td>
										<td>Productos</td>
										<td>Importe</td>
										<td>Estatus</td>
										<td>Acciones</td>
									</tr>
								</thead>
								<tbody>
							';
							while($row_CONSULTA = $CONSULTA -> fetch_assoc()){
								$pedido=$row_CONSULTA['id'];

								

								$segundos=strtotime($row_CONSULTA['fecha']);
								$fecha=date('d-m-Y',$segundos);
								$guia='';
								$fichaUpload='';



								// Verificamos el estatus del pedido
								switch ($row_CONSULTA['estatus']) {
									case 0:
										$clasePagar='';
										$estatusLegend='Pendiente';
										$estatusClase='danger';
										$fichaUpload='
										<br><br>
										<a href="#comprobantemodal" class="comprobantebutton uk-button uk-button-primary uk-button-small" uk-toggle data-id="'.$pedido.'">Subir comprobante</a>';
										$fichaUpload.=(strlen($row_CONSULTA['comprobante'])>0 AND file_exists('img/contenido/comprobantes/'.$row_CONSULTA['comprobante']))?'
										<br><br>
										<a href="img/contenido/comprobantes/'.$row_CONSULTA['comprobante'].'" target="_blank" class="uk-button uk-button-white uk-button-small">Ver adjuntos</a>':'';
										break;
									case 1:
										$clasePagar='uk-hidden';
										$estatusLegend='Pagado';
										$estatusClase='primary';
										break;
									case 2:
										$clasePagar='uk-hidden';
										$estatusLegend='Enviado';
										$estatusClase='warning';
										if (strlen($row_CONSULTA['guia'])>0) {
											$guia='<button class="uk-button uk-width-1-1">Guía:<br>'.$row_CONSULTA['guia'].'</button>';
										}
										break;
									case 3:
										$clasePagar='uk-hidden';
										$estatusLegend='Entregado';
										$estatusClase='success';
										break;
								}

								$numProds=0;

								
								$CONSULTA1 = $CONEXION -> query("SELECT * FROM pedidosdetalle WHERE pedido = $pedido");
								while($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()){
									$productotxt =$row_CONSULTA1['productotxt'];
									$numProds+=$row_CONSULTA1['cantidad'];
								}
								echo '
										<tr id="pedido'.$pedido.'">
											<td><span class="uk-hidden@m uk-text-muted">Orden no. </span>'.$pedido.'</td>
											<td><span class="uk-hidden@m uk-text-muted">Fecha </span>'.$fecha.'</td>
											<td><span class="uk-hidden@m uk-text-muted">Programa </span>'.$productotxt.'</td>
											<td><span class="uk-hidden@m uk-text-muted">Productos </span>'.$numProds.'</td>
											<td><span class="uk-hidden@m uk-text-muted">Importe </span>$'.number_format($row_CONSULTA['importe'],2).'</td>
											<td><span class="uk-hidden@m uk-text-muted">Estatus </span>
												<span class="uk-alert-'.$estatusClase.' padding-10" uk-alert>'.$estatusLegend.'</span>
												'.$fichaUpload.'
											</td>
											<td width="270px" class="uk-text-right">
												'.$guia.'
												<a href="'.$row_CONSULTA['idmd5'].'_por_pagar" class="'.$clasePagar.' uk-button uk-button-small uk-button-primary">PayPal</a>
												<button class="borrarpedido uk-button uk-button-small uk-button-danger" data-id="'.$pedido.'">Borrar</button>
												<a href="'.$row_CONSULTA['idmd5'].'_detalle" class="uk-button uk-button-small uk-button-default">Detalles</a>	
											</td>
										</tr>
									';

								$CONSULTA1 = $CONEXION -> query("SELECT * FROM pedidosdetalle WHERE pedido = $pedido");
								while($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()){
									
									$link=$row_CONSULTA1['producto'].'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($row_CONSULTA1['productotxt'])))).'_.html';
								}
								mysqli_free_result($CONSULTA1);
							}
							mysqli_free_result($CONSULTA);
							echo '
								</tbody>
							</table>
							';
						}
						?>
					</li>
					<!-- Datos personales -->
					<li>
						<div class="uk-child-width-1-2@m" uk-grid>
							<div class="uk-width-1-1 uk-text-muted">
								Si alguna información no es correcta, modifíquela aquí y los cambios se guardarán automáticamente.
							</div>
							<div>
								<div>
									<h2>Datos de cliente</h2>
								</div>
								<div>
									<label for="nombre" class="uk-form-label uk-text-capitalize">nombre</label>
									<input type="text" data-campo="nombre" value="<?=$row_USER['nombre']?>" class="editar uk-input uk-input-grey">
								</div>
								<div>
									<label for="email" class="uk-form-label uk-text-capitalize">email</label>
									<input type="text" data-campo="email" value="<?=$row_USER['email']?>" class="editar uk-input uk-input-grey">
								</div>
								<div>
									<label for="telefono" class="uk-form-label uk-text-capitalize">telefono</label>
									<input type="text" data-campo="telefono" value="<?=$row_USER['telefono']?>" class="editar uk-input uk-input-grey">
								</div>
								
								<div>
									<label for="rfc" class="uk-form-label uk-text-uppercase">rfc</label>
									<input type="text" data-campo="rfc" value="<?=$row_USER['rfc']?>" class="editar uk-input uk-input-grey uk-text-uppercase">
								</div>

							</div>
							<div>
								<div>
									<h2>Domicilio fiscal</h2>
								</div>
								<div>
									<label for="calle" class="uk-form-label uk-text-capitalize">calle</label>
									<input type="text" data-campo="calle" value="<?=$calle?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="noexterior" class="uk-form-label uk-text-capitalize">no. exterior</label>
									<input type="text" data-campo="noexterior" value="<?=$noexterior?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="nointerior" class="uk-form-label uk-text-capitalize">no. interior</label>
									<input type="text" data-campo="nointerior" value="<?=$nointerior?>" class="editar uk-input uk-input-grey">
								</div>
								<div>
									<label for="entrecalles" class="uk-form-label uk-text-capitalize">entrecalles</label>
									<input type="text" data-campo="entrecalles" value="<?=$entrecalles?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="pais" class="uk-form-label uk-text-capitalize">pais</label>
									<input type="text" data-campo="pais" value="<?=$pais?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="estado" class="uk-form-label uk-text-capitalize">estado</label>
									<input type="text" data-campo="estado" value="<?=$estado?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="municipio" class="uk-form-label uk-text-capitalize">municipio</label>
									<input type="text" data-campo="municipio" value="<?=$municipio?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="colonia" class="uk-form-label uk-text-capitalize">colonia</label>
									<input type="text" data-campo="colonia" value="<?=$colonia?>" class="editar uk-input uk-input-grey" >
								</div>
								<div>
									<label for="cp" class="uk-form-label uk-text-uppercase">cp</label>
									<input type="text" data-campo="cp" value="<?=$cp?>" class="editar uk-input uk-input-grey" >
								</div>
							</div>
						</div>
					</li>
					<!-- Documentos -->
					<?php if($row_USER['voluntario']==0):?>
					<li>
						<?php
						// Documentos que debe enviar                   
						echo '
						<div class="uk-width-1-1 padding-v-50">
							<div class="uk-container" style="max-width:700px;">
								<h3 class="uk-text-center">Documentos que debe enviar</h3>
								<table class="uk-table uk-table-large uk-table-striped uk-table-hover uk-table-middle uk-table-responsive">
									<thead>
										<tr>
											<th width="120px">Documento</th>
											<th width="250px">Programa</th>
											<th width="80px" class="uk-text-center">Recibido</th>
											<th width="80px" class="uk-text-center">Cargar</th>
										</tr>
									</thead>
									<tbody>';
									$consultaUserProgramas = $CONEXION -> query("
										SELECT up.*, p.categoria AS categoria_id,p.titulo AS nombre_programa, p.id AS programa_id
										FROM user_programas AS up
										JOIN productos AS p ON p.id = up.programa
										WHERE up.uid = $uid AND up.estatus = 1");

									$numProgramas = $consultaUserProgramas -> num_rows;
								 	//$pagarFlag = false;
									$arrayPagar = array();
									if($numProgramas >= 1){
										while ($programasRow = $consultaUserProgramas -> fetch_assoc()) {
											$cat = $programasRow['categoria_id'];
											
											$CONSULTA = $CONEXION -> query("SELECT * FROM programasdocumentos WHERE categoria = $cat");
											$totalDocs = $CONSULTA -> num_rows;
											$aprovadosDocs = 0;
											while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {
												$titulo=$rowCONSULTA['titulo'];
												$docId = $rowCONSULTA['id'];
												$tipoTxt=$rowCONSULTA['titulo'];
												$sql="SELECT * FROM clientesdocumentos WHERE cliente = $uid and documentoid = $docId";
												$estatus=0;
												$CONSULTA2 = $CONEXION -> query($sql);
												if($CONSULTA2 -> num_rows > 0){
													while ($rowCONSULTA2 = $CONSULTA2 -> fetch_assoc()) {
														
														$estatus=$rowCONSULTA2['estatus'];
														if($estatus) $aprovadosDocs++;
													}
												}
												//debug($aprovadosDocs);
												//debug($totalDocs);
												if($aprovadosDocs == $totalDocs){
													array_push($arrayPagar, $programasRow['programa']);
												}

												$estatusIcon=($estatus==0)?'square uk-text-muted':'check-square uk-text-primary';
												$estatusTxt=($estatus==0)?'Pendiente':'Completo';
												$estatusClaud=($estatus==0)?'primary':'default';
												$estatusClaudToogle=($estatus==0)?'uk-toggle':'';

												echo '
												<tr>
													<td>
														'.$titulo.'
													</td>
													<td>
														'.$programasRow['nombre_programa'].'
													</td>
													<td class="uk-text-center@m">
														<span class="uk-text-muted uk-text-light uk-hidden@m">Recibido</span>
														<i class="far fa-lg fa-'.$estatusIcon.' uk-text-muted" uk-tooltip="title: '.$estatusTxt.'"></i>
													</td>
													<td class="uk-text-center@m">
														<a href="#modaldocumentos" '.$estatusClaudToogle.' class="modaldocumentosbutton uk-icon-button uk-button-'.$estatusClaud.'" data-tipo="'.$tipoTxt.'" data-tipotxt="'.$tipoTxt.'" data-docid="'.$docId.'" data-programa="'.$programasRow['programa_id'].'"><i uk-icon="cloud-upload"></i></a>
													</td>
												</tr>';
											}

										}
									}

									echo '
									</tbody>
								</table>
							</div>';

							// Documentos recibidos                         
							echo '
							<div class="uk-width-1-1 uk-margin-top">
								<div class="uk-container">
									<h3 class="uk-text-center">Documentos recibidos</h3>
									<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-small uk-table-responsive">
										<thead>
											<tr>
												<th width="120px">Fecha recibido</th>
												<th width="150px">Documento</th>
												<th width="auto" >Observaciones</th>
												<th width="50px" ></th>
												<th width="50px" ></th>
											</tr>
										</thead>
										<tbody id="app">';

											$tipoTxt='';
											$sql = "SELECT cd.*,d.titulo,d.obligatorio 
												FROM clientesdocumentos as cd
												JOIN programasdocumentos as d ON cd.documentoid = d.id
												WHERE cd.cliente = $uid";
											$CONSULTA1 = $CONEXION -> query($sql);
											while ($rowCONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
												$prodId  = $rowCONSULTA1['id'];
												$tipoTxt =$rowCONSULTA1['titulo'];
												$file = $rowCONSULTA1['file'];
												$observaciones=$rowCONSULTA1['observaciones'];
											echo '
											<tr id="doc'.$prodId.'">
												<td>
													'.fechaCorta($fecha).'
												</td>
												<td class="">
													'.$tipoTxt.'
												</td>
												<td class="">
													'.$observaciones.'
												</td>
												<td class="uk-text-center@m uk-text-right@m">
													<span class="uk-text-muted uk-text-light uk-hidden@m">Acciones:</span>
													<a href="javascript:borrardocumento(\''.$file.'\',\''.$prodId.'\',\'clientesdocumentos\');" uk-icon="trash" class="uk-icon-button uk-button-danger"></a>
												</td>
												<td class="uk-text-center@m">
													<span class="uk-text-muted uk-text-light uk-hidden@m">Descargar:</span>
													<a href="'.$rutaFinal.$file.'" class="uk-icon-button uk-button-primary" target="_blank" uk-icon="cloud-download"</a>
												</td>
											</tr>';
										}

										echo '
										</tbody>
									</table>
								</div>
							</div>
						</div>';
						?>
					</li>
					<!-- CURSOS INSCRITOS Y SUS ESTATUS -->
					<?php endif ?>
					<li>
						<?php
							$consultaProgIns = $CONEXION -> query("
								SELECT up.*, p.categoria AS categoria_id,p.titulo AS nombre_programa, p.id AS programa_id
								FROM user_programas AS up
								JOIN productos AS p ON p.id = up.programa
								WHERE up.uid = $uid");
							$numProgInscritos=$consultaProgIns->num_rows;
							if ($numProgInscritos==0) {
								echo '
								<div uk-alert class="uk-alert-danger uk-text-center">
									No estas inscrito en ningun programa
								</div>';
							}else{
								echo '
								<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-responsive uk-text-center">
									<thead>
										<tr>
											<td>Fecha</td>
											<td>Nombre</td>
											<td>Escuela</td>
											<td>Estatus</td>
											<td>Pagado</td>
											<td>Acciones</td>
										</tr>
									</thead>
									<tbody>
								'; }

								while($rowInscritos = $consultaProgIns -> fetch_assoc()){	
									$programaId =  $rowInscritos['programa'];
									$pagarFlag =false;
										
									if(in_array($programaId, $arrayPagar)){
										
										$pagarFlag =true;
									}
									
									$consultaEscuela = $CONEXION -> query("
										SELECT es.* 
										FROM productos AS p
										JOIN escuelas as es ON es.id = p.escuelaid
										WHERE p.id = $programaId");
							
									$escuelaRow = $consultaEscuela -> fetch_assoc();
									
									$escuelaNombre = $escuelaRow['titulo'];	
									

									$idInscritos = $rowInscritos['id'];
									$pedidoInscripcion = $rowInscritos['pedido'];
									if($rowInscritos['estatus'] == 1){
										$estadoInscripcion = "success";
										$estadoTexto = "Inscrito";
									}
									elseif($rowInscritos['estatus'] == 2){
										$estadoInscripcion = "danger";
										$estadoTexto = "Cancelado";
									}
									else{
										$estadoInscripcion = "primary";
										$estadoTexto = "Registrado";
									}
									if($rowInscritos['liquidado'] == 1){
										$estadoLiquidado = "success";
										$estadoTextoL = "Liquidado";
										$clasePagar='uk-hidden';
									
									}else{
										$estadoLiquidado = "warning";
										$estadoTextoL = "Pendiente";
										$clasePagar='';
										if(!$pagarFlag){
											$clasePagar='uk-hidden';
										}
									}
									echo '
											<tr id="pedido'.$idInscritos .'">
												<td><span class="uk-hidden@m uk-text-muted">Fecha </span>'.$rowInscritos['fecha'].'</td>
												<td><span class="uk-hidden@m uk-text-muted">Productos </span>'.$rowInscritos['nombre_programa'].'</td>
												<td><span class="uk-hidden@m uk-text-muted">Productos </span>'.$escuelaNombre.'</td>
												<td><span class="uk-hidden@m uk-text-muted">Estatus </span>
													<span class="uk-alert-'.$estadoInscripcion.' padding-10" uk-alert>'.$estadoTexto.'</span>
												</td>
												<td><span class="uk-hidden@m uk-text-muted">Pagado </span>
													<span class="uk-alert-'.$estadoLiquidado.' padding-10" uk-alert>'.$estadoTextoL.'</span>
												</td>
												<td width="270px" class="uk-text-right">
													<button class="buybuttonliquidar '.$clasePagar.' uk-button uk-button-small uk-button-primary" data-id="'.$programaId.'" data-pedido="'.$pedidoInscripcion.'">Liquidar Programa</button>
													<button class="cancelarpedido uk-button uk-button-small uk-button-danger" data-id="'.$idInscritos.'">Cancelar</button>
												</td>
											</tr>
										';
								}

						?>
							</tbody>
						</table>
				
					</li>					
					<!-- Contraseña -->
					<li>
						<div class="uk-container uk-container-small">
							<h3>Cambiar contraseña</h3>
							<input type="password" id="pass1" name="pass1" class="uk-input" required>
							<label for="pass2" class="uk-form-label">Repetir contraseña</label>
							<input type="password" id="pass2" name="pass2" class="uk-input" required>
							<button id="enviarpass" class="uk-button uk-button-personal uk-margin-top">Guardar</button>
						</div>
					</li>
				</ul>

			</div>
		</div>
	</div>
</div>

<div class="padding-v-20">
</div>

<?=$footer?>

<div id="profilepic" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<h2 class="uk-modal-title">Cambiar foto de perfil</h2>
		<div id="fileuploader">
			Cargar
		</div>
	</div>
</div>
 
<div id="comprobantemodal" uk-modal>
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<h2 class="uk-modal-title">Subir comprobante</h2>
		<input type="hidden" id="pedidoid">
		<div id="comprobante">
			Cargar
		</div>
	</div>
</div>

<!-- Subir documentos -->
	<div id="modaldocumentos" uk-modal class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<h2 class="uk-modal-title">Subir documentos</h2>
			<p class="uk-text-muted">Sube tu archivo en formato pdf</p>
			<label for="observacionesdocumentos">Observaciones:</label>
			<input type="text" id="observacionesdocumentos" class="uk-input uk-margin">
			<div id="cargardocumentos">
				Cargar
			</div>
		</div>
	</div>
 
<?=$scriptGNRL?>

<!-- Imágenes -->
<link href="library/upload-file/css/uploadfile.custom.css" rel="stylesheet">
<script src="library/upload-file/js/jquery.uploadfile.js"></script>

<script>
	$(document).ready(function() {
		var docId = null;
		var programa = null;
 		$("#fileuploader").uploadFile({
			url:"library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: 'false',
			allowedTypes: "jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview: false,
			returnType:'json',
			onSuccess:function(data){ 
				window.location = ('includes/acciones.php?profilepicchange='+data);
			}
		});

		$('.comprobantebutton').click(function(){
			var id=$(this).attr('data-id');
			$('#pedidoid').val(id);
		})

		$("#comprobante").uploadFile({
			url:"library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: 'false',
			allowedTypes: "jpeg,jpg,png,gif,pdf",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview: false,
			returnType:'json',
			onSuccess:function(data){ 
				var id = $('#pedidoid').val();
				window.location = ('includes/acciones.php?id='+id+'&comprobantefile='+data);
			}
		});

		$("#cargardocumentos").uploadFile({
			url:"includes/accionuploadfile.php?documentosrecibidos=1",
			fileName:"uploadedfile",
			multiple: true,
			maxFileCount:5,
			showDelete: 'false',
			allowedTypes: "jpeg,jpg,png,pdf",
			maxFileSize: 20000000,
			showFileCounter: false,
			showPreview: false,
			returnType:'json',
			onSuccess:function(files,data,xhr){
				console.log("data",data);
				var id = Math.floor((Math.random() * 100000000) + 1);
				
				var observaciones = $("#observacionesdocumentos").val();
				var tabla = 'clientesdocumentos';
			
				<?php
				$app='$("#app").prepend("';
					$app.='<tr id=\'doc"+id+"\'>';
						$app.='<td>';
							$app.=fechaCorta($hoy);
						$app.='</td>';
						$app.='<td class=\'\'>';
							$app.='"+\'Nuevo documento\'+"';
						$app.='</td>';
						$app.='<td class=\'\'>';
							$app.='"+observaciones+"';
						$app.='</td>';
						$app.='<td class=\'uk-text-center@m uk-text-right@m\'>';
							$app.='<span class=\'uk-text-muted uk-text-light uk-hidden@m\'>Acciones:</span>';
							$app.='<a href=\'javascript:borrardocumento(\""+data+"\",\""+id+"\",\"clientesdocumentos\")\' class=\'uk-icon-button uk-button-danger\' uk-icon=\'trash\'></a>';
						$app.='</td>';
						$app.='<td class=\'uk-text-center@m\'>';
							$app.='<span class=\'uk-text-muted uk-text-light uk-hidden@m\'>Archivo</span>';
							$app.='<a href=\''.$rutaFinal.'"+data+"\' class=\'uk-icon-button uk-button-primary\' target=\'_blank\' uk-icon=\'cloud-download\'></a>';
						$app.='</td>';
					$app.='</tr>';
				$app.='");';
				echo $app;
				?>

				UIkit.modal("#modaldocumentos").hide();
				setTimeout(function(){
					$("#cargardocumentos").val("");
					actualizarfilaarchivos(data);
				},1000);
			}
		});

		$("#pass1").keyup(function() {
			var pass  = $("#pass1").val();
			var len   = (pass).length;

			if(len>6){
				$('#pass1').removeClass("uk-form-danger");
				$('#pass1').addClass("uk-form-success");
			}else{
				$('#pass1').removeClass("uk-form-success");
				$('#pass1').addClass("uk-form-danger");
			}
		});

		$("#pass2").keyup(function() {
			var pass  = $("#pass1").val();
			var len   = (pass).length;
			var passc = $(this).val();

			if(len>6){
				$('#pass1').removeClass("uk-form-danger");
				$('#pass2').addClass("uk-form-danger");
				if(pass!=passc){
					$('#pass1').addClass("uk-form-success");
				}else{
					$('#pass2').addClass("uk-form-success");
				}
			}else{
				$('#pass1').addClass("uk-form-danger");
			}

		});

		$('#enviarpass').click(function(){
			var pass1 = $('#pass1').val();
			var pass2 = $('#pass2').val();
			var len   = (pass1).length;

			if (pass1==pass2 && len > 6) {
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						passwordchange: 1,
						pass1: pass1,
						pass2: pass2
					}
				})
				.done(function( response ) {
					console.log(response);
					datos = JSON.parse(response);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msj);
				});
			}
		})

		$(".editar").focusout(function() {
			var id    = '<?=$uid?>';
			var tipo  = 'personal';
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			//console.log('ID: ' + id  + ' - Tipo: ' + tipo  + ' - Campo: ' + campo  + ' - Valor: ' + valor );
			$.ajax({
				method: "POST",
				url: "includes/acciones.php",
				data: { 
					editacliente: 1,
					id: id,
					tipo: tipo,
					campo: campo,
					valor: valor
				}
			})
			.done(function( response ) {
				console.log( response );
				datos = JSON.parse(response);
				UIkit.notification.closeAll();
				UIkit.notification(datos.msj);
			});
		});

		$(".borrarpedido").click(function() {
			var id    = $(this).attr("data-id");

			UIkit.modal.confirm('Está seguro de borrar este pedido?').then(function () {
				console.log('Confirmed.');
				$.ajax({
					method: "POST",
					url: "includes/acciones.php",
					data: { 
						borrarpedido: 1,
						id: id
					}
				})
				.done(function( response ) {
					console.log( response );
					datos = JSON.parse(response);
					UIkit.notification.closeAll();
					UIkit.notification(datos.msj);
					if (datos.estatus==0) {
						$('#pedido'+id).toggle();
					}
				});
			}, function () {
				console.log('Rejected.')
			});
		});

		$(".modaldocumentosbutton").click(function(){
			docId = $(this).data("docid");
			programa = $(this).data("programa");
			console.log("seleccionado.. ",docId);
		});

		function actualizarfilaarchivos(file){
			console.log(file[0]);
			var observaciones = $("#observacionesdocumentos").val();
			var documentoId = docId;
			var userid    = '<?=$uid?>';
			var productoId =programa;
			//console.log("duc",documentoId);
			$("#observacionesdocumentos").val("");
			$.ajax({
				method: "POST",
				url: "includes/acciones.php",
				data: { 
					actualizarfilaarchivos: 1,
					observaciones: observaciones,
					documentoId : documentoId,
					productoId: productoId,
					file: file[0]
				}
			})
			.done(function( response ) {
				console.log(response);
				datos = JSON.parse(response);
				UIkit.notification.closeAll();
				if (datos.estatus!=0) {
					UIkit.notification(datos.msj,{pos:'bottom-right'});		
				}
			});
		}


		// Agregar al carro
		$(".buybuttonliquidar").click(function(){
			var id=$(this).data("id");
			var pedidoId = $(this).data("pedido");
			var cantidad=1;
			$.ajax({
				method: "POST",
				url: "addtocart",
				data: { 
					id: id,
					cantidad: cantidad,
					liquidado:1,
					pedidoid: pedidoId,
					addtocart: 1
				}
			})
			.done(function( msg ) {
				console.log("response",msg);
				datos = JSON.parse(msg);
				UIkit.notification.closeAll();
				UIkit.notification(datos.msg);
				$("#cartcount").html(datos.count);
				$("#cotizacion-fixed").removeClass("uk-hidden");
			});
		});

	});

	function borrardocumento (data,id,tabla) { 
		var statusConfirm = confirm("Realmente desea eliminar esto?");
		if (statusConfirm == true) {
			console.log(id+"-"+data+"-"+tabla);
			$.ajax({
				method: "POST",
				url: "includes/acciones.php",
				data: { 
					borrardocumento: 1,
					data: data,
					tabla: tabla,
					id: id
				}
			})
			.done(function( response ) {
				console.log( response);
				datos = JSON.parse(response);
				UIkit.notification.closeAll();
				if (datos.estatus!=0) {
					UIkit.notification(datos.msj,{pos:'bottom-right'});
				}
				$("#doc"+id).addClass( "uk-invisible" );
			});
		}
	}

</script>

</body>
</html>