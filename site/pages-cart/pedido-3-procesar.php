<?php
if(isset($_SESSION['carro']) AND count($arreglo)>0){
  // Sandobox o Público
  $sandbox=($payPalCliente=='business@efra.biz')?1:0;

  // Formar la tabla del pedido
  $style[0]='style="background-color:#EEEEEE;"';
  $style[1]='style="background-color:#FFF;"';
  $num=0;
      // Requiere factura
  $requiereFactura=(isset($_SESSION['requierefactura']))?$_SESSION['requierefactura']:0;

  $tabla='
      <table style="width: 100%;" cellspacing="2" border="0" bgcolor="#CCC">
        <tr '.$style[1].'>
          <th style="padding: 8px; color: #666; width: 65%; text-align: left; ">Producto</th>
          <th style="padding: 8px; color: #666; width: 15%; text-align: center; ">Descripción</th>
          <th style="padding: 8px; color: #666; width: 15%; text-align: center; ">Cantidad</th>
          <th style="padding: 8px; color: #666; width: 15%; text-align: right; ">Precio</th>
          <th style="padding: 8px; color: #666; width: 15%; text-align: right; ">Importe</th>
        </tr>'; 

  $subtotal=0;
  $count=0;


  $sql = "INSERT INTO pedidos (uid,fecha,factura) VALUES ('$uid','$ahora','$requiereFactura')";
  if($insertar = $CONEXION->query($sql)){
    $pedidoId=$CONEXION->insert_id;
    $idmd5=md5($pedidoId);

    // Build the query string
    $queryString  = "?cmd=_cart";
    $queryString .= "&upload=1";
    $queryString .= "&trackingId=".$pedidoId;
    $queryString .= "&noshipping=0";
    $queryString .= "&charset=utf-8";
    $queryString .= "&currency_code=MXN";
    $queryString .= "&business=" . urlencode($payPalCliente);
    $queryString .= "&return=".$ruta . 'success';
    $queryString .= "&notify_url=".$ruta . urlencode($idmd5.'_IPN');


    foreach ($arreglo as $key) {
      $prodId=$key['Id'];
      
      $CONSULTA1 = $CONEXION -> query("SELECT * FROM productos WHERE id = $prodId");
      $row_CONSULTA1 = $CONSULTA1 -> fetch_assoc();
      //CUANDO SE COBRA LA INSCRIPCION
      if(isset($key['inscripcion']) and $key['inscripcion']){
        $precio=$row_CONSULTA1['inscripcion'];
        $importe=$row_CONSULTA1['inscripcion']*$key['Cantidad'];
        if(isset($key['preciosIds']) && is_array($key['preciosIds'])){
          $preciosIds =  implode(",", $key['preciosIds']);
        }
        if($key['hospedaje'] != null or $key['hospedaje'] != " " ){
          $hospedaje = $key['hospedaje'];
        }else{
          $hospedaje = 0;
        }
        if(isset($key['rangosIds']) && is_array($key['rangosIds'])){
          $rangosIds =  implode(",", $key['rangosIds']);
        }
    
        $conceptoTxt = "inscripción";
        $inscripcion = 1;
      }
      //CUANDO SE COBRA LA LIQUIDACION
      else if(isset($key['liquidado']) and $key['liquidado']){
        $pedidoInscripcionId = $key['pedido'];
        $conceptoTxt = "Liquidación";
        $liquidacion = 1;
        //FALTA AGREGAR EL SEGURO OBLIGATORIO
        $importe =0;//$row_CONSULTA1['precio'];
        $importe += $row_CONSULTA1['cuota'] - $row_CONSULTA1['inscripcion'] + $row_CONSULTA1['seguro'] ;
        
        //Consultamos el pedido de inscripcion para obtener el detalle
        $consultaPedidoInscripcion = $CONEXION -> query("SELECT p.*, pd.preciosids, pd.hospedajeid,pd.inscripcion,pd.rangosids
          FROM pedidos as p
          JOIN pedidosdetalle as pd ON p.id = pd.pedido and pd.producto = $prodId
          WHERE p.id = $pedidoInscripcionId AND estatus = 1");

          $rowPedidoInscripcion = $consultaPedidoInscripcion -> fetch_assoc();
            //OBTENEMOS PRECIOS IDS Y HOSPEDAJE ID
            $preciosKeys = $rowPedidoInscripcion['preciosids'];

            $rangosIds = $rowPedidoInscripcion['rangosids'];
            $rangosArray = explode(",", $rangosIds);
               
            $hospedajeId =  $rowPedidoInscripcion['hospedajeid'];
            $sql2 = "SELECT * FROM productos_precios WHERE id IN ($preciosKeys)";
            $consultaPrecios = $CONEXION -> query($sql2);
          while ($priceRows = $consultaPrecios -> fetch_assoc()) {
             // array_push($preciosarray, $priceRows)
              $divId = $priceRows['divisa'];

              $multiplicador = 0;
              for ($i=0; $i <sizeof($rangosArray) ; $i++) { 
                $temporal = explode('_', $rangosArray[$i]);
                if($temporal[0] == $priceRows['id']){
                  $multiplicador =$temporal[1];
                }
              }
              //consultamos divisa
              $consultaDivisa = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divId");
              $divRows = $consultaDivisa -> fetch_assoc();
              //$titulo = $priceRows['concepto'];
              //CAMBIAR SI EL PRECIO ES CON DIVISA
              

              $precioMx = $priceRows['precio'] * $divRows['cambio'];
             
              if($multiplicador > 0){                  
                    $precioMx = $precioMx * $multiplicador;
              } 
              $importe += $precioMx;
              
          }
          //HOSPEDAJE
          $consultaHospedaje = $CONEXION -> query("SELECT * FROM hospedajes WHERE id = $hospedajeId");
          $numHosp = $consultaHospedaje -> num_rows;
          if($numHosp > 0){
            $hospedajeRow = $consultaHospedaje -> fetch_assoc(); 
            $divisaHospedajeID = $hospedajeRow['divisa'];
            //consultamos divisa
            $divisaQuery = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divisaHospedajeID");
            $divisaResult = $divisaQuery -> fetch_assoc();
            $precioHospMx = $hospedajeRow['precio'] * $divisaResult['cambio'];
            $importe += $precioHospMx;

          }
        $precio = $importe;
      }
     
      $subtotal+=$importe;

      $cantidad=$key['Cantidad'];
      $productotxt=$row_CONSULTA1['sku'].' - '.$row_CONSULTA1['titulo'];
      
      $sql = "INSERT INTO pedidosdetalle (pedido,producto,productotxt,cantidad,precio,importe,inscripcion,preciosids,rangosids,hospedajeid)".
        "VALUES ('$pedidoId','$prodId','$productotxt','$cantidad','$precio','$importe',$inscripcion,'$preciosIds','$rangosIds',$hospedaje)";
      
     if(isset($key['liquidado']) and $key['liquidado']){
        $sql = "INSERT INTO pedidosdetalle (pedido,producto,productotxt,cantidad,precio,importe,liquidacion,preciosids,rangosids,hospedajeid)".
        "VALUES ('$pedidoId','$prodId','$productotxt','$cantidad','$precio','$importe',$liquidacion,'$preciosKeys','$rangosIds',$hospedajeId)";
      }


      $insertar = $CONEXION->query($sql);


      // Descuenta del inventario
      /*$existenciasRestantes=$row_CONSULTA1['existencias']-$cantidad;
      $actualizar = $CONEXION->query("UPDATE productos SET existencias = '$existenciasRestantes' WHERE id = $prodId");*/

      $count++;

      $queryString .= '&item_number_' . $count . '=' . urlencode($prodId);
      $queryString .= '&item_name_' . $count . '=' . urlencode($productotxt);
      $queryString .= '&amount_' . $count . '=' . urlencode(($precio)*(1.16));
      $queryString .= '&quantity_' . $count . '=' . urlencode($cantidad);
      $queryString .= '&shipping_' . $count . '=' . urlencode($shipping*1.16);
      $queryString .= '&shipping2_' . $count . '=0';


      $num++; 
      if ($num==2) { $num=0; }
      $tabla.='
        <tr '.$style[$num].'>
          <td style="padding: 8px; text-align: left; ">
            '.$row_CONSULTA1['sku'].' -
            '.$row_CONSULTA1['titulo'].'
          </td>
            <td style="padding: 8px; text-align: center; ">
            '.$conceptoTxt.'
          </td>
          <td style="padding: 8px; text-align: center; ">
            '.$key['Cantidad'].'
          </td>
          <td style="padding: 8px; text-align: right; ">
            '.number_format($precio,2).'
          </td>
          <td style="padding: 8px; text-align: right; ">
            '.number_format($importe,2).'
          </td>
        </tr>'; 
    }
  }

  $envio=$shipping*$carroTotalProds;
  $subtotal=$subtotal+$envio+$shippingGlobal;
  $iva=($taxIVA>0)?$subtotal*$taxIVA:0;
  $total=$subtotal+$iva;

  if ($total>0) {
    if ($shippingGlobal>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr '.$style[$num].'>
        <td style="padding: 8px; text-align: left; ">
          Envío global
        </td>
        <td style="padding: 8px; text-align: center; ">
          1
        </td>
        <td style="padding: 8px; text-align: right; ">
          '.number_format($shippingGlobal,2).'
        </td>
        <td style="padding: 8px; text-align: right; ">
          '.number_format($shippingGlobal,2).'
        </td>
      </tr>';
    }
    if ($shipping>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr '.$style[$num].'>
        <td style="padding: 8px; text-align: left;">
          Envío por pieza
        </td>
        <td style="padding: 8px; text-align: center;">
          '.$carroTotalProds.'
        </td>
        <td style="padding: 8px; text-align: right;">
          '.number_format($shipping,2).'
        </td>
        <td style="padding: 8px; text-align: right;">
          '.number_format($envio,2).'
        </td>
      </tr>';
    }

    if ($taxIVA>0) {
      $num=($num==0)?1:0;
      $tabla.='
      <tr>
        <td colspan="3" style="padding: 8px;text-align:right;">
          Subtotal
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($subtotal,2).'
        </td>
      </tr>
      <tr>
        <td colspan="3" style="padding: 8px;text-align:right;">
          IVA
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($iva,2).'
        </td>
      </tr>';
    }

    $num=($num==0)?1:0;
    $tabla.='
      <tr '.$style[$num].'>
        <td colspan="4" style="padding: 8px;text-align:right;">
          Total
        </td>
        <td style="padding: 8px;text-align:right;">
          '.number_format($total,2).'
        </td>
      </tr>
      ';
  }

  $tabla.='
    </table>';


	$actualizar = $CONEXION->query("UPDATE pedidos SET 
    idmd5 = '$idmd5',
    tabla = '$tabla',
    importe = '$total',
    cantidad = '$carroTotalProds'
    WHERE id = $pedidoId");

	unset($_SESSION['carro']);


  $CONSULTA = $CONEXION -> query("SELECT * FROM usuarios WHERE id = '$uid'");
  $numUser=$CONSULTA->num_rows;
  if ($numUser>0) {
    $row_CONSULTA = $CONSULTA -> fetch_assoc();
    $nombre=$row_CONSULTA['nombre'];
    $email =$row_CONSULTA['email'];
    $send2user=1;
    $colorPrimary='#333';
    $asunto='Orden #'.$pedidoId.' en '.$Brand;
    $cuerpoMensaje='
      <div style="width:700px;">
        <div style="width:100%;padding-top:20px;color:'.$colorPrimary.';font-size:22px;">
          <b>Gracias por su compra</b>
        </div>
        <div style="width:100%;padding-top:20px;color:'.$colorPrimary.';font-size:17px;">
          A continuaci&oacute;n el resumen de su compra:
        </div>
        <div style="width:100%;padding-top:30px;">
          <a href="'.$ruta.$idmd5.'_revisar.pdf" style="background-color:'.$mailButton.';color:white;text-decoration:none;border-radius:8px;padding:13px;font-weight:700;">Versi&oacute;n PDF</a>
        </div>
        <div style="width:100%;padding-top:30px;">
          '.$tabla.'
        </div>
        <div style="width:100%;text-align:center;padding-top:50px;padding-bottom:50px;font-size:16px;">
          <a style="color:'.$colorPrimary.';text-decoration:none;" href="mailto:'.$destinatario1.'">'.$destinatario1.'</a><br><br>
          <a style="color:'.$colorPrimary.';text-decoration:none;" href="'.$ruta.'">www.'.$dominio.'</a>
        </div>
      </div>';
    include 'includes/sendmail.php';
  }


  if (isset($_GET['deposito'])) {
      header('Location: ' .$idmd5.'_detalle');
  }else{
    //echo 'https://www.paypal.com/cgi-bin/webscr'.$queryString;
  
    if ($sandbox==0) {
      header('Location: https://www.paypal.com/cgi-bin/webscr' .$queryString);
    }else{
      header('Location: https://www.sandbox.paypal.com/cgi-bin/webscr' .$queryString);
    }

  }
}
