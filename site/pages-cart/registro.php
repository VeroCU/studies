<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta.$logoOg?>">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>

<body>
<?=$header?>

<?php 
	if(isset($_SESSION['code'])){
		$codigoReferido = $_SESSION['code'];
		$CONSULTAREFERIDOR = $CONEXION -> query("SELECT id,voluntario FROM usuarios WHERE codigo = '$codigoReferido'");
		$userRow = $CONSULTAREFERIDOR -> fetch_assoc();
		$referidoId = $userRow['id'];
		$voluntarioRefer = $userRow['voluntario'];
	}
?>
<div class="uk-width-2-3@s  margin-v-50 producto-title-2">
	<div class="padding-v-10 border-white-left">
		<div class="uk-flex uk-flex-middle">

			<div class="uk-container color-blanco text-xl">
				<span style="font-weight: 200;">¿Ya has comprado antes?</span> 
			</div>
		</div>
	</div>
</div>
<div class="uk-container">
	<div class="uk-width-1-1">
		<form action="" method="post" onsubmit="return checkForm(this);">
			<input type="hidden" name="login" value="1">
			<div uk-grid class="uk-grid-small">
				<div class="uk-width-1-3@s">
					<label for="email" class="uk-form-label">Email</label>
					<input name="email" class="uk-input uk-input-grey" id="login-email1" type="email" tabindex="5" value="" required autofocus>
				</div>
				<div class="uk-width-1-3@s">
					<label for="pass" class="uk-form-label">Contraseña</label>
					<input name="password" class="uk-input uk-input-grey" id="login-pass1" type="password" tabindex="5" value="" required>
				</div>
				<div class="uk-width-1-3@s uk-margin uk-text-center">
					<button class="uk-button uk-button-default border-orange" value="Entrar" id="" name="enviar" tabindex="5">Entrar</button>
				</div>
			</div>
		</form>
		<div class="uk-width-1-1 uk-text-center">
			<fb:login-button 
				scope="public_profile,email" 
				onlogin="checkLoginState();"
				class="fb-login-button"
				data-size="large"
				data-button-type="continue_with"
				data-show-faces="false"
				>
			</fb:login-button>
			<div class="fbstatus">
			</div>
		</div>
	</div>
</div>
<div class="uk-width-2-3@s  margin-v-50 producto-title-2">
	<div class="padding-v-10 border-white-left">
		<div class="uk-flex uk-flex-middle">
			<?php if(isset($voluntario)):?>
			<div class="uk-container color-blanco text-xl">
				<span style="font-weight: 200;">¿Deseas unirte como voluntario?</span> 
			</div>
			<?php else:  ?>
			<div class="uk-container color-blanco text-xl">
				<span style="font-weight: 200;">Nuevo en el sitio</span> 
			</div>
			<?php endif  ?>
			
		</div>
	</div>
</div>
<div class="uk-container">
	<div class="uk-width-1-1">
		<div class="uk-grid-small uk-child-width-1-3@m margen-bottom-50 margen-top-20" uk-grid>
			<!-- DATOS PERSONALES -->
				<input type="hidden" id="referido_by" name="referido_by" class="uk-input uk-input-grey" value="<?= $referidoId  ?>">
			<div>
				<label class="uk-form-label" for="nombre1">*Nombre</label>
				<input type="text" id="nombre1" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="email1">*Email</label>
				<input type="text" id="email1" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="password">*Contraseña deseada &nbsp <i class="fas fa-eye-slash uk-float-right pointer" data-estatus="0"></i></label>
				<input type="password" id="password" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="telefono1">*Telefono</label>
				<input type="text" id="telefono1" name="xd" class="uk-input uk-input-grey">
			</div>
			<div class="uk-margin">
				<label class="uk-form-label" for="nacimiento">*Fecha de Nacimiento</label>
		        <input class="uk-input uk-form-width-medium" type="date" id="nacimiento" name="xd" laceholder="Medium">
		    </div>

			<!--div>
				<label class="uk-form-label" for="empresa">Empresa</label>
				<input type="text" id="empresa" name="xd" class="uk-input uk-input-grey">
			</div-->
			<!--div>
				<label class="uk-form-label" for="rfc">RFC</label>
				<input type="text" id="rfc" name="xd" class="uk-input uk-input-grey uk-text-uppercase">
			</div-->
			<!-- DOMICILIO 1 -->
			<div>
				<label class="uk-form-label" for="calle">*Calle</label>
				<input type="text" id="calle" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="noexterior">*No. Exterior</label>
				<input type="text" id="noexterior" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="nointerior">No. Interior</label>
				<input type="text" id="nointerior" name="xd" class="uk-input uk-input-grey" >
			</div>
			<div>
				<label class="uk-form-label" for="entrecalles">*Entre calles</label>
				<input type="text" id="entrecalles" name="xd" class="uk-input uk-input-grey" >
			</div>
			<div>
				<label class="uk-form-label" for="pais">Pais</label>
				<input type="text" id="pais" name="xd" class="uk-input uk-input-grey" value="México" readonly>
			</div>
			<div>
				<label class="uk-form-label" for="estado">*Estado</label>
				<input type="text" id="estado" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="municipio">*Municipio</label>
				<input type="text" id="municipio" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="colonia">*Colonia</label>
				<input type="text" id="colonia" name="xd" class="uk-input uk-input-grey">
			</div>
			<div>
				<label class="uk-form-label" for="cp">*Código postal</label>
				<input type="text" id="cp" name="xd" class="uk-input uk-input-grey">
			</div>
			<!-- SI ES MENOR ACTIVAMOS DATOS DE TUTOR -->
			<div class="uk-width-1-1 uk-margin-top uk-hidden" id="ttitulo">
				<hr class="uk-divider-icon">
				Datos de tutor
			</div>
			<div class="uk-grid-small uk-width-1-1 uk-child-width-1-3@m margen-bottom-50 margen-top-20" uk-grid>
				<div class="uk-hidden" id="tnombre">
					<label class="uk-form-label" for="nombre_tutor">*Nombre del tutor</label>
					<input type="text" id="nombre_tutor" name="xd" class="uk-input uk-input-grey">
				</div>
				<div class="uk-hidden" id="tcel">
					<label class="uk-form-label" for="tel_tutor">*Telefono</label>
					<input type="text" id="tel_tutor" name="xd" class="uk-input uk-input-grey">
				</div>
				<div class="uk-hidden" id="temail">
					<label class="uk-form-label" for="email_tutor">*Email</label>
					<input type="text" id="email_tutor" name="xd" class="uk-input uk-input-grey">
				</div>
			</div>
			
			<div class="uk-width-1-1">
				* Campos obligatorios
			</div>
			<div class="uk-width-1-1">
				<input type="hidden" id="dom" value="0">
				<ul class="uk-subnav uk-subnav-pill" uk-switcher>
					<!--
					<li><a href="#" class="domicilio bg-black"  data-estatus="0">Enviar a mi domicilio fiscal</a></li>
					<li><a href="#" class="domicilio" data-estatus="1">Enviar a otro domicilio</a></li>
					-->
					<li></li>
					<li></li>
				</ul>
				<ul class="uk-switcher uk-margin">
					<li></li>
					<li>
						<div class="uk-grid-small uk-child-width-1-3@m margen-bottom-50" uk-grid>
							<!-- DOMICILIO 2 -->
							<div>
								<label class="uk-form-label" for="ca2lle">Calle</label>
								<input type="text" id="calle2" name="xd" class="uk-input uk-input-grey">
							</div>
							<div>
								<label class="uk-form-label" for="no2exterior">No. Exterior</label>
								<input type="text" id="noexterior2" name="xd" class="uk-input uk-input-grey">
							</div>
							<div>
								<label class="uk-form-label" for="no2interior">No. Interior</label>
								<input type="text" id="nointerior2" name="xd" class="uk-input uk-input-grey" >
							</div>
							<div>
								<label class="uk-form-label" for="en2trecalles">Entre calles</label>
								<input type="text" id="entrecalles2" name="xd" class="uk-input uk-input-grey" >
							</div>
							<div>
								<label class="uk-form-label" for="co2lonia">Colonia</label>
								<input type="text" id="colonia2" name="xd" class="uk-input uk-input-grey">
							</div>
							<div>
								<label class="uk-form-label" for="mu2nicipio">Municipio</label>
								<input type="text" id="municipio2" name="xd" class="uk-input uk-input-grey">
							</div>
							<div>
								<label class="uk-form-label" for="es2tado">Estado</label>
								<input type="text" id="estado2" name="xd" class="uk-input uk-input-grey">
							</div>
							<div>
								<label class="uk-form-label" for="pa2is">Pais</label>
								<input type="text" id="pais2" value="México" class="uk-input uk-input-grey" readonly>
							</div>
							<div>
								<label class="uk-form-label" for="cp2">Código postal</label>
								<input type="text" id="cp2" name="xd" class="uk-input uk-input-grey">
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="uk-width-1-1 uk-text-center">
				<button id="send-registro" class="uk-button uk-button-large uk-button-default border-orange uk-margin-top uk-text-nowrap">CONTINUAR &nbsp; <i uk-icon="icon:arrow-right;ratio:1.5;"></i></button>
			</div>
		</div>
	</div>
</div>

<?=$footer?>
 
<?=$scriptGNRL?>

<script>
	var age = 0;
	$(".domicilio").click(function(){
		var estatus=$(this).attr("data-estatus");
		$('#dom').val(estatus);
	})

	function makeid(length) {
		   var result           = '';
		   var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
		   var charactersLength = characters.length;
		   for ( var i = 0; i < length; i++ ) {
		      result += characters.charAt(Math.floor(Math.random() * charactersLength));
		   }
		   return result;
	}

	$("#send-registro").click(function(){
				
		<?php if(isset($voluntario) and $voluntario ==1){
				echo "var voluntario = 1;";
				echo "var credito = ". $creditov.";";
			}else{
				echo "var voluntario = 0;";
				echo "var credito = ". $credito.";";
			}
		?>
		var referido_by = $("#referido_by").val();
		var nombre = $("#nombre1").val();
		var telefono = $("#telefono1").val();
		var email = $("#email1").val();
		var password = $("#password").val();
		var empresa = $("#empresa").val();
		var nacimiento = $("#nacimiento").val();
		var rfc = $("#rfc").val();

		var calle = $("#calle").val();
		var noexterior = $("#noexterior").val();
		var nointerior = $("#nointerior").val();
		var entrecalles = $("#entrecalles").val();
		var pais = $("#pais").val();
		var estado = $("#estado").val();
		var municipio = $("#municipio").val();
		var colonia = $("#colonia").val();
		var cp = $("#cp").val();

		var calle2 = $("#calle2").val();
		var noexterior2 = $("#noexterior2").val();
		var nointerior2 = $("#nointerior2").val();
		var entrecalles2 = $("#entrecalles2").val();
		var pais2 = $("#pais2").val();
		var estado2 = $("#estado2").val();
		var municipio2 = $("#municipio2").val();
		var colonia2 = $("#colonia2").val();
		var cp2 = $("#cp2").val();
		//TUTOR LO TIENE
		var nombre_tutor = $("#nombre_tutor").val();
		var tel_tutor = $("#tel_tutor").val();
		var email_tutor = $("#email_tutor").val();

		var dom = $("#dom").val();

		//generar un codigo unico de usuario
		temporal = nombre.split(" ");
		codigo = temporal[0].toLowerCase() + makeid(6);

		var fallo = 0;
		var alerta = '';
		$('input').removeClass('uk-form-danger');

		// Si seleccionan recoger en tienda no pedimos domicilio
		if(dom!=2){
			if (cp=='') { fallo=1; alerta='Falta cp'; id='cp'; }
			if (colonia=='') { fallo=1; alerta='Falta colonia'; id='colonia'; }
			if (municipio=='') { fallo=1; alerta='Falta municipio'; id='municipio'; }
			if (estado=='') { fallo=1; alerta='Falta estado'; id='estado'; }
			if (pais=='') { fallo=1; alerta='Falta pais'; id='pais'; }
			if (entrecalles=='') { fallo=1; alerta='Falta entrecalles'; id='entrecalles'; }
			if (noexterior=='') { fallo=1; alerta='Falta noexterior'; id='noexterior'; }
			if (calle=='') { fallo=1; alerta='Falta calle'; id='calle'; }
			if (nacimiento=='') { fallo=1; alerta='Falta Fecha de nacimiento'; id='nacimiento'; }
		}
		if (telefono=='') { fallo=1; alerta='Falta telefono'; id='telefono1'; }

		// Contraseña
		var l = password.length;
		if (l<6) { 
			fallo=1; alerta='Contraseña demasiado débil'; id='password';
		}

		// Correo
		if (email=='') { 
			fallo=1; alerta='Falta email'; id='email1';
		}else{
			var n = email.indexOf('@')
			var o = email.indexOf('.')
			if ((n*o)<6) {
				fallo=1; alerta='Proporcione un email válido'; id='email1';
			}
		}
		if (nombre=='') { fallo=1; alerta='Falta nombre'; id='nombre1'; }



		if (fallo == 0) {
			$('#send-registro').html("<div uk-spinner></div>");
			$('#send-registro').prop("disabled",true);
			$('#send-registro').disabled = true;
			UIkit.notification.closeAll();
			UIkit.notification("<div class='uk-text-center color-blanco bg-blue padding-10 text-lg'><i  uk-spinner></i> Espere...</div>");

			$.post("includes/acciones.php",{
				"registrodeusuarios" : 1,
				"referido_by":referido_by,
				"dom" : dom,
				"nombre" : nombre,
				"email" : email,
				"password" : password,
				"empresa" : empresa,
				"nacimiento":nacimiento,
				"rfc" : rfc,
				"telefono" : telefono,
				"calle" : calle,
				"noexterior" : noexterior,
				"nointerior" : nointerior,
				"entrecalles" : entrecalles,
				"pais" : pais,
				"estado" : estado,
				"municipio" : municipio,
				"colonia" : colonia,
				"cp" : cp,
				"calle2" : calle2,
				"noexterior2" : noexterior2,
				"nointerior2" : nointerior2,
				"entrecalles2" : entrecalles2,
				"pais2" : pais2,
				"estado2" : estado2,
				"municipio2" : municipio2,
				"colonia2" : colonia2,
				"cp2" : cp2,
				"tutor_nombre":nombre_tutor,        
				"tutor_email":email_tutor,
				"tutor_tel":tel_tutor,
				"credito":credito,
				"codigo":codigo,
				"voluntario":voluntario
			},function(msg){
				console.log(msg);
				datos = JSON.parse(msg);
				UIkit.notification.closeAll();
				UIkit.notification(datos.msg);
				if(datos.estatus==1){
					$('#send-registro').html("Volver a intentar");
					$('#send-registro').disabled = false;
					$('#send-registro').prop("disabled",false);
				}else{
					setTimeout(function(){
						<?php
						if ($carroTotalProds==0) {
							echo "window.location = ('mi-cuenta');";
						}else{
							echo "window.location = ('$rutaPedido2');";
						}
						?>

					},2000);
				}
			});
		}else{
			UIkit.notification.closeAll();
			UIkit.notification("<div class='bg-danger color-blanco padding-20 text-lg'><i class='fa fa-ban'></i> "+alerta+"</div>");
			$('#'+id).focus();
			$('#'+id).addClass('uk-form-danger');
		}
	});

	$("#email1").focusout(function() {
		var email  = $("#email1").val();
		var n = email.indexOf('@')
		var o = email.indexOf('.')

		if ((n*o)>6) {
			$.post("includes/acciones.php",{
				emailverify: 1,
				email: email
			},function(msg){
				console.log(msg);
				UIkit.notification.closeAll();
				datos = JSON.parse(msg);
				if(datos.estatus==1){
					UIkit.notification(datos.msg);
					$('#email1').removeClass("uk-form-success");
					$("#email1").addClass("uk-form-danger");
				}else{
					$('#email1').removeClass("uk-form-danger");
					$("#email1").addClass("uk-form-success");
				}
			});
		}
	});

	$("#nacimiento").change(function(){
		var currentDate = new Date();
		var birthdate = new Date($(this).val());
		var diff = currentDate.getTime() - birthdate.getTime();
   		age = Math.floor(diff / (1000 * 60 * 60 * 24 * 365.25));
   		console.log(age);
   		if(age < 18){
   			console.log("menor de edad");
   			$("#ttitulo").removeClass("uk-hidden");
   			$("#tnombre").removeClass("uk-hidden");
   			$("#temail").removeClass("uk-hidden");
   			$("#tcel").removeClass("uk-hidden");
   		
   		}else{
   			$("#ttitulo").addClass("uk-hidden");
   			$("#tnombre").addClass("uk-hidden");
   			$("#temail").addClass("uk-hidden");
   			$("#tcel").addClass("uk-hidden");
   		}
	});


	$("#password").keyup(function() {
		var pass  = $("#password").val();
		var len   = (pass).length;

		if(len>6){
			$('#password').removeClass("uk-form-danger");
			$('#password').addClass("uk-form-success");
		}else{
			$('#password').removeClass("uk-form-success");
			$('#password').addClass("uk-form-danger");
		}
	});

	$(".fa-eye-slash").click(function(){
		var estatus = $(this).attr("data-estatus");
		if (estatus==0) {
			$("#password").prop("type","text");
			$(this).attr("data-estatus",1);
		}else{
			$("#password").prop("type","password");
			$(this).attr("data-estatus",0);
		};
	})

</script>

</body>
</html>