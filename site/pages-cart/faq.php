<!DOCTYPE html>
<html lang="<?=$languaje?>">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# website: http://ogp.me/ns/website#">
  <meta charset="utf-8">
  <title><?=$title?></title>
  <meta name="description" content="<?=$description?>">
  
  <meta property="og:type" content="website">
  <meta property="og:title" content="<?=$title?>">
  <meta property="og:description" content="<?=$description?>">
  <meta property="og:url" content="<?=$rutaEstaPagina?>">
  <meta property="og:image" content="<?=$ruta?>img/design/logo-og.jpg">
  <meta property="fb:app_id" content="<?=$appID?>">

  <?=$headGNRL?>

</head>
<body>
<?=$header?>

<div class="uk-container padding-v-100">

  <div class="text-xxl padding-top-20 blue">
    Preguntas frecuentes
  </div>
  
  <div class="padding-v-50">
    <ul uk-accordion="multiple: true" class="uk-list uk-list-striped">      
      <?php
      $open='uk-open';
      $CONSULTA = $CONEXION -> query("SELECT * FROM faq ORDER BY orden");
      while ($row_CONSULTA = $CONSULTA  -> fetch_assoc()) { 
        echo '
      <li class="'.$open.'">
        <a class="uk-accordion-title" href="#">
          <div class="padding-20">
          '.$row_CONSULTA['pregunta'].'
          </div>
        </a>
        <div class="uk-accordion-content border-top">
          <div class="padding-h-10 text-9">
            '.$row_CONSULTA['respuesta'].'
          </div>
        </div>
      </li>';
      $open='';
      }
      ?>

    </ul>
  </div>
</div>

<?=$footer?>

<?=$scriptGNRL?>

</body>
</htm>