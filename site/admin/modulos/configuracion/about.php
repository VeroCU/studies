<?php
echo '
<div uk-grid class="uk-grid-large">

	<div class="uk-width-1-2@l margen-v-50 uk-text-left">
		Acerca de
		<form action="index.php" method="post">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="editartextosconformato" value="1">
			<input type="hidden" name="frame" value="about">
			<textarea class="editor min-height-150" name="about1">'.$rowCONSULTA['about1'].'</textarea>
			<br>
			<div class="uk-text-center">
				<button class="uk-button uk-button-primary">Guardar</button>
			</div>
		</form>
	</div>
';



$pic='../img/contenido/varios/'.$rowCONSULTA['imagen2'];
if(strlen($rowCONSULTA['imagen2'])>0 AND file_exists($pic)){
	$archivo='
	<div class="uk-panel uk-text-center">
		<a href="'.$pic.'" target="_blank">
			<img src="'.$pic.'">
		</a><br><br>
		<button class="uk-button uk-button-danger uk-button-large borrarpicPrincipal"><i uk-icon="icon:trash"></i> Eliminar</button>
	</div>';
}else{
	$archivo='
	<div class="uk-panel uk-text-center">
		<p class="uk-scrollable-box"><i uk-icon="icon:warning;ratio:5;"></i><br><br>
			Falta imagen<br><br>
		</p>
	</div>';
}

echo '
<div class="uk-width-1-2@l margen-v-50 uk-text-left">
	
	<div class="margen-top-50 uk-text-center uk-container uk-container-xsmall">
		Imagen 
		Dimensiones recomendadas: 420 x 630 px<br><br>
		<div uk-grid>
			<div class="uk-width-1-2@s">
				<div id="fileuploaderprincipal">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">
				'.$archivo.'
			</div>
		</div>
	</div>
</div>';

echo '
	<div class="uk-width-1-3@l margen-v-50 uk-text-left" >
		<b>Misión</b>
		<form action="index.php" method="post">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="editartextosconformato" value="1">
			<input type="hidden" name="frame" value="about">
			<textarea class="editor min-height-150" name="about2">'.$rowCONSULTA['about2'].'</textarea>
			<br>
			<div class="uk-text-center">
				<button class="uk-button uk-button-primary">Guardar</button>
			</div>
		</form>
	</div>

	<div class="uk-width-1-3@l margen-v-50 uk-text-left">
		<b>Visión</b>
		<form action="index.php" method="post">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="editartextosconformato" value="1">
			<input type="hidden" name="frame" value="about">
			<textarea class="editor min-height-150" name="about3">'.$rowCONSULTA['about3'].'</textarea>
			<br>
			<div class="uk-text-center">
				<button class="uk-button uk-button-primary">Guardar</button>
			</div>
		</form>
	</div>
</div>
';


$pic2='../img/contenido/varios/'.$rowCONSULTA['imagen4'];
if(strlen($rowCONSULTA['imagen4'])>0 AND file_exists($pic2)){
	$archivo2='
	<div class="uk-panel uk-text-center">
		<a href="'.$pic2.'" target="_blank">
			<img src="'.$pic2.'">
		</a><br><br>
		<button class="uk-button uk-button-danger uk-button-large borrarpic"><i uk-icon="icon:trash"></i> Eliminar</button>
	</div>';
}else{
	$archivo2='
	<div class="uk-panel uk-text-center">
		<p class="uk-scrollable-box"><i uk-icon="icon:warning;ratio:5;"></i><br><br>
			Falta imagen<br><br>
		</p>
	</div>';
}
echo '
<div class="uk-width-1-3@l margen-v-50 uk-text-left">
	<div class="margen-top-50 uk-text-center uk-container uk-container-xsmall">
		<b>Imagen sección Mision-Vision</b><br>
		<div uk-grid>
			<div class="uk-width-1-2@s">
				<div id="fileuploader">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">
				'.$archivo2.'
			</div>
		</div>
	</div>
</div>';



$scripts.='
	$(document).ready(function() {
		$("#fileuploaderprincipal").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpg,jpeg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&frame='.$frame.'&campo=imagen2&fileuploaded=\'+data);
			}
		});
	});	
	// Borrar imagen
	$(".borrarpicPrincipal").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&frame='.$frame.'&campo=imagen2&borrarpicPrincipal=1&id='.$id.'");
		} 
	});
';
$scripts.='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpg,jpeg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&frame='.$frame.'&campo=imagen4&fileuploaded=\'+data);
			}
		});
	});	
	// Borrar imagen
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&frame='.$frame.'&campo=imagen4&borrarpic=1&id='.$id.'");
		} 
	});
';