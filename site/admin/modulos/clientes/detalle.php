	<?php 
	$rutaPics='../img/contenido/profile/';

	$USER = $CONEXION -> query("SELECT * FROM usuarios WHERE id = $id");
	$row_USER = $USER -> fetch_assoc();
	$edad = floor((time() - strtotime($row_USER['nacimiento'])) / 31556926);

	$rows = ($edad < 18) ? 3:2;
	$pic=(strlen($row_USER['imagen'])>1 AND file_exists($rutaPics.$row_USER['imagen'].'.jpg'))?'<img src="'.$rutaPics.$row_USER['imagen'].'.jpg" class="uk-border-rounded" style="max-height:200px;"><br><br><a href="#" data-id="'.$id.'" class="borrar uk-icon-button uk-button-danger uk-box-shadow-large" data-foto="'.$row_USER['imagen'].'" uk-icon="icon:trash;"></a>':'';
	//obtenemos los pro
	echo '
		<div class="uk-width-1-1 margen-v-20">
			<ul class="uk-breadcrumb">
				<li><a href="index.php?seccion='.$seccion.'">Clientes</a></li>
				<li><a href="index.php?seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">Detalle de cliente</a></li>
			</ul>
		</div>
		<!--pestañas -->
		<div class="uk-width-1-1 margen-v-20">
			<div uk-grid style="margin-left: 0;" >
				<ul class="uk-width-1-1"  uk-tab>
				    <li class="uk-active">
				    	<a href="#" class="uk-flex-middle">
				    		<h3>Datos generales</h3>
				    	</a>
				    </li>
				    <li>
				    	<a href="#">
				    		<h3>Documentación del alumno</h3>
				    	</a>
				    </li>
				    <li>
				    	<a href="#">
				    	
				    		<h3>Programas Inscritos</h3>
				    	</a>
				    </li>
				</ul>
				<!--Contenido pestañas-->
				<ul class="uk-switcher uk-margin uk-width-1-1">
				<!-- DATOS GENERALES -->
				    <li>
				    	<div style="min-height: 400px;margin-left:0" uk-grid>
				    		<div class="uk-width-1-3@m">
								<div class="uk-child-1-2@m" uk-grid> 
									<div class="uk-text-center uk-margin-top" id="piccliente" style="max-width:200px;">
									'.$pic.'
									</div>
									<div class="uk-margin-top">
										<h3>Datos Personales</h3>
										<p><span class="uk-text-muted">Número de cliente:</span> '.$id.'</p>
										<p><span class="uk-text-muted">Nombre:</span> '.$row_USER['nombre'].'</p>
										<p><span class="uk-text-muted">Fecha de nacimiento:</span> '.$row_USER['nacimiento'].'</p>
										<p><span class="uk-text-muted">Edad:</span> '.$edad.' años.</p> 
										<p><span class="uk-text-muted">Email:</span> '.$row_USER['email'].'</p>
										<p><span class="uk-text-muted">Telefono:</span> '.$row_USER['telefono'].'</p>
										<p class="uk-text-uppercase"><span class="uk-text-muted">rfc:</span> '.$row_USER['rfc'].'</p>
										<p><span class="uk-text-muted">Credito disponible:</span>  $'.$row_USER['credito'].' MX</p>
										<p><span class="uk-text-muted">Fecha de registro:</span> '.date('d-m-Y',strtotime($row_USER['alta'])).'</p>	
									</div>
								</div>
								
							</div>
							<div class="uk-width-2-3@m">
								<div uk-grid>';
								if($edad < 18){
									echo '
									<div class="uk-width-1-'.$rows.'@m uk-margin-top">
											<h3>Tutor</h3>
											<p><span class="uk-text-muted uk-text-capitalize">Nombre:</span> '.$row_USER['tutor_nombre'].'</p>
											<p><span class="uk-text-muted uk-text-capitalize">Telefono:</span> '.$row_USER['tutor_tel'].'</p>
											<p><span class="uk-text-muted uk-text-capitalize">Email:</span> '.$row_USER['tutor_email'].'</p>
										</div>';
								}
								echo'
									<div class="uk-width-1-'.$rows.'@m uk-margin-top">
										<h3>Domicilio Fiscal</h3>
										<p><span class="uk-text-muted uk-text-capitalize">calle:</span> '.$row_USER['calle'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">no. exterior:</span> '.$row_USER['noexterior'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">no. interior:</span> '.$row_USER['nointerior'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">entrecalles:</span> '.$row_USER['entrecalles'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">pais:</span> '.$row_USER['pais'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">estado:</span> '.$row_USER['estado'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">municipio:</span> '.$row_USER['municipio'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">colonia:</span> '.$row_USER['colonia'].'</p>
										<p><span class="uk-text-muted uk-text-uppercase">cp:</span> '.$row_USER['cp'].'</p>
									</div>
									<div class="uk-width-1-'.$rows.'@m uk-margin-top">
										<h3>Domicilio de entrega</h3>
										<p><span class="uk-text-muted uk-text-capitalize">calle:</span> '.$row_USER['calle2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">no. exterior:</span> '.$row_USER['noexterior2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">no. interior:</span> '.$row_USER['nointerior2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">entrecalles:</span> '.$row_USER['entrecalles2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">pais:</span> '.$row_USER['pais2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">estado:</span> '.$row_USER['estado2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">municipio:</span> '.$row_USER['municipio2'].'</p>
										<p><span class="uk-text-muted uk-text-capitalize">colonia:</span> '.$row_USER['colonia2'].'</p>
										<p><span class="uk-text-muted uk-text-uppercase">cp:</span> '.$row_USER['cp2'].'</p>
									</div>
								</div>
							</div>

				    	</div>

					</li>
				<!-- DOCUMENTACION -->
					<li>
				    	<div style="min-height: 400px;margin-left:0" uk-grid>';
				    		echo '
							<div class="uk-width-1-1 padding-v-50">
								<h3 class="uk-text-center">Documentacion requerida</h3>
								<div class="uk-container" style="max-width:700px;">
									<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-small uk-table-responsive">
										<thead>
											<tr>
												<th width="120px">Documento</th>
												<th width="120px">Programa</th>
												<th width="120px" class="uk-text-center">Revisado</th>
											</tr>
										</thead>
										<tbody>';
										$consultaUserProgramas = $CONEXION -> query("
										SELECT up.*, p.categoria AS categoria_id,p.titulo AS nombre_programa
										FROM user_programas AS up
										JOIN productos AS p ON p.id = up.programa
										WHERE up.uid = $id AND up.estatus = 1");

										$numProgramas = $consultaUserProgramas -> num_rows;
					
										if($numProgramas >= 1){
											while ($programasRow = $consultaUserProgramas -> fetch_assoc()) {
												$cat = $programasRow['categoria_id'];
												$programa = $programasRow['nombre_programa'];
												$CONSULTA = $CONEXION -> query("SELECT * FROM programasdocumentos WHERE categoria = $cat");
												while ($rowCONSULTA = $CONSULTA -> fetch_assoc()) {

													$titulo=$rowCONSULTA['titulo'];
													$docId = $rowCONSULTA['id'];
													$tipoTxt=$rowCONSULTA['titulo'];

													$estatusIcon='off uk-text-muted uk-hidden"></i><span class="uk-text-danger">Faltante</span><i class="';

													$sql="SELECT * FROM clientesdocumentos WHERE cliente = $id and documentoid = $docId";
												
												
													$CONSULTA2 = $CONEXION -> query($sql);

													if($CONSULTA2 -> num_rows > 0){
														while ($rowCONSULTA2 = $CONSULTA2 -> fetch_assoc()) {
															$estatus=$rowCONSULTA2['estatus'];
															$estatusIcon=($estatus==0)?'off uk-text-muted':'on uk-text-primary';
															$idButton=$rowCONSULTA2['id'];
															if($estatus==0){
																$idSendMail[]=$idButton;
															}
														}
													}
													echo '
													<tr>
														<td>
															'.$tipoTxt.'
														</td>
														<td>
															'.$programa.'
														</td>
														<td class="uk-text-center@m">
															<span class="uk-text-muted uk-text-light uk-hidden@m">Recibido</span>
															<i class="estatuschange buttondocumentos fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="clientesdocumentos" data-campo="estatus" data-id="'.$idButton.'" data-valor="'.$estatus.'" data-tipo="'.$tipoTxt.'"></i>
														</td>
													</tr>';
												}

											}
										}
										echo '
										</tbody>
									</table>';
									if (isset($idSendMail)) {
										echo '
										<!--div class="padding-v-50 uk-text-center">
											<button class="uk-button uk-button-primary" id="sendmail"><i class="far fa-lg fa-envelope"></i> &nbsp; Solicitar faltantes</button>
										</div-->';
									}
								echo '
								</div>
							</div>';


							echo '
							<div class="uk-width-1-1 uk-margin-top">
								<div class="uk-container">
									<h3 class="uk-text-center">Documentos recibidos</h3>
									<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-small uk-table-responsive">
										<thead>
											<tr>
												<th width="120px" >Fecha recibido</th>
												<th width="150px" >Documento</th>
												<th width="auto"  >Observaciones</th>
												<th width="50px"  class="uk-text-center">Descargar</th>
												
												<th width="50px"></th>
											</tr>
										</thead>
										<tbody>';

											$CONSULTA1 = $CONEXION -> query("SELECT * FROM clientesdocumentos WHERE cliente = $id");
											$numDocs = $CONSULTA1->num_rows;
											if ($numDocs>0) {
												while ($rowCONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
													$bgNew = "bg1";
													$docId = $rowCONSULTA1['documentoid'];
													$file = $rowCONSULTA1['file'];
													$CONSULTA2 = $CONEXION -> query("SELECT * FROM programasdocumentos WHERE id = $docId");
													$documentoRow = $CONSULTA2 -> fetch_assoc();
													$tipoTxt=$documentoRow['titulo'];
													$prodId = $rowCONSULTA1['id'];
													$observaciones = $rowCONSULTA1['observaciones'];

													echo '
													<tr id="row'.$prodId.'">
														<td class="'.$bgNew.'">
															'.fechaCorta($fecha).'
														</td>
														<td class="'.$bgNew.'">
															'.$tipoTxt.'
														</td>
														<td class="'.$bgNew.'">
															'.$observaciones.'
														</td>
														<td class="'.$bgNew.' uk-text-center@m uk-text-nowrap">
															<a href="'.$rutaPics.$file.'" class="uk-icon-button uk-button-primary" target="_blank" uk-icon="cloud-download"></a>
															<span class="uk-text-muted uk-text-light uk-hidden@m">Descargar</span>
														</td>
														
														<td class="'.$bgNew.' uk-text-center@m uk-text-right@m uk-text-nowrap">
															<button class="uk-width-1-1 uk-button uk-button-danger eliminardocumento" data-id="'.$prodId.'"><i uk-icon="trash"></i> &nbsp; Eliminar</button>
															<span id="vistobutton'.$prodId.'" data-id="'.$prodId.'" data-visto="'.$visto.'" class="vistobutton pointer uk-text-'.$vistoClass.'">'.$vistoTxt.'</span>
														</td>
													</tr>';
												}
											}

										echo '
										</tbody>
									</table>
									<input type="hidden" id="referencia" >
								</div>
							</div>
				    	</div>
					</li>
				<!-- PROGRAMAS INSCRITOS -->
					<li>
				    	<div style="min-height: 400px;margin-left:0" uk-grid>
				    		<div class="uk-width-1-1 padding-v-20">
								<h3 class="uk-text-center">Historial del alumno</h3>
				    		</div>
				    		<table class="uk-table uk-table-hover uk-table-striped uk-table-small" id="tablaproductos">
								<thead>
									<tr>

										<th onclick="sortTable(1)" width="180px">Fecha de Inscripción</th>
										<th onclick="sortTable(2)">Programa</th>
										<th onclick="sortTable(4)">Escuela</th>
										<th onclick="sortTable(2)">Estatus</th>
										<th width="190px"></th>
									</tr>
								</thead>
								<tbody>';

								$consultaProgramasUser = $CONEXION -> query("SELECT up.* , p.titulo,es.titulo AS nombreEscuela
									FROM user_programas AS up 
									JOIN productos AS p ON p.id = up.programa
									JOIN escuelas AS es ON es.id = p.escuelaid
									WHERE up.uid = $id");

								$numProgramas = $consultaProgramasUser -> num_rows;

								while ($progRows = $consultaProgramasUser -> fetch_assoc()){
									
									$estatus = ($progRows['estatus'] == 1) ? "<button class='uk-button uk-button-small uk-button-success'>Inscrito</button>" : "<button class='uk-button uk-button-small uk-button-primary'>Registrado</button>";
									echo '
										<tr>
											<td><span class="uk-hidden">'.$progRows['fecha'].'</span>'.date('d-m-Y',strtotime($progRows['fecha'])).'</td>
											<td>'.$progRows['titulo'].'</td>
											<td>'.$progRows['nombreEscuela'].'</td>
											<td>'.$estatus.'</td>
											<td class="uk-text-right"><a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fas fa-search-plus"></i></a></td>
										</tr>';
								}	

						echo    '</tbody>
							</table>
				    	</div>
					</li>
				</ul>
			</div>
		</div>

	';


	$CONSULTA1 = $CONEXION -> query("SELECT * FROM pedidos WHERE uid = $id");
	$numPedidos=$CONSULTA1->num_rows;
	echo '
		<div class="uk-width-1-1 margen-v-20">
			<h3>Pedidos realizados: '.$numPedidos.'</h3>
			<table class="uk-table uk-table-hover uk-table-striped uk-table-small" id="tablaproductos">
				<thead>
					<tr>
						<th onclick="sortTable(0)" width="60px">No. de orden</th>
						<th onclick="sortTable(1)" width="120px">Fecha</th>
						<th onclick="sortTable(2)">Estatus</th>
						<th onclick="sortTable(3)" width="150px" class="uk-text-center">PDF</th>
						<th onclick="sortTable(4)" width="120px" class="uk-text-right">Importe</th>
						<th width="190px"></th>
					</tr>
				</thead>
				<tbody>';

		while($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()){
			//echo 'Importe: <br>';
			switch ($row_CONSULTA1['estatus']) {
				case 0:
					$estatus = '<button class="uk-button uk-button-small uk-button-default">Registrado</button>';
					break;
				case 1:
					$estatus = '<button class="uk-button uk-button-small uk-button-warning">Pagado</button>';
					break;
				case 2:
					$estatus = '<button class="uk-button uk-button-small uk-button-primary">Enviado</button>';
					break;
				case 3:
					$estatus = '<button class="uk-button uk-button-small uk-button-success">Entregado</button>';
					break;
			}
			$link='index.php?seccion=pedidos&subseccion=detalle&id='.$row_CONSULTA1['id'];
			echo '
					<tr>
						<td>'.$row_CONSULTA1['id'].'</td>
						<td><span class="uk-hidden">'.$row_CONSULTA1['fecha'].'</span>'.date('d-m-Y',strtotime($row_CONSULTA1['fecha'])).'</td>
						<td>'.$estatus.'</td>
						<td class="uk-text-center"><a href="../'.$row_CONSULTA1['idmd5'].'_revisar.pdf" target="_blank" class="uk-button uk-button-default"><i class="fa fa-file-pdf-o"></i> Ver pdf</a></td>
						<td class="uk-text-right">$'.number_format($row_CONSULTA1['importe'],2).'</td>
						<td class="uk-text-right"><a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fas fa-search-plus"></i></a></td>
					</tr>';
		}
		

	echo'
				</tbody>
			</table>
		</div>



		<div>
			<div id="buttons">
				<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
			</div>
		</div>

	';




	$scripts='
			$(document).ready(function(){
				$(".borrar").on("click", function (e) {
					var foto = $(this).attr("data-foto");

					UIkit.modal.confirm("Desea borrar esta foto?").then(function() {
						$.ajax({
							method: "POST",
							url: "modulos/'.$seccion.'/acciones.php",
							data: { 
								picdelete: 1,
								foto: foto,
								id: '.$id.'
							}
						})
						.done(function( msg ) {
							UIkit.notification.closeAll();
							UIkit.notification(msg);
							$("#piccliente").fadeOut();
						});
					}, function () {
						console.log("Rechazado")
					});
				});
			});


		// Eliminar documento                       
			$(".eliminardocumento").click(function() {    
				var id = $("#referencia").attr("data-id");
				console.log(id);
				var statusConfirm = confirm("Realmente desea eliminar esto?"); 
				if (statusConfirm == true) { 
					$.ajax({
						method: "POST",
						url: "modulos/'.$seccion.'/acciones.php",
						data: { 
							borrardocumentoenviado: 1,
							tabla: "'.$tabla.'",
							id: id
						}
					})
					.done(function( response ) {
						console.log( response );
						UIkit.notification.closeAll();
						UIkit.notification( response, {pos:"bottom-right"} );
						$("#row"+id).fadeOut( "slow" );
						UIkit.modal("#acciones").hide();
					});
				}
			});


		// Marcar como visto o pendiente                            
			$(".vistobutton").click(function(){
				var id = $(this).attr("data-id");
				var visto = $(this).attr("data-visto");
				$.ajax({
					method: "POST",
					url: "modulos/'.$seccion.'/acciones.php",
					data: { 
						marcarvisto: 1,
						visto: visto,
						tabla: "'.$tabla.'",
						id: id
					}
				})
				.done(function( response ) {
					console.log(response);
					datos = JSON.parse(response);
					UIkit.notification.closeAll();
					if(datos.estatus==0){
						if(visto==0){
							$("#vistobutton"+id).attr("data-visto",1);
							$("#vistobutton"+id).text("Visto");
							$("#vistobutton"+id).removeClass("uk-text-danger");
							$("#vistobutton"+id).addClass("uk-text-muted");
							$("#row"+id+" > td").removeClass("bg1");
						}else{
							$("#vistobutton"+id).attr("data-visto",0);
							$("#vistobutton"+id).text("Nuevo");
							$("#vistobutton"+id).removeClass("uk-text-muted");
							$("#vistobutton"+id).addClass("uk-text-danger");
							$("#row"+id+" > td").addClass("bg1");
						}
					}else{
						UIkit.notification(datos.msj, {pos:"bottom-right"} );
					}
				});
			});



	';