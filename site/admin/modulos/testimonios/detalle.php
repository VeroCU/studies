<?php 
//$dominio='//localhost/sites/cleaningbrands/site';

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();

$thisId = $row_catalogo['id'];


$fecha=$row_catalogo['fecha'];
// Calificación
	$star1='';
	$star2='';
	$star3='';
	$star4='';
	$star5='';
	$stars1='r';
	$stars2='r';
	$stars3='r';
	$stars4='r';
	$stars5='r';
	$calificacion=$row_catalogo['qualify'];
	switch ($calificacion) {
		case 1:
			$star1='color-primary';
			$stars1='s';
		break;
		case 2:
			$star1='color-primary';
			$star2='color-primary';
			$stars1='s';
			$stars2='s';
		break;
		case 3:
			$star1='color-primary';
			$star2='color-primary';
			$star3='color-primary';
			$stars1='s';
			$stars2='s';
			$stars3='s';
		break;
		case 4:
			$star1='color-primary';
			$star2='color-primary';
			$star3='color-primary';
			$star4='color-primary';
			$stars1='s';
			$stars2='s';
			$stars3='s';
			$stars4='s';
		break;
		case 5:
			$star1='color-primary';
			$star2='color-primary';
			$star3='color-primary';
			$star4='color-primary';
			$star5='color-primary';
			$stars1='s';
			$stars2='s';
			$stars3='s';
			$stars4='s';
			$stars5='s';
		break;
	}

	$estrellas='<i onclick=\'qualify('.$thisId.',1)\' class=\'fa'.$stars1.' fa-star '.$star1.' pointer qualify'.$thisId.' qualify'.$thisId.'-1\'></i><i onclick=\'qualify('.$thisId.',2)\' class=\'fa'.$stars2.' fa-star '.$star2.' pointer qualify'.$thisId.' qualify'.$thisId.'-2\'></i><i onclick=\'qualify('.$thisId.',3)\' class=\'fa'.$stars3.' fa-star '.$star3.' pointer qualify'.$thisId.' qualify'.$thisId.'-3\'></i><i onclick=\'qualify('.$thisId.',4)\' class=\'fa'.$stars4.' fa-star '.$star4.' pointer qualify'.$thisId.' qualify'.$thisId.'-4\'></i><i onclick=\'qualify('.$thisId.',5)\' class=\'fa'.$stars5.' fa-star '.$star5.' pointer qualify'.$thisId.' qualify'.$thisId.'-5\'></i>';


echo '
<div class="uk-width-1-2@s margen-v-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?seccion='.$seccion.'&subseccion=contenido">'.$seccion.'</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].'</a></li>
	</ul>
</div>
<div class="uk-width-1-2@s uk-text-right margen-v-20">
	<a href="index.php?seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="uk-button uk-button-white">Nuevo</a>
	<a href="index.php?seccion='.$seccion.'&subseccion=contenido&cat='.$cat.'" class="uk-button uk-button-primary">Regresar</a>
</div>


<div class="uk-width-1-1 margen-top-20 uk-form">
	<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
		<input type="hidden" name="editar" value="1">
		<input type="hidden" name="seccion" value="'.$seccion.'">
		<input type="hidden" name="subseccion" value="detalle">
		<input type="hidden" name="cat" value="'.$cat.'">
		<input type="hidden" name="id" value="'.$id.'">
		<div uk-grid>
			<div class="uk-width-1-2">
				<div class="margen-top-20">
					<label for="titulo">Nombre</label>
					<input type="text" class="uk-input" name="titulo" value="'.$row_catalogo['titulo'].'" required>
				</div>
				<div class="margen-top-20">
					<label for="email">Email</label>
					<input type="text" class="uk-input" name="email" value="'.$row_catalogo['email'].'" required>
				</div>
				<div class="uk-margin-top">
					<label for="fecha">Fecha</label>
					<input type="date" name="fecha" class="uk-input" value="'.$fecha.'">
				</div>
				<div class="uk-margin-top">
					<label for="fecha">Calificación</label>
					'.$estrellas.'
				</div>
			</div>
			<div class="uk-width-1-2">
				<div class="margen-top-20">
					<label for="txt">Testimonio</label>
					<textarea class="editor" name="txt">'.$row_catalogo['txt'].'</textarea>
				</div>
			</div>
			<div class="uk-width-1-1 margen-top-20 uk-text-center">
				<a href="index.php?seccion='.$seccion.'&cat='.$cat.'" class="uk-button uk-button-white uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</div>
	</form>
</div>


<!--div class="uk-width-1-2@s padding-v-50">
	<div class="uk-text-muted">
		Archivos tipo: JPG
	</div>
	<div id="fileuploader">
		Cargar
	</div>
</div-->



<!--div class="uk-width-1-2 uk-flex-center uk-flex uk-flex-middle">';
	$pic='../img/contenido/'.$seccion.'/'.$row_catalogo['imagen'];
	if(file_exists($pic) AND strlen($row_catalogo['imagen'])>0){
		echo '
		<div class="uk-card uk-card-default uk-card-body uk-text-center">
			<a href="'.$pic.'" class="uk-icon-button uk-button-default" target="_blank" uk-icon="icon:image"></a> &nbsp;
			<a href="javascript:eliminaPic()" class="uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></a>
			<br>
			<img src="'.$pic.'" class="uk-border-rounded margen-v-20">
		</div>';
	}else{
		echo '
		<div>
			<i uk-icon="icon:warning;ratio:4;"></i>
			<br>
			Sin imagen
		</div>';
	}
	echo '
	</div>
</div-->
';


$scripts='
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&imagen=\'+data);
			}
		});
	});

	// Eliminar foto
	function eliminaPic () { 
		var statusConfirm = confirm("Realmente desea eliminar esta foto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarPic=1");
		} 
	};

	function qualify(id,qualify) {
		var num=0;
		$(".qualify"+id).removeClass("color-primary");
		$(".qualify"+id).removeClass("fas");
		$(".qualify"+id).addClass("far");
		while(num<=qualify){
			$(".qualify"+id+"-"+num).removeClass("far");
			$(".qualify"+id+"-"+num).addClass("color-primary");
			$(".qualify"+id+"-"+num).addClass("fas");
			num++;
		}
		$.post("modulos/'.$seccion.'/acciones.php",
		{
			calificar: 1,
			id: id,
			qualify: qualify
		})
		.done(function( response ) {
			UIkit.notification.closeAll();
			UIkit.notification(response);
		});
	}

	';



