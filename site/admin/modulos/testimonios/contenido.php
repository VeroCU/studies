

<div class="uk-width-auto margin-top-20 uk-text-left">
	<ul class="uk-breadcrumb">
		<?php 
		echo '
		<li><a href="index.php?seccion='.$seccion.'" class="color-red">'.$seccion.'</a></li>
		';
		?>
	</ul>
</div>
<div class="uk-width-expand margin-top-20">
	<div uk-grid class="uk-flex-right">
		<div>
			<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=nuevo" class="uk-button uk-button-success"> 
				<i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nuevo</a>

		</div>
		
	</div>
</div>

<div class="uk-width-medium-1-1 margen-v-20">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-middle uk-table-responsive">
		<thead>
			<tr class="uk-text-muted">
				<th width="30px"></th>
				<th width="150px">Fecha</th>
				<th>Nombre</th>
				<th width="250px" >Email</th>
				<th width="120px"></th>
			</tr>
		</thead>
		<tbody id="sortable">
		<?php
		$productos = $CONEXION -> query("SELECT * FROM $seccion ORDER BY orden");
		while ($row_productos = $productos -> fetch_assoc()) {
			$prodID=$row_productos['id'];

			$inicioButton=($row_productos['inicio']==1)?'success':'white';

			$link='index.php?seccion='.$seccion.'&subseccion=detalle&id='.$row_productos['id'];

			$thisId = $row_productos['id'];
			$pedidoID = $row_productos['pedido'];
			$prodID = $row_CONSULTAR['producto'];
			

			// Calificación
			$star1='';
			$star2='';
			$star3='';
			$star4='';
			$star5='';
			$stars1='r';
			$stars2='r';
			$stars3='r';
			$stars4='r';
			$stars5='r';
			$calificacion=$row_productos['qualify'];
			switch ($calificacion) {
				case 1:
					$star1='color-primary';
					$stars1='s';
				break;
				case 2:
					$star1='color-primary';
					$star2='color-primary';
					$stars1='s';
					$stars2='s';
				break;
				case 3:
					$star1='color-primary';
					$star2='color-primary';
					$star3='color-primary';
					$stars1='s';
					$stars2='s';
					$stars3='s';
				break;
				case 4:
					$star1='color-primary';
					$star2='color-primary';
					$star3='color-primary';
					$star4='color-primary';
					$stars1='s';
					$stars2='s';
					$stars3='s';
					$stars4='s';
				break;
				case 5:
					$star1='color-primary';
					$star2='color-primary';
					$star3='color-primary';
					$star4='color-primary';
					$star5='color-primary';
					$stars1='s';
					$stars2='s';
					$stars3='s';
					$stars4='s';
					$stars5='s';
				break;
			}

			$estrellas='<i onclick=\'qualify('.$thisId.',1)\' class=\'fa'.$stars1.' fa-star '.$star1.' pointer qualify'.$thisId.' qualify'.$thisId.'-1\'></i><i onclick=\'qualify('.$thisId.',2)\' class=\'fa'.$stars2.' fa-star '.$star2.' pointer qualify'.$thisId.' qualify'.$thisId.'-2\'></i><i onclick=\'qualify('.$thisId.',3)\' class=\'fa'.$stars3.' fa-star '.$star3.' pointer qualify'.$thisId.' qualify'.$thisId.'-3\'></i><i onclick=\'qualify('.$thisId.',4)\' class=\'fa'.$stars4.' fa-star '.$star4.' pointer qualify'.$thisId.' qualify'.$thisId.'-4\'></i><i onclick=\'qualify('.$thisId.',5)\' class=\'fa'.$stars5.' fa-star '.$star5.' pointer qualify'.$thisId.' qualify'.$thisId.'-5\'></i>';

			$picTxt='';
			$pic='../img/contenido/'.$seccion.'/'.$row_productos['imagen'];
			if(file_exists($pic) AND strlen($row_productos['imagen'])>0){
				$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}

			echo '
			<tr id="'.$row_productos['id'].'">
				<td>
					'.$picTxt.'
				</td>
				<td class="uk-text-center">
					'.$row_productos['fecha'].'
				</td>
				<td>
					'.$row_productos['titulo'].'
				</td>
				<td>
					'.$row_productos['email'].'
				</td>
				<td class="uk-text-center">
					'.$estrellas.'
				</td>
				<td class="uk-text-center">
					<span data-id="'.$row_productos['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<a href="'.$link.'" class="uk-icon-button uk-button-primary" uk-icon="icon:pencil"></a>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<?php
$scripts='

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion=contenido&borrarPod&id="+id);
		} 
	});

	function qualify(id,qualify) {
		var num=0;
		$(".qualify"+id).removeClass("color-primary");
		$(".qualify"+id).removeClass("fas");
		$(".qualify"+id).addClass("far");
		while(num<=qualify){
			$(".qualify"+id+"-"+num).removeClass("far");
			$(".qualify"+id+"-"+num).addClass("color-primary");
			$(".qualify"+id+"-"+num).addClass("fas");
			num++;
		}
		$.post("modulos/'.$seccion.'/acciones.php",
		{
			calificar: 1,
			id: id,
			qualify: qualify
		})
		.done(function( response ) {
			UIkit.notification.closeAll();
			UIkit.notification(response);
		});
	}

	';



?>