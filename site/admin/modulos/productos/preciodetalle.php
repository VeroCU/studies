<?php
$consoltaPrecio = $CONEXION -> query("SELECT * FROM productos_precios WHERE id = $id");
$row_precio = $consoltaPrecio -> fetch_assoc();
$productoId = $row_precio['producto'];
$estatusIcon=($row_precio['periodo']==0)?'off uk-text-muted':'on uk-text-primary';
$divisaId = $row_precio['divisa'];

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $productoId");
$row_catalogo = $consulta -> fetch_assoc();
$productName = $row_catalogo['titulo'];
$cat = $row_catalogo['categoria'];
$periodo = ["Anual","Semestral","Cuatrimestral","Trimestral","Bimestral","Mensual","Semanal"];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['titulo'];

echo '
	<div class="uk-width-auto margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>	
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" >'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$productoId.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=preciodetalle&id='.$id.'" class="color-red">Detalle precio</a></li>
		</ul>
	</div>

	<div class="uk-width-expand@l uk-text-right margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		
			
		</div>
	</div>
	<div class="uk-width-1-1@s  margin-v-20">
		<div class="uk-container uk-container-small">
			<h3 class="uk-text-center">Detalles precio</h3>
			<div class="uk-card uk-card-default uk-card-body uk-width-expand@m">
			    <div>
					<span class="uk-text-capitalize uk-text-muted"><strong> Concepto: </strong></span>
					<input type="text" class="editarajax uk-input " name="inicio" min="0" value="'.$row_precio['concepto'].'" data-tabla="productos_precios" data-campo="concepto" data-id="'.$id.'">
				</div>
				<div>
					<span class="uk-text-capitalize uk-text-muted"><strong>Precio: $</strong></span>
					<input type="number" class="editarajax uk-input " name="inicio" step="any" value="'.$row_precio['precio'].'" data-tabla="productos_precios" data-campo="precio" data-id="'.$id.'">
					
				</div>
				<div>
					<label class="uk-text-capitalize" for="periodotxt" >Periodo de cobro</label>
					<input type="text" class="editarajax uk-input" name="periodotxt" value="'.$row_precio['periodotxt'].'" data-tabla="productos_precios" data-campo="periodotxt" data-id="'.$id.'">
				</div>
				<div>
					<span class="uk-text-capitalize uk-text-muted"><strong>Descuento:</strong></span>
					<input type="number" class="editarajax uk-input" name="inicio" min="0" max="100" value="'.$row_precio['descuento'].'" data-tabla="productos_precios" data-campo="descuento" data-id="'.$id.'">
				</div>
				<div>
					<label class="uk-text-capitalize" for="divisa">Divisa</label>
					<select name="divisa" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1 editarajax" data-tabla="productos_precios" data-campo="divisa" data-id="'.$id.'">';
					$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas");
					while ($rowDivisas = $consultaDivisas -> fetch_assoc()) {
						
						$nombreDivisa=$rowDivisas['nombre'];
						if (isset($divisaId) AND $divisaId==$rowDivisas['id']) {
								$estatus='selected';
							}else{
								$estatus='';
							}
						
							echo '
								<option value="'.$rowDivisas["id"].'" '.$estatus.'>'.$nombreDivisa.'</option>';
					}
								echo '
								</select>
				</div>';

echo'	
				<div class="uk-margin uk-grid-small uk-child-width-auto uk-grid">
		        
					<i id="periodo" class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="productos_precios" data-campo="periodo" data-id="'.$id.'" data-valor="'.$row_precio['periodo'].'"></i>&nbsp;&nbsp;Rango (opcional) </label>
		            <br>
		        </div>
		        <p class="uk-text-muted">Ej.Anual,Semanal,Semestral etc..</p>

				<div uk-grid id="periodicidad">
					<div class="uk-width-1-3@m uk-margin-top">
						<select class="uk-select editarajax" name="periodotxt" data-tabla="productos_precios" data-campo="periodotxt" data-id="'.$id.'">
						  	<option >Selecciona un Rango</option>';
		for ($i=0; $i < sizeof($periodo) ; $i++) { 
			$selected = "";
			if($row_catalogo['periodotxt'] == $periodo[$i]){
				$estatus='selected';
			}else{
				$estatus='';
			}
			echo '  <option  value="'.$periodo[$i].'" '.$estatus.' >'.$periodo[$i].'</option>';
		}

echo '			              
			            </select>
					</div>
			        <div class="uk-margin-top uk-width-1-3@m">
						<label class="uk-text-capitalize" for="inicio">Inicio&nbsp;</label>
						<input type="number" class="editarajax uk-input uk-form-width-small" name="inicio" value="'.$row_precio['inicio'].'" data-tabla="productos_precios" data-campo="inicio" data-id="'.$id.'">
					</div>
					<div class="uk-margin-top uk-width-1-3@m">
						<label class="uk-text-capitalize" for="fin">Fin&nbsp;&nbsp;</label>
						<input type="number" class="editarajax uk-input uk-form-width-small" name="fin" value="'.$row_precio['fin'].'" data-tabla="productos_precios" data-campo="fin" data-id="'.$id.'">
					</div>
				
				</div>
					
				
			</div>
		</div>	
	</div>
</div>';


// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';


$scripts='
	$( document ).ready(function() {
		//checa si esta activo
    	if($("#periodo").attr("data-valor") > 0){
			$("#periodicidad").show();
		}else{
			$("#periodicidad").hide();
		}
	
	});

	$("#periodo").click(function() {

		var periodo=$(this).attr("data-valor");
		console.log("periodo", periodo)
		var periodicidad=$("#periodicidad");
		if(periodo > 0){
			console.log("checado");
			periodicidad.show();
		}else{
			console.log("no checado");
			periodicidad.hide();
		}

	});


	';



