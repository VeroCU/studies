<?php
$numeroProds=0;
$subcatsNum=0;
$Consulta = $CONEXION -> query("SELECT * FROM $seccioncat WHERE parent = 0 ORDER BY orden");
$numeroSubcats = $Consulta->num_rows;

echo '
<div class="uk-width-auto margin-top-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias" class="color-red">Categorias</a></li>
	</ul>
</div>
<div class="uk-width-expand@s uk-text-right margin-v-20">
	<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		<div>
			<a href="#add" uk-toggle class="uk-button uk-button-success"><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nuevo</a>
		</div>
	</div>
</div>
<div class="uk-width-medium-1-1 margin-v-20">
	<div class="uk-grid">
		<div class="uk-width-1-1 margin-bottom-50">
			<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
				<thead>
					<tr class="uk-text-muted">
						<th style="width:auto;"  onclick="sortTable(1)" class="pointer">Categoría</th>
						<th style="width:100px;" onclick="sortTable(2)" class="pointer uk-text-center">Programas</th>
						<th style="width:120px;" ></th>
					</tr>
				</thead>
				<tbody class="sortable" data-tabla="'.$seccioncat.'">
				';
while ($row_Consulta = $Consulta -> fetch_assoc()) {

	$catId = $row_Consulta['id'];
	$filas = $CONEXION -> query("SELECT * FROM $seccioncat WHERE parent = '$catId'");
	$numeroCats = $filas->num_rows;
	$productosConsulta = $CONEXION -> query("SELECT * FROM $seccion WHERE categoria =  '$catId'");
	$numProducts = $productosConsulta->num_rows;
	$link='index.php?rand='.rand(1,90000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$catId;

	echo '
				<tr id="'.$row_Consulta['id'].'">
					<td class="uk-text-left">
						<input type="text" value="'.$row_Consulta['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccioncat.'" data-campo="txt" data-id="'.$row_Consulta['id'].'" tabindex="10" >
					</td>
					<td class="uk-text-center@m">
						<span class="uk-text-muted uk-hidden@m">Subcategorías: </span>
						'.$numProducts.'
					</td>
					<td class="uk-text-right@m">
						<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
						<span data-id="'.$row_Consulta['id'].'" data-cantidad="'.$numProducts.'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
					</td>
				</tr>';
}

echo '

<div id="add" uk-modal="center: true" class="modal">
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="'.$subseccion.'">
			<input type="hidden" name="cat" value="'.$cat.'">

			<label for="titulo">Nombre de la categoría</label><br>
			<input type="text" name="titulo" class="uk-input" required><br><br>

			<label for="txt">Descripción</label><br>
			<textarea style="min-height:150px;" name="txt" class="uk-textarea" required></textarea><br><br>

			<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
			<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
		</form>
	</div>
</div>
';


$scripts='
	// Eliminar cat
	$(".eliminacat").click(function() {
		var id = $(this).attr(\'data-id\');
		UIkit.modal.confirm("Desea eliminar esto?").then(function() {
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&eliminacat=1&cat="+id);
		});
	});
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		var cantidad = $(this).attr(\'data-cantidad\');
		console.log("si elimina", cantidad);
		var statusConfirm = confirm("Realmente desea eliminar este Programas?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&tabla='.$seccioncat.'&cantidad="+cantidad+"&eliminargeneral&id="+id);
		} 
	});
';

