<?php 
	$CATEGORIAS = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORIAS = $CATEGORIAS -> fetch_assoc();
	$catNAME=$row_CATEGORIAS['titulo'];
	$txt=$row_CATEGORIAS['txt']; 
?>
<?php 
echo '
<div class="uk-width-1-1 margen-v-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?seccion='.$subseccion.'">'.$subseccion.'</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion=editar categoria" class="color-red">editar</a></li>
	</ul>
</div>
';		
?>
<style type="text/css">
	.next{margin:20px 30px;display:inline-block;vertical-align:top;}
</style>
<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" method="post" name="editar">
			<input type="hidden" name="editarcategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="cat" value="<?=$cat?>">

			<div class="uk-margin">
				<label class="uk-text-capitalize" for="titulo">Titulo</label>
				<input type="text" class="uk-input" name="titulo" id="titulo" autofocus value="<?=$catNAME ?>">
			</div>	
			<div class="uk-margin">
				<label>Descripción Larga</label>
				<textarea class="editor" name="txt" id="txt"><?=$txt ?></textarea>
			</div>
			<div class="uk-margin uk-text-center">
				<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>
