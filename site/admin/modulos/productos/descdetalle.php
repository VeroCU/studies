<?php

$consultaDescripcion = $CONEXION -> query("SELECT * FROM descripciones WHERE id = $id");
$row_descripcion = $consultaDescripcion -> fetch_assoc();
$productoId = $row_descripcion['producto'];

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $productoId");
$row_catalogo = $consulta -> fetch_assoc();
$productName = $row_catalogo['titulo'];
$cat = $row_catalogo['categoria'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['titulo'];

$rutaFinal='../img/contenido/descripciones/';

echo '
	<div class="uk-width-auto margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>	
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" >'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$productoId.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descdetalle&id='.$id.'" class="color-red">'.$row_descripcion['titulo'].'</a></li>
		</ul>
	</div>

	<div class="uk-width-expand@l uk-text-right margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
			<div>
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editardesc&id='.$id.'" class="uk-button uk-button-primary"><i uk-icon="pencil"></i>&nbsp; Editar</a>
			</div>
			
		</div>
	</div>
	<div class="uk-width-1-1@s margin-v-20">
			<div>
				<span class="uk-text-capitalize uk-text-muted">titulo:</span>
				'.$row_descripcion['titulo'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Descripción:</span>
				'.$row_descripcion['descripcion'].'
			</div>
			
		</div>
	</div>';

	echo'
	<div class="uk-width-1-2@s">
			<div class="">
				<h3 class="uk-text-left">Imagen de descripción</h3>
			</div>
			<div class="uk-width-1-2@s">
				<div class="uk-text-muted">
					Dimensiones recomendadas: 212 x 212 px o inferiores
				</div>
				<div id="fileuploadermain">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">';

			// sharePic

				$pic=$rutaFinal.$row_descripcion['imagen'];
				if(strlen($row_descripcion['imagen'])>0 AND file_exists($pic)){
					echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$pic.'" target="_blank">
									<img src="'.$pic.'" class="uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
				}else{
					echo '<div class="uk-panel uk-text-center">
								<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
									Falta imagen<br><br>
								</p>
							</div>';
				}

			echo '
			</div>
		</div>
	';

// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';


$scripts='
	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=imagen&descripcionimg=\'+data);
			}
		});
	});
		
	// Borrar foto 
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpic");
		} 
	});

	';



