<?php 
	$consultaDescripcion = $CONEXION -> query("SELECT * FROM descripciones WHERE id = $id");
	$row_descripcion = $consultaDescripcion -> fetch_assoc();
	$productoId = $row_descripcion['producto'];

	$titulo = $row_descripcion['titulo'];
	$descripcion = $row_descripcion['descripcion'];


	$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $productoId");
	$row_catalogo = $consulta -> fetch_assoc();
	$productName = $row_catalogo['titulo'];
	$cat = $row_catalogo['categoria'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catNAME=$row_CATEGORY['titulo'];

	$rutaFinal='../img/contenido/descripciones/';
?>
<?php 
echo '
<div class="uk-width-auto margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>	
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" >'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$productoId.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descdetalle&id='.$id.'">'.$row_descripcion['titulo'].'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editardesc&id='.$id.'" class="color-red">Editar descripción</a></li>
		</ul>
	</div>
';		
?>
<style type="text/css">
	.next{margin:20px 30px;display:inline-block;vertical-align:top;}
</style>
<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" method="post" name="editar">
			<input type="hidden" name="editardescripcion" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="descdetalle">
			<input type="hidden" name="id" value="<?=$id?>">

			<div class="uk-margin">
				<label class="uk-text-capitalize" for="titulo">Titulo</label>
				<input type="text" class="uk-input" name="titulo" id="titulo" autofocus value="<?=$titulo ?>">
			</div>	
			<div class="uk-margin">
				<label>Descripción</label>
				<textarea class="editor" name="descripcion" id="descripcion"><?=$descripcion ?></textarea>
			</div>
			<div class="uk-margin uk-text-center">
				<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=descdetalle&id=<?=$id?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>
