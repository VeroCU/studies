<?php 
$pag=(isset($_GET['pag']))?$_GET['pag']:0;
$prodspagina=(isset($_GET['prodspagina']))?$_GET['prodspagina']:20;
$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY categoria");

$numItems=$consulta->num_rows;
$prodInicial=$pag*$prodspagina;
echo'

<style>
@keyframes configure-clockwise {
  0% {
    transform: rotate(0);
  }
  25% {
    transform: rotate(90deg);
  }
  50% {
    transform: rotate(180deg);
  }
  75% {
    transform: rotate(270deg);
  }
  100% {
    transform: rotate(360deg);
  }
}

@keyframes configure-xclockwise {
  0% {
    transform: rotate(45deg);
  }
  25% {
    transform: rotate(-45deg);
  }
  50% {
    transform: rotate(-135deg);
  }
  75% {
    transform: rotate(-225deg);
  }
  100% {
    transform: rotate(-315deg);
  }
}

@keyframes pulse {
  from {
    opacity: 1;
    transform: scale(1);
  }
  to {
    opacity: .25;
    transform: scale(.75);
  }
}

/* GRID STYLING */



.spinner-box {
  width: 300px;
  height: 300px;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: transparent;
}
	
/* X-ROTATING BOXES */

.configure-border-1 {
  width: 115px;
  height: 115px;
  padding: 3px;
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #ffab91;
  animation: configure-clockwise 3s ease-in-out 0s infinite alternate;
}

.configure-border-2 {
  width: 115px;
  height: 115px;
  padding: 3px;
  left: -115px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgb(63,249,220);
  transform: rotate(45deg);
  animation: configure-xclockwise 3s ease-in-out 0s infinite alternate;
}

.configure-core {
  width: 100%;
  height: 100%;
  background-color: white;
}
</style>
';
echo '
<div class="spinner-box" hidden>
			  <div class="configure-border-1">  
			    <div class="configure-core"></div>
			  </div>  
			  <div class="configure-border-2">
			    <div class="configure-core"></div>
			  </div> 
</div>
<div class="uk-width-auto margin-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Programas &nbsp; <span class="uk-text-muted uk-text-lowercase"> &nbsp; <b>'.$numItems.'</b> programas</span></a></li>
	</ul>
</div>

<div class="uk-width-expand@s uk-text-right margin-v-20">
	<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		<div>
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias" class="uk-button uk-button-primary"><i uk-icon="folder"></i> &nbsp; Categorías</a>
		</div>
		<div>
			<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo" class="uk-button uk-button-success"><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nuevo</a>
		</div>
		<!--div>
			<a href="#" class="uk-button uk-button-success" id="spiner"><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; spiner</a>
		</div-->
	</div>
</div>

<div class="uk-width-1-1">
	<div uk-grid class="uk-grid-small uk-child-width-expand@m uk-child-width-1-2">
		<div><label class="pointer"><i uk-icon="search"></i> SKU<br><input type="text" class="uk-input search" data-campo="sku"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Escuela<br><input type="text" class="uk-input searchrel" data-campo="titulo" data-table="escuelas"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> titulo<br><input type="text" class="uk-input search" data-campo="titulo"></label></div>
		
	</div>
</div>


<div class="uk-width-1-1">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th style="width:90px;" class="uk-text-left"> &nbsp;&nbsp; SKU</th>
				<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Escuela</th>
				<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; titulo</th>
				<th style="width:90px;"  class="uk-text-center">Categoría</th>
				<th style="width:120px;"  class="uk-text-center">Fecha de modificación</th>
				<th style="width:90px;"  class="uk-text-center">Precio</th>
				<th style="width:90px;"  class="uk-text-center">Activo</th>
				<th style="width:90px;"  ></th>
			</tr>
		</thead>
		<tbody id="conetent">';

		$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY escuelaId LIMIT $prodInicial,$prodspagina");
		while ($row_Consulta1 = $consulta -> fetch_assoc()) {
			$escuelaId = $row_Consulta1['escuelaid'];

			// CONSULTAMOS LA ESCUELA
			$consultaEscuela = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $escuelaId");
			$escuelaRow = $consultaEscuela -> fetch_assoc();
			$escuela = $escuelaRow['titulo'];

			$prodID = $row_Consulta1['id'];
			$catId=$row_Consulta1['categoria'];

			$CONSULTA4 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
			$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
			$categoriaTxt=$row_CONSULTA4['titulo'];

			$picTxt = '';
			$rutaFinal = '../img/contenido/productos/';
			$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $prodID ORDER BY orden,id LIMIT 1");
			$numProds=$consultaPIC->num_rows;
			while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {
				$picId=$row_consultaPIC['id'];
				$pic=$rutaFinal.$picId.'.jpg';
				if(file_exists($pic)){
					$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
					$pic=$row_Consulta1['imagen'];
					$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}
			}

			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

			$estatusIcon=($row_Consulta1['estatus']==0)?'off uk-text-muted':'on uk-text-primary';
			$inicioIcon=($row_Consulta1['inicio']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					<span class="uk-text-muted uk-hidden@m">SKU: </span>
					<input value="'.$row_Consulta1['sku'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="10">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">Escuela: </span>
					<input value="'.$escuela.'" class="uk-input uk-form-blank" tabindex="9">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">titulo: </span>
					<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
				</td>
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Categoría: </span>
					'.$categoriaTxt.'
				</td>
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Fecha de modificación: </span>
					'.$row_Consulta1['updatedAt'].'
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">Precio: </span>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['precio']))).'</span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$row_Consulta1['precio'].'" tabindex="7">
				</td>
				
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Activo: </span>
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
				</td>
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$row_Consulta1['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}

	echo '
		</tbody>
	</table>
</div>
';

?>

<!-- PAGINATION -->
<div class="uk-width-1-1 padding-top-50">
	<div uk-grid class="uk-flex-center">
		<div>
			<ul class="uk-pagination uk-flex-center uk-text-center">
			<?php
			if ($pag!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag-1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
			}
			$pagTotal=intval($numItems/$prodspagina);
			$modulo=$numItems % $prodspagina;
			if (($modulo) == 0){
				$pagTotal=($numItems/$prodspagina)-1;
			}
			for ($i=0; $i <= $pagTotal; $i++) { 
				$clase='';
				if ($pag==$i) {
					$clase='uk-badge bg-primary color-white';
				}
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($i).'&prodspagina='.$prodspagina;
				echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
			}
			if ($pag!=$pagTotal AND $numItems!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag+1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>';
			}
			?>

			</ul>
		</div>
		<div class="uk-text-right" style="margin-top: -10px; width:120px;">
			<select name="prodspagina" data-placeholder="Productos por página" id="prodspagina" class="chosen-select uk-select" style="width:120px;">
				<?php
				$arreglo = array(5=>5,20=>20,50=>50,100=>100,500=>500,9999=>"Todos");
				foreach ($arreglo as $key => $value) {
					$checked='';
					if ($key==$prodspagina) {
						$checked='selected';
					}
					echo '
					<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
				}
				?>
				
			</select>
		</div>
	</div>
</div><!-- PAGINATION -->




<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<div id="nuevacat" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<a class="uk-modal-close uk-close"></a>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="categorias">

			<label for="categoria">Nombre de la nueva categoría</label><br><br>
			<input type="text" class="uk-input" name="categoria" tabindex="10" required><br><br>
			<input type="submit" name="send" value="Agregar" tabindex="10" class="uk-button uk-button-primary">
		</form>
	</div>
</div>


<?php 
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&id="+id);
		} 
	});

	$("#prodspagina").change(function(){
		var prodspagina = $(this).val();
		window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&prodspagina="+prodspagina);
	})

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});

	$(".searchrel").keypress(function(e) {
		
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			var table =  $(this).attr("data-table");
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor+"&table="+table);
		}
	});

	$("#spiner").click(function(){
		$(".spinner-box").removeAttr("hidden");
		 $(".spinner-box").show();
		setTimeout(function(){
	 	 $(".spinner-box").hide();
	  }, 3000);
		
	});

	';
?>

