<?php

	$PRODUCT = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
	$rowProduct = $PRODUCT -> fetch_assoc();
	$productName = $rowProduct['titulo'];

	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descripciones&id='.$id.'" class="color-red">Descripciones</a></li>
		</ul>
	</div>

	<div class="uk-width-1-1 uk-text-right margin-bottom-20" style="margin-top: 0px;">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
			<div>
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevadrescripcion&id='.$id.'" class="uk-button uk-button-success"><i uk-icon="plus"></i>&nbsp; Nuevo</a>
			</div>	
		</div>
	</div>

	<div class="uk-width-1-1">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th style="width:20px;"  ></th>
					<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; titulo</th>
					<th style="width:90px;"  ></th>
				</tr>
			</thead>
			<tbody id="conetent">';

			$pag=(isset($_GET['pag']))?$_GET['pag']:0;
			$prodspagina=(isset($_GET['prodspagina']))?$_GET['prodspagina']:20;
			$consulta = $CONEXION -> query("SELECT * FROM $subseccion WHERE producto = $id");
			$numItems=$consulta->num_rows;	
			$prodInicial=$pag*$prodspagina;
			
			while ($row_Consulta1 = $consulta -> fetch_assoc()){
				
				$descripcionId = $row_Consulta1['id'];
				

				$picTxt ='';
				$rutaFinal = '../img/contenido/productos/';
				
			
				$pic=$rutaFinal.$row_Consulta1['imagen'];

				if(file_exists($pic)){
					$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
					$pic=$row_Consulta1['imagen'];
					$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}
			
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descdetalle&id='.$row_Consulta1['id'];

				echo '
				<tr id="'.$prodID.'">
					<td>
						'.$picTxt.'
					</td>
					<td>
						<span class="uk-text-muted uk-hidden@m">titulo: </span>
						<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
					</td>
					<td class="uk-text-right@m">
						<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
						<span data-id="'.$row_Consulta1['id'].'" class="eliminadesc uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
					</td>
				</tr>';
			}
			echo '
			</tbody>
		</table>
	</div>
';

?>

<!-- PAGINATION -->
<div class="uk-width-1-1 padding-top-50">
	<div uk-grid class="uk-flex-center">
		<div>
			<ul class="uk-pagination uk-flex-center uk-text-center">
			<?php
			if ($pag!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag-1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'"><i class="fa fa-lg fa-angle-left"></i> &nbsp;&nbsp; Anterior</a></li>';
			}
			$pagTotal=intval($numItems/$prodspagina);
			$modulo=$numItems % $prodspagina;
			if (($modulo) == 0){
				$pagTotal=($numItems/$prodspagina)-1;
			}
			for ($i=0; $i <= $pagTotal; $i++) { 
				$clase='';
				if ($pag==$i) {
					$clase='uk-badge bg-primary color-white';
				}
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($i).'&prodspagina='.$prodspagina;
				echo '<li><a href="'.$link.'" class="'.$clase.'">'.($i+1).'</a></li>';
			}
			if ($pag!=$pagTotal AND $numItems!=0) {
				$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&pag='.($pag+1).'&prodspagina='.$prodspagina;
				echo'
				<li><a href="'.$link.'">Siguiente &nbsp;&nbsp; <i class="fa fa-lg fa-angle-right"></i></a></li>';
			}
			?>

			</ul>
		</div>
		<div class="uk-text-right" style="margin-top: -10px; width:120px;">
			<select name="prodspagina" data-placeholder="Productos por página" id="prodspagina" class="chosen-select uk-select" style="width:120px;">
				<?php
				$arreglo = array(5=>5,20=>20,50=>50,100=>100,500=>500,9999=>"Todos");
				foreach ($arreglo as $key => $value) {
					$checked='';
					if ($key==$prodspagina) {
						$checked='selected';
					}
					echo '
					<option value="'.$key.'" '.$checked.'>'.$value.'</option>';
				}
				?>
				
			</select>
		</div>
	</div>
</div><!-- PAGINATION -->

<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
<?php 
$scripts='
	// Eliminar producto
	$(".eliminadesc").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&id="+id);
		} 
	});

	$("#prodspagina").change(function(){
		var prodspagina = $(this).val();
		window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&prodspagina="+prodspagina);
	})

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});

	';
?>


