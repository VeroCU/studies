<?php 
$campo=$_GET['campo'];
$valor=$_GET['valor'];
$resultado = array();
if(isset($_GET['table'])){
	$tabla = $_GET['table'];
	$sql = "SELECT * FROM $tabla WHERE $campo LIKE '%$valor%' ORDER BY $campo";

	$CONSULTAREL = $CONEXION -> query($sql);
	while ($rowRelacion =$CONSULTAREL -> fetch_assoc()) {
		
		$idRel = $rowRelacion['id'];
		$escuela = $rowRelacion['titulo'];

		$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE escuelaid = $idRel");
		$numItems=$consulta->num_rows;
		while ($row_Consulta1 = $consulta -> fetch_assoc()) {
			
			array_push($resultado, $row_Consulta1);
		}
		
	}
		
}else{
	$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE $campo LIKE '%$valor%' ORDER BY $campo");
	$numItems=$consulta->num_rows;
	while ($row_Consulta1 = $consulta -> fetch_assoc()) {
		array_push($resultado, $row_Consulta1);
	}
	
}



echo '
<div class="uk-width-1-3@s margen-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&campo='.$campo.'&valor='.$valor.'" class="color-red">Buscar '.$valor.' &nbsp; <span class="uk-text-muted uk-text-lowercase"> &nbsp; <b>'.$numItems.'</b> productos</span></a></li>
	</ul>
</div>

<div class="uk-width-1-1">
	<div uk-grid class="uk-grid-small uk-child-width-expand@m uk-child-width-1-2">
		<div><label class="pointer"><i uk-icon="search"></i> SKU<br><input type="text" class="uk-input search" data-campo="sku"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> Escuela<br><input type="text" class="uk-input searchrel" data-campo="titulo" data-table="escuelas"></label></div>
		<div><label class="pointer"><i uk-icon="search"></i> titulo<br><input type="text" class="uk-input search" data-campo="titulo"></label></div>
		
	</div>
</div>

<div class="uk-width-1-1">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th style="width:20px;"  ></th>
				<th style="width:120px;" onclick="sortTable(1)"   class="pointer uk-text-left"> &nbsp;&nbsp; SKU</th>
				<th style="width:auto;"  onclick="sortTable(2)"   class="pointer uk-text-left"> &nbsp;&nbsp; Escuela</th>
				<th style="width:auto;"  onclick="sortTable(2)"   class="pointer uk-text-left"> &nbsp;&nbsp; titulo</th>
				<th style="width:90px;"  onclick="sortTable(4)"   class="pointer uk-text-center">Categoría</th>
				<th style="width:90px;"  onclick="sortTable(5)"   class="pointer uk-text-center">Precio</th>
				<th style="width:90px;"  onclick="sortTable(8)"   class="pointer uk-text-center">Activo</th>
				<th style="width:90px;"  ></th>
			</tr>
		</thead>
		<tbody id="conetent">';

		for($i = 0 ; $i < sizeof($resultado);$i++) {
			$prodID=$resultado[$i]['id'];
			$catId=$resultado[$i]['categoria'];

			$escuelaId = $resultado[$i]['escuelaid'];

			$consultaEscuela = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $escuelaId");
			$escuelaRow = $consultaEscuela -> fetch_assoc();

			$resultado[$i]['escuelaNombre'] = $escuelaRow['titulo'];

			$CONSULTA4 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
			$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
			$categoriaTxt=$row_CONSULTA4['titulo'];

			$picTxt = '';
			$rutaFinal = '../img/contenido/productos/';
			$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $prodID ORDER BY orden,id LIMIT 1");
			$numProds=$consultaPIC->num_rows;
			while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {
				$picId=$row_consultaPIC['id'];
				$pic=$rutaFinal.$picId.'.jpg';
				if(file_exists($pic)){
					$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}elseif(strlen($resultado[$i]['imagen'])>0 AND strpos($resultado[$i]['imagen'], 'ttp')>0){
					$pic=$resultado[$i]['imagen'];
					$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}
			}


			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$resultado[$i]['id'];

			$estatusIcon=($resultado[$i]['estatus']==0)?'off uk-text-muted':'on uk-text-primary';
			$inicioIcon=($resultado[$i]['inicio']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					<input value="'.$resultado[$i]['sku'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="10">
				</td>
				<td>
					<input value="'.$resultado[$i]['escuelaNombre'].'" class="uk-input uk-form-blank" tabindex="9">
				</td>
				<td>
					<input value="'.$resultado[$i]['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
				</td>
				<td class="uk-text-center">
					'.$categoriaTxt.'
				</td>
				<td>
					<span class="uk-hidden">'.(10000+(1*($resultado[$i]['precio']))).'</span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$resultado[$i]['precio'].'" tabindex="7">
				</td>
				<td class="uk-text-center@m">
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$resultado[$i]['estatus'].'"></i>
				</td>
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$resultado[$i]['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>




<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=nuevo" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<div id="nuevacat" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<a class="uk-modal-close uk-close"></a>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="categorias">

			<label for="categoria">Nombre de la nueva categoría</label><br><br>
			<input type="text" class="uk-input" name="categoria" tabindex="10" required><br><br>
			<input type="submit" name="send" value="Agregar" tabindex="10" class="uk-button uk-button-primary">
		</form>
	</div>
</div>

<?php 
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&campo='.$campo.'&valor='.$valor.'&borrarPod&id="+id);
		} 
	});

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});

	$(".searchrel").keypress(function(e) {
		
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			var table =  $(this).attr("data-table");
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor+"&table="+table);
		}
	});
	';
?>

