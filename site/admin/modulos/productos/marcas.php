<?php
echo '
	<div class="uk-width-1-3 margin-top-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">'.$seccion.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=marcas" class="color-red">Marcas</a></li>
		</ul>
	</div>

	<div class="uk-width-2-3@s uk-text-right margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
			<div>
				<a href="#add" class="uk-button uk-button-success" uk-toggle><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nuevo</a>
			</div>
		</div>
	</div>


<div class="uk-width-1-1 margin-v-20">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th onclick="sortTable(0)" class="pointer uk-text-left">Titulo</th>
					<th width="120px" onclick="sortTable(1)" class="pointer uk-text-center">Imagen</th>
					<th width="120px" ></th>
				</tr>
			</thead>
			<tbody class="sortable" data-tabla="marcas">';



$Consulta = $CONEXION -> query("SELECT * FROM marcas ORDER BY orden");
while ($row_Consulta = $Consulta -> fetch_assoc()) {

	$blogId = $row_Consulta['id'];

	$link='index.php?seccion=promociones&subseccion=catdetalle&id='.$blogId;

	$rutaFinal='../img/contenido/marcas/';
	$pic=$rutaFinal.$row_Consulta['imagen'];
	$fichaIcon='<i class="fa-lg far fa-square uk-text-muted pointer"></i>';
	if(file_exists($pic) AND strlen($row_Consulta['imagen'])>0){
		$fichaIcon='
			<div class="uk-inline">
				<i class="fa-lg fas fa-check-square uk-text-primary pointer"></i>
				<div uk-drop="pos: right-justify">
					<img uk-img data-src="'.$pic.'" class="uk-border-rounded">
				</div>
			</div>';
	}

	echo '
				<tr id="'.$row_Consulta['id'].'">
					<td class="uk-text-left">
						<input type="text" value="'.$row_Consulta['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="marcas" data-campo="titulo" data-id="'.$row_Consulta['id'].'" tabindex="10" >
					</td>
					<td class="uk-text-center">
						<a href="#ficha" uk-toggle data-id="'.$row_Consulta['id'].'" class="fichalink">'.$fichaIcon.'</a>
					</td>

					<td class="uk-text-right">
						<a href="javascript:eliminaMarca(id='.$row_Consulta['id'].')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash"></i></a> 
					</td>
				</tr>';
}

echo '
			</tbody>
		</table>
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>

	<div id="ficha" uk-modal>
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<input type="hidden" id="fichaid">
			<p>PNG 200 x 300 px</p>
			<div id="fileupload">
				Cargar
			</div>
		</div>
	</div>
	

	<div id="add" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar1" onsubmit="return checkForm(this);">

				<input type="hidden" name="nuevamarca" value="1">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">
				<input type="hidden" name="marcas" value="0">

				<label for="marca">Nombre de la marca</label><br><br>
				<input type="text" name="marca" class="uk-input" required><br><br>
				<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
				<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
			</form>
		</div>
	</div>
	';


$scripts='

	function eliminaMarca () { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion=marcas&eliminaMarca&id="+id);
		} 
	}
	$(".fichalink").click(function(){
		var id = $(this).attr("data-id");
		$("#fichaid").val(id);
	})

	$("#fileupload").uploadFile({
		url: "../library/upload-file/php/upload.php",
		fileName: "myfile",
		maxFileCount: 1,
		showDelete: \'false\',
		allowedTypes: "png",
		maxFileSize: 10000000,
		showFileCounter: false,
		showPreview: false,
		returnType: \'json\',
		onSuccess:function(data){
			var id = $("#fichaid").val();
			window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id=\'+id+\'&position=marcas&imagen=\'+data);
		}
	});';

