<?php

	$id = $_GET['id'];
	$PRODUCT = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
	$rowProduct = $PRODUCT -> fetch_assoc();
	$productName = $rowProduct['titulo'];

	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
			<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descripcion&cat='.$id.'" class="color-red">Nueva descripcion</a></li>
		</ul>
	</div>';
?>
<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" class="uk-width-1-1" method="post" name="nuevadescripcion" onsubmit="return checkForm(this);">
			<input type="hidden" name="nuevadescripcion" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="descdetalle">
			<input type="hidden" name="productoid" value="<?=$id?>">

			<div uk-grid class="uk-grid-small uk-child-width-1-1@l uk-child-width-1-1@m">
				<div>
					<label class="uk-text-capitalize" for="titulo">titulo</label>
					<input type="text" class="uk-input" name="titulo" required>
				</div>
				<div class="uk-width-1-1">
					<div class="margin-top-20">
						<label for="descripcion">Descripción</label>
						<textarea class="editor" name="descripcion"></textarea>
					</div>
				</div>	
				<div class="uk-width-1-1 uk-text-center">
					<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=descripciones&id=<?=$id?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
					<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>
			</div>	
		</form>
	</div>
</div>


<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
