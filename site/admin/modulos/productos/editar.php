<?php

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$cat=$row_catalogo['categoria'];
$marca=$row_catalogo['marca'];
$escuela = $row_catalogo['escuelaid'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['txt'];
$catParentID=$row_CATEGORY['parent'];

$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catParent=$row_CATEGORY['txt'];

echo '
<div class="uk-width-1-1 margin-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias">Categorías</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$row_catalogo['titulo'].'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="color-red">Editar</a></li>
	</ul>
</div>

<div class="uk-width-1-1 margin-top-20 uk-form">
	<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
		<input type="hidden" name="editar" value="1">
		<input type="hidden" name="seccion" value="'.$seccion.'">
		<input type="hidden" name="subseccion" value="detalle">
		<input type="hidden" name="cat" value="'.$cat.'">
		<input type="hidden" name="id" value="'.$id.'">
		<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
			<div>
				<label class="uk-text-uppercase" for="sku">sku:</label>
				<input type="text" class="uk-input" name="sku" value="'.$row_catalogo['sku'].'" autofocus>
			</div>
			<div>
				<label class="uk-text-capitalize" for="titulo">titulo:</label>
				<input type="text" class="uk-input" name="titulo" value="'.$row_catalogo['titulo'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="precio">Precio /  Matricula:</label>
				<input type="number" class="uk-input" name="precio" value="'.$row_catalogo['precio'].'" required>
			</div>
			<div>
			<label class="uk-text-capitalize" for="periodotxt">Periodo:</label>
				<input type="txt" class="uk-input" name="periodotxt" value="'.$row_catalogo['periodotxt'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="inscripcion">Precio de Inscripcion:</label>
				<input type="number" class="uk-input" name="inscripcion" value="'.$row_catalogo['inscripcion'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="seguro">Seguro:</label>
				<input type="number" class="uk-input" name="seguro" value="'.$row_catalogo['seguro'].'">
			</div>
			<div>
				<label class="uk-text-capitalize" for="cuota">Cuota de servicio y transferencia:</label>
				<input type="number" class="uk-input" name="cuota" value="'.$row_catalogo['cuota'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="descuento">descuento:</label>
				<input type="number" class="uk-input descuento" name="descuento" value="'.$row_catalogo['descuento'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="categoria">categoria</label>
				<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>';
				$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
					while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
						if (isset($cat) AND $cat==$row_CONSULTA1['id']) {
							$estatus='selected';
						}else{
							$estatus='';
						}
						echo '
											<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['titulo'].'</option>';
					}
					echo '
										</optgroup>';
echo '
				</select>
			</div>

			<div>
				<label class="uk-text-capitalize" for="escuelaid">Escuela</label>
					<select name="escuelaid" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>';
					$CONSULTAESCUELA = $CONEXION -> query("SELECT * FROM escuelas ORDER BY titulo");
					while ($row_escuela = $CONSULTAESCUELA -> fetch_assoc()) {
						$escuelaId=$row_escuela['id'];
						$nombreEscuela = $row_escuela['titulo'];
						if($escuelaId ==$escuela)	{
							$estatus='selected';
							}else{
								$estatus='';
							}
						echo '
							<option value="'.$escuelaId.'" '.$estatus.'>'.$nombreEscuela.'</option>';
					}
			echo '
				</select>
			</div>';
echo '	<div>
		<label class="uk-text-capitalize" for="divisa">Divisa</label>
			<select name="divisa" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1">
';
		$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas");
			while ($rowDivisas = $consultaDivisas -> fetch_assoc()) {
				$divisaid=$rowDivisas['id'];
				$nombreDivisa=$rowDivisas['nombre'];
				if ($divisaid==$row_catalogo['divisa']) {
						$estatus='selected';
					}else{
						$estatus='';
					}
				
					echo '
						<option value="'.$divisaid.'" '.$estatus.'>'.$nombreDivisa.'</option>';
			}
						echo '
						</select>
		</div>';
		
	echo '
			<div class="uk-width-1-1">
				<div class="margin-top-20">
					<label for="txt">Descripción</label>
					<textarea class="editor" name="txt">'.$row_catalogo['txt'].'</textarea>
				</div>
			</div>
			<div class="uk-width-1-1">
				<div class="margin-top-20">
					<label for="palabras">Palabras clave:</label>
						<input id="palabras"  class="uk-input" name="palabras" type="text" value="'.$row_catalogo['palabras'].'">
					</div>
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="title">titulo google</label>
				<input type="text" class="uk-input" name="title" value="'.$row_catalogo['title'].'">
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="metadescription">descripción google</label>
				<textarea class="uk-textarea" name="metadescription">'.$row_catalogo['metadescription'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-text-center">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</div>
	</form>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<script>
	
	$(function() {
		
		$(\'#palabras\').tagsInput({
			\'onAddTag\': function(input, value) {
				console.log(\'tag added\', input.value, value);
			},
			\'onRemoveTag\': function(input, value) {
				console.log(\'tag removed\', input, value);
			},
			\'onChange\': function(input, value) {
				console.log(\'change triggered\', input, value);
			}
		});
				
	});
</script>

';