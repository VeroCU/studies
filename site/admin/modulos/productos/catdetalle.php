<?php 
if ($cat!=false && $cat !='') {
	$CATEGORIAS = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORIAS = $CATEGORIAS -> fetch_assoc();
	$catNAME=$row_CATEGORIAS['titulo'];
	$txt=$row_CATEGORIAS['txt']; 
	
}

echo '
<div class="uk-width-auto margin-top-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias">Categorías</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" class="color-red">'.$catNAME.'</a></li>
	</ul>
</div>
<div class="uk-width-expand@m uk-text-right margin-v-20">
	<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		<div>

			<a href="#addFile" uk-toggle class="uk-button uk-button-success"><i uk-icon="icon:file-edit;ratio:1.2;"></i> &nbsp; Documentos</a>
		</div>
	</div>
</div>

';

echo '
	
<div id="addFile" uk-modal="center: true" class="modal">
	<div class="uk-modal-dialog uk-modal-body">
		<button class="uk-modal-close-default" type="button" uk-close></button>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editarFile">

			<input type="hidden" name="nuevodoc" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="'.$subseccion.'">
			<input type="hidden" name="cat" value="'.$cat.'">

			<label for="titulo">Nombre del documento</label><br>
			<input type="text" name="titulo" class="uk-input" required><br><br>

			<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
			<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
		</form>
	</div>
</div>
';
?>


<div class="uk-width-medium-1-1 margin-v-50">

	<div class="uk-container">
		<div class="uk-margin">
			<label>Título </label>
			<div>
				<input  type="text" value="<?= $catNAME ?>" class="uk-input editarajax" data-tabla="<?= $seccioncat ?>" data-campo="titulo" data-id="<?= $cat ?>">	
			</div>
		</div>
		<div class="uk-margin">
			<label>Desripcion </label>
			<div>
			<textarea class="uk-textarea editarajax" data-tabla="<?= $seccioncat ?>" data-campo="txt" data-id="<?= $cat ?>"><?= $txt ?></textarea>
			</div>
		</div>
		<div class="uk-width-1-1">
			<h3>Checklist de documentos.</h3>
			<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
				<thead>
					<tr class="uk-text-muted">
						<th style="width:auto;"  class="uk-text-left">&nbsp; titulo</th>
						<th style="width:auto;"  class="uk-text-center">categoria</th>
						<th style="width:90px;"  class="uk-text-center">obligatorio</th>
						<th style="width:90px;"  ></th>
					</tr>
				</thead>
				<tbody id="files">
				<?php 
				$consulta = $CONEXION -> query("SELECT * FROM programasdocumentos WHERE categoria = $cat");
				while ($row_Consulta1 = $consulta -> fetch_assoc()) {
					
					$fileId = $row_Consulta1['id'];
					$catId=$row_Consulta1['categoria'];
					
					$CONSULTA4 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
					$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
					$categoriaTxt=$row_CONSULTA4['titulo'];


					$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

					$obligatorioIcon=($row_Consulta1['obligatorio']==0)?'off uk-text-muted':'on uk-text-primary';
				

					echo '
					<tr id="'.$fileId.'">
						<td>
							<span class="uk-text-muted uk-hidden@m">titulo: </span>
							<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="programasdocumentos" data-campo="titulo" data-id="'.$fileId.'" tabindex="9">
						</td>
						<td class="uk-text-center@m">
							<span class="uk-text-muted uk-hidden@m">Categoría: </span>
							'.$categoriaTxt.'
						</td>
						<td class="uk-text-center@m">
							<span class="uk-text-muted uk-hidden@m">Activo: </span>
							<i class="estatuschange fa fa-lg fa-toggle-'.$obligatorioIcon.' uk-text-muted pointer" data-tabla="programasdocumentos" data-campo="obligatorio" data-id="'.$fileId.'" data-valor="'.$row_Consulta1['obligatorio'].'"></i>
						</td>
						<td class="uk-text-right@m">
							
							<span data-id="'.$row_Consulta1['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
						</td>
					</tr>';
				}

					echo '
				</tbody>
			</table>
		</div>'; 
?>
		<div style="margin-left: 0" uk-grid>
			<div class="uk-width-1-2@m">
				<div class="">
					<h3 class="uk-text-logo">Logo de categoria</h3>
				</div>
				<div class="uk-width-1-1@m">
					<div class="uk-text-muted">
						Dimensiones recomendadas: 200 px 200 px
					</div>
					<div id="fileuploader">
						Cargar
					</div>
				</div>
				<div class="uk-width-1-1@m uk-text-center margen-v-20">
					<?php  
					// sharePic
						$logo="../img/contenido/productoscat/".$row_CATEGORIAS['imagen'];
						if(strlen($row_CATEGORIAS['imagen'])>0 AND file_exists($logo)){
							echo '
									<div class="uk-panel uk-text-center">
										<a href="'.$logo.'" target="_blank">
											<img src="'.$logo.'" class="uk-border-rounded margen-top-20">
										</a><br><br>
										<button class="uk-button uk-button-danger borrarimgcat"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
									</div>';
						}else{
							echo '
									<div class="uk-panel uk-text-center">
										<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
											Falta imagen<br><br>
										</p>
									</div>';
						}?>


				</div>
			</div>
			<div class="uk-width-1-2@m">
				<div class="">
					<h3 class="uk-text-logo">Carrousel</h3>
					
				</div>
				<div class="uk-text-muted">Dimensiones recomendadas: 600 x 1600 px</div>
				<div id="fileuploadercarrousel">
					Cargar
				</div>
				

				<div class="uk-width-1-1@m">
					<div uk-grid class="uk-grid-match uk-grid-small sortable" data-tabla="escuelas">
					<?php 
						$rutaSlider="../img/contenido/productoscat/carousel/";
						$consulta = $CONEXION -> query("SELECT * FROM carousel_categorias WHERE categoria = $cat ORDER BY orden");
						$ceri = $consulta ->num_rows;
						if($ceri> 0){
							while ($rowConsulta = $consulta -> fetch_assoc()) {
								$idCarousel=$rowConsulta['id'];
								$file=$idCarousel.'.jpg';
								echo '
									<div id="'.$rowConsulta['id'].'" style="width:250px;">
										<div id="'.$rowConsulta['id'].'" class="uk-card uk-card-default uk-card-body uk-text-center">
											<a href="javascript:borrarfoto(\''.$file.'\',\''.$idCarousel.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
											<br><br>
											<img src="'.$rutaSlider.$rowConsulta['id'].'.jpg" class="uk-border-rounded margin-bottom-20">
											
										</div>
									</div>';
							}
						}
						echo '
							</div>
						</div>';
					?>
					</div>
				</div>
			</div>
		</div>
</div>


<?php 
echo  '
<div class="uk-width-1-1">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th style="width:20px;"  ></th>
				<th style="width:120px;" class="uk-text-left"> &nbsp;&nbsp; SKU</th>
				<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; titulo</th>
				<th style="width:90px;"  class="uk-text-center">Categoría</th>
				<th style="width:90px;"  class="uk-text-center">Precio</th>
				<th style="width:70px;"  class="uk-text-center">Descuento</th>
				<th style="width:90px;"  class="uk-text-center">Activo</th>
				<th style="width:90px;"  ></th>
			</tr>
		</thead>
		<tbody id="conetent">';

		$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE categoria = $cat ORDER BY precio");
		while ($row_Consulta1 = $consulta -> fetch_assoc()) {
			
			$prodID = $row_Consulta1['id'];
			$catId=$row_Consulta1['categoria'];

			$CONSULTA4 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catId");
			$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
			$categoriaTxt=$row_CONSULTA4['titulo'];

			$picTxt = '';
			$rutaFinal = '../img/contenido/productos/';
			$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $prodID ORDER BY orden,id LIMIT 1");
			$numProds=$consultaPIC->num_rows;
			while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {
				$picId=$row_consultaPIC['id'];
				$pic=$rutaFinal.$picId.'.jpg';
				if(file_exists($pic)){
					$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
					$pic=$row_Consulta1['imagen'];
					$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
				}
			}

			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

			$estatusIcon=($row_Consulta1['estatus']==0)?'off uk-text-muted':'on uk-text-primary';
			$inicioIcon=($row_Consulta1['inicio']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">SKU: </span>
					<input value="'.$row_Consulta1['sku'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="10">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">titulo: </span>
					<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
				</td>
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Categoría: </span>
					'.$categoriaTxt.'
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">Precio: </span>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['precio']))).'</span>
					<input class="editarajax uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$row_Consulta1['precio'].'" tabindex="7">
				</td>
				<td>
					<span class="uk-text-muted uk-hidden@m">Descuento: </span>
					<span class="uk-hidden">'.(10000+(1*($row_Consulta1['descuento']))).'</span>
					<input class="editarajax descuento uk-input uk-form-blank uk-text-right@m" data-tabla="'.$seccion.'" type="number" max="99" min="0" data-campo="descuento" data-id="'.$prodID.'" value="'.$row_Consulta1['descuento'].'" tabindex="6">
				</td>
				<td class="uk-text-center@m">
					<span class="uk-text-muted uk-hidden@m">Activo: </span>
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
				</td>
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
				</td>
			</tr>';
		}

	echo '
		</tbody>
	</table>
</div>';


?>
<script>
$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: 'false',
			allowedTypes: "jpeg,jpg,png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:'json',
			onSuccess:function(data){ 
				window.location = ('index.php?seccion=<?= $seccion ?>&subseccion=<?= $subseccion ?>&cat=<?= $cat ?>&position=imagen&imagencat='+data);
			}
		});
		var imagenesArray = [];
		$("#fileuploadercarrousel").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: "false",
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:"json",
			onSuccess:function(data){ 
				window.location = ("index.php?seccion=<?= $seccion?>&subseccion=<?= $subseccion?>&cat=<?= $cat?>&imagenslider="+data);
			}
		});
	});

 // Eliminar foto
	function borrarfoto (file,id) { 
			var statusConfirm = confirm("Realmente desea eliminar esto?"); 
			if (statusConfirm == true) { 
				$.ajax({
					method: "POST",
					url: "modulos/<?=$seccion?>/acciones.php",
					data: { 
						borrarslider: 1,
						id: id
					}
				})
				.done(function( msg ) {
					UIkit.notification.closeAll();
					UIkit.notification(msg);
					$("#"+id).addClass( "uk-invisible" );
				});
			}
		}
// Borrar icono
	$(".borrarimgcat").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion=<?= $seccion ?>&subseccion=<?= $subseccion ?>&cat=<?= $cat ?>&borrarimgcat");
		} 
	});

	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr('data-id');
		var statusConfirm = confirm("Realmente desea eliminar este documento?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion=<?= $seccion ?>&subseccion=<?= $subseccion ?>&cat=<?= $cat ?>&tabla=programasdocumentos&eliminargeneral&id="+id);
		} 
	});
</script>