<?php
	$seccion='productos';
	$seccioncat=$seccion.'cat';
	$seccionpic=$seccion.'pic';
	$seccionmain=$seccion.'main';
	$rutaInicial="../library/upload-file/php/uploads/";
	$rutaFinal='../img/contenido/'.$seccion.'/';
	$rutaDesc='../img/contenido/descripciones/';
	$rutaCategoria='../img/contenido/productoscat/';
	$rutaCategorias='../img/contenido/productoscat/carousel/';

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen  de categoria   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['imagencat'])){
		$position=$_GET['position'];
		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_GET['imagencat'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if(file_exists($rutaInicial.$imagenName)){
			$rand=rand(1,999999999);
			$imgFinal=$rand.'.'.$ext;
			if(file_exists($rutaCategoria.$imgFinal)){
				$rand=rand(1,999999999);
				$imgFinal=$rand.'.'.$ext;
			}
			$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $cat");
			$row_CONSULTA = $CONSULTA -> fetch_assoc();
			if (strlen($row_CONSULTA[$position])>0 AND file_exists($rutaCategoria.$row_CONSULTA[$position])) {
				unlink($rutaCategoria.$row_CONSULTA[$position]);
			}
			copy($rutaInicial.$imagenName, $rutaCategoria.$imgFinal);
			$actualizar = $CONEXION->query("UPDATE productoscat SET $position = '$imgFinal' WHERE id = $cat");
		}else{	
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Imagen categoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarimgcat'])){
		
		$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $cat");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaCategoria.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE productoscat SET imagen = '' WHERE id = $cat");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la logoescuela en la base de datos";
			$fallo=1;
		}
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['logo'])){
		$position=$_GET['position'];

		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_GET['logo'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if(file_exists($rutaInicial.$imagenName)){
			$rand=rand(1,999999999);
			$imgFinal=$rand.'.'.$ext;
			if(file_exists($rutaFinal.$imgFinal)){
				$rand=rand(1,999999999);
				$imgFinal=$rand.'.'.$ext;
			}
			$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
			$row_CONSULTA = $CONSULTA -> fetch_assoc();
			if (strlen($row_CONSULTA[$position])>0 AND file_exists($rutaFinal.$row_CONSULTA[$position])) {
				unlink($rutaFinal.$row_CONSULTA[$position]);
			}
			copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
			$actualizar = $CONEXION->query("UPDATE $seccion SET $position = '$imgFinal' WHERE id = $id");
		}else{	
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen de descripcion   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['descripcionimg'])){
		$position=$_GET['position'];
	
				//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_GET['descripcionimg'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if(file_exists($rutaInicial.$imagenName)){
			$rand=rand(1,999999999);
			$imgFinal=$rand.'.'.$ext;
			if(file_exists($rutaDesc.$imgFinal)){
				$rand=rand(1,999999999);
				$imgFinal=$rand.'.'.$ext;
			}
			$CONSULTA = $CONEXION -> query("SELECT * FROM descripciones WHERE id = $id");
			$row_CONSULTA = $CONSULTA -> fetch_assoc();
			if (strlen($row_CONSULTA[$position])>0 AND file_exists($rutaDesc.$row_CONSULTA[$position])) {
				unlink($rutaDesc.$row_CONSULTA[$position]);
			}
			copy($rutaInicial.$imagenName, $rutaDesc.$imgFinal);
			$actualizar = $CONEXION->query("UPDATE descripciones SET $position = '$imgFinal' WHERE id = $id");
		}else{
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Logo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarlogo'])){
		
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['logoescuela'])>0) {
			unlink($rutaFinal.$row_CONSULTA['logoescuela']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET logoescuela = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la logoescuela en la base de datos";
			$fallo=1;
		}
	}

	//    Categoria Nueva                   
	if(isset($_POST['nuevacategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['txt'])) { $categoria=$_POST['txt'];   }else{	$categoria=false; $fallo=1; }
		if (isset($_POST['titulo'])) { $titulo=$_POST['titulo']; }else{ $titulo=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=(htmlentities($categoria, ENT_QUOTES));

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt,titulo,parent) VALUES ('$categoria','$titulo',0)";
			
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevo'])){ 
		// Insertar en la base de datos
		$sql = "INSERT INTO $seccion (fecha)".
			"VALUES ('$hoy')";
		if($insertar = $CONEXION->query($sql)){
			$exito=1;
			$legendSuccess .= "<br>Producto nuevo";
			$editarNuevo=1;
			$id=$CONEXION->insert_id;
			$subseccion='detalle';
		}else{
			$fallo=1;  
			$legendFail .= "<br>No se pudo agregar a la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['editar']) OR isset($editarNuevo)) {
		
		foreach ($_POST as $key => $value) {
			if ($key=='txt' OR $key=='txt1') {
				$dato = trim(str_replace("'", "&#039;", $value));
			}else{
				$dato = trim(htmlentities($value, ENT_QUOTES));
			}
			
			$actualizar = $CONEXION->query("UPDATE $seccion SET $key = '$dato' WHERE id = $id");
			$exito=1;
			$actualizado = 1;
			unset($fallo);
		}
	}



//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarPod'])){
		$rutaIMG="../img/contenido/".$seccionmain."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;

		if (strlen($pic)>0 AND file_exists($picRow)) {
			unlink($picRow);
		}


		$rutaIMG="../img/contenido/".$seccion."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['id'].'-orig.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}
		$pic=$rowConsulta['id'].'-sm.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE producto = $id");
			$exito=1;
			$legendSuccess .= "<br>Producto eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicredes'])){
		$rutaFinal="../img/contenido/".$seccionmain."/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar PDF    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpdf'])){
		$rutaFinal="../img/contenido/".$seccion."pdf/";
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['pdf'])>0) {
			unlink($rutaFinal.$row_CONSULTA['pdf']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET pdf = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Ficha eliminada';
		}else{
			$legendFail .= "<br>No se encontró en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto Redes    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpicgallery'])){
		include '../../../includes/connection.php';
		$rutaIMG="../../../img/contenido/".$seccion."/";
		$id=$_POST['id'];
		$mensaje='';


		$pic=$id.'-orig.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
			$exito=1;
		}else{
			$mensaje.='<br>No existe orig';
		}
		$pic=$id.'-sm.jpg';
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
			$exito=1;
		}else{
			$mensaje.='<br>No existe sm';
		}

		if (isset($exito)) {
			$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $id");
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje.='Borrado';
		}else{
			$mensajeClase='danger';
			$mensajeIcon='exclamation-triangle';
			$mensaje.='<br>No se pudo borrar';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Descripcion     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevadescripcion'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['titulo'])) { $titulo=$_POST['titulo'];   }else{	$titulo=false; $fallo=1; }
		if (isset($_POST['descripcion'])) { $descripcion=$_POST['descripcion'];   }else{	$descripcion=false; $fallo=1; }
		// Sustituimos los caracteres inválidos
		$titulo=(htmlentities($titulo, ENT_QUOTES));
		$producto = $_POST['productoid'];

		$descripcion = trim(str_replace("'", "&#039;", $descripcion));
			
		// Actualizamos la base de datos

		if($titulo!="" && $descripcion!=""){
			$sql = "INSERT INTO descripciones (titulo,descripcion,producto) VALUES ('$titulo','$descripcion',$producto)";
			
			if($insertar = $CONEXION->query($sql)){
				$id = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva descripcion";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Descripcion %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['editardescripcion'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['editardescripcion'])) { $titulo=$_POST['titulo'];   }else{	$titulo=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$titulo=(htmlentities($titulo, ENT_QUOTES));
		$descripcion=$_POST['descripcion'];
		
		// Actualizamos la base de datos
		if($titulo!=""){
			$sql = "UPDATE descripciones SET titulo='$titulo',descripcion='$descripcion' WHERE id=$id";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Descripcion     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarDes'])){
		$rutaDesc='../img/contenido/descripciones/';
		$idDesc = $_REQUEST['desid'];
		$consulta= $CONEXION -> query("SELECT * FROM descripciones WHERE id = $idDesc");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaDesc.$pic;

		if (strlen($pic)>0 AND file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM descripciones WHERE id = $idDesc")){
			$exito=1;
			$legendSuccess .= "<br>Descripción eliminada";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Precio     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrarPrecio'])){
		include '../../../includes/connection.php';
		$idPrecio = $_POST['precioid'];

		if($borrar = $CONEXION->query("DELETE FROM productos_precios WHERE id = $idPrecio")){
			$exito=1;
			$legendSuccess .= "<br>Descripción eliminada";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}  


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Precio     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevoprecio'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['concepto'])) { $concepto=$_POST['concepto'];   }else{	$concepto=false; $fallo=1; }
		if (isset($_POST['precio'])) { $precio=$_POST['precio'];   }else{	$precio=false; $fallo=1; }
		if (isset($_POST['descuento'])){$descuento=$_POST['descuento'];   }else{	$descuento=false; $fallo=1; }
		if (isset($_POST['periodotxt'])){$periodotxt=$_POST['periodotxt'];   }else{	$periodotxt=false; $fallo=1; }
		// Sustituimos los caracteres inválidos
		$concepto=(htmlentities($concepto, ENT_QUOTES));
		$producto = $_POST['producto'];
		$divisa = $_POST['divisa'];
		// Actualizamos la base de datos
		if($concepto!="" && $precio!=0){
		
			$sql = "INSERT INTO productos_precios (concepto,precio,descuento,producto,divisa,periodotxt) VALUES ('$concepto',$precio,$descuento,$producto,$divisa,'$periodotxt')";
			if($insertar = $CONEXION->query($sql)){
				$id = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva Precio";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Periodo de  Precio     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['editarperiodo'])){ 
		
		if (isset($_POST['inicio'])) { $inicio=$_POST['inicio'];   }else{	$inicio=null;}
		if (isset($_POST['fin'])) { $fin=$_POST['fin'];   }else{	$fin=null;}
	
		$precioId = $_POST['producto'];
		// Actualizamos la base de datos
		if($inicio!="" && $fin!=""){
		
			$sql = "UPDATE productos_precios SET inicio='$inicio',fin='$fin' WHERE id=$precioId";

			if($insertar = $CONEXION->query($sql)){
				$id = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Precio editado correctamente";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Categoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['editarcategoria'])){ 
		// Obtenemos los valores enviados
		
		if (isset($_POST['editarcategoria'])) { $titulo=$_POST['titulo'];   }else{	$titulo=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$titulo=(htmlentities($titulo, ENT_QUOTES));
		$txt=$_POST['txt'];
		$idCat =$_POST['cat'];
		// Actualizamos la base de datos
		if($titulo!=""){
			$sql = "UPDATE $seccioncat SET titulo='$titulo',txt='$txt' WHERE id=$idCat";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nueva categoria";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Subategoria     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevasubcategoria'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['categoria'])) { $categoria=$_POST['categoria'];   }else{	$categoria=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$categoria=htmlentities($categoria, ENT_QUOTES);

		// Actualizamos la base de datos
		if($categoria!=""){
			$sql = "INSERT INTO $seccioncat (txt,parent) VALUES ('$categoria',$cat)";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Nueva subcategoria";
			}else{
				$fallo=1;  
				$legendFail .= "<br>No pudo agregarse a la base de datos ".$seccioncat.'-'.$cat.'-'.$categoria;
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminacat'])){
		$rutaIMG="../img/contenido/".$seccioncat."/";

		$consulta= $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
		$rowConsulta = $consulta-> fetch_assoc();

		$pic=$rowConsulta['imagen'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		$pic=$rowConsulta['imagenhover'];
		$picRow=$rutaIMG.$pic;
		if (file_exists($picRow)) {
			unlink($picRow);
		}

		if($borrar = $CONEXION->query("DELETE FROM $seccioncat WHERE id = $cat")){
			$exito=1;
			$legendSuccess .= "<br>Categoria eliminada";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar varios tipos de dato     %%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminargeneral'])){
		$tabla=$_GET['tabla'];
		$cantidad=$_GET['cantidad'];
		if($cantidad == 0){
			if($borrar = $CONEXION->query("DELETE FROM $tabla WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Eliminado";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se pudo borrar de la base de datos";
			}
		}
		else{
			$fallo=1;
				$legendFail .= "<br>No se pudo borrar porque tienes programas activos en esta categoria";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    		Eliminar Marca  		   %%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminaMarca'])){
		if($borrar = $CONEXION->query("DELETE FROM marcas WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Eliminado";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_FILES["uploadedfile"])){
		include '../../../includes/connection.php';
		$rutaFinal = '../../../img/contenido/'.$seccion.'/';

		$id=$_GET['id'];
		$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
		if($insertar = $CONEXION->query($sql)){
			$picId=$CONEXION->insert_id;

			$ret = array();
			
			$error = $_FILES["uploadedfile"]["error"];
			//You need to handle  both cases
			//If Any browser does not support serializing of multiple files using FormData() 
			if(!is_array($_FILES["uploadedfile"]["name"])) //single file
			{
		 	 	$archivoInicial = $_FILES["uploadedfile"]["name"];
				$i = strrpos($archivoInicial,'.');
				$l = strlen($archivoInicial) - $i;
				$ext = strtolower(substr($archivoInicial,$i+1,$l));

		 	 	$archivoFinal  =$picId.'.'.$ext;
		 		move_uploaded_file($_FILES["uploadedfile"]["tmp_name"],$rutaFinal.$archivoFinal);
		    	$ret[]= $archivoFinal;
			}
			else  //Multiple files, file[]
			{
			  $fileCount = count($_FILES["uploadedfile"]["name"]);
			  for($i=0; $i < $fileCount; $i++)
			  {
			  	$archivoInicial = $_FILES["uploadedfile"]["name"][$i];
				$i = strrpos($archivoInicial,'.');
				$l = strlen($archivoInicial) - $i;
				$ext = strtolower(substr($archivoInicial,$i+1,$l));

			  	$archivoFinal  =$picId.'.'.$ext;
				move_uploaded_file($_FILES["uploadedfile"]["tmp_name"][$i],$rutaFinal.$archivoFinal);
			  	$ret[]= $archivoFinal;
			  }
			}
			//$actualizar = $CONEXION->query("UPDATE $seccionpic SET file = '$archivoFinal' WHERE id = $picId");
			echo json_encode($ret);



		    // %%%%%%%%%%%%%%%%%%%
		    //    MINIATURAS
		    // %%%%%%%%%%%%%%%%%%%
				$xs=1;
				$sm=1;
				$lg=1;

				// Leer el archivo para hacer la nueva imagen
				$original = imagecreatefromjpeg($rutaFinal.$archivoFinal);

				// Tomamos las dimensiones de la imagen original
				$ancho  = imagesx($original);
				$alto   = imagesy($original);


				if ($xs==1) {
					//  Imagen xs
					$miniaturaName=$rutaFinal.$picId."-xs.jpg";
					$anchoNuevo = 80;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($sm==1) {
					//  Imagen sm
					$miniaturaName=$rutaFinal.$picId."-sm.jpg";
					$anchoNuevo = 400;
					$altoNuevo  = $anchoNuevo*$alto/$ancho;

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

				if ($lg==1) {
					//  Imagen lg
					$miniaturaName=$rutaFinal.$picId."-lg.jpg";
					if ($ancho>$alto) {
						$anchoNuevo = 1000;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;
					}else{
						$altoNuevo  = 1000;
						$anchoNuevo = $altoNuevo*$ancho/$alto;
					}

					// Creamos la imagen
					$archivoFinal = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
					// Copiamos el contenido de la original para pegarlo en el archivo nuevo
					imagecopyresampled($archivoFinal,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
					// Pegamos el contenido de la imagen
					if(imagejpeg($archivoFinal,$miniaturaName,90)){ // 90 es la calidad de compresión
						$exito=1;
					}
				}

		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar foto galería    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['borrarfoto'])){
		include '../../../includes/connection.php';
		$rutaFinal='../../../img/contenido/'.$seccion.'/';
		$id=$_POST['id'];
		// Borramos el archivo de imagen
		$filehandle = opendir($rutaFinal); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != "..") {
				// Id de la imagen
				if (strpos($file,'-')===false) {
					$imagenID = strstr($file,'.',TRUE);
				}else{
					$imagenID = strstr($file,'-',TRUE);
				}
				// Comprobamos que sean iguales
				if($imagenID==$id){
					$pic=$rutaFinal.$file;
					$exito=1;
					unlink($pic);
				}
			}
		}

		$borrar = $CONEXION->query("DELETE FROM $seccionpic WHERE id = $id");
		if(isset($exito)){
			echo "<div class='bg-success color-blanco'><i uk-icon='icon: trash;ratio:2;'></i> &nbsp; Borrado</div>";
		}else{
			echo "<div class='bg-danger color-blanco'><i uk-icon='icon: warning;ratio:2;'></i> &nbsp; No se pudo borrar</div>";
		}
	}


//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen Otros      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['imagen'])){
		$position=$_GET['position'];

		$xs=0;
		$sm=1;
		$lg=0;
		$rutaFinal=($position=='main')?'../img/contenido/'.$seccion.'main/':'../img/contenido/'.$seccion.'/';


		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_REQUEST['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if (!isset($fallo)) {
			if(file_exists($rutaInicial.$imagenName)){
				$pic=$id;
				if ($position=='gallery') {
					$sql = "INSERT INTO $seccionpic (producto) VALUES ($id)";
					$insertar = $CONEXION->query($sql);
					$pic=$CONEXION->insert_id;
				}elseif($position=='categoria'){
					$rutaFinal='../img/contenido/'.$seccion.'cat/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccioncat SET imagen = '$imgFinal' WHERE id = $cat");
				}elseif($position=='marcas'){
					$rutaFinal='../img/contenido/marcas/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM marcas WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE marcas SET imagen = '$imgFinal' WHERE id = $id");
				}elseif($position=='main'){
					$rutaFinal='../img/contenido/'.$seccionmain.'/';
					$imgFinal=rand(111111111,999999999).'.'.$ext;
					if(file_exists($rutaFinal.$imgFinal)){
						$imgFinal=rand(111111111,999999999).'.'.$ext;
					}
					$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
					$row_CONSULTA = $CONSULTA -> fetch_assoc();
					if ($row_CONSULTA['imagen']!='' AND file_exists($rutaFinal.$row_CONSULTA['imagen'])) {
						unlink($rutaFinal.$row_CONSULTA['imagen']);
					}
					$legendFail.='<br>Fail - '.$position;
					copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
					$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '$imgFinal' WHERE id = $id");
				}
			}else{
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}
		}

		// Borramos las imágenes que estén remanentes en el directorio files
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle);
	}


	// Borramos archivos de datos importados
	if (isset($_GET['deleteprev'])) {
		$rutaImportar='../img/contenido/importar/';
		$filehandle = opendir($rutaImportar); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != "ejemplo.csv") {
				if(file_exists($rutaImportar.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaImportar.$file);
				}
			}
		} 
		closedir($filehandle);
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir slaider Categorias  	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_GET['imagenslider'])){
			//Obtenemos la extensión de la imagen4
			$categoria =$_GET['cat']; 
			$imagenName=$_GET['imagenslider'];
			$i = strrpos($imagenName,'.');
			$l = strlen($imagenName) - $i;
			$ext = strtolower(substr($imagenName,$i+1,$l));


			// Imagenes a crear
			$xs=0;
			$sm=0;
			$med=0;
			$og=0;
			$nat400=0;
			$nat800=1;
			$nat1500=0;
			$Otra=0;

			// Dimensiones
			// Small
			$anchoXS=100;
			$altoXS =100;
			// Small
			$anchoSM=250;
			$altoSM =250;
			// Mediana
			$anchoMED=500;
			$altoMED =500;
			// OG
			$anchoOG=1000;
			$altoOG =700;
			// Otra
			$anchoOtra=1920;
			$altoOtra =780;



			// Si no es JPG cancelamos
			if ($ext!='jpg' and $ext!='jpeg') {
				$fallo=1;
				$legendFail='<br>El archivo debe ser JPG';
			}
			
			if(!file_exists($rutaInicial.$imagenName)){
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}

			if (!isset($fallo)) {

				$sql = "INSERT INTO carousel_categorias (orden,categoria) VALUES ('99',$categoria)";
				
				if($insertar = $CONEXION->query($sql)){

					$pic = $CONEXION->insert_id;
					$imgAux=$rutaCategorias.$pic."-orig.jpg";

					// Lo movemos al directorio final
					copy($rutaInicial.$imagenName, $imgAux);
					unlink($rutaInicial.$imagenName);


					// Leer el archivo para hacer la nueva imagen
					if ($ext=='jpg' or $ext=='jpeg') $original = imagecreatefromjpeg($imgAux);

					// Tomamos las dimensiones de la imagen original
					$ancho  = imagesx($original);
					$alto   = imagesy($original);

					if ($xs==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Pequeña
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-xs.jpg";

						$anchoNew=$anchoXS;
						$altoNew =$altoXS;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($sm==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Pequeña
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-sm.jpg";

						$anchoNew=$anchoSM;
						$altoNew =$altoSM;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($med==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Mediana
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-med.jpg";

						$anchoNew=$anchoMED;
						$altoNew =$altoMED;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($og==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen OG
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-og.jpg";

						$anchoNew=$anchoOG;
						$altoNew =$altoOG;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($alto/$ancho>(.7)){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen OG agregada";
						}
					}

					if ($nat400==1) {
						//  Imagen nat400
						$newName=$pic."-nat400.jpg";
						$anchoNuevo = 400;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($nat800==1) {
						//  Imagen nat1000
						$newName    = $pic.".jpg";
						$anchoNuevo = 800;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($nat1500==1) {
						//  Imagen nat1500
						$newName=$pic."-nat1500.jpg";
						if ($ancho>$alto) {
							$anchoNuevo = 1500;
							$altoNuevo  = $anchoNuevo*$alto/$ancho;
						}else{
							$altoNuevo  = 1500;
							$anchoNuevo = $altoNuevo*$ancho/$alto;
						}

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($Otra==1) {
						//  Imagen Otra
						$newName=$pic."-otra.jpg";
						$anchoNew=$anchoOtra;
						$altoNew =$altoOtra;
						$dst_x=0;
						$dst_y=0;
						$src_x=0;
						$src_y=0;
						$dst_w=$ancho;
						$dst_h=$alto;
						$src_w=$ancho;
						$src_h=$alto;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Corregimos el ancho
							$dst_w=$anchoProporcional;
							// Corregimos el ancho
							$dst_h=$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$src_x= $excedente/2;
							//$legendSuccess.='<br>Opt 2';
						}else{
							// Ancho proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Corregimos el alto
							$dst_h=$altoProporcional;
							// Corregimos el ancho
							$dst_w=$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$src_y= $excedente/4;
							//$legendSuccess.='<br>Opt 2';
							//$legendSuccess.='<br>Alto Original: '.$alto;
							//$legendSuccess.='<br>Alto Nuevo: '.$altoNew;
							//$legendSuccess.='<br>Alto Proporcional: '.$altoProporcional;
							//$legendSuccess.='<br>Excedente: '.$excedente;
							//$legendSuccess.='<br>Src Y: '.$src_y;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						imagecopyresampled($New,$original,$dst_x,$dst_y,$src_x,$src_y,$dst_w,$dst_h,$src_w,$src_h);

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaCategorias.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen Otra agregada";
						}
					}

					if ($originalPic==0) {
						unlink($imgAux);
					}else{
						rename ($imgAux, $rutaCategorias.$pic."-orig.jpg");
					}

					if($exito==1){
						$legendSuccess .= "<br>Imagen actualizada";
					}
				}else{
					$fallo=1;
					$legendFail='<br>No se pudo guardar en la base de datos';
				}
			}

		}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Imagen      	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_POST['borrarslider'])){
			include '../../../includes/connection.php';
			$id=$_POST['id'];
			// Borramos de la base de datos
			if($borrar = $CONEXION->query("DELETE FROM carousel_categorias WHERE id = $id")){
				$legendSuccess.= "<br> Imagen eliminada";
				$exito='success';
			}else{
				$legendFail .= "<br>No se pudo borrar de la base de datos";
				$fallo='danger';  
			}
			// Borramos el archivo de imagen
			

				
			$filehandle = opendir("../../../img/contenido/productoscat/carousel/"); // Abrir archivos
			
			
			while ($file = readdir($filehandle)) {
				if ($file != "." && $file != "..") {
					// Id de la imagen
					if (strpos($file,'-')===false) {
						$imagenID = strstr($file,'.',TRUE);
					}else{
						$imagenID = strstr($file,'-',TRUE);
					}
					// Comprobamos que sean iguales
					if($imagenID==$id){
						$pic=$rutaEscuela.$file;
						$exito=1;
						unlink($pic);
					}
				}
			}
			if (isset($exito)) {
				$mensajeClase='success';
				$mensajeIcon='check';
				$mensaje='Eliminado';
			}else{
				$mensajeClase='danger';
				$mensajeIcon='ban';
				$mensaje='No se pudo guardar';
			}

			echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
		}
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevodoc'])){ 
		if(isset($_POST['titulo'])){
			$titulo = $_POST['titulo'];
		}else{
			$fallo=1;
		}
		$categoria = $_POST['cat'];
		// Insertar en la base de datos
		$sql = "INSERT INTO programasdocumentos (titulo,categoria)".
			"VALUES ('$titulo',$categoria)";
		if($insertar = $CONEXION->query($sql)){
			$exito=1;
			$legendSuccess .= "<br>Producto nuevo";
				
		}else{
			$fallo=1;  
			$legendFail .= "<br>No se pudo agregar a la base de datos";
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%   Duplicar programa  	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(isset($_POST['clonarprograma'])){
	include '../../../includes/connection.php';

	if(isset($_POST['id'])){
		$id = $_POST['id'];
		$consulta = $CONEXION -> query("SELECT * FROM productos WHERE id = $id");
		if($consulta->num_rows > 0){
			$productosRow = $consulta ->fetch_assoc();
			$sql = "INSERT INTO productos (categoria,escuelaid,palabras,metadescription,precio,
				inscripcion,cuota,titulo,sku,txt,periodotxt,inicio,estatus,fecha,orden,divisa)"
				."VALUES ($productosRow[categoria],$productosRow[escuelaid],'$productosRow[palabras]','$productosRow[metadescription]',$productosRow[precio]
				,$productosRow[inscripcion],$productosRow[cuota],'$productosRow[titulo]','$productosRow[sku]','$productosRow[txt]','$productosRow[periodotxt]',
				$productosRow[inicio],$productosRow[estatus],'$hoy',$productosRow[orden],$productosRow[divisa])";
			
			if($insertar = $CONEXION->query($sql)){
				//BUSCAR E INSERTAR DESCRIPCIONES
				$newId=$CONEXION->insert_id;
				$consultaDescripciones = $CONEXION -> query("SELECT titulo,descripcion,imagen FROM descripciones WHERE producto = $id");

				while ($rowDescripcion = $consultaDescripciones -> fetch_assoc()){
					$titulo = $rowDescripcion['titulo'];
					$descripcion = $rowDescripcion['descripcion'];
					$imagen = $rowDescripcion['imagen'];
					$sqlD = "INSERT INTO descripciones (titulo,descripcion,imagen,producto)"."VALUES ('$titulo','$descripcion','$imagen',$newId)";
					$CONEXION->query($sqlD);
				}

				//BUSCAR E INSERTAR PRECIOS
				$consultaPrecios = $CONEXION -> query("SELECT * FROM productos_precios WHERE producto = $id");

				while ($rowPrecios = $consultaPrecios -> fetch_assoc()){
					if(!is_numeric($rowPrecios['inicio'])){
						$rowPrecios['inicio']='null';
					}
					if(!is_numeric($rowPrecios['fin'])){
						$rowPrecios['fin'] ='null';
					}
					$sqlP = "INSERT INTO productos_precios (concepto,precio,descuento,obligatorio,estatus,periodo,periodotxt,inicio,fin,producto,divisa)"
					."VALUES ('$rowPrecios[concepto]',$rowPrecios[precio],$rowPrecios[descuento],$rowPrecios[obligatorio],$rowPrecios[estatus],$rowPrecios[periodo],'$rowPrecios[periodotxto]',$rowPrecios[inicio],$rowPrecios[fin],$newId,$rowPrecios[divisa])";
				
					$CONEXION->query($sqlP);
				}

				$id=$newId;
				$exito=1;
				$legendSuccess .= "<br>Programa Clonado";
				$mensajeClase='success';
				$mensajeIcon='check';
				$mensaje='Programa duplicado correctamente';

			}else{
				$legendFail .= "<br>Error al insertar";
				$fallo=1;
				$mensajeClase='danger';
				$mensajeIcon='ban';
				$mensaje="Error al insertar";	
			}
			

		}else{
			$legendFail .= "<br>Error al duplicar el programa, no existe";
			$fallo=1;
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje="Error al duplicar el programa, no existe";
		}
		
	}else{
		$legendFail .= "<br>No se ha envido un id, error al duplicar el programa";
		$fallo=1;
		$mensajeClase='danger';
		$mensajeIcon='ban';
		$mensaje="No se ha envido un id, error al duplicar el programa";
	}

		header('Content-Type: application/json');
		echo json_encode(array('mensaje'=>'<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>',
		'id'=>$newId));
		exit;
	
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%   Actualizar fecha programa  	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if(isset($actualizado)){
	$today = date("Y-m-d H:i:s");  
	$actualizar = $CONEXION->query("UPDATE $seccion SET updatedAt = '$today' WHERE id = $id");
	$exito=1;
}



