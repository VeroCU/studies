<?php
$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$row_catalogo = $consulta -> fetch_assoc();
$cat=$row_catalogo['categoria'];
$esc = $row_catalogo['escuelaid'];
$divId = $row_catalogo['divisa'];


$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
$row_CATEGORY = $CATEGORY -> fetch_assoc();
$catNAME=$row_CATEGORY['titulo'];
$catParentID=$row_CATEGORY['parent'];

$CATEGORY2 = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
$row_CATEGORY2 = $CATEGORY2 -> fetch_assoc();
$catParent=$row_CATEGORY2['txt'];

$consultaEsc = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $esc");
$row_escuela = $consultaEsc -> fetch_assoc();
$tituloEscuela = $row_escuela['titulo'];
$escuelaid = $row_escuela['id'];
$fechaSQL=$row_catalogo['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);

$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divId");

if($consultaDivisas -> num_rows > 0){
	
	$rowDivisa = $consultaDivisas -> fetch_assoc();
	$divisa = $rowDivisa['nombre'];
}else{
	$divisa="";
}


$rutaFinal='../img/contenido/'.$seccion.'/';

echo '
	<div class="uk-width-auto margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'" >'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$row_catalogo['titulo'].'</a></li>
		</ul>
	</div>

	<div class="uk-width-expand@m uk-text-right margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
			<div>
				<button data-id="'.$id.'" class="uk-button uk-button-primary" id="duplicarbutton"><i uk-icon="copy"></i>&nbsp; Duplicar</button>
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-button uk-button-primary"><i uk-icon="pencil"></i>&nbsp; Editar</a>
			</div>

		</div>
	</div>

	<div class="uk-width-1-1">
	</div>
	<!--Descripcion y seo -->
	<div class="uk-width-1-2@s margin-v-20">
		
		<div class="uk-card uk-card-default uk-card-body">
			<div>
				<span class=" uk-text-muted">SKU:</span>
				'.$row_catalogo['sku'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">titulo:</span>
				'.$row_catalogo['titulo'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">escuela:</span>
				'.$tituloEscuela.'
			</div>
			
			<div>
				<span class="uk-text-capitalize uk-text-muted">Precio / Matricula: $</span>
				'.number_format($row_catalogo['precio'],0).'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Periodo:</span>
				'.$row_catalogo['periodotxt'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Cuota de servicio y transferencia: $</span>
				'.number_format($row_catalogo['cuota'],0).'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Divisa:</span>
				'.$divisa.'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">descuento:</span>
				'.$row_catalogo['descuento'].'%
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Palabras claves:</span>
				'.$row_catalogo['palabras'].'
			</div>
			<div>
				<span class="uk-text-capitalize uk-text-muted">Descripción:</span>
				'.$row_catalogo['txt'].'
			</div>
			<div class="uk-width-1-1 uk-text-right">
				<span class="uk-text-muted">Fecha de captura:</span>
				'.$fechaUI.'
			</div>
		</div>
		<div class="uk-width-1-1 uk-margin-top">
			<div class="uk-card uk-card-default uk-card-body">
				<div class="uk-width-1-1 uk-margin-top">
					<h4>SEO</h4>
					<span class="uk-text-capitalize uk-text-muted">titulo google:</span>
					'.$row_catalogo['title'].'
				</div>
				<div class="uk-width-1-1">
					<span class="uk-text-capitalize uk-text-muted">descripción google:</span>
					'.$row_catalogo['metadescription'].'
				</div>
			</div>
		</div>
		<!--Descripcion y seo  FIN-->
	</div>

	<!-- DESCRIPCIONES -->
	<div class="uk-width-1-2@s margin-v-10">
		<div class="uk-width-1-1">
			<div class="uk-card uk-card-default uk-card-body">
				<div class="uk-width-1-1 uk-text-right margin-bottom-20" style="margin-top: 0px;">
					<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
						<div>
							<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevadrescripcion&id='.$id.'" class="uk-button uk-button-success"><i uk-icon="plus"></i>&nbsp; Nueva descripcion</a>
						</div>	
					</div>
				</div>
				<div class="uk-width-1-1">
					<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
						<thead>
							<tr class="uk-text-muted">
								<th style="width:20px;"  ></th>
								<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; titulo</th>
								<th style="width:90px;"  ></th>
							</tr>
						</thead>
						<tbody id="conetent">';

						$sql = "SELECT * FROM descripciones WHERE producto = $id";
						$consulta = $CONEXION -> query($sql);
						$numItems=$consulta->num_rows;	
						
						
						while ($row_Consulta1 = $consulta -> fetch_assoc()){
							
							$descripcionId = $row_Consulta1['id'];
							

							$picTxt ='';
							$rutaFinal = '../img/contenido/descripciones/';
							
						
							$pic=$rutaFinal.$row_Consulta1['imagen'];

							if(file_exists($pic)){
								$picTxt='
								<div class="uk-inline">
									<i uk-icon="camera"></i>
									<div uk-drop="pos: right-justify">
										<img src="'.$pic.'" class="uk-border-rounded">
									</div>
								</div>';
							}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
								$pic=$row_Consulta1['imagen'];
								$picTxt= '
								<div class="uk-inline">
									<i uk-icon="camera"></i>
									<div uk-drop="pos: right-justify">
										<img src="'.$pic.'" class="uk-border-rounded">
									</div>
								</div>';
							}
						
							$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=descdetalle&id='.$row_Consulta1['id'];

							echo '
							<tr id="'.$prodID.'">
								<td>
									'.$picTxt.'
								</td>
								<td>
									<span class="uk-text-muted uk-hidden@m">titulo: </span>
									<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
								</td>
								<td class="uk-text-right@m">
									<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
									<span data-id="'.$row_Consulta1['id'].'" class="eliminadesc uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
								</td>
							</tr>';
						}
						echo '
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--FIN DESCRIPCIONES -->

 	<!-- PRECIOS / Tarifas -->
	<div class="uk-width-1-1@s uk-width-1-1@m margin-v-10">
		<div class="uk-width-1-1">
			<div class="uk-card uk-card-default uk-card-body">

				<div class="uk-width-1-1 uk-text-right margin-bottom-20" style="margin-top: 0px;">

					<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
						<div class="uk-width-expand@s text-xl uk-text-left">Listado de precios. </div>
						<div>
							<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevoprecio&id='.$id.'" class="uk-button uk-button-success"><i uk-icon="plus"></i>&nbsp; Nuevo precio</a>
						</div>	
					</div>
				</div>
				<div class="uk-width-1-1">
					<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
						<thead>
							<tr class="uk-text-muted">
								<th style="width:300px;"  class="uk-text-left"> &nbsp;&nbsp; Concepto precio</th>
								<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Precio</th>
								<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Divisa</th>
								<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Descuento</th>
								<th style="width:90px;"  class="uk-text-center">Activo</th>
								<th style="width:90px;"  class="uk-text-center">Obligatorio</th>
								<th style="width:90px;"  ></th>
							</tr>
						</thead>
						<tbody id="concepto">';

						$sql = "SELECT * FROM productos_precios WHERE producto = $id ORDER BY obligatorio DESC";
						$consulta = $CONEXION -> query($sql);
						$numItems=$consulta->num_rows;	
						
						
						while ($row_Consulta1 = $consulta -> fetch_assoc()){
							//debug($row_Consulta1);
							$estatusIcon=($row_Consulta1['estatus']==0)?'off uk-text-muted':'on uk-text-primary';
							$obligatorioIcon=($row_Consulta1['obligatorio']==0)?'off uk-text-muted':'on uk-text-primary';

							$precioId = $row_Consulta1['id'];
							$divID = $row_Consulta1['divisa'];
							$consultaDiv = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divID");
							$divisaRow = $consultaDiv -> fetch_assoc();
							$divisa = $divisaRow['nombre'];
						
							$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=preciodetalle&id='.$row_Consulta1['id'];

							echo '
							<tr id="'.$precioId.'">
								<td>
									<span class="uk-text-muted uk-hidden@m">Concepto: </span>
									<input value="'.$row_Consulta1['concepto'].'" class="editarajax uk-input uk-form-blank" data-tabla="productos_precios" data-campo="concepto" data-id="'.$precioId.'" tabindex="9">
								</td>
								<td>
									<span class="uk-text-muted uk-hidden@m">Concepto: </span>
									<input value="'.$row_Consulta1['precio'].'" class="editarajax uk-input uk-form-blank" data-tabla="productos_precios" data-campo="precio" data-id="'.$precioId.'" tabindex="9">
								</td>
									<td>
									<span class="uk-text-muted uk-hidden@m">Concepto: </span>
									<input value="'.$divisa.'" class="uk-input uk-form-blank" tabindex="9">
								</td>
								<td>
									<span class="uk-text-muted uk-hidden@m">Concepto: </span>
									<input value="'.$row_Consulta1['descuento'].'" class="editarajax uk-input uk-form-blank" data-tabla="productos_precios" data-campo="descuento" data-id="'.$precioId.'" tabindex="9">
								</td>
								<td class="uk-text-center@m">
									<span class="uk-text-muted uk-hidden@m">Activo: </span>
									<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="productos_precios" data-campo="estatus" data-id="'.$precioId.'" data-valor="'.$row_Consulta1['estatus'].'"></i>
								</td>
								<td class="uk-text-center@m">
									<span class="uk-text-muted uk-hidden@m">Obligatorio: </span>
									<i class="estatuschange fa fa-lg fa-toggle-'.$obligatorioIcon.' uk-text-muted pointer" data-tabla="productos_precios" data-campo="obligatorio" data-id="'.$precioId.'" data-valor="'.$row_Consulta1['obligatorio'].'"></i>
								</td>
								<td class="uk-text-right@m">
									<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
									<span data-id="'.$row_Consulta1['id'].'" class="eliminaprecio uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
								</td>
							</tr>';
						}
						echo '
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="uk-width-1-1@s uk-width-1-1@m margin-v-10">
		<div class="uk-width-1-1">
			<div class="uk-card uk-card-default uk-card-body">
				<h3 class="uk-text-center">Sección de fotos.</h3>';
				$rutaFinal = '../img/contenido/escuelas/';
				$pic = $rutaFinal.$row_escuela['logoescuela'];
				
				if(file_exists($pic)){
					echo '
						<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
							<div class="uk-card uk-card-default uk-card-body uk-text-center">
								<div class="uk-margin" uk-lightbox>
									<a href="'.$pic.'">
										<img src="'.$pic.'" class="uk-border-rounded margin-top-20">
									</a>
								</div>
							</div>
						</div>';
				}else{
					echo '
						<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
							<div class="uk-card uk-card-default uk-card-body uk-text-center padding-5">
								<a href="javascript:borrarfoto(\''.$picId.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
								<br>
								Imagen rota<br>
								<i uk-icon="icon:ban;ratio:2;"></i><br>
								'.$pic.'
							</div>
						</div>';
				}

	echo '	
			<div class="uk-width-1-1">';
			$rutaCarousel = '../img/contenido/escuelas/carousel/';
			
			$consultaCarousel = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $escuelaid");

			echo '<div uk-grid>';
				while($rowSlider = $consultaCarousel -> fetch_assoc()){
					$imgId = $rowSlider['id'].".jpg";
					echo '

					<div class="uk-width-1-6@l uk-width-1-3@m uk-width-1-1@s uk-margin-bottom">
						<div class="uk-card uk-card-default uk-card-body uk-text-center padding-5">
							<div class="uk-margin-small" uk-lightbox>	
								<img src="'.$rutaCarousel.$imgId.'" class="uk-border-rounded margin-top-5">
							</div>
						</div>
					</div>';
				}

	echo '
			</div>
		</div>
	</div>
	';

/* echo '
	<!-- Fotografías -->
	<div class="uk-width-1-1 margin-top-50">
		<h3 class="uk-text-center">Fotografías Carrousel</h3>
	</div>

	<div class="uk-width-1-1">
		<div id="fileuploader">
			Cargar
		</div>
	</div>
	<div class="uk-width-1-1 uk-text-center">
		<div uk-grid id="pics" class="uk-grid-small uk-grid-match sortable" data-tabla="'.$seccionpic.'">';

	$rutaFinal = '../img/contenido/productos/';
	$consultaPIC = $CONEXION -> query("SELECT * FROM $seccionpic WHERE producto = $id ORDER BY orden,id");
	$numProds=$consultaPIC->num_rows;
	while ($row_consultaPIC = $consultaPIC -> fetch_assoc()) {
		$picId=$row_consultaPIC['id'];
		$pic=$rutaFinal.$picId.'.jpg';
		if(file_exists($pic)){
			echo '
				<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
					<div class="uk-card uk-card-default uk-card-body uk-text-center">
						<a href="javascript:borrarfoto(\''.$picId.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
						<div class="uk-margin" uk-lightbox>
							<a href="'.$pic.'">
								<img src="'.$pic.'" class="uk-border-rounded margin-top-20">
							</a>
						</div>
					</div>
				</div>';
		}else{
			echo '
				<div class="uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom" id="'.$picId.'">
					<div class="uk-card uk-card-default uk-card-body uk-text-center">
						<a href="javascript:borrarfoto(\''.$picId.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
						<br>
						Imagen rota<br>
						<i uk-icon="icon:ban;ratio:2;"></i><br>
						'.$pic.'
					</div>
				</div>';
		}
	}
	echo '
		</div>
	</div>
	';

	echo'
		<div class="uk-width-1-2@s">
			<div class="">
				<h3 class="uk-text-logo">Logo de Programa</h3>
			</div>
			<div class="uk-width-1-2@s">
				<div class="uk-text-muted">
					Dimensiones recomendadas: 500 x 500 px
				</div>
				<div id="fileuploaderLogo">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">';

			// sharePic
				$logo=$rutaFinal.$row_catalogo['imagen'];
				if(strlen($row_catalogo['imagen'])>0 AND file_exists($logo)){
					echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$logo.'" target="_blank">
									<img src="'.$logo.'" class="uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarlogo"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
				}else{
					echo '
							<div class="uk-panel uk-text-center">
								<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
									Falta imagen<br><br>
								</p>
							</div>';
				}

			echo '
			</div>
		</div>
	';*/

// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';



$scripts='


	// Eliminar foto
	function borrarfoto (id) { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarfoto: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	}

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"modulos/'.$seccion.'/acciones.php?id='.$id.'",
			multiple: true,
			maxFileCount:1000,
			fileName:"uploadedfile",
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6000000,
			showFileCounter: false,
			showDelete: "false",
			showPreview:false,
			showQueueDiv:true,
			returnType:"json",
			onSuccess:function(files,data,xhr){
				console.log(data);
				var l = data[0].indexOf(".");
				var id = data[0].substring(0,l);
				$("#pics").append("';
				$scripts.='<div class=\'uk-width-1-4@l uk-width-1-2@m uk-width-1-1@s uk-margin-bottom\' id=\'"+id+"\'>';
				$scripts.='<div class=\'uk-card uk-card-default uk-card-body uk-text-center\'>';
				$scripts.='<div>';
				$scripts.='<a href=\'javascript:borrarfoto(\""+id+"\")\' class=\'uk-icon-button uk-button-danger\' uk-icon=\'trash\'></a>';
				$scripts.='</div>';
				$scripts.='<div class=\'uk-margin\' uk-lightbox>';
				$scripts.='<a href=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='<img src=\''.$rutaFinal.'"+data+"\'>';
				$scripts.='</a>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='</div>';
				$scripts.='");';
				$scripts.='
			}
		});
	});



	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=main&imagen=\'+data);
			}
		});

		$("#fileuploaderpdf").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "pdf",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&cat='.$cat.'&id='.$id.'&position=pdf&imagen=\'+data);
			}
		});
	});

	// Eliminar descripcion
	$(".eliminadesc").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este descripcion?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&borrarDes&id='.$id.'&desid="+id);
		} 
	});

	// Eliminar precio
	$(".eliminaprecio").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este precio?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarPrecio: 1,
					id: '.$id.',
					precioid:id
				}
			})
			.done(function(data) {
				console.log("data",data);
				UIkit.notification.closeAll();
				UIkit.notification(data.mensaje);
				$(\'#\'+id).fadeOut("slow");
				
			});
			
		} 
	});

	// Borrar foto redes
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpicredes");
		} 
	});

	// Borrar PDF
	$(".borrarpdf").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpdf");
		} 
	});

	//duplicar Programa
	$("#duplicarbutton").click(function(){
		
		var id = $(this).attr(\'data-id\');

		$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					clonarprograma: 1,
					id: id
				}
			})
			.done(function(data) {
				console.log("data",data);
				UIkit.notification.closeAll();
				UIkit.notification(data.mensaje);
				setTimeout(function(){
					window.location = (\'index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id=\'+data.id);
				}, 3000);
			});
	});


	$(document).ready(function() {
		$("#fileuploaderLogo").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg,png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=imagen&logo=\'+data);
			}
		});
	});

	// Borrar icono
	$(".borrarlogo").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarlogo");
		} 
	});
	';
