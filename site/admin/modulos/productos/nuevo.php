<?php
if (isset($_GET['cat'])) {
	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $cat");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catNAME=$row_CATEGORY['txt'];
	$catParentID=$row_CATEGORY['parent'];

	$CATEGORY = $CONEXION -> query("SELECT * FROM $seccioncat WHERE id = $catParentID");
	$row_CATEGORY = $CATEGORY -> fetch_assoc();
	$catParent=$row_CATEGORY['txt'];
	$esc = $row_CATEGORY['escuelaid'];

	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Programas</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=parent">Categorías</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categorias&cat='.$catParentID.'">'.$catParent.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=catdetalle&cat='.$cat.'">'.$catNAME.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo&cat='.$cat.'" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}else{
	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Programas</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
		</ul>
	</div>';
}
?>

<form action="index.php" class="uk-width-1-1" method="post" name="editar" onsubmit="return checkForm(this);">
	<input type="hidden" name="nuevo" value="1">
	<input type="hidden" name="seccion" value="<?=$seccion?>">

	<div uk-grid class="uk-grid-small uk-child-width-1-3@l uk-child-width-1-2@m">
		<div>
			<label class="uk-text-uppercase" for="sku">sku</label>
			<input type="text" class="uk-input" name="sku" placeholder="sku">
		</div>
		<div>
			<label class="uk-text-capitalize" for="titulo">titulo</label>
			<input type="text" class="uk-input" name="titulo" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="precio">Precio /  Matricula:</label>
			<input type="number" class="uk-input" name="precio" placeholder="Ej. 1850" min="0" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="periodotxt">Periodo:</label>
			<input type="txt" class="uk-input" name="periodotxt" placeholder="Ej. por año" required>
		</div>
		<div>
			<label class="uk-text-capitalize" for="inscripcion">Precio de inscripcion:</label>
			<input type="number" class="uk-input" name="inscripcion" placeholder="Ej. 1850" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="seguro">Seguro:</label>
			<input type="number" class="uk-input" name="seguro" placeholder="Ej. 1850" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="cuota">Cuota de servicio y transferencia:</label>
			<input type="number" class="uk-input" name="cuota" placeholder="Ej. 1850" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="descuento">descuento</label>
			<input type="number" class="uk-input descuento" name="descuento" placeholder="Ej. 10" value="0" max="100" min="0">
		</div>
		<div>
			<label class="uk-text-capitalize" for="categoria">categoria</label>
			<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>
			<?php
			$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
			while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
				$parentId=$row_CONSULTA['id'];
				$parentTxt=$row_CONSULTA['txt'];
				if (isset($cat) AND $cat==$parentId) {
						$estatus='selected';
					}else{
						$estatus='';
					}
				
					echo '
						<option value="'.$parentId.'" '.$estatus.'>'.$row_CONSULTA['titulo'].'</option>';
			}
						echo '
						</select>
		</div>';
		 ?>
		 <div>
			<label class="uk-text-capitalize" for="escuelaid">Escuela</label>
			<select name="escuelaid" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1">
			<?php
			$CONSULTAESCUELA = $CONEXION -> query("SELECT * FROM escuelas ORDER BY titulo");
		
			while ($row_CONSULTA1 = $CONSULTAESCUELA -> fetch_assoc()) {
				$nombreEscuela = $row_CONSULTA1['titulo'];
				$escuelaId=$row_CONSULTA1['id'];
				
					echo '
						<option value="'.$escuelaId.'">'.$nombreEscuela.'</option>';
			}
						echo '
						</select>
		</div>';
		 ?>
		  <div>
			<label class="uk-text-capitalize" for="divisa">Divisa</label>
			<select name="divisa" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1">
			<?php
			//divisas
			$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas");
			while ($rowDivisas = $consultaDivisas -> fetch_assoc()) {
				
				$nombreDivisa=$rowDivisas['nombre'];
				if (isset($esc) AND $esc==$nombreDivisa) {
						$estatus='selected';
					}else{
						$estatus='';
					}
				
					echo '
						<option value="'.$rowDivisas['id'].'" '.$estatus.'>'.$nombreDivisa.'</option>';
			}
						echo '
						</select>
		</div>';
		 ?>
		 
		<div class="uk-width-1-1">
			<div class="margin-top-20">
				<label for="txt">Descripción</label>
				<textarea class="editor" name="txt"></textarea>
			</div>
		</div>	
		<div class="uk-width-1-1">
			<div class="margin-top-20">
				<label for="palabras">Palabras clave:</label>
				<input id="form-tags-2"  class="uk-input" name="palabras" type="text">
			</div>
		</div>
	</div>
	<div uk-grid>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="title">titulo google</label>
			<input type="text" class="uk-input" name="title" placeholder="Término como alguien nos buscaría">
		</div>
		<div class="uk-width-1-1">
			<label class="uk-text-capitalize" for="metadescription">descripción google</label>
			<textarea class="uk-textarea" name="metadescription" placeholder="Descripción explícita para que google muestre a quienes nos vean en las búsquedas"></textarea>
		</div>
		<div class="uk-width-1-1 uk-text-center">
			<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
			<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
		</div>
	</div>
</form>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<script>
	
	$(function() {
		
		$('#form-tags-2').tagsInput({
			'onAddTag': function(input, value) {
				console.log('tag added', input.value, value);
			},
			'onRemoveTag': function(input, value) {
				console.log('tag removed', input, value);
			},
			'onChange': function(input, value) {
				console.log('change triggered', input, value);
			}
		});
				
	});
</script>



	
