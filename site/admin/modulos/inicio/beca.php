<?php 
echo '
<div class="uk-width-auto margin-top-20 uk-text-left">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?seccion='.$seccion.'">'.$seccion.'</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion='.$subseccion.'" class="color-red">Beca</a></li>
	</ul>
</div>
';

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = 1");
$rowConsulta = $consulta -> fetch_assoc();
foreach ($rowConsulta as $key => $value) {
	${$key}=$value;
}


$picTxt='
		<div class="uk-margin">
			<img src="../img/design/camara.jpg" class="uk-border-rounded">
		</div>';
$buttonBorrar='<span class="uk-button uk-button-default">Eliminar</span>';
$pic=$rutaFinal.$imagen6;
if(file_exists($pic) AND strlen($imagen6)>0){
	$buttonBorrar='<a href="javascript:eliminaPic(6)" class="uk-button uk-button-danger">Eliminar</a>';
	$picTxt='
		<div class="uk-margin">
			<img src="'.$pic.'" class="uk-border-rounded">
		</div>';
}


echo '
<div class="uk-width-1-1 margin-top-50">
	<div class="uk-container">
		<div class="uk-card uk-card-default uk-card-body">
			<h3 class="uk-text-center">Detalle de la Beca</h3>
			<form action="index.php" method="post">

				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">
				<input type="hidden" name="editartextobeca" value="1">

				<div>
					<div class="uk-margin">
						<label>Título </label>
						<input class="uk-input" name="titulo1" placeholder="Título" value="'.$titulo1.'">
					</div>
					<div class="uk-margin">
						<label>Descripción Corta</label>
						<textarea class="uk-textarea" style="min-height:150px;" name="texto1" placeholder="Descripción">'.$texto1.'</textarea>
					</div>
					<div class="uk-margin">
						<label>Descripción Larga</label>
						<textarea class="editor" style="min-height:300px;" name="texto2" placeholder="Descripción">'.$texto2.'</textarea>
					</div>

					<div class="uk-margin">
						<label>Subtítulo 1</label>
						<input class="uk-input" name="titulo3" placeholder="Título" value="'.$titulo3.'">
					</div>
					<div class="uk-margin">
						<label>Descripción </label>
						<textarea class="editor" style="min-height:300px;" name="texto3" placeholder="Descripción">'.$texto3.'</textarea>
					</div>

					<div class="uk-margin">
						<label>Subtítulo 2 </label>
						<input class="uk-input" name="titulo4" placeholder="Título" value="'.$titulo4.'">
					</div>
					<div class="uk-margin">
						<label>Descripción </label>
						<textarea class="editor" style="min-height:300px;" name="texto4" placeholder="Descripción">'.$texto4.'</textarea>
					</div>

					<div class="uk-margin">
						<label>Titulo  comparte Izq.</label>
						<input class="uk-input" name="titulocomparte" placeholder="Titulo" value="'.$rowConsulta['titulocomparte'].'">
					</div>
					<div class="uk-margin">
						<label>Descripción comparte izq </label>
						<textarea class="editor" style="min-height:300px;" name="textocomparte" placeholder="Descripción">'.$rowConsulta['textocomparte'].'</textarea>
					</div>

					<div class="uk-margin">
						<label>Titulo  comparte Der.</label>
						<input class="uk-input" name="titulocomparte2" placeholder="Titulo" value="'.$rowConsulta['titulocomparte2'].'">
					</div>
					<div class="uk-margin">
						<label>Descripción comparte der </label>
						<textarea class="editor" style="min-height:300px;" name="textocomparte2" placeholder="Descripción">'.$rowConsulta['textocomparte2'].'</textarea>
					</div>

					<div class="uk-margin uk-text-center">
						<button class="uk-button uk-button-primary uk-button-large"> GUARDAR </button>
					</div>

				</div>

			</form>
		</div>
	</div>
</div>
';



echo '
	<div class="uk-width-1-1">
		<div class="uk-container">
			<div class="uk-card uk-card-body uk-card-default">
				<div uk-grid>
					<div class="uk-width-auto uk-text-center">
						'.$buttonBorrar.'
						<a href="#upload" data-campo="6" uk-toggle class="abremodal uk-button uk-button-primary">Subir</a>
					</div>
					<div class="uk-width-expand uk-text-center">
						'.$picTxt.'
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="upload" uk-modal>
	    <div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-outside" type="button" uk-close></button>
			<input type="hidden" id="itemcampo">
			<div id="fileuploader">
				Cargar
			</div>
	    </div>
	</div>';


$scripts='
	function eliminaPic (campo) {
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true){
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&borrarimagen=1&campo=imagen"+campo);
		}
	};

	$(".abremodal").click(function(){
		var campo = $(this).attr("data-campo");
		$("#itemcampo").val(campo);
	});

	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){
				var campo = $("#itemcampo").val();
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&campo=imagen\'+campo+\'&uploadedfile=\'+data);
			}
		});
	});

	';
?>