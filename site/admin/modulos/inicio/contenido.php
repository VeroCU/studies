<?php
$subsecciones[] = array(
	  'title' => 'Fondo superior',
 'subseccion' => 'bg-superior',
	   'icon' => 'image');

$subsecciones[] = array(
	  'title' => 'Slider superior',
 'subseccion' => 'slider-txt',
	   'icon' => 'comments');

$subsecciones[] = array(
	  'title' => 'Imágenes flotantes',
 'subseccion' => 'pic-txt',
	   'icon' => 'images');

$subsecciones[] = array(
	  'title' => 'beca',
 'subseccion' => 'beca',
	   'icon' => 'user-graduate');

$subsecciones[] = array(
	  'title' => 'encontrar',
 'subseccion' => 'encontrar',
	   'icon' => 'search-location');

$subsecciones[] = array(
	  'title' => 'imagen',
 'subseccion' => 'bg-inferior',
	   'icon' => 'file-image');


echo '
<div class="uk-width-auto@m margin-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">'.$seccion.'</a></li>
	</ul>
</div>




<div class="uk-width-1-1">
	<div class="uk-container">
		<div uk-grid class="uk-flex-center" style="margin-top: 100px;">';


		foreach ($subsecciones as $key => $value) {

			echo '
			<div class="uk-width-auto">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$value['subseccion'].'">
					<div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle uk-text-center uk-text-capitalize" style="width: 250px;height: 250px;">
						<div>
							<i class="fa fa-3x fa-'.$value['icon'].'"></i>
							<br><br>
							'.$value['title'].'
						</div>
					</div>
				</a>
			</div>';

		}	

echo '
		</div>
	</div>
</div>';



