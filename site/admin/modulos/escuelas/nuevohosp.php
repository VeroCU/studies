<?php

	$id = $_GET['id'];
	$PRODUCT = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
	$rowProduct = $PRODUCT -> fetch_assoc();
	$productName = $rowProduct['titulo'];

	echo '
	<div class="uk-width-1-1 margin-v-20 uk-text-left">
			<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?seccion='.$seccion.'">Escuelas</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$productName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevohosp&cat='.$id.'" class="color-red">Nuevo hospedaje</a></li>
		</ul>
	</div>';
?>
<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" class="uk-width-1-1" method="post" name="nuevohosp" onsubmit="return checkForm(this);">
			<input type="hidden" name="nuevohospedaje" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="hospdetalle">
			<input type="hidden" name="escuelaid" value="<?=$id?>">

			<div uk-grid class="uk-grid-small uk-child-width-1-1@l uk-child-width-1-1@m">
				<div>
					<label class="uk-text-capitalize" for="titulo">titulo</label>
					<input type="text" class="uk-input" name="titulo" required>
				</div>
				<div>
					<label class="uk-text-capitalize" for="titulo">Precio</label>
					<input type="number" class="uk-input" name="precio" min="0" step="any"  required>
				</div>
				<div class="uk-width-1-1">
					<div class="margin-top-20">
						<label for="txt">Descripción</label>
						<textarea class="editor" name="txt"></textarea>
					</div>
				</div>	
				<div>
					<label class="uk-text-capitalize" for="divisa">Divisa</label>
					<select name="divisa" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1">
					<?php
					//divisas
					$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas");
					while ($rowDivisas = $consultaDivisas -> fetch_assoc()) {
						
						$nombreDivisa=$rowDivisas['nombre'];
						if (isset($esc) AND $esc==$nombreDivisa) {
								$estatus='selected';
							}else{
								$estatus='';
							}
						
							echo '
								<option value="'.$rowDivisas["id"].'" '.$estatus.'>'.$nombreDivisa.'</option>';
					}
								echo '
								</select>
				</div>';
				 ?>
				<div class="uk-width-1-1 uk-text-center">
					<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=detalle&id=<?=$id?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
					<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>
			</div>	
		</form>
	</div>
</div>


<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>
