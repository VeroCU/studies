<?php 
$campo=$_GET['campo'];
$valor=$_GET['valor'];

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE $campo LIKE '%$valor%' ORDER BY $campo");
$numItems=$consulta->num_rows;

echo '
<div class="uk-width-1-3@s margen-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Escuelas</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&campo='.$campo.'&valor='.$valor.'" class="color-red">Buscar '.$valor.' &nbsp; <span class="uk-text-muted uk-text-lowercase"> &nbsp; <b>'.$numItems.'</b> escuelas</span></a></li>
	</ul>
</div>

<div class="uk-width-1-1">
	<div uk-grid class="uk-grid-small uk-child-width-1-3@m uk-child-width-1-2">
		
		<div><label class="pointer"><i uk-icon="search"></i> titulo<br><input type="text" class="uk-input search" data-campo="titulo"></label></div>
		
	</div>
</div>

<div class="uk-width-1-1">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th width="90px"></th>
				<th width="50px">Orden</th>
				<th onclick="sortTable(0)">Titulo</th>
				<th>Categoria</th>
				<th width="120px"></th>
			</tr>
		</thead>
		<tbody id="conetent">';

		while ($row_Consulta1 = $consulta -> fetch_assoc()) {
			$prodID=$row_Consulta1['id'];
			$catId=$row_Consulta1['categoria'];
			//debug($row_Consulta1);
			$CONSULTA4 = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
			$row_CONSULTA4 = $CONSULTA4 -> fetch_assoc();
			$categoriaTxt=$row_CONSULTA4['titulo'];

			
			$picTxt = '';
			$rutaFinal = '../img/contenido/escuelas/';
			$picId=$row_Consulta1['imagen'];
			$pic=$rutaFinal.$picId;
			if(file_exists($pic)){
				$picTxt='
				<div class="uk-inline">
					<i uk-icon="camera"></i>
					<div uk-drop="pos: right-justify">
						<img src="'.$pic.'" class="uk-border-rounded">
					</div>
				</div>';
			}elseif(strlen($row_Consulta1['imagen'])>0 AND strpos($row_Consulta1['imagen'], 'ttp')>0){
				$pic=$row_Consulta1['imagen'];
				$picTxt= '
				<div class="uk-inline">
					<i uk-icon="camera"></i>
					<div uk-drop="pos: right-justify">
						<img src="'.$pic.'" class="uk-border-rounded">
					</div>
				</div>';
			}
			


			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];


			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					<input value="'.$row_Consulta1['orden'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="sku" data-id="'.$prodID.'" tabindex="10">
				</td>
				<td>
					<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccion.'" data-campo="titulo" data-id="'.$prodID.'" tabindex="9">
				</td>
				<td class="uk-text-center">
					'.$categoriaTxt.'
				</td>	
				<td class="uk-text-right@m">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$row_Consulta1['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>




<div style="min-height:300px;">
</div>


<div>
	<div id="buttons">
		<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>&subseccion=nuevo" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>


<div id="nuevacat" uk-modal="center: true">
	<div class="uk-modal-dialog uk-modal-body">
		<a class="uk-modal-close uk-close"></a>
		<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

			<input type="hidden" name="nuevacategoria" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">
			<input type="hidden" name="subseccion" value="categorias">

			<label for="categoria">Nombre de la nueva categoría</label><br><br>
			<input type="text" class="uk-input" name="categoria" tabindex="10" required><br><br>
			<input type="submit" name="send" value="Agregar" tabindex="10" class="uk-button uk-button-primary">
		</form>
	</div>
</div>

<?php 
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?"); 
		if (statusConfirm == true) { 
			//window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&campo='.$campo.'&valor='.$valor.'&borrarPod&id="+id);
		} 
	});

	$(".search").keypress(function(e) {
		if(e.which == 13) {
			var campo = $(this).attr("data-campo");
			var valor = $(this).val();
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=search&campo="+campo+"&valor="+valor);
		}
	});
	';
?>

