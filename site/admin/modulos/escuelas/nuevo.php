<?php 
echo '
<div class="uk-width-1-1 margen-v-20 uk-text-left">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?seccion='.$seccion.'">'.$seccion.'</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion=nuevo" class="color-red">Nuevo</a></li>
	</ul>
</div>
';		
?>
<style type="text/css">
	.next{margin:20px 0px;display:inline-block;vertical-align:top;}
</style>
<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" method="post" name="editar" class="uk-grid-small" uk-grid>
			<input type="hidden" name="nuevo" value="1">
			<input type="hidden" name="seccion" value="<?=$seccion?>">

			<div class="uk-width-1-1 uk-margin">
				<label class="uk-text-capitalize" for="titulo">Titulo</label>
				<input type="text" class="uk-input" name="titulo" id="titulo" autofocus>
			</div>	
			 <div class="uk-width-1-2">
				<label class="uk-text-capitalize" for="categoria">categoria</label>
				<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>
				<?php
				$CONSULTA = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
				while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
					$parentId=$row_CONSULTA['id'];
					$parentTxt=$row_CONSULTA['txt'];
					if (isset($cat) AND $cat==$parentId) {
							$estatus='selected';
						}else{
							$estatus='';
						}
					
						echo '
							<option value="'.$parentId.'" '.$estatus.'>'.$row_CONSULTA['titulo'].'</option>';
				}
							echo '
							</select>
			</div>';
			 ?>
			 <div class="uk-width-1-2">
				<label class="uk-text-capitalize" for="pais">Pais</label>
				<select id="pais" name="pais" data-placeholder="Seleccione un" class="chosen-select uk-select uk-width-1-1" required>
					<option value="Selecciona un país">Selecciona un país</option>
				<?php
				$CONSULTA = $CONEXION -> query("SELECT * FROM paises ORDER BY nombre");
				while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
					$paisId=$row_CONSULTA['id'];
					$parentTxt=$row_CONSULTA['nombre'];
					if (isset($pais) AND $pais==$paisId) {
							$estatus='selected';
						}else{
							$estatus='';
						}
						echo '
							<option value="'.$paisId.'" '.$estatus.'>'.$row_CONSULTA['nombre'].'</option>';
				}
							echo '
							</select>
			</div>';
			 ?>
			<div id="container" class="uk-width-1-2">
				<label class="uk-text-capitalize" for="estado">Estado</label>
				<select id="provincia" name="provincia" data-placeholder="Seleccione un" class="uk-select uk-width-1-1" required>
	
				</select>
			
			</div>

			<div class="uk-width-1-2">
				<label class="uk-text-capitalize" for="ciudad">Ciudad</label>
				<input type="text" class="uk-input" name="ciudad" id="ciudad" autofocus>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<label>Descripción Corta</label>
				<textarea class="uk-textarea" name="txt" id="txt"> </textarea>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<label>Descripción Larga</label>
				<textarea class="editor" name="txtdetalle" id="txtdetalle"> </textarea>
			</div>
			<div class="uk-width-1-1 uk-margin uk-text-center">
				<a href="index.php?rand=<?=rand(1,1000)?>&seccion=<?=$seccion?>" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>
<script>
	$("#pais").on('change', function() {
		var estadoId = this.value;
		
		$.ajax({
			type: "POST",
			url: "modulos/escuelas/acciones.php",
			data: {
			 procesarestados : 1,
			 idpais : estadoId
			} 
		}).done(function(data){
			$('#provincia').find('option').remove();	
			console.log("data",data);
			$('#provincia').html(data);	

			
		}); 
		
	});

</script>
