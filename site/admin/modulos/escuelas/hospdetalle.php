<?php

$consultaHospedaje = $CONEXION -> query("SELECT * FROM hospedajes WHERE id = $id");
$row_descripcion = $consultaHospedaje -> fetch_assoc();
$escuelaId = $row_descripcion['escuela'];
$divisaId=$row_descripcion['divisa'];

$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $escuelaId");
$row_catalogo = $consulta -> fetch_assoc();
$escuelaName = $row_catalogo['titulo'];
$cat = $row_catalogo['categoria'];


$rutaFinal='../img/contenido/descripciones/';

echo '
	<div class="uk-width-auto margin-v-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Escuelas</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$escuelaId.'">'.$escuelaName.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=hospdetalle&id='.$id.'" class="color-red">'.$row_descripcion['titulo'].'</a></li>
		</ul>
	</div>

	<div class="uk-width-expand@l uk-text-right margin-v-20">
		<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
		</div>
	</div>
	<div class="uk-width-1-1">
	<div class="uk-container uk-container-small">
		<form action="index.php" method="post" name="editar">
			<input type="hidden" name="editarhospedaje" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="hospdetalle">
			<input type="hidden" name="id" value="'.$id.'">

			<div class="uk-margin">
				<label class="uk-text-capitalize" for="titulo">Titulo</label>
				<input type="text" class="uk-input" name="titulo" id="titulo" autofocus value="'.$row_descripcion['titulo'] .'">
			</div>	
			<div class="uk-margin">
				<label class="uk-text-capitalize" for="precio">Precio</label>
				<input type="text" class="uk-input" name="precio" id="precio" min="0" step="any" autofocus value="'.$row_descripcion['precio'] .'">
			</div>	
			<div class="uk-margin">
				<label>Descripción</label>
				<textarea class="editor" name="txt" id="txt">'.$row_descripcion['txt'] .'</textarea>
			</div>
			<div>
				<label class="uk-text-capitalize" for="divisa">Divisa</label>
					<select name="divisa" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1">';
					$consultaDivisas = $CONEXION -> query("SELECT * FROM divisas");
					while ($rowDivisas = $consultaDivisas -> fetch_assoc()) {
						
						$nombreDivisa=$rowDivisas['nombre'];
						if (isset($divisaId) AND $divisaId==$rowDivisas['id']) {
								$estatus='selected';
							}else{
								$estatus='';
							}
						
							echo '
								<option value="'.$rowDivisas["id"].'" '.$estatus.'>'.$nombreDivisa.'</option>';
					}
								echo '
								</select>
			</div>
			<div class="uk-margin uk-text-center">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$escuelaId.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>
';


// Buttons
	echo '
	<div style="min-height:300px;">
	</div>

	<div>
		<div id="buttons">
			<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
		</div>
	</div>
	';


$scripts='

	';



