<?php 
$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$rowCONSULTA = $CONSULTA -> fetch_assoc();

$rutaSlider="../img/contenido/escuelas/carousel/";
$fechaSQL=$rowCONSULTA['fecha'];
$segundos=strtotime($fechaSQL);
$fechaUI=date('m/d/Y',$segundos);
$titulo = $rowCONSULTA['titulo'];

$CONSULTAPAIS = $CONEXION -> query("SELECT * FROM paises WHERE id =$rowCONSULTA[pais]");
$rowPais = $CONSULTAPAIS -> fetch_assoc();
$estado="";
$pais = $rowPais['nombre'];
if($rowCONSULTA['provincia'] != ""){
	$consultaProvincia = $CONEXION -> query("SELECT * FROM provincias WHERE id =$rowCONSULTA[provincia]");
	$rowProvincia = $consultaProvincia -> fetch_assoc();
	$estado = $rowProvincia['nombre'];
}

$ciudad = $rowCONSULTA['ciudad'];
$txt = $rowCONSULTA['txt'];
$txtdetalle = $rowCONSULTA['txtdetalle'];
$cat="";
$catId = $rowCONSULTA['categoria'];

$CONSULTACAT = $CONEXION -> query("SELECT * FROM productoscat WHERE id = $catId");
if($CONSULTACAT->num_rows >0){
	$rowCat = $CONSULTACAT -> fetch_assoc();
	$cat = $rowCat['titulo'];
}


echo '
<div class="uk-width-1-2@s margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">'.$seccion.'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="color-red">'.$rowCONSULTA['titulo'].'</a></li>
	</ul>
</div>
<div class="uk-width-1-2@s uk-text-right margen-v-20">
	<a href="index.php?seccion='.$seccion.'&subseccion=nuevo&" class="uk-button uk-button-default"><i class="fa fa-lg fa-plus"></i> &nbsp; Nuevo</a>
	<a href="index.php?seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="uk-button uk-button-primary"><i class="fas fa-pencil-alt"></i> &nbsp; Editar</a>
	<button id="duplicarbutton" data-id="'.$id.'" class="uk-button uk-button-primary"><i uk-icon="copy"></i>&nbsp; Duplicar</button>

</div>
';

echo '
<div class="uk-width-1-1 margin-top-50">
	<div class="uk-container">
		<div class="uk-card uk-card-default uk-card-body">
			<h3 class="uk-text-center">Detalle de la Escuela</h3>
				<div>
					<div class="uk-margin">
						<label>Título </label>
						<div class="uk-textarea"> '.$titulo.'</div>
					</div>
					<div uk-grid>
						<div class="uk-width-1-4">
							<label>Categoria </label>
							<div class="uk-textarea"> '.$cat.'</div>
						</div>
						<div class="uk-width-1-4">
							<label>Pais </label>
							<div class="uk-textarea"> '.$pais.'</div>
						</div>
						<div class="uk-width-1-4">
							<label>Estado </label>
							<div class="uk-textarea"> '.$estado.'</div>
						</div>
						<div class="uk-width-1-4">
							<label>Ciudad </label>
							<div class="uk-textarea"> '.$ciudad.'</div>
						</div>
					</div>
					<div class="uk-margin">
						<label>Descripción Corta</label>
						<div class="uk-textarea"> '.$txt.'</div>
					</div>

					<div class="uk-margin">
						<label>Descripción Larga</label>
						<div class="uk-textarea"> '.$txtdetalle.'</div>
					</div>
					
				</div>

			
		</div>
	</div>
</div>
';
echo '
<div class="uk-width-1-1 margen-v-20">
	<div uk-grid>
		<div class="uk-width-1-2@s">
			<div class="uk-card uk-card-body uk-card-default">	
				<div class="">
					<h3 class="uk-text-left">Selecciona tu ubicación</h3>
				</div>			
				<div id="map" class="uk-height-large">
				</div>
				<div class="uk-margin uk-text-center">
					<input type="hidden" id="mapahidden" class="uk-input" value="('.($rowCONSULTA['lat']*1).', '.($rowCONSULTA['lon']*1).')">
					<button class="uk-button uk-button-primary" id="mapasave">Guardar mapa</button>
				</div>
				<div class="uk-width-1-1 uk-text-right">
					<span class="uk-text-muted">Fecha de captura:</span>
					'.$fechaUI.'
				</div>
			</div>
		</div>
		<!-- HOSPEDAJE -->
		<div class="uk-width-1-2@s margin-v-10">
			<div class="uk-width-1-1">
				<div class="uk-card uk-card-default uk-card-body">
					<div class="uk-width-1-1 uk-text-right margin-bottom-20" style="margin-top: 0px;">
						<div uk-grid class="uk-grid-small uk-flex-right uk-child-width-auto@m">
							<div>
								<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=nuevohosp&id='.$id.'" class="uk-button uk-button-success"><i uk-icon="plus"></i>&nbsp; Nuevo hospedaje</a>
							</div>	
						</div>
					</div>
					<div class="uk-width-1-1">
						<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
							<thead>
								<tr class="uk-text-muted">
									<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Titulo</th>
									<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Precio</th>
									<th style="width:auto;"  class="uk-text-left"> &nbsp;&nbsp; Divisa</th>
									<th style="width:90px;"  ></th>
								</tr>
							</thead>
							<tbody id="conetent">';

							$sql = "SELECT * FROM hospedajes WHERE escuela = $id";
							$consulta = $CONEXION -> query($sql);
							$numItems=$consulta->num_rows;	
							
							
							while ($row_Consulta1 = $consulta -> fetch_assoc()){
								$divisa="";
								$hospedajeId = $row_Consulta1['id'];
								if($row_Consulta1['divisa']){
									$divID = $row_Consulta1['divisa'];
									$consultaDivisa = $CONEXION -> query("SELECT * FROM divisas WHERE id = $divID");
									$divisaRow = $consultaDivisa -> fetch_assoc(); 
									$divisa = $divisaRow['nombre'];
								} 
							
								$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=hospdetalle&id='.$hospedajeId;

								echo '
								<tr id="'.$hospedajeId.'">
									
									<td>
										<span class="uk-text-muted uk-hidden@m">titulo: </span>
										<input value="'.$row_Consulta1['titulo'].'" class="editarajax uk-input uk-form-blank" data-tabla="hospedajes" data-campo="titulo" data-id="'.$hospedajeId.'" tabindex="9">
									</td>
									<td>
										<span class="uk-text-muted uk-hidden@m">Precio: </span>
										<input value="'.$row_Consulta1['precio'].'" class="editarajax uk-input uk-form-blank" data-tabla="hospedajes" data-campo="precio" data-id="'.$hospedajeId.'" tabindex="9">
									</td>
									<td>
										<span class="uk-text-muted uk-hidden@m">Precio: </span>
										<input value="'.$divisa.'" class="editarajax uk-input uk-form-blank" data-tabla="hospedajes" data-campo="precio" data-id="'.$hospedajeId.'" tabindex="9">
									</td>
									<td class="uk-text-right@m">
										<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
										<span data-id="'.$hospedajeId.'" class="eliminahosp uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
									</td>
								</tr>';
							}
							echo '
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!--FIN HOSPEDAJE -->

		<div class="uk-width-1-2@s">
			<div class="">
				<h3 class="uk-text-left">Foto de la escuela</h3>
			</div>
			<div class="uk-width-1-2@s">
				<div class="uk-text-muted">
					Dimensiones recomendadas: 500 x 500 px
				</div>
				<div id="fileuploadermain">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">';

			// sharePic
				$pic=$rutaFinal.$rowCONSULTA['imagen'];
				if(strlen($rowCONSULTA['imagen'])>0 AND file_exists($pic)){
					echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$pic.'" target="_blank">
									<img src="'.$pic.'" class="uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
				}else{
					echo '
							<div class="uk-panel uk-text-center">
								<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
									Falta imagen<br><br>
								</p>
							</div>';
				}

			echo '
			</div>
		</div>

		<div class="uk-width-1-2@s">
			<div class="">
				<h3 class="uk-text-logo">Logo de la escuela</h3>
			</div>
			<div class="uk-width-1-2@s">
				<div class="uk-text-muted">
					Dimensiones recomendadas: 500 x 500 px
				</div>
				<div id="fileuploader">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">';

			// sharePic
				$logo=$rutaFinal.$rowCONSULTA['logoescuela'];
				if(strlen($rowCONSULTA['logoescuela'])>0 AND file_exists($logo)){
					echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$logo.'" target="_blank">
									<img src="'.$logo.'" class="uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarlogo"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
				}else{
					echo '
							<div class="uk-panel uk-text-center">
								<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
									Falta imagen<br><br>
								</p>
							</div>';
				}

			echo '
			</div>
		</div>
	</div>
</div>


<div style="width:400px;">
	<div class="">
		<h3 class="uk-text-logo">Carrousel</h3>
		
	</div>
	<p class="uk-text-muted">Dimensiones recomendadas: 600 x 1600 px</p>
	<div id="fileuploadercarrousel">
		Cargar
	</div>
</div>

<div class="uk-width-expand@xl margin-top-20">
	<div uk-grid class="uk-grid-match uk-grid-small sortable" data-tabla="escuelas">';

	$consulta = $CONEXION -> query("SELECT * FROM carrousel_escuelas WHERE escuelaid = $id ORDER BY orden");
	$ceri = $consulta ->num_rows;
	if($ceri> 0){
		while ($rowConsulta = $consulta -> fetch_assoc()) {
			$idCarousel=$rowConsulta['id'];
			$file=$idCarousel.'.jpg';
			echo '
				<div id="'.$rowConsulta['id'].'" style="width:250px;">
					<div id="'.$rowConsulta['id'].'" class="uk-card uk-card-default uk-card-body uk-text-center">
						<a href="javascript:borrarfoto(\''.$file.'\',\''.$idCarousel.'\')" class="uk-icon-button uk-button-danger" uk-icon="trash"></a>
						<br><br>
						<img src="'.$rutaSlider.$rowConsulta['id'].'.jpg" class="uk-border-rounded margin-bottom-20">
						
					</div>
				</div>';
		}
	}
	echo '
	</div>
</div>


<script async defer src="https://maps.googleapis.com/maps/api/js?key='.$googleMaps.'&callback=initMap"></script>
';


$mapzoom='13';
if (isset($rowCONSULTA['lat'])==0) {
	$maplat='20.606857';
	$maplng='-103.4018826';
}else{
	$maplat=$rowCONSULTA['lat']*1;
	$maplng=$rowCONSULTA['lon']*1;
}

$scripts='
	var map;
	var markers = [];

	function initMap() {
	  var haightAshbury = {lat: '.$maplat.', lng: '.$maplng.'};

	  map = new google.maps.Map(document.getElementById("map"), {
	    zoom: '.$mapzoom.',
	    center: haightAshbury
	  });

      addMarker(haightAshbury);

	  // This event listener will call addMarker() when the map is clicked.
	  map.addListener("click", function(event) {
	    addMarker(event.latLng);
	    var mapa=event.latLng;
	    $("#mapahidden").attr("value",mapa);
	  });
	}

	// Adds a marker to the map and push to the array.
	function addMarker(location) {
	  var marker = new google.maps.Marker({
	    position: location,
	    map: map
	  });
	  setMapOnAll(null);
	  markers = [];
	  markers.push(marker);
	}

	// Sets the map on all markers in the array.
	function setMapOnAll(map) {
	  for (var i = 0; i < markers.length; i++) {
	    markers[i].setMap(map);
	  }
	}

	// Guardar mapa
	$("#mapasave").click(function(){
		var valor = $("#mapahidden").val();
		$.ajax({
			method: "POST",
			url: "modulos/'.$seccion.'/acciones.php",
			data: { 
				editarmapa : 1,
				id : '.$id.',
				valor : valor
			}
		})
		.done(function( msg ) {
			UIkit.notification.closeAll();
			UIkit.notification(msg);
		});
	})

	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=imagen&imagen=\'+data);
			}
		});
	});
	$(document).ready(function() {
		$("#fileuploader").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg,png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=logoescuela&imagen=\'+data);
			}
		});
	});

	// Borrar foto 
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpic");
		} 
	});
	$(".borrarlogo").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarlogo");
		} 
	});

	';



$scripts.='
// Eliminar descripcion
	$(".eliminahosp").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este hospedaje?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&borrarhosp&id='.$id.'&hospid="+id);
		} 
	});
	// Eliminar foto
	function borrarfoto (file,id) { 
		var statusConfirm = confirm("Realmente desea eliminar esto?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrarslider: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg);
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	}

	var imagenesArray = [];
	$("#fileuploadercarrousel").uploadFile({
		url:"../library/upload-file/php/upload.php",
		fileName:"myfile",
		maxFileCount:1,
		showDelete: "false",
		allowedTypes: "jpeg,jpg",
		maxFileSize: 6291456,
		showFileCounter: false,
		showPreview:false,
		returnType:"json",
		onSuccess:function(data){ 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&imagenslider="+data);
		}
	});

	//Duplicar escuela
	$("#duplicarbutton").click(function(){
		
		var id = $(this).attr(\'data-id\');

		$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					duplicarescuela: 1,
					id: id
				}
			})
			.done(function(data) {
			
				UIkit.notification.closeAll();
				UIkit.notification(data.mensaje);
				setTimeout(function(){
					window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id=\'+data.id);
				 }, 3000);
				
			});
	});
';

