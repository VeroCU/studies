<?php 
$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
$rowCONSULTA = $CONSULTA -> fetch_assoc();
$cat = $rowCONSULTA['categoria'];
$pais = $rowCONSULTA['pais'];
$provincia = $rowCONSULTA['provincia'];
//debug($rowCONSULTA['estado']);

echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Escuelas</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$rowCONSULTA['titulo'].'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="color-red">Editar</a></li>
	</ul>
</div>
<style type="text/css">
	.next{margin:20px 30px;display:inline-block;vertical-align:top;}
</style>
<div class="uk-width-1-1 margen-top-20">
	<div class="uk-container uk-container-small">
		<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);" class="uk-grid-small" uk-grid>
			<input type="hidden" name="editar" value="1">
			<input type="hidden" name="seccion" value="'.$seccion.'">
			<input type="hidden" name="subseccion" value="detalle">
			<input type="hidden" name="id" value="'.$id.'">
			<div class="uk-width-1-1 uk-margin">
				<label class="uk-text-capitalize" for="titulo">Título:</label>
				<input type="text" class="uk-input" name="titulo" value="'.$rowCONSULTA['titulo'].'" autofocus required>
			</div>
			<div class="uk-width-1-2">
				<label class="uk-text-capitalize" for="categoria">categoria</label>
				<select name="categoria" data-placeholder="Seleccione una" class="chosen-select uk-select uk-width-1-1" required>';

					$CONSULTA1 = $CONEXION -> query("SELECT * FROM productoscat WHERE parent = 0 ORDER BY txt");
					while ($row_CONSULTA1 = $CONSULTA1 -> fetch_assoc()) {
						
						
						if (isset($cat) AND $cat==$row_CONSULTA1['id']) {
							$estatus='selected';
						}else{
							$estatus='';
						}
						echo '
											<option value="'.$row_CONSULTA1['id'].'" '.$estatus.'>'.$row_CONSULTA1['titulo'].'</option>';
					}
					echo '
										</optgroup>';
				
				echo '
				</select>
			</div>
			<div class="uk-width-1-2 uk-margin">
				<label class="uk-text-capitalize" for="pais">Pais</label>
					<select id="pais" name="pais" data-placeholder="Seleccione un" class="chosen-select uk-select uk-width-1-1" required>
					<option value="Selecciona un país">Selecciona un país</option>
			';
			
				
				$CONSULTA = $CONEXION -> query("SELECT * FROM paises ORDER BY nombre");
				while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
					$paisId=$row_CONSULTA['id'];
					$parentTxt=$row_CONSULTA['nombre'];
					if (isset($pais) AND $pais==$paisId) {
							$estatus='selected';
						}else{
							$estatus='';
						}
						echo '
							<option value="'.$paisId.'" '.$estatus.'>'.$row_CONSULTA['nombre'].'</option>';
				}
							echo '
							</select>
			</div>';
			
		echo '
			<div id="container" class="uk-width-1-2">
				<label class="uk-text-capitalize" for="estado">Estado</label>
				<select id="provincia" name="provincia" data-placeholder="Seleccione un" class="uk-select uk-width-1-1" required>';
				$consultaProv = $CONEXION -> query("SELECT * FROM provincias where pais = $pais");
				while ($provincias = $consultaProv -> fetch_assoc()) {
					$provId=$provincias['id'];
				
					if (isset($provincia) AND $provincia==$provId) {
							$estatus='selected';
						}else{
							$estatus='';
						}
						echo '
							<option value="'.$provId.'" '.$estatus.'>'.$provincias['nombre'].'</option>';
				}
					
			echo '	</select>
			
			</div>
			<div class="uk-width-1-2 uk-margin">
				<label class="uk-text-capitalize" for="Ciudad">Ciudad:</label>
				<input type="text" class="uk-input" name="Ciudad" value="'.$rowCONSULTA['ciudad'].'" autofocus required>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<label for="txt">Descripcion Corta</label>
				<textarea class="editor" name="txt">'.$rowCONSULTA['txt'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-margin">
				<label for="txt">Descripcion Larga</label>
				<textarea class="editor" name="txtdetalle">'.$rowCONSULTA['txtdetalle'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-margin uk-text-center">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</form>
	</div>
</div>


';

echo '
<script>
console.log("hola");	
	$("#pais").on("change", function() {
		var estadoId = this.value;
		console.log("entree");
		$.ajax({
			type: "POST",
			url: "modulos/escuelas/acciones.php",
			data: {
			 procesarestados : 1,
			 idpais : estadoId
			} 
		}).done(function(data){
			$(\'#provincia\').find(\'option\').remove();	
			console.log("data",data);
			$(\'#provincia\').html(data);	

		}); 
		
	});

</script>';