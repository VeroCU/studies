<?php
	$seccion='escuelas';
	$rutaFinal='../img/contenido/'.$seccion.'/';
	$rutaEscuela='../img/contenido/'.$seccion.'/carousel/';
	$rutaInicial="../library/upload-file/php/uploads/";
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevo'])){ 
		// Obtenemos los valores enviados
			$lat='20.667703809107746';
			$lon='-103.34699871873852';

		// Actualizamos la base de datos
		if(!isset($fallo)){
			$sql = "INSERT INTO $seccion (fecha, lat, lon)".
				"VALUES ('$hoy', '$lat', '$lon')";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Nuevo";
				$editarNuevo=1;
				$id=$CONEXION->insert_id;
				$subseccion='detalle';
			}else{
				$fallo=1;  
				$legendFail .= "<br>No se pudo agregar a la base de datos - $cat";
			}
		}else{
			$legendFail .= "<br>La categoría o marca están vacíos.";
		}
	}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%     Procesar estados    	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

	if (isset($_POST['procesarestados'])){
		include '../../../includes/connection.php';
			$idpais = $_POST['idpais'];
			$sql = "SELECT id,nombre FROM provincias WHERE pais = $idpais";

			$CONSULTA = $CONEXION->query($sql);	
			while ($row_CONSULTA = $CONSULTA -> fetch_assoc()) {
				echo '<option value="'.$row_CONSULTA['id'].'">'.$row_CONSULTA['nombre'].'</option>';
			}

		

	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['editar']) OR isset($editarNuevo)) {
		
		// Obtenemos los valores enviado

		$fallo=1;  
		$legendFail .= "<br>No se pudo modificar la base de datos";
		foreach ($_POST as $key => $value) {

				$dato = str_replace("'", "&#039;", $value);
			
			$actualizar = $CONEXION->query("UPDATE $seccion SET $key = '$dato' WHERE id = $id");
			$exito=1;
			unset($fallo);
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar mapa   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if (isset($_POST['editarmapa'])) {
		include '../../../includes/connection.php';

		$id    = $_POST['id'];
		$valor = $_POST['valor'];
		$valor = str_replace('(', '', $valor);
		$valor = str_replace(')', '', $valor);
		$array = explode(',', $valor);
		$lat   = trim($array[0]);
		$lon   = trim($array[1]);

		if ($actualizar = $CONEXION->query("UPDATE $seccion SET lat = '$lat', lon = '$lon' WHERE id = $id")) {
			$mensajeClase='success';
			$mensajeIcon='check';
			$mensaje='Guardado -'.$id;
		}else{
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje='No se pudo guardar';
		}
		echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarProd'])){
		if($borrar = $CONEXION->query("DELETE FROM $seccion WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>escuela eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpic'])){

		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Logo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarlogo'])){
		
		$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['logoescuela'])>0) {
			unlink($rutaFinal.$row_CONSULTA['logoescuela']);
			$actualizar = $CONEXION->query("UPDATE $seccion SET logoescuela = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la logoescuela en la base de datos";
			$fallo=1;
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['imagen'])){
		$position=$_GET['position'];

		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_GET['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if(file_exists($rutaInicial.$imagenName)){
			$rand=rand(1,999999999);
			$imgFinal=$rand.'.'.$ext;
			if(file_exists($rutaFinal.$imgFinal)){
				$rand=rand(1,999999999);
				$imgFinal=$rand.'.'.$ext;
			}
			$CONSULTA = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
			$row_CONSULTA = $CONSULTA -> fetch_assoc();
			if (strlen($row_CONSULTA[$position])>0 AND file_exists($rutaFinal.$row_CONSULTA[$position])) {
				unlink($rutaFinal.$row_CONSULTA[$position]);
			}
			copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
			$actualizar = $CONEXION->query("UPDATE $seccion SET $position = '$imgFinal' WHERE id = $id");
		}else{
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}
		



		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// Borramos las imágenes que estén remanentes en el directorio files
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle); 
	}

	
//	SLIDER CIRCULO

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Imagen      	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_POST['borrarslider'])){
			include '../../../includes/connection.php';
			$id=$_POST['id'];
			

			// Borramos de la base de datos
			if($borrar = $CONEXION->query("DELETE FROM carrousel_escuelas WHERE id = $id")){
				$legendSuccess.= "<br> Imagen eliminada";
				$exito='success';
			}else{
				$legendFail .= "<br>No se pudo borrar de la base de datos";
				$fallo='danger';  
			}
			// Borramos el archivo de imagen
			

				
			$filehandle = opendir("../../../img/contenido/escuelas/carousel/"); // Abrir archivos
			
			
			while ($file = readdir($filehandle)) {
				if ($file != "." && $file != "..") {
					// Id de la imagen
					if (strpos($file,'-')===false) {
						$imagenID = strstr($file,'.',TRUE);
					}else{
						$imagenID = strstr($file,'-',TRUE);
					}
					// Comprobamos que sean iguales
					if($imagenID==$id){
						$pic=$rutaEscuela.$file;
						$exito=1;
						unlink($pic);
					}
				}
			}
			if (isset($exito)) {
				$mensajeClase='success';
				$mensajeIcon='check';
				$mensaje='Eliminado';
			}else{
				$mensajeClase='danger';
				$mensajeIcon='ban';
				$mensaje='No se pudo guardar';
			}

			echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
		}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imagen     	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_GET['imagenslider'])){
			//Obtenemos la extensión de la imagen4
			$escuelaid =$_GET['id']; 
			$imagenName=$_GET['imagenslider'];
			$i = strrpos($imagenName,'.');
			$l = strlen($imagenName) - $i;
			$ext = strtolower(substr($imagenName,$i+1,$l));


			// Imagenes a crear
			$xs=0;
			$sm=0;
			$med=0;
			$og=0;
			$nat400=0;
			$nat800=1;
			$nat1500=0;
			$Otra=0;

			// Dimensiones
			// Small
			$anchoXS=100;
			$altoXS =100;
			// Small
			$anchoSM=250;
			$altoSM =250;
			// Mediana
			$anchoMED=500;
			$altoMED =500;
			// OG
			$anchoOG=1000;
			$altoOG =700;
			// Otra
			$anchoOtra=1920;
			$altoOtra =780;



			// Si no es JPG cancelamos
			if ($ext!='jpg' and $ext!='jpeg') {
				$fallo=1;
				$legendFail='<br>El archivo debe ser JPG';
			}
			
			if(!file_exists($rutaInicial.$imagenName)){
				$fallo=1;
				$legendFail='<br>No se permite refrescar la página.';
			}

			if (!isset($fallo)) {

				$sql = "INSERT INTO carrousel_escuelas (orden,escuelaid) VALUES ('99',$escuelaid)";
				
				if($insertar = $CONEXION->query($sql)){

					$pic = $CONEXION->insert_id;
					$imgAux=$rutaEscuela.$pic."-orig.jpg";

					// Lo movemos al directorio final
					copy($rutaInicial.$imagenName, $imgAux);
					unlink($rutaInicial.$imagenName);


					// Leer el archivo para hacer la nueva imagen
					if ($ext=='jpg' or $ext=='jpeg') $original = imagecreatefromjpeg($imgAux);

					// Tomamos las dimensiones de la imagen original
					$ancho  = imagesx($original);
					$alto   = imagesy($original);

					if ($xs==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Pequeña
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-xs.jpg";

						$anchoNew=$anchoXS;
						$altoNew =$altoXS;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($sm==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Pequeña
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-sm.jpg";

						$anchoNew=$anchoSM;
						$altoNew =$altoSM;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($med==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen Mediana
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-med.jpg";

						$anchoNew=$anchoMED;
						$altoNew =$altoMED;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen pequeña agregada";
						}
					}

					if ($og==1) {
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						//  Imagen OG
						// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
						$newName=$pic."-og.jpg";

						$anchoNew=$anchoOG;
						$altoNew =$altoOG;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($alto/$ancho>(.7)){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$xinicial= -$excedente/2;
						}else{
							// Alto proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$yinicial= -$excedente/2;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						if(isset($xinicial)){
							imagecopyresampled($New,$original,$xinicial,0,0,0,$anchoProporcional,$altoNew,$ancho,$alto);
						}else{
							imagecopyresampled($New,$original,0,$yinicial,0,0,$anchoNew,$altoProporcional,$ancho,$alto);
						}

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen OG agregada";
						}
					}

					if ($nat400==1) {
						//  Imagen nat400
						$newName=$pic."-nat400.jpg";
						$anchoNuevo = 400;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($nat800==1) {
						//  Imagen nat1000
						$newName    = $pic.".jpg";
						$anchoNuevo = 800;
						$altoNuevo  = $anchoNuevo*$alto/$ancho;

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($nat1500==1) {
						//  Imagen nat1500
						$newName=$pic."-nat1500.jpg";
						if ($ancho>$alto) {
							$anchoNuevo = 1500;
							$altoNuevo  = $anchoNuevo*$alto/$ancho;
						}else{
							$altoNuevo  = 1500;
							$anchoNuevo = $altoNuevo*$ancho/$alto;
						}

						// Creamos la imagen
						$imagenAux = imagecreatetruecolor($anchoNuevo,$altoNuevo); 
						// Copiamos el contenido de la original para pegarlo en el archivo nuevo
						imagecopyresampled($imagenAux,$original,0,0,0,0,$anchoNuevo,$altoNuevo,$ancho,$alto);
						// Pegamos el contenido de la imagen
						if(imagejpeg($imagenAux,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
						}
					}

					if ($Otra==1) {
						//  Imagen Otra
						$newName=$pic."-otra.jpg";
						$anchoNew=$anchoOtra;
						$altoNew =$altoOtra;
						$dst_x=0;
						$dst_y=0;
						$src_x=0;
						$src_y=0;
						$dst_w=$ancho;
						$dst_h=$alto;
						$src_w=$ancho;
						$src_h=$alto;

						// Proporcionalmente, la imagen es más ancha que la de destino
						if($ancho/$alto>$anchoNew/$altoNew){
							// Ancho proporcional
							$anchoProporcional=$ancho/$alto*$altoNew;
							// Corregimos el ancho
							$dst_w=$anchoProporcional;
							// Corregimos el ancho
							$dst_h=$altoNew;
							// Excedente 
							$excedente=$anchoProporcional-$anchoNew;
							// Posición inicial de la coordenada x
							$src_x= $excedente/2;
							//$legendSuccess.='<br>Opt 2';
						}else{
							// Ancho proporcional
							$altoProporcional=$alto/$ancho*$anchoNew;
							// Corregimos el alto
							$dst_h=$altoProporcional;
							// Corregimos el ancho
							$dst_w=$anchoNew;
							// Excedente
							$excedente=$altoProporcional-$altoNew;
							// Posición inicial de la coordenada y
							$src_y= $excedente/4;
							//$legendSuccess.='<br>Opt 2';
							//$legendSuccess.='<br>Alto Original: '.$alto;
							//$legendSuccess.='<br>Alto Nuevo: '.$altoNew;
							//$legendSuccess.='<br>Alto Proporcional: '.$altoProporcional;
							//$legendSuccess.='<br>Excedente: '.$excedente;
							//$legendSuccess.='<br>Src Y: '.$src_y;
						}

						// Copiamos el contenido de la original para pegarlo en el archivo New
						$New = imagecreatetruecolor($anchoNew,$altoNew); 

						imagecopyresampled($New,$original,$dst_x,$dst_y,$src_x,$src_y,$dst_w,$dst_h,$src_w,$src_h);

						// Pegamos el contenido de la imagen
						if(imagejpeg($New,$rutaEscuela.$newName,90)){ // 90 es la calidad de compresión
							$exito=1;
							//$legendSuccess .= "<br>Imagen Otra agregada";
						}
					}

					if ($originalPic==0) {
						unlink($imgAux);
					}else{
						rename ($imgAux, $rutaEscuela.$pic."-orig.jpg");
					}

					if($exito==1){
						$legendSuccess .= "<br>Imagen actualizada";
					}
				}else{
					$fallo=1;
					$legendFail='<br>No se pudo guardar en la base de datos';
				}
			}

		}
	
	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Hospedaje    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarhosp'])){
		$idhosp = $_REQUEST['hospid'];
		if($borrar = $CONEXION->query("DELETE FROM hospedajes WHERE id = $idhosp")){
			$exito=1;
			$legendSuccess .= "<br>Hospedaje eliminado";
		}else{
			$legendFail .= "<br>No se pudo borrar de la base de datos";
			$fallo=1;  
		}
	}

//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo hospedaje    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevohospedaje'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['titulo'])) { $titulo=$_POST['titulo'];   }else{	$titulo=false; $fallo=1; }
		if (isset($_POST['txt'])) { $descripcion=$_POST['txt'];   }else{	$descripcion=false; $fallo=1; }
		// Sustituimos los caracteres inválidos
		$titulo=(htmlentities($titulo, ENT_QUOTES));
		$precio =$_POST['precio'];
		$escuela = $_POST['escuelaid'];
		$divisa = $_POST['divisa'];
		// Actualizamos la base de datos
		if($titulo!="" && $descripcion!=""){
			$sql = "INSERT INTO hospedajes (titulo,txt,precio,escuela,divisa) VALUES ('$titulo','$descripcion',$precio,$escuela,$divisa)";
			if($insertar = $CONEXION->query($sql)){
				$id = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Nuevo hospedaje";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Hospedaje %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['editarhospedaje'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['editarhospedaje'])) { $titulo=$_POST['titulo'];   }else{	$titulo=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$titulo=(htmlentities($titulo, ENT_QUOTES));
		$descripcion=$_POST['txt'];
		$precio = $_POST['precio'];
		$divisa = $_POST['divisa'];
		
		// Actualizamos la base de datos
		if($titulo!=""){
			$sql = "UPDATE hospedajes SET titulo='$titulo',txt='$descripcion',precio=$precio ,divisa=$divisa WHERE id=$id";
			if($insertar = $CONEXION->query($sql)){
				$cat = $CONEXION->insert_id;
				$exito=1;
				$legendSuccess .= "<br>Hospedaje Editado";
			}else{
				$fallo=1;
				$legendFail .= "<br>No se puede guardar";
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}
//%%%%%%%%%%%%%%%%%%%%%%%%%%    Clonar Escuela %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['duplicarescuela'])){
		include '../../../includes/connection.php';
		$id= $_POST['id'];
		$newId=null;
		$consulta = $CONEXION -> query("SELECT * FROM escuelas WHERE id = $id");
		if($consulta->num_rows > 0){
			$escuelaRow = $consulta ->fetch_assoc();
			$escuelaId = $escuelaRow['id'];
			 $sql = "INSERT INTO escuelas (categoria,titulo,pais,provincia,ciudad,txt,txtdetalle,fecha,imagen,logoescuela,lat,lon)".
			 " VALUES ($escuelaRow[categoria],'$escuelaRow[titulo]',$escuelaRow[pais],$escuelaRow[provincia],'$escuelaRow[ciudad]','$escuelaRow[txt]','$escuelaRow[txtdetalle]','$hoy','$escuelaRow[imagen]','$escuelaRow[logoescuela]','$escuelaRow[lat]','$escuelaRow[lon]') ";
			 if($insertar = $CONEXION->query($sql)){
			 	$newId=$CONEXION->insert_id;

			 	$consultaHospedajes = $CONEXION -> query("SELECT * FROM hospedajes WHERE escuela = $escuelaId");
			 	while ($rowHospedaje = $consultaHospedajes -> fetch_assoc()){
			 		$titulo = $rowHospedaje['titulo'];
			 		$txt = $rowHospedaje['txt'];
			 		$precio = $rowHospedaje['precio'];
			 		$divisa = $rowHospedaje['divisa'];

			 		$sqlH = "INSERT INTO hospedajes (titulo,txt,precio,escuela)"."VALUES ('$titulo','$txt',$precio,$newId)";
			 		$CONEXION->query($sqlH);
			 	}

			 	$mensajeClase='success';
				$mensajeIcon='check';
				$mensaje='Escuela duplicada';
			 }
			else{
				$mensajeClase='danger';
				$mensajeIcon='ban';
				$mensaje="error al insertar";
			} 	
		}else{
			$mensajeClase='danger';
			$mensajeIcon='ban';
			$mensaje='No se existe la escuela';
		} 	
		header('Content-Type: application/json');
		echo json_encode(array('mensaje'=>'<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>',
						'id'=>$newId));
		exit;
		
		
	}



