<?php 
	$consulta = $CONEXION -> query("SELECT * FROM $seccion WHERE id = $id");
	$rowCONSULTA = $consulta -> fetch_assoc();
	$link='../'.$id.'_'.urlencode(str_replace($caracteres_no_validos,$caracteres_si_validos,html_entity_decode(strtolower($rowCONSULTA['titulo'])))).'_.html';

	$fechaSQL=$rowCONSULTA['fecha'];
	$segundos=strtotime($fechaSQL);
	$fechaUI=date('m/d/Y',$segundos);

echo '
<div class="uk-width-1-1 margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Productos</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$id.'">'.$rowCONSULTA['titulo'].'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=editar&id='.$id.'" class="color-red">Editar</a></li>
	</ul>
</div>

<div class="uk-width-1-1 margen-top-20 uk-form">
	<form action="index.php" method="post" enctype="multipart/form-data" name="datos" onsubmit="return checkForm(this);">
		<input type="hidden" name="editar" value="1">
		<input type="hidden" name="seccion" value="'.$seccion.'">
		<input type="hidden" name="subseccion" value="detalle">
		<input type="hidden" name="cat" value="'.$cat.'">
		<input type="hidden" name="id" value="'.$id.'">
		<div uk-grid class="uk-grid-small uk-child-width-1-3@m">
			<div>
				<label class="uk-text-capitalize" for="sku">SKU</label>
				<input type="text" class="uk-input" name="sku" value="'.$rowCONSULTA['sku'].'" autofocus required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="titulo">Producto</label>
				<input type="text" class="uk-input" name="titulo" value="'.$rowCONSULTA['titulo'].'" required>
			</div>
			<div>
				<label class="uk-text-capitalize" for="precio">Precio</label>
				<input type="text" class="uk-input" name="precio" value="'.$rowCONSULTA['precio'].'">
			</div>

			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="txt">descripción</label>
				<textarea class="editor" name="txt" id="txt">'.$rowCONSULTA['txt'].'</textarea>
			</div>


			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="title">titulo google</label>
				<input type="text" class="uk-input" name="title" value="'.$rowCONSULTA['title'].'">
			</div>
			<div class="uk-width-1-1">
				<label class="uk-text-capitalize" for="metadescription">descripción google</label>
				<textarea class="uk-textarea" name="metadescription">'.$rowCONSULTA['metadescription'].'</textarea>
			</div>
			<div class="uk-width-1-1 uk-text-center">
				<a href="index.php?seccion='.$seccion.'&subseccion=detalle&id='.$id.'" class="uk-button uk-button-default uk-button-large" tabindex="10">Cancelar</a>					
				<button name="send" class="uk-button uk-button-primary uk-button-large">Guardar</button>
			</div>
		</div>
	</form>
</div>

<div>
	<div id="buttons">
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

';


$scripts='';

?>