

<div class="uk-width-1-3 margen-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<?php 
		echo '
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Productos</a></li>
		';
		?>

	</ul>
</div>

<?php
echo '';
?>


<div class="uk-width-1-1 margen-v-50">
	<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
		<thead>
			<tr class="uk-text-muted">
				<th width="40px" >Foto</th>
				<th width="150px" onclick="sortTable(1)">SKU</th>
				<th width="auto"  onclick="sortTable(2)">Título</th>
				<th width="100px" onclick="sortTable(4)" class="uk-visible@m">Precio</th>
				<th width="50px"  onclick="sortTable(5)" class="uk-visible@m">Activo</th>
				<th width="90px"></th>
			</tr>
		</thead>
		<tbody class="sortable" data-tabla="<?=$seccion?>">
		<?php
		$consulta = $CONEXION -> query("SELECT * FROM $seccion ORDER BY orden");
		while ($rowCONSULTA = $consulta -> fetch_assoc()) {
			$prodID=$rowCONSULTA['id'];

			$picTxt='';
			$pic='../img/contenido/'.$seccion.'main/'.$rowCONSULTA['imagen'];
			if(strlen($rowCONSULTA['imagen'])>0 AND file_exists($pic)){
				$picTxt='
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}elseif(strlen($rowCONSULTA['imagen'])>0 AND strpos($rowCONSULTA['imagen'], 'ttp')>0){
				$pic=$rowCONSULTA['imagen'];
				$picTxt= '
					<div class="uk-inline">
						<i uk-icon="camera"></i>
						<div uk-drop="pos: right-justify">
							<img src="'.$pic.'" class="uk-border-rounded">
						</div>
					</div>';
			}


			$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$rowCONSULTA['id'];

			$estatusIcon=($rowCONSULTA['estatus']==0)?'off uk-text-muted':'on uk-text-primary';

			echo '
			<tr id="'.$prodID.'">
				<td>
					'.$picTxt.'
				</td>
				<td>
					'.$rowCONSULTA['sku'].'
				</td>
				<td>
					'.$rowCONSULTA['titulo'].'
				</td>
				<td class="uk-text-right">
					<span class="uk-hidden">'.(10000+(1*($rowCONSULTA['precio']))).'</span>
					<input type="number" class="editarajax uk-input uk-form-small" data-tabla="'.$seccion.'" data-campo="precio" data-id="'.$prodID.'" value="'.$rowCONSULTA['precio'].'">
				</td>
				<td class="uk-text-center">
					<i class="estatuschange fa fa-lg fa-toggle-'.$estatusIcon.' uk-text-muted pointer" data-tabla="'.$seccion.'" data-campo="estatus" data-id="'.$prodID.'" data-valor="'.$rowCONSULTA['estatus'].'"></i>
				</td>
				<td class="uk-text-center">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a> &nbsp;
					<span data-id="'.$rowCONSULTA['id'].'" class="eliminaprod uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span>
				</td>
			</tr>';
		}
		?>

		</tbody>
	</table>
</div>
<div>
	<div id="buttons">
		<a href="index.php?seccion=<?=$seccion?>&subseccion=nuevo" class="uk-icon-button uk-button-primary uk-box-shadow-large" uk-icon="icon:plus;ratio:1.4;"></a>
		<a href="#menu-movil" class="uk-icon-button uk-button-primary uk-box-shadow-large uk-hidden@l" uk-icon="icon:menu;ratio:1.4;" uk-toggle></a>
	</div>
</div>

<?php
$scripts='
	// Eliminar producto
	$(".eliminaprod").click(function() {
		var id = $(this).attr(\'data-id\');
		//console.log(id);
		var statusConfirm = confirm("Realmente desea eliminar este Producto?");
		if (statusConfirm == true) {
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&borrarPod&id="+id);
		}
	});
	';
?>

