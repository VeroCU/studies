<?php
$seccion='catalogos';
$rutaFinal='../img/contenido/paises/';

// SLIDER TEXTO
	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nuevo Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_POST['nuevadivisa'])){ 
			// Insertar en la base de datos
			$nombre = $_POST['nombre'];
			$pesos = $_POST['precio_pesos'];
			$cambio = $_POST['cambio'];
			$simbolo = $_POST['simbolo'];
			$actual = $_POST['valor_actual'];

			$sql = "INSERT INTO divisas (nombre,simbolo,precio_pesos,cambio,valor_actual) VALUES ('$nombre','$simbolo',$pesos,$cambio,$actual)";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$editarNuevo=1;
				$id=$CONEXION->insert_id;
			}else{
				$fallo=1;  
				$legendFail .= "<br>No se pudo agregar a la base de datos";
			}
		}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Editar Artículo     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_REQUEST['editardivisa']) OR isset($editarNuevo)) {
			if (isset($_POST['id'])) {
				$id=$_POST['id'];
			}
			foreach ($_POST as $key => $value) {
				$dato = trim(htmlentities($value, ENT_QUOTES));
				$sql="UPDATE preguntas SET $key = '$dato' WHERE id = $id";
				//$legendSuccess.='<br>'.$sql;
				$actualizar = $CONEXION->query($sql);
				$exito=1;
				unset($fallo);
			}
		}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Imagen      	 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		if(isset($_POST['borrardivisa'])){
			include '../../../includes/connection.php';
			$id=$_POST['id'];
			// Borramos de la base de datos
			if($borrar = $CONEXION->query("DELETE FROM divisas WHERE id = $id")){
				$mensajeClase='success';
				$mensajeIcon='check';
				$mensaje='Eliminado';
			}else{
				$mensajeClase='danger';
				$mensajeIcon='ban';
				$mensaje='No se pudo guardar';
			}

			echo '<div class="uk-text-center color-white bg-'.$mensajeClase.' padding-10 text-lg"><i class="fa fa-'.$mensajeIcon.'"></i> &nbsp; '.$mensaje.'</div>';		
		}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Pais     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevopais'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['nombre'])) { $pais=$_POST['nombre'];   }else{	$pais=false; $fallo=1; }

		// Sustituimos los caracteres inválidos
		$pais=htmlentities($pais, ENT_QUOTES);

		// Actualizamos la base de datos
		if($pais!=""){
			$sql = "INSERT INTO paises (nombre) VALUES ('$pais')";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Nuevo pais";
			}else{
				$fallo=1;  
				$legendFail .= "<br>No pudo agregarse a la base de datos ".$seccioncat.'-'.$categoria;
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Pais     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminacat'])){
		
		$id = $_REQUEST['cat'];
		if($borrar = $CONEXION->query("DELETE FROM paises WHERE id = $id")){
			$exito=1;
			$legendSuccess .= "<br>Categoria eliminada";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}
		//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar provincia     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['eliminaprov'])){
		
		$idcat = $_REQUEST['cat'];
		if($borrar = $CONEXION->query("DELETE FROM provincias WHERE id = $idcat")){
			$exito=1;
			$legendSuccess .= "<br>Provincia eliminada";
		}else{
			$fallo=1;
			$legendFail .= "<br>No se pudo borrar de la base de datos";
		}
	}


	 //%%%%%%%%%%%%%%%%%%%%%%%%%%    Nueva Provincia     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_POST['nuevaprovincia'])){ 
		// Obtenemos los valores enviados
		if (isset($_POST['nombre'])) { $provincia=$_POST['nombre'];   }else{	$provincia=false; $fallo=1; }
		// Sustituimos los caracteres inválidos
	
		// Actualizamos la base de datos
		if($provincia!=""){
			$sql = "INSERT INTO provincias (nombre,pais) VALUES ('$provincia','$id')";
			if($insertar = $CONEXION->query($sql)){
				$exito=1;
				$legendSuccess .= "<br>Nuevo pais";
			}else{
				$fallo=1;  
				$legendFail .= "<br>No pudo agregarse a la base de datos ".$seccioncat.'-'.$categoria;
			}
		}else{
			$fallo=1;  
			$legendFail .= "<br>El campo está vacío";
		}
	}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Subir Imágen pais     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_GET['imagen'])){
		$position=$_GET['position'];

		//Obtenemos la extensión de la imagen
		$rutaInicial="../library/upload-file/php/uploads/";
		$imagenName=$_GET['imagen'];
		$i = strrpos($imagenName,'.');
		$l = strlen($imagenName) - $i;
		$ext = strtolower(substr($imagenName,$i+1,$l));


		// Guardar en la base de datos
		if(file_exists($rutaInicial.$imagenName)){
			$rand=rand(1,999999999);
			$imgFinal=$rand.'.'.$ext;
			if(file_exists($rutaFinal.$imgFinal)){
				$rand=rand(1,999999999);
				$imgFinal=$rand.'.'.$ext;
			}
			$CONSULTA = $CONEXION -> query("SELECT * FROM paises WHERE id = $id");
			$row_CONSULTA = $CONSULTA -> fetch_assoc();
			if (strlen($row_CONSULTA[$position])>0 AND file_exists($rutaFinal.$row_CONSULTA[$position])) {
				unlink($rutaFinal.$row_CONSULTA[$position]);
			}
			copy($rutaInicial.$imagenName, $rutaFinal.$imgFinal);
			$actualizar = $CONEXION->query("UPDATE paises SET $position = '$imgFinal' WHERE id = $id");
		}else{
			$fallo=1;
			$legendFail='<br>No se permite refrescar la página.';
		}
		



		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		// Borramos las imágenes que estén remanentes en el directorio files
		// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		$filehandle = opendir($rutaInicial); // Abrir archivos
		while ($file = readdir($filehandle)) {
			if ($file != "." && $file != ".." && $file != ".gitignore" && $file != ".htaccess" && $file != "thumbnail") {
				if(file_exists($rutaInicial.$file)){
					//echo $ruta.$file.'<br>';
					unlink($rutaInicial.$file);
				}
			}
		} 
		closedir($filehandle); 
	}

	//%%%%%%%%%%%%%%%%%%%%%%%%%%    Borrar Foto     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	if(isset($_REQUEST['borrarpic'])){

		$CONSULTA = $CONEXION -> query("SELECT * FROM paises WHERE id = $id");
		$row_CONSULTA = $CONSULTA -> fetch_assoc();
		if (strlen($row_CONSULTA['imagen'])>0) {
			unlink($rutaFinal.$row_CONSULTA['imagen']);
			$actualizar = $CONEXION->query("UPDATE paises SET imagen = '' WHERE id = $id");
			$exito=1;
			$legendSuccess.='<br>Foto eliminada';
		}else{
			$legendFail .= "<br>No se encontró la imagen en la base de datos";
			$fallo=1;
		}
	}


