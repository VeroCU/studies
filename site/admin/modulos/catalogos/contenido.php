<?php
$subsecciones[] = array(
	  'title' => 'Divisas',
 'subseccion' => 'divisas',
	   'icon' => 'hand-holding-usd');

$subsecciones[] = array(
	  'title' => 'Paises',
 'subseccion' => 'pais',
	   'icon' => 'globe-americas');
	
echo '
<div class="uk-width-auto@m margin-top-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'" class="color-red">Catologos</a></li>
	</ul>
</div>

<div class="uk-width-1-1">
	<div class="uk-container">
		<div uk-grid class="uk-flex-center" style="margin-top: 100px;">';


		foreach ($subsecciones as $key => $value) {

			echo '
			<div class="uk-width-auto">
				<a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$value['subseccion'].'">
					<div class="uk-card uk-card-default uk-flex uk-flex-center uk-flex-middle uk-text-center uk-text-capitalize" style="width: 250px;height: 250px;">
						<div>
							<i class="fa fa-3x fa-'.$value['icon'].'"></i>
							<br><br>
							'.$value['title'].'
						</div>
					</div>
				</a>
			</div>';

		}	

echo '
		</div>
	</div>
</div>';



