<?php 
echo '
<div class="uk-width-1-1 margin-top-20">
	<ul class="uk-breadcrumb">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">Catalogos</a></li>
		<li><a href="index.php?seccion='.$seccion.'&subseccion='.$subseccion.'" class="color-red">Divisas</a></li>
	</ul>
	<div id="add" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<h2 class="Nuevo"></h2>
			<form method="post" action="index.php" onsubmit="return checkForm(this);">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">
				<input type="hidden" name="nuevadivisa" value="1">

				<div class="uk-margin">
					<label>Nombre</label>
					<input type="text" name="nombre" class="uk-input">
				</div>
				<div class="uk-margin">
					<label>Simbolo</label>
					<input type="text" name="simbolo" class="uk-input" maxlength="1">
				</div>
				<div class="uk-margin">
					<label>Precio en pesos</label>
					<input type="number" name="precio_pesos" class="uk-input"  step="any">
				</div>
				<div class="uk-margin">
					<label>Cambio</label>
					<input type="number" name="cambio" class="uk-input"  step="any">
				</div>
				<div class="uk-margin">
					<label>Valor Actual</label>
					<input type="number" name="valor_actual" class="uk-input" step="any">
				</div>
				<div class="uk-margin uk-text-center">
					<span class="uk-button uk-button-default uk-button-large uk-modal-close" type="button">Cancelar</span>
					<button type="submit" class="uk-button uk-button-primary uk-button-large">Guardar</button>
				</div>
			</form>
		</div>
	</div>
</div>
';
?>


<div class="uk-width-1-1 margen-v-50">
	<div class="uk-container">
		<div class="uk-text-right">
			<a href="#add" uk-toggle class="uk-button uk-button-success"><i uk-icon="icon: plus;ratio:1.4"></i> &nbsp; Nuevo</a>
		</div>
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle uk-table-responsive" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th width="250px">Nombre</th>
					<th>Simbolo</th>
					<th>Valor en pesos</th>
					<th>Cambio</th>
					<th>Valor actual</th>
					<th width="120px"></th>
				</tr>
			</thead>
			<tbody  data-tabla="<?=$seccion?>">
			<?php
				$consulta1 = $CONEXION -> query("SELECT * FROM divisas ORDER BY nombre");
				while ($row_Consulta1 = $consulta1 -> fetch_assoc()) {
					$prodID=$row_Consulta1['id'];
					
					$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detalle&id='.$row_Consulta1['id'];

					$estatusIcon=($row_Consulta1['estatus']==1)?'off uk-text-muted':'on uk-text-primary';

					echo '
					<tr id="'.$row_Consulta1['id'].'">	
						<td>
							<input value="'.$row_Consulta1['nombre'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$subseccion.'" data-campo="nombre" data-id="'.$prodID.'" tabindex="10">
						</td>
						<td>
							<input  type="text" value="'.$row_Consulta1['simbolo'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$subseccion.'" data-campo="simbolo	" data-id="'.$prodID.'" tabindex="10">
						</td>
						<td>
							<input  type="number"  step="any" value="'.$row_Consulta1['precio_pesos'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$subseccion.'" data-campo="precio_pesos" data-id="'.$prodID.'" tabindex="10">
						</td>
						<td>
							<input  type="number"  step="any" value="'.$row_Consulta1['cambio'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$subseccion.'" data-campo="cambio" data-id="'.$prodID.'" tabindex="10">
						</td>
						<td>
							<input  type="number"  step="any" value="'.$row_Consulta1['valor_actual'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$subseccion.'" data-campo="valor_actual" data-id="'.$prodID.'" tabindex="10">
						</td>
						<td class="uk-text-center">
							<span data-id="'.$row_Consulta1['id'].'" class="eliminadiv uk-icon-button uk-button-danger" tabindex="1" uk-icon="icon:trash"></span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						</td>
					</tr>';
				}
			?>
			</tbody>
		</table>
	</div>
</div>


<?php
$scripts='

	// Eliminar producto	
	$(".eliminadiv").click(function() {
		var id = $(this).attr(\'data-id\');
		var statusConfirm = confirm("Realmente desea eliminar esta divisa?"); 
		if (statusConfirm == true) { 
			$.ajax({
				method: "POST",
				url: "modulos/'.$seccion.'/acciones.php",
				data: { 
					borrardivisa: 1,
					id: id
				}
			})
			.done(function( msg ) {
				UIkit.notification.closeAll();
				UIkit.notification(msg, {pos: "bottom-right"});
				$("#"+id).addClass( "uk-invisible" );
			});
		}
	});

	$(".editar").click(function(){
		var id = $(this).attr("data-id");
		$("#ideditar").val(id);

		var campos = ["pregunta"];
		campos.forEach(leerlineas);
		function leerlineas(item){
			var id = $("#ideditar").val();
			var valor = $("#"+item+"_"+id).text();
			console.log(valor);
			$("#"+item).val(valor);
		};
	});
	';



?>