<?php
$seccioncat= 'paises';
echo '
	<div class="uk-width-1-3@s margin-top-20">
		<ul class="uk-breadcrumb uk-text-capitalize">
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">'.$seccion.'</a></li>
			<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=categoria" class="color-red">Paises</a></li>
		</ul>
	</div>
	

	<div class="uk-width-2-3@s uk-text-right margin-v-20">
		
		<div>
			<div id="buttons">
				<a href="#add" class="uk-button uk-button-success" uk-toggle><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nuevo</a>
			</div>
		</div>
	</div>

	<div class="uk-width-1-1 margin-v-20">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th width="90px"></th>
					<th onclick="sortTable(0)" class="pointer uk-text-left">Categoría</th>
					<th width="20px" ></th>
					<th width="20px" ></th>
				</tr>
			</thead>
			<tbody class="sortable" data-tabla="'.$seccioncat.'">';

// Obtener subcategorías

$subcatsNum=0;
$Consulta = $CONEXION -> query("SELECT * FROM paises ORDER BY nombre ASC");
$numeroPaises = $Consulta->num_rows;
while ($row_Consulta = $Consulta -> fetch_assoc()) {


	$paisId = $row_Consulta['id'];

	$picTxt='';
		$pic=$rutaFinal.$row_Consulta['imagen'];
		if(strlen($row_Consulta['imagen'])>0 AND file_exists($pic)){
			$picTxt='
				<div class="uk-inline">
					<i uk-icon="camera"></i>
					<div uk-drop="pos: right-justify">
						<img src="'.$pic.'" class="uk-border-rounded">
					</div>
				</div>';
		}elseif(strlen($row_Consulta['imagen'])>0 AND strpos($row_Consulta['imagen'], 'ttp')>0){
			$pic=$row_Consulta['imagen'];
			$picTxt= '
				<div class="uk-inline">
					<i uk-icon="camera"></i>
					<div uk-drop="pos: right-justify">
						<img src="'.$pic.'" class="uk-border-rounded">
					</div>
				</div>';
		}
	
	$link='index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detallepais&id='.$paisId;
	

	$borrarSubcat='<a href="javascript:eliminaCat(id='.$paisId.')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash"></a>';
	
	echo '
				<tr id="'.$row_Consulta['id'].'">
					<td>
						'.$picTxt.'
					</td>
					<td class="uk-text-left">
						<input type="text" value="'.$row_Consulta['nombre'].'" class="editarajax uk-input uk-form-blank" data-tabla="'.$seccioncat.'" data-campo="nombre" data-id="'.$row_Consulta['id'].'" tabindex="10" >
					</td>
					<td class="uk-text-right uk-text-nowrap">
					<a href="'.$link.'" class="uk-icon-button uk-button-primary"><i class="fa fa-search-plus"></i></a>
						'.$borrarSubcat.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>';
}

echo '
			</tbody>
		</table>
	</div>


	<div id="add" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

				<input type="hidden" name="nuevopais" value="1">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">

				<label for="categoria">Nuevo Pais</label><br><br>
				<input type="text" name="nombre" class="uk-input" required><br><br>
				<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
				<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
			</form>
		</div>
	</div>
	';


$scripts='
	// Eliminar
	function eliminaCat () { 
		var statusConfirm = confirm("Realmente desea eliminar esta categoria?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&eliminacat=1&cat="+id);
		} 
	};

	$(".fichalink").click(function(){
		var id = $(this).attr("data-id");
		$("#fichaid").val(id);
	})';

