<?php 
$rutaFinal='../img/contenido/paises/';
$consultaPais = $CONEXION -> query("SELECT * FROM paises WHERE id = $id");
$rowPais = $consultaPais -> fetch_assoc();

echo '
<div class="uk-width-1-2@s margen-v-20">
	<ul class="uk-breadcrumb uk-text-capitalize">
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'">'.$seccion.'</a></li>
		<li><a href="index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion=detallepais&id='.$id.'" class="color-red">'.$rowPais['nombre'].'</a></li>
	</ul>
</div>
<div class="uk-width-1-2@s uk-text-right margen-v-20">
	<a href="#add" class="uk-button uk-button-success" uk-toggle><i uk-icon="icon:plus;ratio:1.4;"></i> &nbsp; Nueva Provincia</a>
</div>
';

echo '
	<div class="uk-width-1-1 margin-v-20">
		<table class="uk-table uk-table-striped uk-table-hover uk-table-small uk-table-middle" id="ordenar">
			<thead>
				<tr class="uk-text-muted">
					<th onclick="sortTable(0)" class="pointer uk-text-left">Provincia</th>
					<th width="20px" ></th>
					<th width="20px" ></th>
				</tr>
			</thead>
			<tbody class="sortable" data-tabla="'.$seccioncat.'">
';

// Obtener subcategorías

$subcatsNum=0;
$Consulta = $CONEXION -> query("SELECT * FROM provincias WHERE pais = $id ORDER BY nombre ASC");

while ($row_Consulta = $Consulta -> fetch_assoc()) {

	$pId = $row_Consulta['id'];

	$borrarSubcat='<a href="javascript:eliminaprov(id='.$pId.')" class="uk-icon-button uk-button-danger" uk-icon="icon:trash"></a>';
	
	echo '
				<tr id="'.$row_Consulta['id'].'">
				
					<td class="uk-text-left">
						<input type="text" value="'.$row_Consulta['nombre'].'" class="editarajax uk-input uk-form-blank" data-tabla="provincias" data-campo="nombre" data-id="'.$row_Consulta['id'].'" tabindex="10" >
					</td>
					<td class="uk-text-right uk-text-nowrap">
						'.$borrarSubcat.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>';
}

echo '
			</tbody>
		</table>
	</div>


	<div id="add" uk-modal="center: true" class="modal">
		<div class="uk-modal-dialog uk-modal-body">
			<button class="uk-modal-close-default" type="button" uk-close></button>
			<form action="index.php" class="uk-width-1-1 uk-text-center uk-form" method="post" name="editar" onsubmit="return checkForm(this);">

				<input type="hidden" name="nuevaprovincia" value="1">
				<input type="hidden" name="id" value="'.$id.'">
				<input type="hidden" name="seccion" value="'.$seccion.'">
				<input type="hidden" name="subseccion" value="'.$subseccion.'">

				<label for="categoria">Nueva provincia</label><br><br>
				<input type="text" name="nombre" class="uk-input" required><br><br>
				<a class="uk-button uk-button-white uk-modal-close">Cerrar</a>
				<input type="submit" name="send" value="Agregar" class="uk-button uk-button-primary">
			</form>
		</div>
	</div>';

echo '
<div class="uk-width-1-1 margen-v-20">
	<div uk-grid>
		<div class="uk-width-1-2@s">
			<div class="">
				<h3 class="uk-text-left">Bandera</h3>
			</div>
			<div class="uk-width-1-2@s">
				<div class="uk-text-muted">
					Dimensiones recomendadas: 500 x 500 px
				</div>
				<div id="fileuploadermain">
					Cargar
				</div>
			</div>
			<div class="uk-width-1-2@s uk-text-center margen-v-20">';

			// sharePic
				$pic=$rutaFinal.$rowPais['imagen'];
				if(strlen($rowPais['imagen'])>0 AND file_exists($pic)){
					echo '
							<div class="uk-panel uk-text-center">
								<a href="'.$pic.'" target="_blank">
									<img src="'.$pic.'" class="uk-border-rounded margen-top-20">
								</a><br><br>
								<button class="uk-button uk-button-danger borrarpic"><i uk-icon="icon:trash"></i> Eliminar imagen</button>
							</div>';
				}else{
					echo '
							<div class="uk-panel uk-text-center">
								<p class="uk-scrollable-box"><i uk-icon="icon:image;ratio:5;"></i><br><br>
									Falta imagen<br><br>
								</p>
							</div>';
				}

			echo '
			</div>
		</div>
	</div>
</div>';


$scripts='
// Eliminar
	function eliminaprov () { 
		var statusConfirm = confirm("Realmente desea eliminar esta categoria?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?rand='.rand(1,1000).'&seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&eliminaprov=1&cat="+id);
		} 
	};

	$(document).ready(function() {
		$("#fileuploadermain").uploadFile({
			url:"../library/upload-file/php/upload.php",
			fileName:"myfile",
			maxFileCount:1,
			showDelete: \'false\',
			allowedTypes: "jpeg,jpg,png",
			maxFileSize: 6291456,
			showFileCounter: false,
			showPreview:false,
			returnType:\'json\',
			onSuccess:function(data){ 
				window.location = (\'index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&position=imagen&imagen=\'+data);
			}
		});
	});

	// Borrar foto 
	$(".borrarpic").click(function() {
		var statusConfirm = confirm("Realmente desea borrar esto?"); 
		if (statusConfirm == true) { 
			window.location = ("index.php?seccion='.$seccion.'&subseccion='.$subseccion.'&id='.$id.'&borrarpic");
		} 
	});
	';

